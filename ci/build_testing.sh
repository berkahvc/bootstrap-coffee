#! /bin/sh

set -eu

cd bootstrap-coffee

git fetch
git reset --hard
git checkout "$BRANCH"
git pull

export COMPOSE_FILE=staging.yml
docker-compose up --build -d

docker-compose run --rm django python manage.py migrate
