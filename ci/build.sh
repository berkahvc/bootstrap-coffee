#! /bin/sh

set -eu

# Update repository
cd bootstrap-coffee

git fetch
git reset --hard
git checkout "$BRANCH"
git pull

# let's wake the beast!
export COMPOSE_FILE=staging.yml
docker-compose build
docker-compose up -d

# make sure everything is migrated
docker-compose run --rm django python manage.py migrate
