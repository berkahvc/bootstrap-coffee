#! /bin/sh

set -eu

# Update repository
cd bootstrap-coffee
# commented until repository is fix
git checkout master
git pull origin master

# let's wake the beast!
export COMPOSE_FILE=production.yml
docker-compose build
docker-compose up -d

# make sure everything is migrated
docker-compose run --rm django python manage.py migrate
