from django.apps import apps
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path
from django.views import defaults as default_views
from django.views.generic import TemplateView
from django.views.i18n import JavaScriptCatalog

from ecomm.catalogue.views import (
    CatalogueFuelUpView,
    CatalogueTagsView,
    CatalogueView,
    ContactUsView,
    DiscountView,
    HomeView,
    LoginView,
    ProductAdsDetailView,
    StoreListView,
)
from ecomm.checkout.views import ReceiptView
from ecomm.order.views import (
    delivery_labels,
    optimo_orders,
    print_cruffin_orders,
    print_customer_list,
    print_monthly_report,
    print_operation_stuff,
    print_production_list,
    print_production_list_by_date,
    reschedule_order_date_admin_view,
    download_label, print_subscriber_list,
)
from ecomm.subscription_management.views import (
    CancelSubscriptionView,
    RescheduleAdminView,
    ResumeSubscriptionView,
    SkipSubscriptionView,
    SubscriptionExtraDeliveryView,
    SubscriptionView,
)

urlpatterns = [
    path("i18n/", include("django.conf.urls.i18n")),
    path("", HomeView.as_view(), name="home"),
    path("subscription/", SubscriptionView.as_view(), name="subscription"),
    path(
        "cancel-subscription/<str:pk>/",
        CancelSubscriptionView.as_view(),
        name="cancel-subscription",
    ),
    path(
        "skip-subscription/<str:pk>/",
        SkipSubscriptionView.as_view(),
        name="skip-subscription",
    ),
    path(
        "resume-subscription/<str:pk>/",
        ResumeSubscriptionView.as_view(),
        name="resume-subscription",
    ),
    path(
        "subscription-extra-delivery/<str:pk>/",
        SubscriptionExtraDeliveryView.as_view(),
        name="subscription-extra-delivery",
    ),
    path("reschedule-admin/", RescheduleAdminView.as_view(), name="reschedule-admin"),
    path(
        "reschedule-order-date-admin/",
        reschedule_order_date_admin_view,
        name="reschedule-order-date-admin",
    ),
    path(
        "about/",
        TemplateView.as_view(template_name="pages/about-us.html"),
        name="about-us",
    ),
    # -------------------------------------------------------------------------
    # For Advertising Purposes
    # -------------------------------------------------------------------------
    path("discount/<str:code>/", DiscountView.as_view(), name="redirect-collections"),
    path(
        "collections/fuel-up/",
        CatalogueFuelUpView.as_view(),
        name="fuel-up-collections",
    ),
    path(
        "collections/subscribe-save/",
        TemplateView.as_view(template_name="pages/subscribe-and-save-fb.html"),
        name="subscribe-save",
    ),
    path("collections/", CatalogueView.as_view(), name="collections"),
    path(
        "collections/fuel-up/<str:tags>/",
        CatalogueTagsView.as_view(),
        name="collections-tags",
    ),
    path(
        "collections/fuel-up/products/<slug:slug>/",
        ProductAdsDetailView.as_view(),
        name="products-ads",
    ),
    path(
        "subscribe-and-save/",
        TemplateView.as_view(template_name="pages/subscription-static.html"),
        name="sub-and-save",
    ),
    path(
        "checkout-static/",
        TemplateView.as_view(template_name="pages/checkout/checkout.html"),
        name="checkout-static",
    ),
    path(
        "stockists-static/",
        TemplateView.as_view(template_name="pages/stockists.html"),
        name="stockists-static",
    ),
    path(
        "boot-checkout/",
        TemplateView.as_view(template_name="pages/checkout/checkout.html"),
        name="boot-checkout",
    ),
    path(
        "payment-status/",
        TemplateView.as_view(template_name="orders/payment-status.html"),
        name="boot-checkout",
    ),
    path(
        "product-detail/",
        TemplateView.as_view(template_name="pages/product-detail.html"),
        name="product-detail",
    ),
    path(
        "terms-of-service/",
        TemplateView.as_view(template_name="pages/terms-of-service.html"),
        name="terms-of-service",
    ),
    path(
        "faq/",
        TemplateView.as_view(template_name="pages/faq.html"),
        name="faq",
    ),
    path(
        "blogs/",
        TemplateView.as_view(template_name="pages/blogs.html"),
        name="blogs",
    ),
    path(
        "blogs-category/",
        TemplateView.as_view(template_name="pages/blogs-category.html"),
        name="blogs-category",
    ),
    path(
        "blogs-detail/",
        TemplateView.as_view(template_name="pages/blogs-detail.html"),
        name="blogs-detail",
    ),
    path(
        "contact-us/",
        ContactUsView.as_view(),
        name="contact-us",
    ),
    path(
        "search/",
        TemplateView.as_view(template_name="pages/search.html"),
        name="search",
    ),
    path(
        "forgot-password/",
        TemplateView.as_view(template_name="account-boot/forgot-password.html"),
        name="forgot-password",
    ),
    path(
        "refer-a-friend/",
        TemplateView.as_view(template_name="pages/refer-a-friend.html"),
        name="refer-a-friend",
    ),
    path(
        "coffee-concentrate/",
        TemplateView.as_view(template_name="pages/concentrate.html"),
        name="coffee-concentrate",
    ),
    # path(
    #     "subscription/",
    #     TemplateView.as_view(template_name="subscription/subscription.html"),
    #     name="subscription",
    # ),
    # path(
    #     "address-list/",
    #     TemplateView.as_view(template_name="pages/user-page/address-list.html"),
    #     name="address-list",
    # ),
    path(
        "signin/",
        TemplateView.as_view(template_name="account-boot/signin.html"),
        name="signin",
    ),
    path(
        "signup/",
        TemplateView.as_view(template_name="account-boot/signup.html"),
        name="signup",
    ),
    path(
        "user-subscription/",
        TemplateView.as_view(template_name="pages/user-page/subscription.html"),
        name="user-subscription",
    ),
    path(
        "edit-subscription-address/",
        TemplateView.as_view(template_name="pages/user-page/edit-subs-address.html"),
        name="edit-subs-address",
    ),
    path(
        "edit-delivery/",
        TemplateView.as_view(template_name="pages/user-page/edit-delivery.html"),
        name="edit-delivery",
    ),
    path(
        "edit-product-subscription/",
        TemplateView.as_view(
            template_name="pages/user-page/edit-product-subscription.html"
        ),
        name="edit-product-subscription",
    ),
    # path(
    #     "transaction-history/",
    #     TemplateView.as_view(template_name="pages/user-page/transaction-history.html"),
    #     name="transaction-history",
    # ),
    # Django Admin, use {% url 'admin:index' %}
    path(settings.ADMIN_URL, admin.site.urls),
    path("stripe/", include("djstripe.urls", namespace="djstripe")),
    # User management
    path("users/", include("ecomm.users.urls", namespace="users")),
    path("checkout/", include("ecomm.checkout.urls", namespace="custom_checkout")),
    path("shipping/", include("ecomm.shipping.urls", namespace="custom_shipping")),
    path("accounts/login/", LoginView.as_view(), name="account_login_custom"),
    path("accounts/", include("allauth.urls")),
    # Your stuff: custom urls includes go here
    path("", include(apps.get_app_config("oscar").urls[0])),
    # adds URLs for the dashboard store manager
    path("dashboard/stockists/", apps.get_app_config("stores_dashboard").urls),
    # adds URLs for overview and detail pages
    path("stores/", apps.get_app_config("stores").urls),
    path("stockists/", StoreListView.as_view(), name="stockists"),
    # adds internationalization URLs
    path("jsi18n/", JavaScriptCatalog.as_view(), name="javascript-catalogue"),
    url(r"^dashboard/accounts/", apps.get_app_config("accounts_dashboard").urls),
    path(
        "thank-you-midtrans/",
        TemplateView.as_view(template_name="oscar/checkout/paid_thank_you.html"),
    ),
    path("api/", include("oscarapi.urls")),
    # For printing next day delivery
    path("optimo/", optimo_orders, name="optimo"),
    path("print_labels/", delivery_labels, name="print_labels"),
    path("cruffin/", print_cruffin_orders, name="cruffin"),
    path("print/production_list/", print_production_list, name="print_production_list"),
    path(
        "print/production_list_2/",
        print_production_list_by_date,
        name="print_production_list_2",
    ),
    path(
        "print/operations/",
        print_operation_stuff,
        name="print_view",
    ),
    path(
        "print/monthly_report/",
        print_monthly_report,
        name="print_monthly_report",
    ),
    path(
        "print/customer_list/",
        print_customer_list,
        name="print_customer_list",
    ),
    path(
        "print/subscriber_list/",
        print_subscriber_list,
        name="print_subscriber_list",
    ),
    path("print/labels/",
         download_label,
         name="print_label",
         ),
    path(
        "robots.txt",
        TemplateView.as_view(template_name="robots.txt", content_type="text/plain"),
    ),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # Static file serving when using Gunicorn + Uvicorn for local web socket development
    urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            "400/",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", default_views.server_error),
        path(
            "receipt/",
            ReceiptView.as_view(), name="receipt",
        ),
    ]
    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
