import os

from celery import Celery

# set the default Django settings module for the 'celery' program.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.local")

app = Celery("ecomm")

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object("django.conf:settings", namespace="CELERY")
app.control.rate_limit(
    task_name="ecomm.checkout.tasks.send_email_with_rate_limit", rate_limit="70/h"
)

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()
