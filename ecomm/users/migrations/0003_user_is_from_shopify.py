# Generated by Django 3.0.11 on 2021-10-28 02:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_user_phone'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='is_from_shopify',
            field=models.BooleanField(blank=True, db_index=True, default=False, null=True),
        ),
    ]
