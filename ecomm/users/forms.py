from allauth.account.forms import ChangePasswordForm, LoginForm, SignupForm
from django import forms
from django.contrib.auth import forms as admin_forms
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

User = get_user_model()


class UserChangeForm(admin_forms.UserChangeForm):
    class Meta(admin_forms.UserChangeForm.Meta):
        model = User


class UserCreationForm(admin_forms.UserCreationForm):
    error_message = admin_forms.UserCreationForm.error_messages.update(
        {"duplicate_username": _("This username has already been taken.")}
    )

    class Meta(admin_forms.UserCreationForm.Meta):
        model = User

    def clean_username(self):
        username = self.cleaned_data["username"]

        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username

        raise ValidationError(self.error_messages["duplicate_username"])


class CustomUserSignupForm(SignupForm):
    first_name = forms.CharField(max_length=255, label="First name", required=True)
    last_name = forms.CharField(max_length=255, label="Last name", required=True)

    def save(self, request):
        user = super(CustomUserSignupForm, self).save(request)
        first_name = self.cleaned_data["first_name"]
        last_name = self.cleaned_data["last_name"]
        user.first_name = first_name
        user.last_name = last_name
        user.save()
        return user


class CustomChangePasswordForm(ChangePasswordForm):
    first_name = forms.CharField(max_length=255, label="First name", required=True)
    last_name = forms.CharField(max_length=255, label="Last name", required=True)
    email = forms.EmailField(max_length=255, required=True, widget=forms.HiddenInput)

    def __init__(self, *args, **kwargs):
        super(ChangePasswordForm, self).__init__(*args, **kwargs)
        self.fields["first_name"].initial = self.user.first_name
        self.fields["last_name"].initial = self.user.last_name
        self.fields["email"].initial = self.user.email
        self.fields["password1"].user = self.user
        self.fields["password1"].widget.attrs.update({"class": "password", "placeholder": "Input new password"})
        self.fields["password2"].widget.attrs.update({"class": "password2", "placeholder": "Retype new password"})
        self.fields["oldpassword"].widget.attrs.update({"placeholder": "Current password"})


class CustomLoginForm(LoginForm):
    error_message = LoginForm.error_messages.update(
        {
            "email_password_mismatch": _(
                "The e-mail address and/or password you submitted are incorrect."
            ),
        }
    )
