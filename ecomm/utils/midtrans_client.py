import midtransclient
from django.conf import settings

# Create Snap API instance
snap = midtransclient.Snap(
    is_production=settings.IS_PRODUCTION,
    server_key=settings.MIDTRANS_SERVER_KEY,
    client_key=settings.MIDTRANS_CLIENT_KEY,
)


def get_snap_client():
    return snap
