import json
import logging

import requests
from django.conf import settings

from .utils import merge_two_dicts

logger = logging.getLogger(__name__)

OPTIMO_API_KEY = settings.OPTIMO_API_KEY
OPTIMO_BASE_URL = settings.OPTIMO_BASE_URL


class OptimoRouteClient:
    @staticmethod
    def check_and_return_response(response):
        response_body = str(response.text)
        if response_body is None or response_body == "":
            return json.loads("{}")

        response_json = json.loads(response_body)
        return response_json

    def get(
        self,
        path,
        additional_path=None,
        data=None,
        url=OPTIMO_BASE_URL,
        additional_headers=None,
    ):
        """
        Takes the "url" of the request

        :param data: query string of the request
        :param path: target path of the request
        :param additional_path: additional target path of the request
        :param url: target url of the request
        :param additional_headers: additional headers of the request
        :return: the result of the request
        """

        if additional_headers is None:
            additional_headers = {}

        headers = merge_two_dicts(
            {"Accept": "application/json", "Content-Type": "application/json"},
            additional_headers,
        )
        params = merge_two_dicts({"key": OPTIMO_API_KEY}, additional_path)
        logger.info(f"get data from: {path}", extra={"data": data})
        result = requests.get(
            url=f"{url}{path}", headers=headers, json=data, params=params
        )
        logger.info(f"url: {result.url}")
        json_result = self.check_and_return_response(response=result)
        logger.info(
            f"response get data from: {path}, result: {json_result}",
            extra={"response": json_result},
        )

        return json_result

    def post(
        self,
        path,
        additional_path=None,
        data=None,
        url=OPTIMO_BASE_URL,
        additional_headers=None,
    ):
        """
        Takes the "url" of the request

        :param data: query string of the request
        :param path: target path of the request
        :param additional_path: additional target path of the request
        :param url: target url of the request
        :param additional_headers: additional headers of the request
        :return: the result of the request
        """

        if additional_headers is None:
            additional_headers = {}

        headers = merge_two_dicts(
            {"Accept": "application/json", "Content-Type": "application/json"},
            additional_headers,
        )
        params = merge_two_dicts({"key": OPTIMO_API_KEY}, additional_path)
        logger.info(f"post data to: {path}", extra={"data": data})
        result = requests.post(
            url=f"{url}{path}", headers=headers, json=data, params=params
        )
        logger.info(f"url: {result.url}")
        json_result = self.check_and_return_response(response=result)
        logger.info(
            f"response post data to: {path}, result: {json_result}",
            extra={"response": json_result},
        )

        return json_result

    def get_orders(self):
        return self.post(path="/get_orders")

    def create_order(self, order):
        payload = order.optimo_route_payload()
        logger.info(f"payload: {payload}")
        return self.post(path="/create_order", data=payload)
