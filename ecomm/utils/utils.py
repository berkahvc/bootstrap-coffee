import json
import re
from datetime import date, datetime, timedelta

import pandas as pd
from django.core.exceptions import ValidationError
from django.utils.timezone import localdate

from ecomm.schedule_management.models import DeliveryTime


def merge_two_dicts(x, y):
    z = x.copy()  # start with x's keys and values
    if y:
        z.update(y)  # modifies z with y's keys and values & returns None
    return z


def cleanup_dictionary(dictionary):
    return {k: v for k, v in dictionary.items() if v is not None}


def get_body_json(request):
    body_unicode = request.body.decode("utf-8")
    body = json.loads(body_unicode)

    return body


def validate_body_request(body, required_attributes):
    for required_attribute in required_attributes:
        if required_attribute not in body:
            raise ValidationError(
                f"Key {required_attribute} is required in request body"
            )


def get_delivery_time(delivery_time):
    delivery_time_str = delivery_time.split(" - ")

    try:
        delivery_time_start = datetime.strptime(
            delivery_time_str[0].strip(), "%I:%M %p"
        ).strftime("%H:%M")
    except ValueError:
        delivery_time_start = datetime.strptime(
            delivery_time_str[0].strip(), "%H:%M"
        ).strftime("%H:%M")

    try:
        delivery_time_end = datetime.strptime(
            delivery_time_str[1].strip(), "%I:%M %p"
        ).strftime("%H:%M")
    except ValueError:
        delivery_time_end = datetime.strptime(
            delivery_time_str[1].strip(), "%H:%M"
        ).strftime("%H:%M")

    obj_dt = DeliveryTime.objects.filter(
        start=delivery_time_start, end=delivery_time_end
    )
    delivery_time = None
    if obj_dt.exists():
        delivery_time = obj_dt.first()

    return delivery_time


def calculate_delivery_date(delivery_day: str, charge_date: date):
    """
    Function to calculate when to send the order.

    When charged on Sunday, the next delivery date is calculated based on the
    delivery_day of the order. If the charge_date is not a Sunday, the next delivery
    date is calculated based on the delivery_day if it is not the same/next day.

    :param delivery_day: Delivery day of the week in the form of %A
    :param charge_date: Date of the charge on Stripe
    :return: calculated date of the next delivery in the form of yyyy-mm-dd
    """

    # TODO: if the delivery day is tomorrow or less, then send an error message
    # If the delivery day is Monday, no not send an error message

    day_of_week = int_day_conversion(delivery_day)

    delivery_date = charge_date + timedelta(
        days=day_of_week - charge_date.weekday(),
        weeks=1 if charge_date.weekday() == 6 else 0,
    )

    return delivery_date.strftime("%Y-%m-%d")


def calculate_next_billing_date(
    delivery_date: datetime, deliveries_left: int = 0, delivery_frequency: int = 1
) -> date:
    """
    A function to calculate the next billing date for a particular subscription.

    :param delivery_date:       The date of the next delivery
    :param deliveries_left:     deliveries_left (if any)
    :param delivery_frequency:  delivery_frequency (in weeks)
    :return:                    Next billing date (Sunday)
    """

    weeks_to_charge = int(deliveries_left) * int(delivery_frequency)
    assumed_charged_date = get_previous_sunday(delivery_date)

    return assumed_charged_date + timedelta(weeks=weeks_to_charge)


def get_previous_sunday(date_to_check: date):
    """
    A function that returns the date of the previous Sunday

    :param date_to_check: The date to check
    :return:              The previous Sunday if the day is not Sunday
    """

    if date_to_check.weekday() == 6:
        raise ValueError("The date is a Sunday please check again")

    day_of_week = date_to_check.weekday() + 1
    return date_to_check - timedelta(days=day_of_week)


def int_day_conversion(value):
    """
    Function to convert days to integer

    :param value:   String of the name of the days
    :return:        integer from 0-6
    """
    day_dict = {
        "Monday": 0,
        "Tuesday": 1,
        "Wednesday": 2,
        "Thursday": 3,
        "Friday": 4,
        "Saturday": 5,
        "Sunday": 6,
    }

    return day_dict.get(value)


def is_delivery_date_in_this_week(delivery_date):
    """
    this function should check the delivery date in this week and not past due delivery date
    :param delivery_date:
    :return: boolean
    """
    if type(delivery_date) == str:
        delivery_date = datetime.strptime(delivery_date, "%Y-%m-%d").date()
    elif type(delivery_date) == date:
        delivery_date = delivery_date
    else:
        delivery_date = delivery_date.date()

    today = localdate()

    if today > delivery_date:
        return False

    start_of_week = today
    days = 6
    if today.weekday() != 6:
        days = 5
        start_of_week = today - timedelta(days=today.weekday())

    end_of_week = start_of_week + timedelta(days=days)
    date_range_list = pd.date_range(start_of_week, end_of_week).to_list()

    for i in date_range_list:
        if delivery_date == i.date():
            return True

    return False


def check_product_names(subscription_description: str):
    """
    Check the validity of product names on the notes and description of the subscription

    :param subscription_description:
    :return: True if the product names are valid, False otherwise
    """

    products = subscription_description.split(",")
    list_of_products = [
        "black",
        "strong",
        "oat",
        "manuka",
        "hojicha",
        "mocha",
        "rooibos",
        "concentrate"
    ]

    for product in products:
        result = re.search(r"^\d+x\s(\w+\s?){1,5}$", product.strip())
        if result:
            if any(set(list_of_products).intersection(result.group(1).lower().split())):
                return True
            else:
                return False
            pass
        else:
            return False
    return True
