from decimal import Decimal

from ..math_util import decimal_ceil


def test_decimal_ceil_should_work_fine():
    result = decimal_ceil(Decimal("8.0000001"))

    assert result == 9


def test_decimal_ceil_should_work_fine_if_no_ceiling_needed():
    result = decimal_ceil(Decimal("80.0000000"))

    assert result == 80
