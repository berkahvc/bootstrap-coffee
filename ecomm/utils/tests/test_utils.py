import unittest
from datetime import datetime

import pytest
from django.core.exceptions import ValidationError
from freezegun import freeze_time

from ..utils import (
    calculate_delivery_date,
    calculate_next_billing_date,
    get_previous_sunday,
    int_day_conversion,
    is_delivery_date_in_this_week,
    validate_body_request, check_product_names,
)


class ValidationTestCases(unittest.TestCase):
    def test_validate_body_request_throw_error_on_missing_key(
        self,
    ):
        self.assertRaises(
            ValidationError,
            validate_body_request,
            {
                "coba": "coba",
                "coba2": "coba2",
                "coba3": "coba3",
                "coba4": "coba4",
            },
            ["coba", "coba2", "coba5"],
        )


def test_int_day_conversion():
    assert int_day_conversion("Monday") == 0
    assert int_day_conversion("Tuesday") == 1
    assert int_day_conversion("Wednesday") == 2
    assert int_day_conversion("Thursday") == 3
    assert int_day_conversion("Friday") == 4
    assert int_day_conversion("Saturday") == 5
    assert int_day_conversion("Sunday") == 6


def test_calculate_delivery_date():
    assert (
        calculate_delivery_date(
            "Monday",
            datetime.strptime("2022-03-04", "%Y-%m-%d"),
        )
        == "2022-02-28"
    ), "date should be 2022-02-28"

    assert (
        calculate_delivery_date(
            "Monday",
            datetime.strptime("2022-02-27", "%Y-%m-%d"),
        )
        == "2022-02-28"
    )
    assert (
        calculate_delivery_date(
            "Tuesday",
            datetime.strptime("2022-02-27", "%Y-%m-%d"),
        )
        == "2022-03-01"
    )


class IsDeliveryDateInThisWeekTest(unittest.TestCase):
    @freeze_time("2022-03-20")
    def test_generate_prepaid_order_run_on_sunday(self):
        delivery_date = datetime.strptime("2022-03-25", "%Y-%m-%d")
        assert is_delivery_date_in_this_week(delivery_date), "Should True"

    @freeze_time("2022-03-20")
    def test_generate_prepaid_order_run_on_sunday_1(self):
        delivery_date = "2022-03-25"
        assert is_delivery_date_in_this_week(delivery_date), "Should True"

    @freeze_time("2022-03-21")
    def test_generate_prepaid_order_run_on_monday(self):
        delivery_date = datetime.strptime("2022-03-25", "%Y-%m-%d")
        assert is_delivery_date_in_this_week(delivery_date), "Should True"

    @freeze_time("2022-03-21")
    def test_generate_prepaid_order_run_on_monday_1(self):
        delivery_date = "2022-03-25"
        assert is_delivery_date_in_this_week(delivery_date), "Should True"

    @freeze_time("2022-03-22")
    def test_generate_prepaid_order_run_on_tuesday(self):
        delivery_date = datetime.strptime("2022-03-25", "%Y-%m-%d")
        assert is_delivery_date_in_this_week(delivery_date), "Should True"

    @freeze_time("2022-03-22")
    def test_generate_prepaid_order_run_on_tuesday_1(self):
        delivery_date = "2022-03-25"
        assert is_delivery_date_in_this_week(delivery_date), "Should True"

    @freeze_time("2022-03-23")
    def test_generate_prepaid_order_run_on_wednesday(self):
        delivery_date = datetime.strptime("2022-03-25", "%Y-%m-%d")
        assert is_delivery_date_in_this_week(delivery_date), "Should True"

    @freeze_time("2022-03-23")
    def test_generate_prepaid_order_run_on_wednesday_1(self):
        delivery_date = "2022-03-25"
        assert is_delivery_date_in_this_week(delivery_date), "Should True"

    @freeze_time("2022-03-24")
    def test_generate_prepaid_order_run_on_thursday(self):
        delivery_date = datetime.strptime("2022-03-25", "%Y-%m-%d")
        assert is_delivery_date_in_this_week(delivery_date), "Should True"

    @freeze_time("2022-03-24")
    def test_generate_prepaid_order_run_on_thursday_1(self):
        delivery_date = "2022-03-25"
        assert is_delivery_date_in_this_week(delivery_date), "Should True"

    @freeze_time("2022-03-25")
    def test_generate_prepaid_order_run_on_friday(self):
        delivery_date = datetime.strptime("2022-03-25", "%Y-%m-%d")
        assert is_delivery_date_in_this_week(delivery_date), "Should True"

    @freeze_time("2022-03-25")
    def test_generate_prepaid_order_run_on_friday_1(self):
        delivery_date = "2022-03-25"
        assert is_delivery_date_in_this_week(delivery_date), "Should True"

    @freeze_time("2022-03-25")
    def test_generate_prepaid_order_delivery_date_less_than_today(self):
        delivery_date = datetime.strptime("2022-03-21", "%Y-%m-%d")
        assert not is_delivery_date_in_this_week(delivery_date), "Should False"

    @freeze_time("2022-03-25")
    def test_generate_prepaid_order_delivery_date_less_than_today_1(self):
        delivery_date = "2022-03-21"
        assert not is_delivery_date_in_this_week(delivery_date), "Should False"


def test_get_previous_sunday():
    assert get_previous_sunday(datetime(2022, 3, 25)) == datetime(2022, 3, 20)


def test_get_previous_sunday_with_sunday():
    with pytest.raises(ValueError):
        get_previous_sunday(datetime(2022, 3, 20))


def test_calculate_next_billing_date():
    same_week_billing_date = calculate_next_billing_date(
        delivery_date=datetime(2022, 3, 26),
        deliveries_left=4,
        delivery_frequency=2,
    )
    next_week_billing_date = calculate_next_billing_date(
        delivery_date=datetime(2022, 3, 31),
        deliveries_left=4,
        delivery_frequency=2,
    )
    next_billing_date_pause = calculate_next_billing_date(
        delivery_date=datetime(2022, 3, 25),
        deliveries_left=1,
        delivery_frequency=20,
    )
    same_week_billing_date1 = calculate_next_billing_date(
        delivery_date=datetime(2022, 5, 21),
        deliveries_left=1,
        delivery_frequency=2,
    )
    assert same_week_billing_date1 == datetime(2022, 5, 29)
    assert same_week_billing_date == datetime(2022, 5, 15)
    assert next_week_billing_date == datetime(2022, 5, 22)
    assert next_billing_date_pause == datetime(2022, 8, 7)


def test_check_product_names():
    false_1 = "Something that is not a name"
    assert not check_product_names(false_1)

    false_2 = "1 black, 2 strong, 3 manukah"
    assert not check_product_names(false_2)

    false_3 = "12x Something"
    assert not check_product_names(false_3)

    true_1 = "12x black"
    assert check_product_names(true_1)

    true_2 = "12x black, 6x strong"
    assert check_product_names(true_2)

    true_3 = "6x Black"
    assert check_product_names(true_3)

    true_4 = "6x Cold Brew Black, 3x Strong, 3x Cold Brew Manukah"
    assert check_product_names(true_4)
