import enum


# Using enum class create enumerations
class AvailablePayment(enum.Enum):
    STRIPE = "STRIPE"

    @classmethod
    def choices(cls):
        return tuple((i.name, i.value) for i in cls)

    @classmethod
    def name_choices(cls):
        return tuple(i.name for i in cls)
