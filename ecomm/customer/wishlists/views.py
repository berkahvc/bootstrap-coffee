from django.contrib import messages
from django.http import JsonResponse
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from oscar.apps.customer.wishlists.views import (
    WishListAddProduct as CoreWishListAddProduct,
)
from oscar.apps.customer.wishlists.views import (
    WishListRemoveProduct as CoreWishListRemoveProduct,
)
from oscar.core.utils import redirect_to_referrer

from ecomm.catalogue.templatetags.custom_tags import is_on_wishlist


class WishListAddProduct(CoreWishListAddProduct):
    def add_product(self):
        self.wishlist.add(self.product)

        ajax_request = self.request.is_ajax()

        if ajax_request:
            wishlist_info = is_on_wishlist(request=self.request, product=self.product)

            return JsonResponse(
                {
                    "is_on_wishlist": wishlist_info.is_on_wishlist,
                    "remove_url": wishlist_info.remove_url,
                }
            )

        msg = _("'%s' was added to your wish list.") % self.product.get_title()
        messages.success(self.request, msg)
        return redirect_to_referrer(self.request, self.product.get_absolute_url())


class WishListRemoveProduct(CoreWishListRemoveProduct):
    def post(self, request, *args, **kwargs):
        result = super(WishListRemoveProduct, self).post(request, args, kwargs)

        ajax_request = self.request.is_ajax()

        if ajax_request:
            wishlist_info = is_on_wishlist(request=self.request, product=self.product)

            return JsonResponse(
                {
                    "is_on_wishlist": wishlist_info.is_on_wishlist,
                    "remove_url": wishlist_info.remove_url,
                }
            )

        return result

    def get_success_url(self):
        msg = _("'%(title)s' was removed from your '%(name)s' wish list") % {
            "title": self.line.get_title(),
            "name": self.wishlist.name,
        }
        messages.success(self.request, msg)

        return reverse("customer:wishlists-list")
