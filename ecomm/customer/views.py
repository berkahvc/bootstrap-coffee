from datetime import datetime

import pytz
import stripe
from braces.views import AjaxResponseMixin
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import redirect, render
from django.urls import reverse
from django.utils.timezone import localdate, localtime, timedelta
from django.utils.translation import gettext_lazy as _
from django.views.generic import DetailView, FormView, ListView, UpdateView, View
from djstripe import enums
from djstripe.models import Customer, Plan
from djstripe.models import Product as DjStripeProduct
from djstripe.models import Subscription
from djstripe.settings import STRIPE_SECRET_KEY
from oscar.apps.customer.views import (
    AccountRegistrationView as CoreAccountRegistrationView,
)
from oscar.apps.customer.views import AddressCreateView as CoreAddressCreateView
from oscar.apps.customer.views import AddressListView as CoreAddressListView
from oscar.apps.customer.views import AddressUpdateView as CoreAddressUpdateView
from oscar.apps.customer.views import ChangePasswordView as CoreChangePasswordView
from oscar.apps.customer.views import ProfileView as CoreProfileView
from oscar.core.compat import get_user_model
from oscar.core.loading import get_class, get_model

from ecomm.checkout.tasks import send_email_subscription_renewed
from ecomm.order.utils import create_order
from ecomm.schedule_management.utils import get_holidays, get_start_available_day
from ecomm.subscription_management.tasks import send_resubscribe_email_confirmation
from ecomm.subscription_management.utils import (  # update_stripe_subscription,
    create_or_update_payment_method,
    reschedule_subscription,
    update_stripe_product_description,
)
from ecomm.users.forms import CustomChangePasswordForm
from ecomm.utils.utils import calculate_next_billing_date

from .forms import CreditCardForm, ResubscribeForm

stripe.api_key = STRIPE_SECRET_KEY

User = get_user_model()
Order = get_model("order", "Order")
SubscriptionPrepaid = get_model("subscription_management", "SubscriptionPrepaid")
UserAddress = get_model("address", "UserAddress")
DeliveryTime = get_model("schedule_management", "DeliveryTime")
Prepaid = get_model("subscription_management", "Prepaid")
Product = get_model("catalogue", "Product")
Category = get_model("catalogue", "Category")
ShippingAddress = get_model("order", "ShippingAddress")

UserAddressForm = get_class("address.forms", "UserAddressForm")
SubscriptionDeliveryEditForm = get_class(
    "customer.forms", "SubscriptionDeliveryEditForm"
)
SubscriptionProductEditForm = get_class("customer.forms", "SubscriptionProductEditForm")
ShippingAddressForm = get_class("shipping.forms", "ShippingAddressForm")


class AccountRegistrationView(CoreAccountRegistrationView):
    template_name = "account/signup.html"


class ProfileView(CoreProfileView):
    template_name = "pages/user-page/my-account.html"

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx["order_count"] = Order.objects.filter(user=self.request.user).count()
        ctx["profile_fields"] = self.get_profile_fields(self.request.user)
        ctx["profile_form"] = CustomChangePasswordForm(user=self.request.user)
        return ctx


class ChangePasswordView(CoreChangePasswordView):
    form_class = CustomChangePasswordForm

    def form_valid(self, form):
        form.save()
        self.request.user.first_name = form.cleaned_data["first_name"]
        self.request.user.last_name = form.cleaned_data["last_name"]
        self.request.user.save()
        update_session_auth_hash(self.request, self.request.user)
        messages.success(self.request, _("Password updated"))

        self.send_password_changed_email()

        return redirect(self.get_success_url())

    def form_invalid(self, form):
        profile_form = CustomChangePasswordForm(user=self.request.user)
        try:
            for field in form.errors:
                messages.error(
                    self.request,
                    f"{profile_form.fields[field].label} - {form.errors[field][0]}",
                )
        except KeyError:
            for field in form.errors:
                messages.error(
                    self.request,
                    f"{form.errors[field][0]}",
                )
        return HttpResponseRedirect(reverse("customer:profile-view"))


class AddressListView(CoreAddressListView):
    template_name = "pages/user-page/address-list.html"

    def get_context_data(self, *, object_list=None, **kwargs):
        ctx = super(AddressListView, self).get_context_data(**kwargs)
        ctx.update({"address_form": UserAddressForm(user=self.request.user)})
        return ctx


class AddressCreateView(CoreAddressCreateView):
    template_name = "pages/user-page/address_form.html"


class AddressUpdateView(CoreAddressUpdateView):
    template_name = "pages/user-page/address_form.html"


class CustomAddressUpdateView(AjaxResponseMixin, View):
    def get_ajax(self, request, *args, **kwargs):
        address = UserAddress.objects.get(pk=self.kwargs.get("pk"))
        address_form = UserAddressForm(instance=address, user=self.request.user)
        ctx = {"address_form": address_form, "id": self.kwargs.get("pk")}
        return render(self.request, "pages/user-page/_edit_address.html", ctx)


class SubscriptionListView(LoginRequiredMixin, ListView):
    model = Subscription
    template_name = "pages/user-page/subscription.html"

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        try:
            stripe_customer = Customer.objects.get(subscriber=self.request.user)
            ctx["subscriptions"] = Subscription.objects.filter(
                customer=stripe_customer,
                status__in=[
                    enums.SubscriptionStatus.canceled,
                    enums.SubscriptionStatus.active,
                    enums.SubscriptionStatus.trialing,
                ],
            ).order_by("-canceled_at")
        except Customer.DoesNotExist:
            ctx["subscriptions"] = []
        return ctx


class TransactionHistoryListView(LoginRequiredMixin, ListView):
    model = Order
    template_name = "pages/user-page/transaction-history.html"
    context_object_name = "transaction_histories"
    paginate_by = 10

    def get_queryset(self):
        date = self.request.GET.get("date", None)
        transaction_type = self.request.GET.get("transactionType", None)
        stripe_customer, _ = Customer.objects.get_or_create(
            subscriber=self.request.user
        )

        queryset = self.model.objects.filter(user=self.request.user)

        if date:
            queryset = queryset.filter(
                created__date=datetime.strptime(date, "%m/%d/%Y")
            )

        if transaction_type:
            subscription_prepaid = SubscriptionPrepaid.objects.filter(
                djcustomer_id=stripe_customer.id
            )

            if transaction_type == "regular":
                queryset = queryset.filter(subscription_id__isnull=True)
            elif transaction_type == "prepaid":
                prepaid = subscription_prepaid.values_list(
                    "djsubscription_id", flat=True
                ).filter(subscription_type="prepaid")
                queryset = queryset.filter(subscription_id__in=list(set(prepaid)))
            elif transaction_type == "payasyougo":
                pay_as_you_go = subscription_prepaid.values_list(
                    "djsubscription_id", flat=True
                ).filter(subscription_type="pay_as_you_go")
                queryset = queryset.filter(subscription_id__in=list(set(pay_as_you_go)))

        return queryset

    def get_context_data(self, *, object_list=None, **kwargs):
        ctx = super(TransactionHistoryListView, self).get_context_data(**kwargs)
        date = self.request.GET.get("date", None)
        transaction_type = self.request.GET.get("transactionType", None)
        ctx["profile_form"] = CustomChangePasswordForm(user=self.request.user)
        ctx["transaction_type"] = transaction_type
        ctx["date"] = date
        return ctx


def subscription_prepaid_get_or_create(
    q, subscription, product_selected, djstripe_product_description_split
):
    subscription_prepaid, created = SubscriptionPrepaid.objects.get_or_create(**q)
    if created:
        total_pack = 0
        for product in product_selected:
            product_split = product.split("x ")
            total_pack += int(product_split[0].strip())

        credit_per_delivery = int(total_pack) / 6
        interval_count = subscription.plan.interval_count

        subscription_prepaid.delivery_day = (
            djstripe_product_description_split[2]
            .split("Delivery Day:")[1]
            .strip()
            .replace(",", "")
            .replace("<p>", "")
            .replace("</p>", "")
        )
        subscription_prepaid.delivery_time = (
            djstripe_product_description_split[3]
            .split("Delivery Time:")[1]
            .strip()
            .replace("<p>", "")
            .replace("</p>", "")
        )
        subscription_prepaid.pack_size = total_pack
        subscription_prepaid.credit_per_delivery = credit_per_delivery
        subscription_prepaid.next_credit_charge = credit_per_delivery
        subscription_prepaid.description = ",".join(product_selected)
        subscription_prepaid.delivery_frequency = 1
        subscription_prepaid.charge_frequency = interval_count
        subscription_prepaid.initial_credit = int(interval_count) * int(
            credit_per_delivery
        )
        subscription_prepaid.save()

    return subscription_prepaid


class SubscriptionDetailView(LoginRequiredMixin, DetailView):
    template_name = "pages/user-page/subscription-detail.html"
    model = Subscription
    context_object_name = "subscription"

    def get_object(self, queryset=None):
        return Subscription.objects.get(id=self.kwargs.get("pk"))

    def get_context_data(self, **kwargs):
        ctx = super(SubscriptionDetailView, self).get_context_data(**kwargs)
        subscription = self.get_object()
        pause_collection = None
        try:
            stripe_subscription = stripe.Subscription.retrieve(subscription.id)
            pause_collection = stripe_subscription.get("pause_collection")
        except Exception as err:
            print(f"===== {err}")

        start_date = get_start_available_day(Category.objects.filter(is_public=True))

        q = {
            "user": self.request.user,
            "djsubscription_id": subscription.id,
            "djcustomer_id": subscription.customer.id,
        }
        djstripe_product_description = subscription.plan.product.description

        if "<p>" in djstripe_product_description:
            djstripe_product_description_split = djstripe_product_description.split(
                "</p><p>"
            )
        else:
            djstripe_product_description_split = djstripe_product_description.replace(
                "\r\n", " "
            ).split(", ")

        product_selected = list(
            filter(
                None,
                djstripe_product_description_split[0]
                .replace("<p>", "")
                .replace("</p>", "")
                .split(","),
            )
        )

        subscription_prepaid = subscription_prepaid_get_or_create(
            q, subscription, product_selected, djstripe_product_description_split
        )

        day = (
            subscription_prepaid.delivery_date.strftime("%A")
            if subscription_prepaid.delivery_date
            else subscription_prepaid.delivery_day
        )
        delivery_times = DeliveryTime.objects.filter(
            days__icontains=day, is_active=True
        )
        option = '<option value="">--- Select time ---</option>'

        for delivery_time in delivery_times:
            selected = (
                " selected"
                if subscription_prepaid.delivery_time == delivery_time.__str__()
                else ""
            )
            option += (
                f'<option value="{delivery_time.id}"{selected}>{delivery_time}</option>'
            )

        ctx.update(
            {
                "products": product_selected,
                "pause_collection": pause_collection,
                "status": subscription_prepaid.status,
                "delivery_date": subscription_prepaid.delivery_date,
                "delivery_time": subscription_prepaid.delivery_time,
                "delivery_remaining": subscription_prepaid.deliveries_left(),
                "holidays": get_holidays(),
                "start_date": start_date,
                "options": option,
            }
        )
        return ctx


class SubscriptionDeliveryUpdateView(LoginRequiredMixin, FormView):
    template_name = "pages/user-page/edit-delivery.html"
    model = Subscription
    form_class = SubscriptionDeliveryEditForm

    def get_object(self, queryset=None):
        return Subscription.objects.get(id=self.kwargs.get("pk"))

    def get_context_data(self, **kwargs):
        ctx = super(SubscriptionDeliveryUpdateView, self).get_context_data(**kwargs)
        today = localdate()

        if today.weekday() == 5:
            start_date = today + timedelta(days=3)
        else:
            start_date = today + timedelta(days=2)

        max_date = 6
        index = 0
        dates_available = []
        while index < max_date:
            if start_date.weekday() == 6:
                start_date += timedelta(days=1)
            else:
                dates_available.append(start_date)
                start_date += timedelta(days=1)
                index += 1

        subscription = self.get_object()
        djstripe_product_description = subscription.plan.product.description
        djstripe_product_description_split = djstripe_product_description.split(
            "</p><p>"
        )
        product_selected = list(
            filter(
                None,
                djstripe_product_description_split[0]
                .replace("<p>", "")
                .replace("</p>", "")
                .split(","),
            )
        )
        q = {
            "user": self.request.user,
            "djsubscription_id": subscription.id,
            "djcustomer_id": subscription.customer.id,
        }

        subscription_prepaid = subscription_prepaid_get_or_create(
            q, subscription, product_selected, djstripe_product_description_split
        )

        amount = subscription.plan.amount
        currency = subscription.plan.currency
        interval = subscription.plan.interval
        interval_count = subscription.plan.interval_count
        delivery_day = subscription_prepaid.delivery_day
        delivery_time = subscription_prepaid.delivery_time

        ctx.update(
            {
                "subscription": subscription,
                "delivery_times": DeliveryTime.objects.filter(is_active=True).order_by(
                    "start"
                ),
                "start_date": start_date,
                "dates_available": dates_available,
                "holidays": get_holidays(),
                "amount": amount,
                "currency": currency,
                "interval": interval,
                "interval_count": interval_count,
                "delivery_day": delivery_day,
                "delivery_time": delivery_time,
                "delivery_frequency": subscription_prepaid.delivery_frequency,
                "charge_frequency": subscription_prepaid.charge_frequency,
            }
        )
        return ctx

    # def form_valid(self, form):
    #     subscription = self.get_object()
    #     subscription_prepaid = SubscriptionPrepaid.objects.get(
    #         djsubscription_id=subscription.id
    #     )
    #
    #     new_delivery_frequency = form.cleaned_data.get("subsChooseFrequency")
    #     delivery_date = form.cleaned_data.get("delivery_date")
    #     delivery_time = form.cleaned_data.get("subsChooseTime")
    #     delivery_day = delivery_date.strftime("%A")
    #     delivery_time_str = DeliveryTime.objects.get(id=delivery_time).__str__()
    #
    #     amount = subscription.plan.amount
    #     currency = subscription.plan.currency
    #     interval_count = subscription.plan.interval_count
    #
    #     if subscription_prepaid.initial_credit <= 4:
    #         interval_count = new_delivery_frequency
    #
    #     if int(new_delivery_frequency) == 1:
    #         delivery_frequency_str = "<p>Delivery Frequency: 1 week</p>"
    #     else:
    #         delivery_frequency_str = (
    #             f"<p>Delivery Frequency: {new_delivery_frequency} weeks</p>"
    #         )
    #
    #     delivery_date_str = f"<p>Delivery Day: {delivery_date.strftime('%A')}</p>"
    #     dt_str = f"<p>Delivery Time: {delivery_time_str}</p>"
    #     description_combined = (
    #         f"<p>{subscription_prepaid.description}</p>{delivery_frequency_str}"
    #         f"{delivery_date_str}{dt_str}"
    #     )
    #
    #     stripe_product = stripe.Product.modify(
    #         subscription.plan.product.id, description=description_combined
    #     )
    #     djstripe_product = DjStripeProduct.sync_from_stripe_data(stripe_product)
    #
    #     if subscription_prepaid.initial_credit <= 4:
    #         current_period_start = subscription.current_period_start
    #         i = 0
    #         while current_period_start.weekday() != 6:
    #             i += 1
    #             current_period_start += timedelta(days=1)
    #
    #         new_trial = current_period_start + timedelta(
    #             weeks=int(new_delivery_frequency)
    #         )
    #         plan = Plan.create(
    #             product=djstripe_product,
    #             amount=amount,
    #             interval="week",
    #             interval_count=interval_count,
    #             currency=currency,
    #         )
    #         subscription.update(
    #             plan, trial_end=int(new_trial.timestamp()), proration_behavior="none"
    #         )
    #
    #     if subscription_prepaid.subscription_type == "pay_as_you_go":
    #         subscription_prepaid.delivery_frequency = new_delivery_frequency
    #         subscription_prepaid.charge_frequency = new_delivery_frequency
    #         subscription_prepaid.save()
    #         reschedule_subscription(
    #             subscription=subscription_prepaid,
    #             delivery_date=delivery_date,
    #         )
    #     else:
    #         subscription_prepaid.delivery_frequency = new_delivery_frequency
    #         subscription_prepaid.save()
    #         reschedule_subscription(
    #             subscription=subscription_prepaid, delivery_date=delivery_date
    #         )
    #
    #     subscription_prepaid.delivery_time = delivery_time_str
    #     subscription_prepaid.delivery_day = delivery_day
    #     subscription_prepaid.save()
    #
    #     messages.success(self.request, "Delivery Updated")
    #
    #     return HttpResponseRedirect(reverse("customer:subscriptions"))

    def form_valid(self, form):
        subscription = self.get_object()
        subscription_prepaid = SubscriptionPrepaid.objects.get(
            djsubscription_id=subscription.id
        )
        new_delivery_frequency = form.cleaned_data.get("subsChooseFrequency")

        amount = subscription.plan.amount
        currency = subscription.plan.currency

        with transaction.atomic():
            if subscription_prepaid.subscription_type == "pay_as_you_go":
                if subscription_prepaid.deliveries_left() == 0:
                    delivery_date = subscription_prepaid.next_delivery_date + timedelta(
                        weeks=int(new_delivery_frequency)
                    )
                else:
                    delivery_date = subscription_prepaid.delivery_date
            else:
                if (
                    subscription_prepaid.deliveries_left()
                    == subscription_prepaid.initial_credit
                ):
                    delivery_date = subscription_prepaid.delivery_date
                else:
                    delivery_date = subscription_prepaid.next_delivery_date

            next_billing = calculate_next_billing_date(
                delivery_date=delivery_date,
                deliveries_left=subscription_prepaid.deliveries_left(),
                delivery_frequency=new_delivery_frequency,
            )

            next_billing_cycle_anchor = datetime.combine(
                next_billing, localtime().now().time()
            )
            djstripe_product = update_stripe_product_description(
                subscription=subscription,
                subscription_prepaid=subscription_prepaid,
                delivery_frequency=new_delivery_frequency,
            )

            plan = Plan.create(
                product=djstripe_product,
                amount=amount,
                interval="week",
                interval_count=new_delivery_frequency,
                currency=currency,
            )
            subscription.update(
                plan,
                trial_end=int(next_billing_cycle_anchor.timestamp()),
                proration_behavior="none",
            )
            subscription_prepaid.delivery_frequency = new_delivery_frequency
            subscription_prepaid.delivery_date = delivery_date
            subscription_prepaid.next_delivery_date = delivery_date + timedelta(
                weeks=int(new_delivery_frequency)
            )
            subscription_prepaid.save()

        messages.success(self.request, "Delivery Frequency Updated")
        return HttpResponseRedirect(
            reverse(
                "customer:subscription-detail", kwargs={"pk": self.kwargs.get("pk")}
            )
        )


class SubscriptionProductsUpdateView(LoginRequiredMixin, FormView):
    template_name = "pages/user-page/edit-product-subscription.html"
    model = Subscription
    form_class = SubscriptionProductEditForm

    def get_object(self, queryset=None):
        return Subscription.objects.get(id=self.kwargs.get("pk"))

    def get_context_data(self, **kwargs):
        ctx = super(SubscriptionProductsUpdateView, self).get_context_data(**kwargs)

        subscription = self.get_object()
        djstripe_product_description = subscription.plan.product.description

        if "<p>" in djstripe_product_description:
            djstripe_product_description_split = djstripe_product_description.split(
                "</p><p>"
            )
            product_selected = list(
                filter(
                    None,
                    djstripe_product_description_split[0]
                    .replace("<p>", "")
                    .replace("</p>", "")
                    .split(","),
                )
            )
        else:
            djstripe_product_description_split = djstripe_product_description.split(
                ", Delivery Day:"
            )
            product_selected = list(
                filter(None, djstripe_product_description_split[0].strip().split(","))
            )
        q = {
            "user": self.request.user,
            "djsubscription_id": subscription.id,
            "djcustomer_id": subscription.customer.id,
        }

        subscription_prepaid = subscription_prepaid_get_or_create(
            q, subscription, product_selected, djstripe_product_description_split
        )

        ctx.update(
            {
                "products": Product.objects.browsable().filter(
                    categories__slug="subscription"
                ),
                "subscription": subscription,
                "size_pack": subscription_prepaid.pack_size,
                "product_selected": product_selected,
            }
        )
        return ctx

    def form_valid(self, form):
        subscription = self.get_object()
        subscription_prepaid = SubscriptionPrepaid.objects.get(
            djsubscription_id=subscription.id
        )
        description = form.cleaned_data.get("description")

        djstripe_product_description = subscription.plan.product.description

        if "<p>" in djstripe_product_description:
            djstripe_product_description_split = djstripe_product_description.split(
                "</p><p>"
            )
            delivery_freq_str = f'<p>{djstripe_product_description_split[1].replace("<p>", "").replace("</p>", "")}</p>'
            delivery_date_str = f'<p>{djstripe_product_description_split[2].replace("<p>", "").replace("</p>", "")}</p>'
            delivery_time_str = f'<p>{djstripe_product_description_split[3].replace("<p>", "").replace("</p>", "")}</p>'

            combined_description = f"<p>{description}</p>{delivery_freq_str}{delivery_date_str}{delivery_time_str}"
        else:
            djstripe_product_description_split = djstripe_product_description.split(
                ", Delivery Day:"
            )
            delivery_date_time = f"Delivery Day:{djstripe_product_description_split[1]}"
            combined_description = f"{description}, {delivery_date_time}"

        stripe_product = stripe.Product.modify(
            subscription.plan.product.id,
            description=combined_description,
        )

        DjStripeProduct.sync_from_stripe_data(stripe_product)

        subscription_prepaid.description = description
        subscription_prepaid.note = description
        subscription_prepaid.save()

        messages.success(self.request, "Products Updated")

        return HttpResponseRedirect(reverse("customer:subscriptions"))


class CardUpdateView(LoginRequiredMixin, FormView):
    template_name = "pages/user-page/update-your-card.html"
    model = Subscription
    form_class = CreditCardForm

    def form_valid(self, form) -> HttpResponseRedirect:
        customer = Customer.objects.get(subscriber=self.request.user)

        create_or_update_payment_method(
            number=form.cleaned_data.get("number"),
            exp_month=form.cleaned_data.get("exp_month"),
            exp_year=form.cleaned_data.get("exp_year"),
            cvc=form.cleaned_data.get("cvc"),
            card_holder_name=form.cleaned_data.get("card_holder_name"),
            customer=customer,
        )

        messages.success(self.request, "Card Updated")
        return HttpResponseRedirect("/accounts/profile/")


class ResubscribeView(LoginRequiredMixin, FormView):
    template_name = "pages/user-page/resubscribe.html"
    model = Subscription
    form_class = ResubscribeForm

    def get_object(self, queryset=None):
        return Subscription.objects.get(id=self.kwargs.get("pk"))

    def get_context_data(self, **kwargs):
        ctx = super(ResubscribeView, self).get_context_data(**kwargs)
        subscription = self.get_object()
        subscription_prepaid = SubscriptionPrepaid.objects.get(
            djsubscription_id=self.kwargs.get("pk")
        )
        categories = Category.objects.filter(slug="coffee")
        start_date = get_start_available_day(categories)
        ctx.update(
            {
                "subscription": subscription,
                "subscription_prepaid": subscription_prepaid,
                "holidays": get_holidays(),
                "start_date": start_date,
                "form": ResubscribeForm(
                    initial={"subscription_id_old": subscription.id}
                ),
            }
        )
        return ctx

    def form_valid(self, form) -> HttpResponseRedirect:
        customer = Customer.objects.get(subscriber=self.request.user)
        subscription_id_old = form.cleaned_data.get("subscription_id_old")
        delivery_time_id = form.cleaned_data.get("delivery_time")
        subscription_old = Subscription.objects.get(id=subscription_id_old)
        subscription_prepaid_old = SubscriptionPrepaid.objects.get(
            djsubscription_id=subscription_id_old
        )
        delivery_date = form.cleaned_data.get("delivery_date")

        with transaction.atomic():
            create_or_update_payment_method(
                number=form.cleaned_data.get("number"),
                exp_month=form.cleaned_data.get("exp_month"),
                exp_year=form.cleaned_data.get("exp_year"),
                cvc=form.cleaned_data.get("cvc"),
                card_holder_name=form.cleaned_data.get("card_holder_name"),
                customer=customer,
            )

            plan = subscription_old.plan
            delivery_frequency = subscription_old.plan.interval_count
            next_billing = calculate_next_billing_date(
                delivery_date=delivery_date, delivery_frequency=delivery_frequency
            )
            next_billing_cycle_anchor = datetime.combine(
                next_billing, localtime().now().time()
            ) + timedelta(weeks=delivery_frequency)
            stripe_subscription = stripe.Subscription.create(
                customer=customer.id,
                items=[
                    {"price": f"{plan}", "quantity": subscription_prepaid_old.quantity},
                ],
            )

            # update next billing
            stripe_subscription = stripe.Subscription.modify(
                stripe_subscription.get("id"),
                trial_end=int(next_billing_cycle_anchor.timestamp()),
                proration_behavior="none",
            )

            delivery_time = DeliveryTime.objects.get(id=delivery_time_id)

            djstripe_subscription = Subscription.sync_from_stripe_data(
                stripe_subscription
            )
            subscription_prepaid = SubscriptionPrepaid.objects.create(
                user=subscription_prepaid_old.user,
                djsubscription_id=djstripe_subscription.id,
                djcustomer_id=customer.id,
                pack_size=subscription_prepaid_old.pack_size,
                description=subscription_prepaid_old.description,
                delivery_date=delivery_date,
                delivery_day=delivery_date.strftime("%A"),
                delivery_time=delivery_time.__str__(),
                delivery_frequency=int(subscription_prepaid_old.delivery_frequency),
                charge_frequency=int(subscription_prepaid_old.charge_frequency),
                initial_credit=subscription_prepaid_old.initial_credit,
                credit_per_delivery=int(subscription_prepaid_old.credit_per_delivery),
                next_credit_charge=int(subscription_prepaid_old.next_credit_charge),
                next_delivery_date=delivery_date,
                quantity=int(subscription_prepaid_old.quantity),
                note=subscription_prepaid_old.note,
                shipping_address=subscription_prepaid_old.shipping_address,
            )

            try:
                order = create_order(
                    delivery_time, djstripe_subscription, subscription_prepaid
                )
                send_resubscribe_email_confirmation.delay(
                    subscription_prepaid_id=subscription_prepaid.id, order_id=order.id
                )
            except Exception as err:
                print(str(err))

            messages.success(self.request, "Resubscribe created")
            return HttpResponseRedirect(
                reverse(
                    "customer:subscription-detail",
                    kwargs={"pk": djstripe_subscription.id},
                )
            )


class SubscriptionRescheduleView(LoginRequiredMixin, AjaxResponseMixin, DetailView):
    model = Subscription

    def get_ajax(self, request, *args, **kwargs):
        sub_id = self.kwargs.get("pk")
        subscription = Subscription.objects.get(id=sub_id)
        subscription_prepaid = SubscriptionPrepaid.objects.get(djsubscription_id=sub_id)
        delivery_date = self.request.GET.get("delivery_date", None)
        delivery_time_str = self.request.GET.get("delivery_time_str", None)

        delivery_date_fmt = datetime.strptime(delivery_date, "%d/%m/%Y")
        new_billing = self.get_calculate_next_billing_date(
            delivery_date_fmt, subscription_prepaid
        )
        previous_billing = localtime(
            subscription.current_period_end, timezone=pytz.timezone("Asia/Singapore")
        ).strftime("%d/%m/%Y")
        amount = subscription.plan.amount
        currency = subscription.plan.currency

        resp = {
            "status": "success",
            "delivery_remaining": subscription_prepaid.deliveries_left(),
            "delivery_order_sent": subscription_prepaid.delivery_order_sent(),
            "previous_billing": previous_billing,
            "previous_delivery_date": subscription_prepaid.delivery_date.strftime(
                "%d/%m/%Y"
            ),
            "previous_delivery_day": subscription_prepaid.delivery_day,
            "previous_delivery_day2": subscription_prepaid.delivery_day,
            "previous_delivery_time": subscription_prepaid.delivery_time,
            "new_delivery_date": delivery_date_fmt.strftime("%d/%m/%Y"),
            "new_delivery_date2": delivery_date_fmt.strftime("%B %d, %Y"),
            "new_delivery_day": delivery_date_fmt.strftime("%A"),
            "new_delivery_time": delivery_time_str,
            "new_billing": new_billing.strftime("%d/%m/%Y"),
            "day": delivery_date_fmt.strftime("%A"),
            "amount": f"{currency}{amount}",
        }
        return JsonResponse(resp)

    def post_ajax(self, request, *args, **kwargs):
        sub_id = self.kwargs.get("pk")
        subscription = Subscription.objects.get(id=sub_id)
        subscription_prepaid = SubscriptionPrepaid.objects.get(djsubscription_id=sub_id)
        delivery_date = self.request.POST.get("delivery_date", None)
        delivery_time_id = self.request.POST.get("delivery_time", None)
        delivery_time_str = self.request.POST.get("delivery_time_str", None)

        delivery_date_fmt = datetime.strptime(delivery_date, "%d/%m/%Y")
        interval_count = subscription.plan.interval_count
        resp = {"status": "success"}
        try:
            charged = reschedule_subscription(
                subscription=subscription_prepaid,
                delivery_date=delivery_date_fmt,
                delivery_time=DeliveryTime.objects.get(id=delivery_time_id),
            )

            update_stripe_product_description(
                subscription=subscription,
                subscription_prepaid=subscription_prepaid,
                delivery_frequency=interval_count,
                delivery_day=delivery_date_fmt.strftime("%A"),
                delivery_time=delivery_time_str,
            )

            if charged and subscription_prepaid.deliveries_left() <= 0:
                customer = Customer.objects.get(id=subscription_prepaid.djcustomer_id)
                renewed_kwargs = {
                    "subscription_id": sub_id,
                    "email": customer.subscriber.email,
                    "full_name": customer.subscriber.get_full_name(),
                }
                send_email_subscription_renewed.delay(**renewed_kwargs)
        except Exception as err:
            print(err)
            resp.update({"status": "error", "message": str(err)})

        return JsonResponse(resp)

    def get_calculate_next_billing_date(
        self, delivery_date_fmt: datetime, subscription_prepaid: SubscriptionPrepaid
    ):
        new_billing = calculate_next_billing_date(
            delivery_date=delivery_date_fmt,
            deliveries_left=subscription_prepaid.deliveries_left(),
            delivery_frequency=subscription_prepaid.delivery_frequency,
        )

        return new_billing


class SubscriptionDeliveryAddressEditView(
    LoginRequiredMixin, AjaxResponseMixin, UpdateView
):
    template_name = "pages/user-page/subscription_shipping_address_form.html"
    model = ShippingAddress
    form_class = ShippingAddressForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["user"] = self.request.user
        return kwargs

    def get_context_data(self, **kwargs):
        ctx = super(SubscriptionDeliveryAddressEditView, self).get_context_data(
            **kwargs
        )
        ctx.update(
            {
                "subscription_id": self.kwargs.get("sub_pk"),
                "shipping_address_id": self.kwargs.get("pk"),
            }
        )
        return ctx

    def get_success_url(self):
        return reverse(
            "customer:subscription-detail", kwargs={"pk": self.kwargs.get("sub_pk")}
        )
