import oscar.apps.customer.apps as apps
from django.conf.urls import url
from django.urls import path
from oscar.core.loading import get_class


class CustomerConfig(apps.CustomerConfig):
    name = "ecomm.customer"

    def ready(self):
        super(CustomerConfig, self).ready()

        self.transaction_history_list_view = get_class(
            "customer.views", "TransactionHistoryListView"
        )

        self.custom_address_update_view = get_class(
            "customer.views", "CustomAddressUpdateView"
        )

        self.subcription_list_view = get_class("customer.views", "SubscriptionListView")

        self.subcription_detail_view = get_class(
            "customer.views", "SubscriptionDetailView"
        )

        self.subcription_product_update_view = get_class(
            "customer.views", "SubscriptionProductsUpdateView"
        )

        self.subcription_delivery_update_view = get_class(
            "customer.views", "SubscriptionDeliveryUpdateView"
        )

        self.card_update_view = get_class("customer.views", "CardUpdateView")
        self.resubscribe_view = get_class("customer.views", "ResubscribeView")
        self.reschedule_view = get_class("customer.views", "SubscriptionRescheduleView")
        self.subscription_delivery_address_edit_view = get_class("customer.views", "SubscriptionDeliveryAddressEditView")

    def get_urls(self):
        my_urls = [
            url(
                r"^custom-update-address/(?P<pk>\d+)/$",
                self.custom_address_update_view.as_view(),
                name="custom-update-address",
            ),
            url(
                r"^transaction-history/$",
                self.transaction_history_list_view.as_view(),
                name="transaction-history-list",
            ),
            # url(
            #     r"^subscription-delivery/(?P<pk>\d+)/edit/$",
            #     self.subcription_delivery_update_view.as_view(),
            #     name="subscription-delivery-edit",
            # ),
            path(
                "subscriptions/",
                self.subcription_list_view.as_view(),
                name="subscriptions",
            ),
            path(
                "subscription/<str:pk>/",
                self.subcription_detail_view.as_view(),
                name="subscription-detail",
            ),
            path(
                "subscription-product/<str:pk>/edit/",
                self.subcription_product_update_view.as_view(),
                name="subscription-product-edit",
            ),
            path(
                "subscription-delivery/<str:pk>/edit/",
                self.subcription_delivery_update_view.as_view(),
                name="subscription-delivery-edit",
            ),
            path(
                "subscription/<str:pk>/resubscribe/",
                self.resubscribe_view.as_view(),
                name="subscription-resubscribe",
            ),
            path(
                "subscription/<str:pk>/reschedule/",
                self.reschedule_view.as_view(),
                name="subscription-reschedule",
            ),
            path(
                "card/edit/",
                self.card_update_view.as_view(),
                name="card-edit",
            ),
            path(
                "subscription/<str:sub_pk>/delivery_address/<str:pk>/edit/",
                self.subscription_delivery_address_edit_view.as_view(),
                name="subscription-delivery-address-edit",
            ),
        ]

        default_urls = super(CustomerConfig, self).get_urls()

        return self.post_process_urls(my_urls) + default_urls
