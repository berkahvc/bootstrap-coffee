from django import forms
from oscar.apps.customer.forms import EmailUserCreationForm as CoreEmailUserCreationForm
from oscar.core.compat import get_user_model
from oscar.core.loading import get_model

User = get_user_model()
DeliveryTime = get_model("schedule_management", "DeliveryTime")


class EmailUserCreationForm(CoreEmailUserCreationForm):
    class Meta:
        model = User
        fields = ("email", "first_name", "last_name")

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.username = self.cleaned_data["email"]
        user.is_from_shopify = False
        if commit:
            user.save()
        return user


class SubscriptionDeliveryEditForm(forms.Form):
    subsChooseFrequency = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(SubscriptionDeliveryEditForm, self).__init__(**kwargs)

        self.fields["subsChooseFrequency"].label = "CHOOSE YOUR FREQUENCY"


class SubscriptionProductEditForm(forms.Form):
    description = forms.CharField(required=True, widget=forms.HiddenInput)
    total_selected = forms.CharField(required=True, widget=forms.HiddenInput, initial=0)

    def __init__(self, *args, **kwargs):
        super(SubscriptionProductEditForm, self).__init__(**kwargs)

        self.fields["description"].label = "CHOOSE YOUR COFFEE"


class CardForm(forms.Form):
    card_name = forms.CharField(required=False)
    exp_month = forms.CharField(required=True)
    exp_year = forms.CharField(required=True)


class CreditCardForm(forms.Form):
    number = forms.CharField(label="Card Number", max_length=20, required=True)
    cardName = forms.CharField(label="Card Name", max_length=255, required=True)
    exp_month = forms.CharField(label="Expiration Month", max_length=2, required=True)
    exp_year = forms.CharField(label="Expiration Year", max_length=4, required=True)
    cvc = forms.CharField(label="CVC", max_length=4, required=True)

    def __init__(self, *args, **kwargs):
        super(CreditCardForm, self).__init__(*args, **kwargs)


class ResubscribeForm(CreditCardForm):
    subscription_id_old = forms.CharField(widget=forms.HiddenInput(), required=True)
    delivery_date = forms.DateField(required=True, input_formats=["%d/%m/%Y"])
    delivery_time = forms.CharField(
        required=True, widget=forms.Select(attrs={"style": "color: #FFFFFF;"})
    )
