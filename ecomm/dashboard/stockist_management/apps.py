from django.apps import AppConfig


class StockistManagementDashboardConfig(AppConfig):
    name = "ecomm.dashboard.stockist_management"
