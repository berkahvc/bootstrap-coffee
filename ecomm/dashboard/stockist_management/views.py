from django.conf import settings
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView, DeleteView, UpdateView
from django_tables2 import SingleTableView
from oscar.core.loading import get_model

from .forms import StockistForm
from .tables import StockistTable

Stockist = get_model("stockist_management", "Stockist")


class StockistListView(SingleTableView):
    template_name = "dashboard/stockist_management/stockist_index.html"
    model = Stockist
    context_table_name = "stockist"
    table_class = StockistTable

    def get_table_pagination(self, table):
        return dict(per_page=settings.OSCAR_DASHBOARD_ITEMS_PER_PAGE)


class StockistCreateView(CreateView):
    template_name = (
        "dashboard/subscription_management/delivery_frequency_update_create.html"
    )
    model = Stockist
    form_class = StockistForm
    success_url = reverse_lazy("dashboard:delivery-frequencies-list")

    def get_context_data(self, **kwargs):
        ctx = super(StockistCreateView, self).get_context_data(**kwargs)
        ctx.update({"title": _("Add a new delivery frequency")})

        return ctx


class StockistUpdateView(UpdateView):
    template_name = (
        "dashboard/subscription_management/delivery_frequency_update_create.html"
    )
    model = Stockist
    form_class = StockistForm
    success_url = reverse_lazy("dashboard:delivery-frequencies-list")

    def get_context_data(self, **kwargs):
        ctx = super(StockistUpdateView, self).get_context_data(**kwargs)
        ctx.update({"title": _(f"Update delivery frequency - {self.object.name}")})

        return ctx


class StockistDeleteView(DeleteView):
    template_name = "dashboard/subscription_management/delivery_frequency_delete.html"
    model = Stockist
    success_url = reverse_lazy("dashboard:delivery-frequencies-list")
