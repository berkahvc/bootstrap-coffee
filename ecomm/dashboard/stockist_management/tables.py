from django.utils.translation import gettext_lazy as _
from django.utils.translation import ngettext_lazy
from django_tables2 import TemplateColumn
from oscar.core.loading import get_class, get_model

DashboardTable = get_class("dashboard.tables", "DashboardTable")

Stockist = get_model("stockist_management", "Stockist")


class StockistTable(DashboardTable):
    actions = TemplateColumn(
        verbose_name=_("Actions"),
        template_name="dashboard/stockist_management/stockist_row_actions.html",
        orderable=False,
    )

    icon = "sitemap"
    caption = ngettext_lazy("%s Stockist", "%s Stockist")

    class Meta(DashboardTable.Meta):
        model = Stockist
        fields = ("name",)
