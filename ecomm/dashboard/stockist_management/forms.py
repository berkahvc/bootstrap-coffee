from django import forms
from oscar.core.loading import get_model

Stockist = get_model("stockist_management", "Stockist")


class StockistForm(forms.ModelForm):
    class Meta:
        model = Stockist
        fields = "__all__"
