from django.utils.translation import gettext_lazy as _
from django.utils.translation import ngettext_lazy
from django_tables2 import A, Column, TemplateColumn
from oscar.core.loading import get_class, get_model

DashboardTable = get_class("dashboard.tables", "DashboardTable")

Banner = get_model("catalogue", "Banner")
Product = get_model("catalogue", "Product")


class ProductTable(DashboardTable):
    title = TemplateColumn(
        verbose_name=_("Title"),
        template_name="oscar/dashboard/catalogue/product_row_title.html",
        order_by="title",
        accessor=A("title"),
    )
    image = TemplateColumn(
        verbose_name=_("Image"),
        template_name="oscar/dashboard/catalogue/product_row_image.html",
        orderable=False,
    )
    product_class = Column(
        verbose_name=_("Product type"),
        accessor=A("product_class"),
        order_by="product_class__name",
    )
    categories = TemplateColumn(
        verbose_name=_("Categories"),
        template_name="oscar/dashboard/catalogue/product_row_categories.html",
        orderable=False,
    )
    variant_name = TemplateColumn(
        verbose_name=_("Variant Name"),
        template_name="oscar/dashboard/catalogue/product_row_variants.html",
        orderable=False,
    )
    total_variants = TemplateColumn(
        verbose_name=_("Total Variants"),
        template_name="oscar/dashboard/catalogue/product_row_total_variants.html",
        orderable=False,
    )
    actions = TemplateColumn(
        verbose_name=_("Actions"),
        template_name="oscar/dashboard/catalogue/product_row_actions.html",
        orderable=False,
    )

    icon = "sitemap"

    class Meta(DashboardTable.Meta):
        model = Product
        fields = ("date_updated",)
        sequence = (
            "title",
            "image",
            "product_class",
            "categories",
            "variant_name",
            "total_variants",
            "date_updated",
            "actions",
        )
        order_by = "-date_updated"


class BannerTable(DashboardTable):
    image = TemplateColumn(
        verbose_name=_("Image"),
        template_name="dashboard/banner/banner_row_image.html",
        orderable=False,
    )
    actions = TemplateColumn(
        verbose_name=_("Actions"),
        template_name="dashboard/banner/banner_row_actions.html",
        orderable=False,
    )

    icon = "sitemap"
    caption = ngettext_lazy("%s Banner", "%s Banners")

    class Meta(DashboardTable.Meta):
        model = Banner
        fields = ("image", "url", "open_new_tab")
