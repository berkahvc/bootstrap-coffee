from django import forms
from django.db.models import Q
from django.utils.translation import gettext_lazy as _
from oscar.apps.dashboard.catalogue.forms import (
    ProductClassSelectForm as CoreProductClassSelectForm,
)
from oscar.apps.dashboard.catalogue.forms import (
    ProductRecommendationForm as CoreProductRecommendationForm,
)
from oscar.core.loading import get_model

Banner = get_model("catalogue", "Banner")
Product = get_model("catalogue", "Product")
Category = get_model("catalogue", "Category")
NutritionalInformation = get_model("catalogue", "NutritionalInformation")
NutritionalFact = get_model("catalogue", "NutritionalFact")
ProductClass = get_model("catalogue", "ProductClass")


class ProductClassSelectForm(CoreProductClassSelectForm):
    product_class = forms.ModelChoiceField(
        label=_("Create a new product of type"),
        empty_label=_("-- Choose type --"),
        queryset=ProductClass.objects.exclude(name__icontains="subscription"),
    )


class ProductRecommendationForm(CoreProductRecommendationForm):
    recommendation = forms.ModelChoiceField(
        queryset=Product.objects.browsable().exclude(
            Q(product_class__name__icontains="subscription")
            | Q(categories__name__icontains="subscription")
        )
    )


class ProductSearchForm(forms.Form):
    VARIANT_CHOICES = (
        ("", "---------"),
        ("bundle", "Bundle"),
        ("gift_card", "Gift Card"),
        ("pack", "Pack"),
    )
    category = forms.ModelChoiceField(queryset=Category.objects.all(), required=False)
    variant_name = forms.ChoiceField(
        choices=VARIANT_CHOICES, required=False, label=_("Variant Name")
    )
    title = forms.CharField(max_length=255, required=False, label=_("Product title"))

    def clean(self):
        cleaned_data = super().clean()
        cleaned_data["title"] = cleaned_data["title"].strip()
        return cleaned_data


class ProductNutritionalInformationForm(forms.ModelForm):
    class Meta:
        model = NutritionalInformation
        fields = ["ingredients"]


class ProductNutritionalFactForm(forms.ModelForm):
    class Meta:
        model = NutritionalFact
        fields = ["name", "quantity", "unit_of_measurement"]


class BannerForm(forms.ModelForm):
    class Meta:
        model = Banner
        fields = "__all__"
