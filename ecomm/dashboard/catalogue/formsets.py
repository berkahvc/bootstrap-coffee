from django.forms.models import inlineformset_factory
from oscar.core.loading import get_class, get_model

Product = get_model("catalogue", "Product")
NutritionalInformation = get_model("catalogue", "NutritionalInformation")
NutritionalFact = get_model("catalogue", "NutritionalFact")

ProductNutritionalInformationForm = get_class(
    "dashboard.catalogue.forms", "ProductNutritionalInformationForm"
)
ProductNutritionalFactForm = get_class(
    "dashboard.catalogue.forms", "ProductNutritionalFactForm"
)


BaseNutritionalInformationFormSet = inlineformset_factory(
    Product, NutritionalInformation, form=ProductNutritionalInformationForm, extra=1
)
BaseNutritionalFactFormSet = inlineformset_factory(
    Product, NutritionalFact, form=ProductNutritionalFactForm, extra=1
)


class NutritionalInformationFormSet(BaseNutritionalInformationFormSet):
    def __init__(self, product_class, user, *args, **kwargs):
        super().__init__(*args, **kwargs)


class NutritionalFactFormSet(BaseNutritionalFactFormSet):
    def __init__(self, product_class, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
