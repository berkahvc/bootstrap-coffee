from django.conf import settings
from django.db.models import Q
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView, DeleteView, UpdateView
from django_tables2 import SingleTableView
from oscar.apps.dashboard.catalogue.views import (
    ProductCreateUpdateView as BaseProductCreateUpdateView,
)
from oscar.apps.dashboard.catalogue.views import ProductListView as BaseProductListView
from oscar.core.loading import get_class, get_model

from .forms import BannerForm
from .tables import BannerTable

Product = get_model("catalogue", "Product")
Banner = get_model("catalogue", "Banner")

NutritionalInformationFormSet = get_class(
    "dashboard.catalogue.formsets", "NutritionalInformationFormSet"
)
NutritionalFactFormSet = get_class(
    "dashboard.catalogue.formsets", "NutritionalFactFormSet"
)


class ProductListView(BaseProductListView):
    def get_queryset(self):
        """
        Build the queryset for this list
        """
        queryset = (
            Product.objects.browsable_dashboard()
            .base_queryset()
            .exclude(product_class__name__icontains="subscription")
        )
        queryset = self.filter_queryset(queryset)
        queryset = self.apply_search(queryset)
        return queryset

    def apply_search(self, queryset):
        """
        Search through the filtered queryset.

        We must make sure that we don't return search results that the user is not allowed
        to see (see filter_queryset).
        """
        self.form = self.form_class(self.request.GET)

        if not self.form.is_valid():
            return queryset

        data = self.form.cleaned_data

        if data.get("variant_name"):
            matches_variant_name = Product.objects.filter(
                title__icontains=data["variant_name"], parent__isnull=False
            )
            qs_match = queryset.filter(
                Q(id__in=matches_variant_name.values("id"))
                | Q(id__in=matches_variant_name.values("parent_id"))
            )

            queryset = qs_match

        if data.get("category"):
            queryset = queryset.filter(categories=data["category"])

        if data.get("title"):
            queryset = queryset.filter(title__icontains=data["title"])

        return queryset


class ProductCreateUpdateView(BaseProductCreateUpdateView):
    nutritional_information_formset = NutritionalInformationFormSet
    nutritional_fact_formset = NutritionalFactFormSet

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.formsets = {
            "category_formset": self.category_formset,
            "image_formset": self.image_formset,
            "recommended_formset": self.recommendations_formset,
            "stockrecord_formset": self.stockrecord_formset,
            "nutritional_information_formset": self.nutritional_information_formset,
            "nutritional_fact_formset": self.nutritional_fact_formset,
        }


class BannerListView(SingleTableView):
    template_name = "dashboard/banner/index.html"
    model = Banner
    context_table_name = "banners"
    table_class = BannerTable

    def get_table_pagination(self, table):
        return dict(per_page=settings.OSCAR_DASHBOARD_ITEMS_PER_PAGE)


class BannerCreateView(CreateView):
    template_name = "dashboard/banner/banner_update_create.html"
    model = Banner
    form_class = BannerForm
    success_url = reverse_lazy("dashboard:banner-list")

    def get_context_data(self, **kwargs):
        ctx = super(BannerCreateView, self).get_context_data(**kwargs)
        ctx.update({"title": _("Add a new banner")})

        return ctx


class BannerUpdateView(UpdateView):
    template_name = "dashboard/banner/banner_update_create.html"
    model = Banner
    form_class = BannerForm
    success_url = reverse_lazy("dashboard:banner-list")

    def get_context_data(self, **kwargs):
        ctx = super(BannerUpdateView, self).get_context_data(**kwargs)
        ctx.update({"title": _("Update banner")})

        return ctx


class BannerDeleteView(DeleteView):
    template_name = "dashboard/banner/banner_delete.html"
    model = Banner
    success_url = reverse_lazy("dashboard:banner-list")
