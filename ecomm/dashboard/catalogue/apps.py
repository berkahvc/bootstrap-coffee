import oscar.apps.dashboard.catalogue.apps as apps


class CatalogueDashboardConfig(apps.CatalogueDashboardConfig):
    name = "ecomm.dashboard.catalogue"
