from collections import OrderedDict

from django.http import HttpResponse
from django.utils.translation import gettext_lazy as _
from oscar.apps.dashboard.orders.views import OrderListView as CoreOrderListView
from oscar.core.compat import UnicodeCSVWriter
from oscar.core.utils import format_datetime


class OrderListView(CoreOrderListView):
    def download_selected_orders(self, request, orders):
        response = HttpResponse(content_type="text/csv")
        response[
            "Content-Disposition"
        ] = "attachment; filename=%s" % self.get_download_filename(request)
        writer = UnicodeCSVWriter(open_file=response)

        meta_data = (
            ("number", _("Order Number")),
            ("sku", _("SKU")),
            ("num_items", _("QTY")),
            ("address", _("Alamat")),
            ("customer", _("Nama Customer")),
            ("voucher", _("Voucher")),
            ("price", _("Price")),
            ("order_created", _("Order Created")),
        )
        columns = OrderedDict()
        for k, v in meta_data:
            columns[k] = v

        writer.writerow(columns.values())
        for order in orders:
            for item in order.lines.all():
                row = columns.copy()
                row["number"] = order.number
                row["sku"] = item.partner_sku
                row["num_items"] = item.quantity
                row["price"] = item.line_price_before_discounts_incl_tax
                row["voucher"] = ", ".join(
                    list(order.basket.vouchers.values_list("name", flat=True))
                )
                row["order_created"] = format_datetime(
                    order.date_placed, "DATETIME_FORMAT"
                )
                if order.shipping_address:
                    active_address_field = (
                        order.shipping_address.active_address_fields()
                    )
                    row["customer"] = order.shipping_address.name
                    row["address"] = ", ".join(active_address_field[1:])
                else:
                    row["customer"] = ""
                    row["address"] = ""
                writer.writerow(row.values())
        return response
