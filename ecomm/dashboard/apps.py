import oscar.apps.dashboard.apps as apps
from django.conf.urls import url
from oscar.core.loading import get_class


class DashboardConfig(apps.DashboardConfig):
    name = "ecomm.dashboard"

    def ready(self):
        super(DashboardConfig, self).ready()

        # banner
        self.banner_list_view = get_class("dashboard.catalogue.views", "BannerListView")
        self.banner_create_view = get_class(
            "dashboard.catalogue.views", "BannerCreateView"
        )
        self.banner_update_view = get_class(
            "dashboard.catalogue.views", "BannerUpdateView"
        )
        self.banner_delete_view = get_class(
            "dashboard.catalogue.views", "BannerDeleteView"
        )
        # ====================================

        # schedule management
        self.preparation_times_list_view = get_class(
            "dashboard.schedule_management.views", "PreparationTimeListView"
        )
        self.preparation_times_create_view = get_class(
            "dashboard.schedule_management.views", "PreparationTimeCreateView"
        )
        self.preparation_times_update_view = get_class(
            "dashboard.schedule_management.views", "PreparationTimeUpdateView"
        )
        self.preparation_times_delete_view = get_class(
            "dashboard.schedule_management.views", "PreparationTimeDeleteView"
        )

        self.delivery_times_list_view = get_class(
            "dashboard.schedule_management.views", "DeliveryTimeListView"
        )
        self.delivery_times_create_view = get_class(
            "dashboard.schedule_management.views", "DeliveryTimeCreateView"
        )
        self.delivery_times_update_view = get_class(
            "dashboard.schedule_management.views", "DeliveryTimeUpdateView"
        )
        self.delivery_times_delete_view = get_class(
            "dashboard.schedule_management.views", "DeliveryTimeDeleteView"
        )

        self.holidays_list_view = get_class(
            "dashboard.schedule_management.views", "HolidayListView"
        )
        self.holidays_create_view = get_class(
            "dashboard.schedule_management.views", "HolidayCreateView"
        )
        self.holidays_update_view = get_class(
            "dashboard.schedule_management.views", "HolidayUpdateView"
        )
        self.holidays_delete_view = get_class(
            "dashboard.schedule_management.views", "HolidayDeleteView"
        )
        # =================================

        self.delivery_frequencies_list_view = get_class(
            "dashboard.subscription_management.views", "DeliveryFrequencyListView"
        )
        self.delivery_frequencies_create_view = get_class(
            "dashboard.subscription_management.views", "DeliveryFrequencyCreateView"
        )
        self.delivery_frequencies_update_view = get_class(
            "dashboard.subscription_management.views", "DeliveryFrequencyUpdateView"
        )
        self.delivery_frequencies_delete_view = get_class(
            "dashboard.subscription_management.views", "DeliveryFrequencyDeleteView"
        )

        self.prepaid_list_view = get_class(
            "dashboard.subscription_management.views", "PrepaidListView"
        )
        self.prepaid_create_view = get_class(
            "dashboard.subscription_management.views", "PrepaidCreateView"
        )
        self.prepaid_update_view = get_class(
            "dashboard.subscription_management.views", "PrepaidUpdateView"
        )
        self.prepaid_delete_view = get_class(
            "dashboard.subscription_management.views", "PrepaidDeleteView"
        )

        # stockist management
        self.stockist_list_view = get_class(
            "dashboard.stockist_management.views", "StockistListView"
        )
        self.stockist_create_view = get_class(
            "dashboard.stockist_management.views", "StockistCreateView"
        )
        self.stockist_update_view = get_class(
            "dashboard.stockist_management.views", "StockistUpdateView"
        )
        self.stockist_delete_view = get_class(
            "dashboard.stockist_management.views", "StockistDeleteView"
        )
        # ====================================

    def get_urls(self):
        my_urls = [
            url(r"^banners/$", self.banner_list_view.as_view(), name="banner-list"),
            url(
                r"^banners/new/$",
                self.banner_create_view.as_view(),
                name="banner-create",
            ),
            url(
                r"^banners/edit/(?P<pk>\d+)/$",
                self.banner_update_view.as_view(),
                name="banner-update",
            ),
            url(
                r"^banners/delete/(?P<pk>\d+)/$",
                self.banner_delete_view.as_view(),
                name="banner-delete",
            ),
            # preparation times
            url(
                r"^preparation-times/$",
                self.preparation_times_list_view.as_view(),
                name="preparation-times-list",
            ),
            url(
                r"^preparation-times/new/$",
                self.preparation_times_create_view.as_view(),
                name="preparation-times-create",
            ),
            url(
                r"^preparation-times/edit/(?P<pk>\d+)/$",
                self.preparation_times_update_view.as_view(),
                name="preparation-times-update",
            ),
            url(
                r"^preparation-times/delete/(?P<pk>\d+)/$",
                self.preparation_times_delete_view.as_view(),
                name="preparation-times-delete",
            ),
            # delivery times
            url(
                r"^delivery-times/$",
                self.delivery_times_list_view.as_view(),
                name="delivery-times-list",
            ),
            url(
                r"^delivery-times/new/$",
                self.delivery_times_create_view.as_view(),
                name="delivery-times-create",
            ),
            url(
                r"^delivery-times/edit/(?P<pk>\d+)/$",
                self.delivery_times_update_view.as_view(),
                name="delivery-times-update",
            ),
            url(
                r"^delivery-times/delete/(?P<pk>\d+)/$",
                self.delivery_times_delete_view.as_view(),
                name="delivery-times-delete",
            ),
            # holidays
            url(
                r"^holidays/$", self.holidays_list_view.as_view(), name="holidays-list"
            ),
            url(
                r"^holidays/new/$",
                self.holidays_create_view.as_view(),
                name="holidays-create",
            ),
            url(
                r"^holidays/edit/(?P<pk>\d+)/$",
                self.holidays_update_view.as_view(),
                name="holidays-update",
            ),
            url(
                r"^holidays/delete/(?P<pk>\d+)/$",
                self.holidays_delete_view.as_view(),
                name="holidays-delete",
            ),
        ]

        subscription_management_urls = [
            url(
                r"^delivery-frequencies/$",
                self.delivery_frequencies_list_view.as_view(),
                name="delivery-frequencies-list",
            ),
            url(
                r"^delivery-frequencies/new/$",
                self.delivery_frequencies_create_view.as_view(),
                name="delivery-frequencies-create",
            ),
            url(
                r"^delivery-frequencies/edit/(?P<pk>\d+)/$",
                self.delivery_frequencies_update_view.as_view(),
                name="delivery-frequencies-update",
            ),
            url(
                r"^delivery-frequencies/delete/(?P<pk>\d+)/$",
                self.delivery_frequencies_delete_view.as_view(),
                name="delivery-frequencies-delete",
            ),
            url(
                r"^prepaid/$",
                self.prepaid_list_view.as_view(),
                name="prepaid-list",
            ),
            url(
                r"^prepaid/new/$",
                self.prepaid_create_view.as_view(),
                name="prepaid-create",
            ),
            url(
                r"^prepaid/edit/(?P<pk>\d+)/$",
                self.prepaid_update_view.as_view(),
                name="prepaid-update",
            ),
            url(
                r"^prepaid/delete/(?P<pk>\d+)/$",
                self.prepaid_delete_view.as_view(),
                name="prepaid-delete",
            ),
        ]

        default_urls = super(DashboardConfig, self).get_urls()
        return (
            self.post_process_urls(my_urls)
            + self.post_process_urls(subscription_management_urls)
            + default_urls
        )
