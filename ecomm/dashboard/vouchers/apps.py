import oscar.apps.dashboard.vouchers.apps as apps


class VouchersDashboardConfig(apps.VouchersDashboardConfig):
    name = "ecomm.dashboard.vouchers"
