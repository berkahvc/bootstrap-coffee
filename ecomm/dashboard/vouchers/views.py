from django.db import transaction
from django.http import HttpResponseRedirect
from oscar.apps.dashboard.vouchers.views import (
    VoucherCreateView as CoreVoucherCreateView,
)
from oscar.apps.dashboard.vouchers.views import (
    VoucherSetCreateView as CoreVoucherSetCreateView,
)
from oscar.apps.dashboard.vouchers.views import (
    VoucherSetUpdateView as CoreVoucherSetUpdateView,
)
from oscar.apps.dashboard.vouchers.views import (
    VoucherUpdateView as CoreVoucherUpdateView,
)
from oscar.apps.voucher.utils import get_offer_name
from oscar.core.loading import get_model

Voucher = get_model("voucher", "Voucher")
VoucherSet = get_model("voucher", "VoucherSet")
ConditionalOffer = get_model("offer", "ConditionalOffer")
Benefit = get_model("offer", "Benefit")
Condition = get_model("offer", "Condition")
OrderDiscount = get_model("order", "OrderDiscount")


def create_offer_and_condition(voucher, form):
    condition = Condition.objects.create(
        range=form.cleaned_data["benefit_range"], type=Condition.COUNT, value=1
    )
    benefit = Benefit.objects.create(
        range=form.cleaned_data["benefit_range"],
        type=form.cleaned_data["benefit_type"],
        value=form.cleaned_data["benefit_value"],
        minimum_payment_value=form.cleaned_data["benefit_minimum_payment_value"] or 0,
    )
    name = form.cleaned_data["name"]
    offer = ConditionalOffer.objects.create(
        name=get_offer_name(name),
        offer_type=ConditionalOffer.VOUCHER,
        benefit=benefit,
        condition=condition,
        exclusive=form.cleaned_data["exclusive"],
    )
    voucher.offers.add(offer)


class VoucherCreateView(CoreVoucherCreateView):
    @transaction.atomic()
    def form_valid(self, form):
        name = form.cleaned_data["name"]
        voucher = Voucher.objects.create(
            name=name,
            code=form.cleaned_data["code"],
            usage=form.cleaned_data["usage"],
            start_datetime=form.cleaned_data["start_datetime"],
            end_datetime=form.cleaned_data["end_datetime"],
            add_free_shipping=form.cleaned_data["add_free_shipping"],
        )
        create_offer_and_condition(voucher, form)
        return HttpResponseRedirect(self.get_success_url())


class VoucherUpdateView(CoreVoucherUpdateView):
    def get_initial(self):
        voucher = self.get_voucher()
        offers = voucher.offers.all()
        offer = None
        benefit = None
        if offers.count() > 0:
            offer = offers[0]
            benefit = offer.benefit
        return {
            "name": voucher.name,
            "code": voucher.code,
            "start_datetime": voucher.start_datetime,
            "end_datetime": voucher.end_datetime,
            "usage": voucher.usage,
            "benefit_type": benefit.type if benefit else "-",
            "benefit_range": benefit.range if benefit else "-",
            "benefit_value": benefit.value if benefit else "-",
            "benefit_minimum_payment_value": benefit.minimum_payment_value
            if benefit
            else 0,
            "exclusive": offer.exclusive if offer else "-",
            "add_free_shipping": voucher.add_free_shipping,
        }

    @transaction.atomic()
    def form_valid(self, form):
        voucher = self.get_voucher()
        voucher.name = form.cleaned_data["name"]
        voucher.code = form.cleaned_data["code"]
        voucher.usage = form.cleaned_data["usage"]
        voucher.start_datetime = form.cleaned_data["start_datetime"]
        voucher.end_datetime = form.cleaned_data["end_datetime"]
        voucher.add_free_shipping = form.cleaned_data["add_free_shipping"]
        voucher.save()

        offers = voucher.offers.all()
        if offers.count() > 0:
            offer = offers[0]
            offer.condition.range = form.cleaned_data["benefit_range"]
            offer.condition.save()

            offer.exclusive = form.cleaned_data["exclusive"]
            offer.name = get_offer_name(voucher.name)
            offer.save()

            benefit = voucher.benefit
            benefit.range = form.cleaned_data["benefit_range"]
            benefit.type = form.cleaned_data["benefit_type"]
            benefit.value = form.cleaned_data["benefit_value"]
            benefit.minimum_payment_value = form.cleaned_data[
                "benefit_minimum_payment_value"
            ]
            benefit.save()
        else:
            create_offer_and_condition(voucher, form)

        return HttpResponseRedirect(self.get_success_url())


class VoucherSetCreateView(CoreVoucherSetCreateView):
    def form_valid(self, form):
        condition = Condition.objects.create(
            range=form.cleaned_data["benefit_range"], type=Condition.COUNT, value=1
        )
        benefit = Benefit.objects.create(
            range=form.cleaned_data["benefit_range"],
            type=form.cleaned_data["benefit_type"],
            value=form.cleaned_data["benefit_value"],
            minimum_payment_value=form.cleaned_data["benefit_minimum_payment_value"]
            or 0,
        )
        name = form.cleaned_data["name"]
        offer = ConditionalOffer.objects.create(
            name=get_offer_name(name),
            offer_type=ConditionalOffer.VOUCHER,
            benefit=benefit,
            condition=condition,
        )

        VoucherSet.objects.create(
            name=name,
            count=form.cleaned_data["count"],
            code_length=form.cleaned_data["code_length"],
            description=form.cleaned_data["description"],
            start_datetime=form.cleaned_data["start_datetime"],
            end_datetime=form.cleaned_data["end_datetime"],
            offer=offer,
            prefix=form.cleaned_data["voucher_prefix"],
            suffix=form.cleaned_data["voucher_suffix"],
            add_free_shipping=form.cleaned_data["add_free_shipping"],
        )
        return HttpResponseRedirect(self.get_success_url())


class VoucherSetUpdateView(CoreVoucherSetUpdateView):
    def get_initial(self):
        voucherset = self.get_voucherset()
        offer = voucherset.offer
        benefit = offer.benefit
        return {
            "name": voucherset.name,
            "count": voucherset.count,
            "code_length": voucherset.code_length,
            "start_datetime": voucherset.start_datetime,
            "end_datetime": voucherset.end_datetime,
            "description": voucherset.description,
            "benefit_type": benefit.type,
            "benefit_range": benefit.range,
            "benefit_value": benefit.value,
            "benefit_minimum_payment_value": benefit.minimum_payment_value
            if benefit
            else 0,
            "exclusive": offer.exclusive if offer else "-",
            "voucher_prefix": voucherset.prefix,
            "voucher_suffix": voucherset.suffix,
            "add_free_shipping": voucherset.add_free_shipping,
        }

    def form_valid(self, form):
        voucherset = form.save()
        if not voucherset.offer:
            condition = Condition.objects.create(
                range=form.cleaned_data["benefit_range"], type=Condition.COUNT, value=1
            )
            benefit = Benefit.objects.create(
                range=form.cleaned_data["benefit_range"],
                type=form.cleaned_data["benefit_type"],
                value=form.cleaned_data["benefit_value"],
                minimum_payment_value=form.cleaned_data["benefit_minimum_payment_value"]
                or 0,
            )
            name = form.cleaned_data["name"]
            offer, __ = ConditionalOffer.objects.update_or_create(
                name=get_offer_name(name),
                defaults=dict(
                    offer_type=ConditionalOffer.VOUCHER,
                    benefit=benefit,
                    condition=condition,
                ),
            )
            voucherset.offer = offer
            for voucher in voucherset.vouchers.all():
                if offer not in voucher.offers.all():
                    voucher.offers.add(offer)

        else:
            benefit = voucherset.offer.benefit
            benefit.range = form.cleaned_data["benefit_range"]
            benefit.type = form.cleaned_data["benefit_type"]
            benefit.value = form.cleaned_data["benefit_value"]
            benefit.minimum_payment_value = (
                form.cleaned_data["benefit_minimum_payment_value"] or 0
            )
            benefit.save()
            condition = voucherset.offer.condition
            condition.range = form.cleaned_data["benefit_range"]
            condition.save()
        voucherset.save()

        return HttpResponseRedirect(self.get_success_url())
