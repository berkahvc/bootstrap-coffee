from django import forms
from django.utils.translation import gettext_lazy as _
from oscar.apps.dashboard.vouchers.forms import VoucherForm as CoreVoucherForm
from oscar.apps.dashboard.vouchers.forms import VoucherSetForm as CoreVoucherSetForm
from oscar.core.loading import get_model
from oscar.forms import widgets

VoucherSet = get_model("voucher", "VoucherSet")


class VoucherForm(CoreVoucherForm):
    benefit_minimum_payment_value = forms.DecimalField(
        label=_("Minimum payment value"), required=False
    )
    add_free_shipping = forms.BooleanField(required=False)

    def clean_benefit_minimum_payment_value(self):
        benefit_minimum_payment_value = (
            self.cleaned_data["benefit_minimum_payment_value"] or 0
        )
        benefit_type = self.cleaned_data["benefit_type"].strip()
        if benefit_type == "Minimum Pay" and benefit_minimum_payment_value == 0:
            raise forms.ValidationError(_("Please enter Minimum Payment Value"))
        return benefit_minimum_payment_value


class VoucherSetForm(CoreVoucherSetForm):
    benefit_minimum_payment_value = forms.DecimalField(
        label=_("Minimum payment value"), required=False
    )
    voucher_prefix = forms.CharField(label=_("Prefix"), required=False)
    voucher_suffix = forms.CharField(label=_("Suffix"), required=False)
    add_free_shipping = forms.BooleanField(required=False)

    class Meta:
        model = VoucherSet
        fields = [
            "name",
            "code_length",
            "description",
            "start_datetime",
            "end_datetime",
            "count",
            "add_free_shipping",
        ]
        widgets = {
            "start_datetime": widgets.DateTimePickerInput(),
            "end_datetime": widgets.DateTimePickerInput(),
        }

    def clean_benefit_minimum_payment_value(self):
        benefit_minimum_payment_value = (
            self.cleaned_data["benefit_minimum_payment_value"] or 0
        )
        benefit_type = self.cleaned_data["benefit_type"].strip()
        if benefit_type == "Minimum Pay" and benefit_minimum_payment_value == 0:
            raise forms.ValidationError(_("Please enter Minimum Payment Value"))
        return benefit_minimum_payment_value
