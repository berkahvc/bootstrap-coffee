from django import forms
from oscar.core.loading import get_model
from oscar.forms import widgets

PreparationTime = get_model("schedule_management", "PreparationTime")
Holiday = get_model("schedule_management", "Holiday")
DeliveryTime = get_model("schedule_management", "DeliveryTime")
Category = get_model("catalogue", "category")


class PreparationTimeForm(forms.ModelForm):
    cutoff = forms.TimeField(required=False, widget=widgets.TimePickerInput())
    category = forms.ModelChoiceField(queryset=Category.objects.filter(is_public=True))

    class Meta:
        model = PreparationTime
        fields = ["category", "day", "prepTime", "cutoff"]


class DeliveryTimeForm(forms.ModelForm):
    days = forms.MultipleChoiceField(
        required=True,
        choices=DeliveryTime.DAYS_CHOICES,
    )

    class Meta:
        model = DeliveryTime
        fields = ["start", "end", "days", "icon", "is_active"]


class HolidayForm(forms.ModelForm):
    date = forms.DateField(required=False, widget=widgets.DatePickerInput())

    class Meta:
        model = Holiday
        fields = ["date"]
