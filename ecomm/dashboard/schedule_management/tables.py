from django.utils.translation import gettext_lazy as _
from django.utils.translation import ngettext_lazy
from django_tables2 import TemplateColumn
from oscar.core.loading import get_class, get_model

DashboardTable = get_class("dashboard.tables", "DashboardTable")

# models
PreparationTime = get_model("schedule_management", "PreparationTime")
DeliveryTime = get_model("schedule_management", "DeliveryTime")
Holiday = get_model("schedule_management", "Holiday")


class PreparationTimeTable(DashboardTable):
    actions = TemplateColumn(
        verbose_name=_("Actions"),
        template_name="dashboard/schedule_management/preparation_time_row_actions.html",
        orderable=False,
    )

    icon = "sitemap"
    caption = ngettext_lazy("%s Preparation Time", "%s Preparation Times")

    class Meta(DashboardTable.Meta):
        model = PreparationTime
        fields = ("category", "day", "prepTime", "cutoff")


class DeliveryTimeTable(DashboardTable):
    actions = TemplateColumn(
        verbose_name=_("Actions"),
        template_name="dashboard/schedule_management/delivery_time_row_actions.html",
        orderable=False,
    )

    icon = "sitemap"
    caption = ngettext_lazy("%s Delivery Time", "%s Delivery Times")

    class Meta(DashboardTable.Meta):
        model = DeliveryTime
        fields = ("start", "end", "days", "is_active")


class HolidayTable(DashboardTable):
    actions = TemplateColumn(
        verbose_name=_("Actions"),
        template_name="dashboard/schedule_management/holiday_row_actions.html",
        orderable=False,
    )

    icon = "sitemap"
    caption = ngettext_lazy("%s Holiday", "%s Holidays")

    class Meta(DashboardTable.Meta):
        model = Holiday
        fields = ("date",)
