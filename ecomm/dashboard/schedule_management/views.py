from django.conf import settings
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView, DeleteView, UpdateView
from django_tables2 import SingleTableView
from oscar.core.loading import get_model

from .forms import DeliveryTimeForm, HolidayForm, PreparationTimeForm
from .tables import DeliveryTimeTable, HolidayTable, PreparationTimeTable

PreparationTime = get_model("schedule_management", "PreparationTime")
Holiday = get_model("schedule_management", "Holiday")
DeliveryTime = get_model("schedule_management", "DeliveryTime")


class PreparationTimeListView(SingleTableView):
    template_name = "dashboard/schedule_management/index.html"
    model = PreparationTime
    context_table_name = "preparation_times"
    table_class = PreparationTimeTable

    def get_table_pagination(self, table):
        return dict(per_page=settings.OSCAR_DASHBOARD_ITEMS_PER_PAGE)


class PreparationTimeCreateView(CreateView):
    template_name = "dashboard/schedule_management/preparation_time_update_create.html"
    model = PreparationTime
    form_class = PreparationTimeForm
    success_url = reverse_lazy("dashboard:preparation-times-list")

    def get_context_data(self, **kwargs):
        ctx = super(PreparationTimeCreateView, self).get_context_data(**kwargs)
        ctx.update({"title": _("Add a new preparation time")})

        return ctx


class PreparationTimeUpdateView(UpdateView):
    template_name = "dashboard/schedule_management/preparation_time_update_create.html"
    model = PreparationTime
    form_class = PreparationTimeForm
    success_url = reverse_lazy("dashboard:preparation-times-list")

    def get_context_data(self, **kwargs):
        ctx = super(PreparationTimeUpdateView, self).get_context_data(**kwargs)
        ctx.update({"title": _("Update preparation time")})

        return ctx


class PreparationTimeDeleteView(DeleteView):
    template_name = "dashboard/schedule_management/preparation_time_delete.html"
    model = PreparationTime
    success_url = reverse_lazy("dashboard:preparation-times-list")


class DeliveryTimeListView(SingleTableView):
    template_name = "dashboard/schedule_management/delivery_time_index.html"
    model = DeliveryTime
    context_table_name = "delivery_times"
    table_class = DeliveryTimeTable

    def get_table_pagination(self, table):
        return dict(per_page=settings.OSCAR_DASHBOARD_ITEMS_PER_PAGE)


class DeliveryTimeCreateView(CreateView):
    template_name = "dashboard/schedule_management/delivery_time_update_create.html"
    model = DeliveryTime
    form_class = DeliveryTimeForm
    success_url = reverse_lazy("dashboard:delivery-times-list")

    def get_context_data(self, **kwargs):
        ctx = super(DeliveryTimeCreateView, self).get_context_data(**kwargs)
        ctx.update({"title": _("Add a new delivery time")})

        return ctx


class DeliveryTimeUpdateView(UpdateView):
    template_name = "dashboard/schedule_management/delivery_time_update_create.html"
    model = DeliveryTime
    form_class = DeliveryTimeForm
    success_url = reverse_lazy("dashboard:delivery-times-list")

    def get_context_data(self, **kwargs):
        ctx = super(DeliveryTimeUpdateView, self).get_context_data(**kwargs)
        ctx.update({"title": _("Update delivery time")})

        return ctx


class DeliveryTimeDeleteView(DeleteView):
    template_name = "dashboard/schedule_management/delivery_time_delete.html"
    model = DeliveryTime
    success_url = reverse_lazy("dashboard:delivery-times-list")


class HolidayListView(SingleTableView):
    template_name = "dashboard/schedule_management/holiday_index.html"
    model = Holiday
    context_table_name = "holidays"
    table_class = HolidayTable

    def get_table_pagination(self, table):
        return dict(per_page=settings.OSCAR_DASHBOARD_ITEMS_PER_PAGE)


class HolidayCreateView(CreateView):
    template_name = "dashboard/schedule_management/holiday_update_create.html"
    model = Holiday
    form_class = HolidayForm
    success_url = reverse_lazy("dashboard:holidays-list")

    def get_context_data(self, **kwargs):
        ctx = super(HolidayCreateView, self).get_context_data(**kwargs)
        ctx.update({"title": _("Add a new holiday")})

        return ctx


class HolidayUpdateView(UpdateView):
    template_name = "dashboard/schedule_management/holiday_update_create.html"
    model = Holiday
    form_class = HolidayForm
    success_url = reverse_lazy("dashboard:holidays-list")

    def get_context_data(self, **kwargs):
        ctx = super(HolidayUpdateView, self).get_context_data(**kwargs)
        ctx.update({"title": _("Update holiday")})

        return ctx


class HolidayDeleteView(DeleteView):
    template_name = "dashboard/schedule_management/holiday_delete.html"
    model = Holiday
    success_url = reverse_lazy("dashboard:holidays-list")
