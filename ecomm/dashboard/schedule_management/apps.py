from django.apps import AppConfig


class ScheduleManagementDashboardConfig(AppConfig):
    name = 'ecomm.dashboard.schedule_management'
