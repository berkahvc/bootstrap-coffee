from django.apps import AppConfig


class SubscriptionManagementDashboardConfig(AppConfig):
    name = "ecomm.dashboard.subscription_management"
