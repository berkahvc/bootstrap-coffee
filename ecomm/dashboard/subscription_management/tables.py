from django.utils.translation import gettext_lazy as _
from django.utils.translation import ngettext_lazy
from django_tables2 import TemplateColumn
from oscar.core.loading import get_class, get_model

DashboardTable = get_class("dashboard.tables", "DashboardTable")

DeliveryFrequency = get_model("subscription_management", "DeliveryFrequency")
Prepaid = get_model("subscription_management", "Prepaid")


class DeliveryFrequencyTable(DashboardTable):
    actions = TemplateColumn(
        verbose_name=_("Actions"),
        template_name="dashboard/subscription_management/delivery_frequency_row_actions.html",
        orderable=False,
    )

    icon = "sitemap"
    caption = ngettext_lazy("%s Delivery Frequency", "%s Delivery Frequencies")

    class Meta(DashboardTable.Meta):
        model = DeliveryFrequency
        fields = ("name",)


class PrepaidTable(DashboardTable):
    actions = TemplateColumn(
        verbose_name=_("Actions"),
        template_name="dashboard/subscription_management/prepaid/row_actions.html",
        orderable=False,
    )

    icon = "sitemap"
    caption = ngettext_lazy("%s Prepaid", "%s Prepaid")

    class Meta(DashboardTable.Meta):
        model = Prepaid
        fields = ("number_of_delivery", "discount", "price")
