from django.conf import settings
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView, DeleteView, UpdateView
from django_tables2 import SingleTableView
from oscar.core.loading import get_model

from .forms import DeliveryFrequencyForm, PrepaidForm
from .tables import DeliveryFrequencyTable, PrepaidTable

DeliveryFrequency = get_model("subscription_management", "DeliveryFrequency")
Prepaid = get_model("subscription_management", "Prepaid")


class DeliveryFrequencyListView(SingleTableView):
    template_name = "dashboard/subscription_management/delivery_frequency_index.html"
    model = DeliveryFrequency
    context_table_name = "delivery_frequencies"
    table_class = DeliveryFrequencyTable

    def get_table_pagination(self, table):
        return dict(per_page=settings.OSCAR_DASHBOARD_ITEMS_PER_PAGE)


class DeliveryFrequencyCreateView(CreateView):
    template_name = (
        "dashboard/subscription_management/delivery_frequency_update_create.html"
    )
    model = DeliveryFrequency
    form_class = DeliveryFrequencyForm
    success_url = reverse_lazy("dashboard:delivery-frequencies-list")

    def get_context_data(self, **kwargs):
        ctx = super(DeliveryFrequencyCreateView, self).get_context_data(**kwargs)
        ctx.update({"title": _("Add a new delivery frequency")})

        return ctx


class DeliveryFrequencyUpdateView(UpdateView):
    template_name = (
        "dashboard/subscription_management/delivery_frequency_update_create.html"
    )
    model = DeliveryFrequency
    form_class = DeliveryFrequencyForm
    success_url = reverse_lazy("dashboard:delivery-frequencies-list")

    def get_context_data(self, **kwargs):
        ctx = super(DeliveryFrequencyUpdateView, self).get_context_data(**kwargs)
        ctx.update({"title": _(f"Update delivery frequency - {self.object.name}")})

        return ctx


class DeliveryFrequencyDeleteView(DeleteView):
    template_name = "dashboard/subscription_management/delivery_frequency_delete.html"
    model = DeliveryFrequency
    success_url = reverse_lazy("dashboard:delivery-frequencies-list")


class PrepaidListView(SingleTableView):
    template_name = "dashboard/subscription_management/prepaid/index.html"
    model = Prepaid
    table_class = PrepaidTable
    context_table_name = "object_list"

    def get_table_pagination(self, table):
        return dict(per_page=settings.OSCAR_DASHBOARD_ITEMS_PER_PAGE)


class PrepaidCreateView(CreateView):
    template_name = "dashboard/subscription_management/prepaid/update_create.html"
    model = Prepaid
    form_class = PrepaidForm
    success_url = reverse_lazy("dashboard:prepaid-list")

    def get_context_data(self, **kwargs):
        ctx = super(PrepaidCreateView, self).get_context_data(**kwargs)
        ctx.update({"title": _("Add a new prepaid")})

        return ctx


class PrepaidUpdateView(UpdateView):
    template_name = "dashboard/subscription_management/prepaid/update_create.html"
    model = Prepaid
    form_class = PrepaidForm
    success_url = reverse_lazy("dashboard:prepaid-list")

    def get_context_data(self, **kwargs):
        ctx = super(PrepaidUpdateView, self).get_context_data(**kwargs)
        ctx.update({"title": _("Update prepaid")})

        return ctx


class PrepaidDeleteView(DeleteView):
    template_name = "dashboard/subscription_management/prepaid/delete.html"
    model = Prepaid
    success_url = reverse_lazy("dashboard:prepaid-list")
