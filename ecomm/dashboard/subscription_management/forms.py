from django import forms
from oscar.core.loading import get_model

DeliveryFrequency = get_model("subscription_management", "DeliveryFrequency")
Prepaid = get_model("subscription_management", "Prepaid")


class DeliveryFrequencyForm(forms.ModelForm):
    class Meta:
        model = DeliveryFrequency
        fields = ["name", "value"]


class PrepaidForm(forms.ModelForm):
    class Meta:
        model = Prepaid
        fields = ("number_of_delivery", "discount", "price")
