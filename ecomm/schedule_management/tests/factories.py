from factory import DjangoModelFactory, SubFactory

from ecomm.catalogue.models import Category
from ecomm.schedule_management.models import PreparationTime


class CategoryFactory(DjangoModelFactory):
    class Meta:
        model = Category
        django_get_or_create = ["name"]

    name = "Coffee"
    depth = 1


class PreparationTimeFactory(DjangoModelFactory):
    class Meta:
        model = PreparationTime
        django_get_or_create = ["day"]

    category = SubFactory(CategoryFactory)
