import pytest
from django.urls import reverse
from django.utils.timezone import localtime, timedelta
from freezegun import freeze_time

from .factories import CategoryFactory, PreparationTimeFactory


class TestDeliveryAvailability:
    pytestmark = pytest.mark.django_db

    @pytest.fixture
    def category(self):
        return CategoryFactory()

    def test_get_start_delivery_available_without_cutoff(self, client, category):
        today = localtime().today()
        pt = PreparationTimeFactory(day=today.strftime("%A").upper())
        data = {"category_ids": f"{category.id}"}
        resp = client.post(
            reverse("custom_checkout:delivery_availability"),
            data=data,
            **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"},
        )

        resp_json = resp.json()

        str_start_date = today + timedelta(days=pt.prepTime)
        assert str_start_date.strftime("%Y-%m-%d") == resp_json["data"]

    @freeze_time("2021-06-09 15:30:34", tz_offset=+7)
    def test_get_start_delivery_available_with_cutoff(self, client, category):
        today = localtime().today()
        pt = PreparationTimeFactory(day=today.strftime("%A").upper(), cutoff="15:00")
        data = {"category_ids": f"{category.id}"}
        resp = client.post(
            reverse("custom_checkout:delivery_availability"),
            data=data,
            **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"},
        )

        resp_json = resp.json()

        str_start_date = today + timedelta(days=pt.prepTime + 1)
        assert str_start_date.strftime("%Y-%m-%d") == resp_json["data"]
