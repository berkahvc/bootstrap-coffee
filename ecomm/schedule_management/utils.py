from datetime import datetime

from django.db.models import CharField, F, Func, Value
from django.utils.timezone import localtime, timedelta
from oscar.core.loading import get_model

from ecomm.schedule_management.models import Holiday, PreparationTime

Category = get_model("catalogue", "Category")


def check_delivery_date_validation(lines):
    """
    :param lines: basket lines
    :return: Date Object
    """
    categories = []
    for line in lines:
        if line.is_subscription:
            categories += Category.objects.filter(slug="coffee")
        else:
            categories += line.product.get_categories().all()

    start_date = get_start_available_day(categories)
    return datetime.strptime(start_date, "%Y-%m-%d").date()


def get_start_available_day(categories):
    days = []
    now = localtime()
    today = now.date()
    time = now.time()

    for category in categories:
        preparation_times = PreparationTime.objects.filter(
            category=category, day=today.strftime("%A").upper()
        )

        if preparation_times.exists():
            pt = preparation_times.first()
            if pt.cutoff:
                if time > pt.cutoff:
                    days.append(pt.prepTime + 1)
                else:
                    days.append(pt.prepTime)
    if len(days) > 0:
        return (today + timedelta(days=max(days))).strftime("%Y-%m-%d")
    return (today + timedelta(days=1)).strftime("%Y-%m-%d")


def get_holidays():
    return list(
        Holiday.objects.annotate(
            str_date=Func(
                F("date"),
                Value("MM/dd/yyyy"),
                function="to_char",
                output_field=CharField(),
            )
        ).values_list("str_date", flat=True)
    )
