from django.apps import AppConfig


class ScheduleManagementConfig(AppConfig):
    name = 'ecomm.schedule_management'
