from braces.views import AjaxResponseMixin
from django.http import JsonResponse
from django.views.generic import ListView

from ecomm.catalogue.models import Category

from .models import Holiday, PreparationTime
from .utils import get_holidays, get_start_available_day


class DeliveryAvailabilityView(AjaxResponseMixin, ListView):
    model = PreparationTime

    def post_ajax(self, request, *args, **kwargs):
        category_ids = self.request.POST.get("category_ids")
        categories = Category.objects.filter(id__in=category_ids.split(","))

        resp = {"data": get_start_available_day(categories)}

        return JsonResponse(resp)


class HolidaysView(AjaxResponseMixin, ListView):
    model = Holiday

    def get_ajax(self, request, *args, **kwargs):
        resp = {"data": get_holidays()}

        return JsonResponse(resp)
