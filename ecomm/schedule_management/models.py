from datetime import datetime

from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils.translation import gettext_lazy as _
from model_utils import Choices

from ecomm.catalogue.models import Category
from ecomm.timestamp_models import TimestampModel

from .enums import Day as EnumDay


class PreparationTime(TimestampModel):
    class Day(models.TextChoices):
        SUNDAY = "SUNDAY", _("SUNDAY")
        MONDAY = "MONDAY", _("MONDAY")
        TUESDAY = "TUESDAY", _("TUESDAY")
        WEDNESDAY = "WEDNESDAY", _("WEDNESDAY")
        THURSDAY = "THURSDAY", _("THURSDAY")
        FRIDAY = "FRIDAY", _("FRIDAY")
        SATURDAY = "SATURDAY", _("SATURDAY")

    category = models.ForeignKey(Category, null=True, on_delete=models.CASCADE)
    day = models.CharField(
        max_length=255, null=True, db_index=True, choices=Day.choices
    )
    active = models.BooleanField(default=True)
    prepTime = models.IntegerField(
        default=1, null=True, db_index=True, help_text="In days"
    )
    cutoff = models.TimeField(null=True, blank=True, db_index=True)

    def __str__(self):
        return f"{self.day}"

    def to_dict(self):
        week_days = [day[0] for day in PreparationTime.Day.choices]
        return {
            "day_index": week_days.index(self.day),
            "day": self.day,
            "prepTime": self.prepTime,
            "cutoff": self.cutoff.strftime("%H:%M") if self.cutoff else None,
        }


class DeliveryTime(TimestampModel):
    DAYS_CHOICES = Choices(
        (EnumDay.SUNDAY.value, EnumDay.SUNDAY.value),
        (EnumDay.MONDAY.value, EnumDay.MONDAY.value),
        (EnumDay.TUESDAY.value, EnumDay.TUESDAY.value),
        (EnumDay.WEDNESDAY.value, EnumDay.WEDNESDAY.value),
        (EnumDay.THURSDAY.value, EnumDay.THURSDAY.value),
        (EnumDay.FRIDAY.value, EnumDay.FRIDAY.value),
        (EnumDay.SATURDAY.value, EnumDay.SATURDAY.value),
    )

    start = models.CharField(max_length=255, db_index=True)
    end = models.CharField(max_length=255, db_index=True)
    days = ArrayField(
        models.CharField(
            max_length=255, null=True, blank=True, db_index=True, choices=DAYS_CHOICES
        ),
        default=list,
        blank=True,
    )
    icon = models.ImageField(upload_to="delivery_times", null=True)
    is_active = models.BooleanField(default=False, null=True)

    class Meta:
        ordering = ["start"]

    def __str__(self):
        return f"{self.start} - {self.end}"

    def start_time(self):
        if "pm" in self.start.lower() or "am" in self.start.lower():
            return f"{datetime.strptime(self.start, '%H:%M %p').strftime('%H:%M')}"
        return f"{self.start.strip()}"

    def end_time(self):
        if "pm" in self.end.lower() or "am" in self.end.lower():
            return f"{datetime.strptime(self.end, '%H:%M %p').strftime('%H:%M')}"
        return f"{self.end.strip()}"

    def icon_name(self):
        return self.icon.name.split("/")[1].split(".")[0]


class Holiday(TimestampModel):
    date = models.DateField(null=True)
