from django.views.generic import ListView

from ecomm.catalogue.models import Product


class SearchProductView(ListView):
    model = Product
    context_object_name = "products"
    template_name = "oscar/search/results.html"
    paginate_by = 20

    def get_queryset(self):
        q = self.request.GET.get("q", None)
        sort_by = self.request.GET.get("sort_by", None)

        queryset = super(SearchProductView, self).get_queryset()

        queryset = queryset.user_search_filter(
            q=q,
            sort=sort_by,
        )

        return queryset
