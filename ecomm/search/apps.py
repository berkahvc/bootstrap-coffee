import oscar.apps.search.apps as apps
from django.conf.urls import url
from oscar.core.loading import get_class


class SearchConfig(apps.SearchConfig):
    name = "ecomm.search"

    def ready(self):
        self.search_view = get_class("search.views", "SearchProductView")

    def get_urls(self):
        # The form class has to be passed to the __init__ method as that is how
        # Haystack works.  It's slightly different to normal CBVs.
        urlpatterns = [
            url(r"^$", self.search_view.as_view(), name="search"),
        ]

        return self.post_process_urls(urlpatterns)
