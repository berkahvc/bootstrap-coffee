from decimal import Decimal as D

from oscar.apps.offer.benefits import (
    ZERO_DISCOUNT,
    AbsoluteDiscountBenefit,
    BasketDiscount,
    PercentageDiscountBenefit,
    ShippingFixedPriceBenefit,
    apply_discount,
)


class CustomShippingFixedPriceBenefit(ShippingFixedPriceBenefit):
    def shipping_discount(self, charge):
        if not charge:
            return self.value
        if charge < self.value:
            return D("0.00")
        return charge - self.value


class CustomPercentageDiscountBenefit(PercentageDiscountBenefit):
    def apply(
        self,
        basket,
        condition,
        offer,
        discount_percent=None,
        max_total_discount=None,
        **kwargs,
    ):
        if discount_percent is None:
            discount_percent = self.value

        discount_amount_available = max_total_discount

        line_tuples = self.get_applicable_lines(offer, basket)
        discount_percent = min(discount_percent, D("100.0"))
        discount = D("0.00")
        affected_items = 0
        max_affected_items = self._effective_max_affected_items()
        affected_lines = []

        is_minimum_payment = True
        if self.minimum_payment_value > 0:
            affected_items_total = D("0.00")
            for price, line in line_tuples:
                quantity_affected = min(
                    line.quantity_without_offer_discount(offer),
                    max_affected_items - affected_items,
                )
                if quantity_affected <= 0:
                    break

                affected_items_total += quantity_affected * price

            if affected_items_total < self.minimum_payment_value:
                is_minimum_payment = False

        if not is_minimum_payment:
            return ZERO_DISCOUNT

        for price, line in line_tuples:
            if affected_items >= max_affected_items:
                break
            if discount_amount_available == 0:
                break

            quantity_affected = min(
                line.quantity_without_offer_discount(offer),
                max_affected_items - affected_items,
            )
            if quantity_affected <= 0:
                break

            line_discount = self.round(
                discount_percent / D("100.0") * price * int(quantity_affected)
            )

            if discount_amount_available is not None:
                line_discount = min(line_discount, discount_amount_available)
                discount_amount_available -= line_discount

            apply_discount(line, line_discount, quantity_affected, offer)

            affected_lines.append((line, line_discount, quantity_affected))
            affected_items += quantity_affected
            discount += line_discount

        return BasketDiscount(discount)


class MinimumPaymentAmountBenefit(AbsoluteDiscountBenefit):
    def apply(
        self,
        basket,
        condition,
        offer,
        discount_amount=None,
        max_total_discount=None,
        **kwargs,
    ):
        if discount_amount is None:
            discount_amount = self.value

        # Fetch basket lines that are in the range and available to be used in
        # an offer.
        line_tuples = self.get_applicable_lines(offer, basket)

        # Determine which lines can have the discount applied to them
        max_affected_items = self._effective_max_affected_items()
        num_affected_items = 0
        affected_items_total = D("0.00")
        lines_to_discount = []
        for price, line in line_tuples:
            if num_affected_items >= max_affected_items:
                break
            qty = min(
                line.quantity_without_offer_discount(offer),
                max_affected_items - num_affected_items,
            )
            lines_to_discount.append((line, price, qty))
            num_affected_items += qty
            affected_items_total += qty * price

        # Ensure we don't try to apply a discount larger than the total of the
        # matching items.

        discount = min(discount_amount, affected_items_total)
        if max_total_discount is not None:
            discount = min(discount, max_total_discount)

        if affected_items_total < self.minimum_payment_value:
            discount = 0

        if discount == 0:
            return ZERO_DISCOUNT

        # XXX: spreading the discount is a policy decision that may not apply

        # Apply discount equally amongst them
        affected_lines = []
        applied_discount = D("0.00")
        last_line_idx = len(lines_to_discount) - 1
        for i, (line, price, qty) in enumerate(lines_to_discount):
            if i == last_line_idx:
                # If last line, then take the delta as the discount to ensure
                # the total discount is correct and doesn't mismatch due to
                # rounding.
                line_discount = discount - applied_discount
            else:
                # Calculate a weighted discount for the line
                line_discount = self.round(
                    ((price * qty) / affected_items_total) * discount
                )
            apply_discount(line, line_discount, qty, offer)
            affected_lines.append((line, line_discount, qty))
            applied_discount += line_discount

        return BasketDiscount(discount)
