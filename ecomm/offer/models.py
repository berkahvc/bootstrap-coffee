from django.db import models
from django.utils.translation import gettext_lazy as _
from oscar.apps.offer.abstract_models import AbstractBenefit
from oscar.core.loading import get_class


class Benefit(AbstractBenefit):
    MINIMUM_PAY = "Minimum Pay"
    CUSTOM_TYPE_CHOICES = (
        (MINIMUM_PAY, _("Discount is a Minimum Payment amount off products")),
    )
    TYPE_CHOICES = AbstractBenefit.TYPE_CHOICES + CUSTOM_TYPE_CHOICES

    minimum_payment_value = models.PositiveIntegerField(
        _("Minimum payment value"), default=0
    )

    @property
    def proxy_map(self):
        custom_proxy_map = super().proxy_map
        custom_proxy_map[self.PERCENTAGE] = get_class(
            "offer.benefits", "CustomPercentageDiscountBenefit"
        )
        custom_proxy_map[self.SHIPPING_FIXED_PRICE] = get_class(
            "offer.benefits", "CustomShippingFixedPriceBenefit"
        )
        custom_proxy_map[self.MINIMUM_PAY] = get_class(
            "offer.benefits", "MinimumPaymentAmountBenefit"
        )
        return custom_proxy_map


from oscar.apps.offer.models import *  # noqa isort:skip
