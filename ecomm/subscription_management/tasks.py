import logging

import pytz
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.timezone import localdate
from oscar.core.loading import get_model

from config import celery_app
from ecomm.checkout.tasks import send_email_with_rate_limit

from .models import SubscriptionPaymentFailed, SubscriptionPrepaid

Order = get_model("order", "Order")

logger = logging.getLogger(__name__)

from_email = "info@bootstrapbeverages.com"


@celery_app.task()
def send_paused_indefinitely_email_confirmation(**kwargs):
    subscription_prepaid_id = kwargs.get("subscription_prepaid_id")
    subscription_prepaid = SubscriptionPrepaid.objects.get(id=subscription_prepaid_id)

    context = {"full_name": subscription_prepaid.user.get_full_name()}
    template_prefix = "subscription/email/send_paused_indefinitely_email_confirmation"
    subject = render_to_string(
        "{0}_subject.txt".format(template_prefix), context
    ).strip()

    template_name = "{0}_message.html".format(template_prefix)
    body = render_to_string(template_name, context).strip()

    msg = EmailMessage(
        subject, body, from_email=from_email, to=[subscription_prepaid.user.username]
    )
    msg.content_subtype = "html"
    msg.send()


@celery_app.task()
def send_cancel_subscription_email_confirmation(**kwargs):
    subscription_prepaid_id = kwargs.get("subscription_prepaid_id")
    subscription_prepaid = SubscriptionPrepaid.objects.get(id=subscription_prepaid_id)

    context = {"full_name": subscription_prepaid.user.get_full_name()}
    template_prefix = "subscription/email/send_cancel_subscription_email_confirmation"
    subject = render_to_string(
        "{0}_subject.txt".format(template_prefix), context
    ).strip()

    template_name = "{0}_message.html".format(template_prefix)
    body = render_to_string(template_name, context).strip()

    msg = EmailMessage(
        subject, body, from_email=from_email, to=[subscription_prepaid.user.username]
    )
    msg.content_subtype = "html"
    msg.send()


@celery_app.task()
def send_subscription_payment_failed_to_admin() -> None:
    """
    Function to send the list of failed subscription payments to the admin.

    The feature still needs to be added to periodic tasks.
    """
    tz = pytz.timezone("Asia/Singapore")
    today = localdate(timezone=tz)

    subscription_payment_failed_list = SubscriptionPaymentFailed.objects.filter(
        date=today
    )

    if subscription_payment_failed_list.count() > 0:
        payment_failed_list = []
        for subscription_payment_failed in subscription_payment_failed_list:
            subscription_prepaid = SubscriptionPrepaid.objects.get(
                djsubscription_id=subscription_payment_failed.subscription.id
            )
            phone_number = "-"
            if subscription_prepaid.shipping_address:
                phone_number = subscription_prepaid.shipping_address.phone_number

            payment_failed_list.append(
                f"Customer Name: {subscription_payment_failed.customer.subscriber.get_full_name()}\n"
                f"Customer Email: {subscription_payment_failed.customer.subscriber.email}\n"
                f"Shipping phone number: {phone_number}\n"
                f"Reason for failure: {subscription_payment_failed.message}"
            )

        subject = f"Subscription Payment Failures for {today}"
        send_email = [
            "info@bootstrapbeverages.com",
            "william@berkah.vc",
        ]
        body = f"""<p>List of failed subscription:</p>
        <p>{'<br><br>'.join(payment_failed_list)}</p>
        <br>

        <p>--<br>
        Best Regards & Thanks,<br>
        <br>
        Team Bootstrap</p>
        <img src="https://storage.googleapis.com/berkah-bootstrap/static/images/boot/bootstrap-beverages-logo.png"
        alt="bootstrap-logo">
        """

        for email in send_email:
            send_email_with_rate_limit.delay(
                subject=subject, to=email, from_email=from_email, body=body
            )


@celery_app.task()
def send_subscription_payment_failed_to_customer(**kwargs):
    """
    Function to send the list of failed subscription payments to the customer.

    The feature still needs to be added to periodic tasks.

    :param kwargs: kwargs should be: {"email": "", "full_name": ""}

    """
    email = kwargs.get("email")
    full_name = kwargs.get("full_name")
    link = "https://bootstrapbeverages.com/accounts/profile/"

    context = {
        "link": link,
        "full_name": full_name,
    }
    subject = "Your Bootstrap Subscription Renewal Failed. Update to Prevent Disruption to Your Mornings."
    send_email = [email]
    body = render_to_string(
        "subscription/email/customer_subscription_renewal_problem.html",
        context=context,
    )

    for email in send_email:
        send_email_with_rate_limit.delay(
            subject=subject, to=email, from_email=from_email, body=body
        )


def send_resubscribe_email_confirmation(**kwargs):
    subscription_prepaid_id = kwargs.get("subscription_prepaid_id")
    order_id = kwargs.get("order_id")

    order = Order.objects.get(id=order_id)
    subscription_prepaid = SubscriptionPrepaid.objects.get(id=subscription_prepaid_id)

    context = {"full_name": subscription_prepaid.user.get_full_name(), "order": order}
    template_prefix = "subscription/email/send_resubscribe"
    subject = render_to_string(
        "{0}_subject.txt".format(template_prefix), context
    ).strip()

    template_name = "{0}_message.html".format(template_prefix)
    body = render_to_string(template_name, context).strip()

    msg = EmailMessage(
        subject, body, from_email=from_email, to=[subscription_prepaid.user.username]
    )
    msg.content_subtype = "html"
    msg.send()


@celery_app.task()
def send_test_email_for_subscription_failed():
    """
    Testing subscription failed email.

    """

    send_subscription_payment_failed_to_customer.delay(
        email="sigit.aryo@lodi.id", full_name="Sigit Aryo"
    )
