import logging
from datetime import datetime, time

import stripe
from django.db import transaction
from django.utils.crypto import get_random_string
from django.utils.timezone import localdate, timedelta
from djstripe.models import Card, Customer, PaymentMethod, Plan
from djstripe.models import Product as djstripe_Product
from djstripe.models import Subscription

from ecomm.catalogue.models import Category, Product, ProductCategory, ProductClass
from ecomm.order.models import Order
from ecomm.partner.models import Partner, StockRecord
from ecomm.schedule_management.models import DeliveryTime
from ecomm.utils.utils import calculate_next_billing_date

from .models import SubscriptionPrepaid

logger = logging.getLogger(__name__)


def create_product_subscription(username="", description="", price=0, title=None):
    """
    Create a product for subscriptions by combining using the username
    and description to create a unique product name.

    :param title: the title of product
    :param username: the email/username of the user
    :param description: the content of the subscription
    :param price: the price of the subscription
    :return: Product object
    """

    default_title = (
        f"{username}-subscription"
        if username
        else f"{get_random_string(7).lower()}-subscription"
    )

    if title:
        default_title = title

    products = Product.objects.filter(title__istartswith=default_title).order_by("-id")

    if products.exists():
        product_title = products.first().title
        split_product_title = product_title.split("-")
        if len(split_product_title) > 2:
            counter = int(split_product_title[len(split_product_title) - 1]) + 1
            default_title = (
                f"{split_product_title[0]}-{split_product_title[1]}-{counter}"
            )
        else:
            default_title = f"{default_title}-1"

    sku = get_random_string(5).upper()

    product_class, _ = ProductClass.objects.get_or_create(
        name="Subscription", track_stock=False
    )
    product = Product.objects.create(
        title=default_title, product_class=product_class, description=description
    )
    partner, _ = Partner.objects.get_or_create(name="Singapore")

    try:
        category = Category.objects.get(slug="subscription-offer")
        ProductCategory.objects.create(product=product, category=category)
    except Category.DoesNotExist:
        pass

    StockRecord.objects.create(
        product=product,
        partner=partner,
        partner_sku=sku,
        price_excl_tax=price,
    )

    return product


def calculation_new_billing_subscription(
    subscription_prepaid, subscription, new_delivery_date
):
    """
    :param subscription_prepaid: SubscriptionPrepaid Object
    :param subscription: Subscription Object
    :param new_delivery_date: New delivery date with Date format
    :return:
    """
    remaining_credits = subscription_prepaid.remaining_credits()
    new_billing_date = new_delivery_date

    if remaining_credits == 0:
        i = 0
        while new_billing_date.weekday() != 6:
            i += 1
            new_billing_date -= timedelta(days=1)

        return new_billing_date
    elif remaining_credits == 1:
        while new_billing_date.weekday() != 6:
            new_billing_date += timedelta(days=1)
    elif remaining_credits > 1:
        while new_billing_date.weekday() != 6:
            new_billing_date += timedelta(days=1)
        new_billing_date += timedelta(weeks=int(remaining_credits) - 1)
        return new_billing_date
    else:
        return subscription.current_period_end


@transaction.atomic
def reschedule_subscription(
    subscription: SubscriptionPrepaid,
    delivery_date: datetime,
    order_exist: bool = False,
    delivery_time: DeliveryTime = None,
):
    """
    Function to reschedule the subscription

    If the subscription is rescheduled while the subscription is paid,
    the existing order delivery date will be changed. If there are no orders scheduled,
    the subscription will be rescheduled to the new delivery date.

    The steps are as follows:

    1. Check if there are any orders scheduled for the subscription

    2. If there are orders, change the delivery date of the existing order

    3. If there are no orders, reschedule the next_delivery_date of the subscription to the new delivery date

    4. Change the billing on Stripe (add a trial period to adjust the billing date)

    :param subscription: SubscriptionPrepaid Object
    :param delivery_date: New delivery date with Date format
    :param order_exist: Whether there are any undelivered orders scheduled for the subscription
    :param delivery_time: New delivery date with DeliveryTime model object
    """
    if type(delivery_date) is datetime:
        delivery_date = delivery_date.date()

    if not order_exist:
        order_exist = Order.objects.filter(
            subscription_id=subscription.djsubscription_id,
            delivery_date__gte=localdate(),
        ).exists()

    charge_now = False
    if order_exist:
        order = Order.objects.filter(
            subscription_id=subscription.djsubscription_id
        ).first()
        order.delivery_date = delivery_date

        subscription.delivery_date = delivery_date
        subscription.next_delivery_date = delivery_date + timedelta(
            weeks=int(subscription.delivery_frequency)
        )

        if delivery_time:
            order.delivery_time = delivery_time
        order.save()

    else:
        subscription.next_delivery_date = delivery_date

    update_stripe_subscription(subscription, delivery_date, charge_now=charge_now)

    subscription.delivery_date = delivery_date
    if delivery_time:
        subscription.delivery_time = delivery_time.__str__()

    subscription.save()

    return charge_now


def update_stripe_product_description(
    subscription: Subscription,
    subscription_prepaid: SubscriptionPrepaid,
    delivery_frequency: int,
    delivery_day: str = None,
    delivery_time: str = None,
):
    delivery_date_str = f"<p>Delivery Day: { delivery_day if delivery_day else subscription_prepaid.delivery_day }</p>"

    delivery_time_str = (
        f"<p>Delivery Time: { delivery_time if delivery_time else subscription_prepaid.delivery_time }"
        f"</p>"
    )

    delivery_frequency_str = (
        "<p>Delivery Frequency: 1 week</p>"
        if delivery_frequency == 1
        else f"<p>Delivery Frequency: { delivery_frequency } weeks</p>"
    )

    description_combined = (
        f"<p>{ subscription_prepaid.description }</p>{ delivery_frequency_str }"
        f"{delivery_date_str}{delivery_time_str}"
    )
    stripe_product = stripe.Product.modify(
        subscription.plan.product.id, description=description_combined
    )
    return djstripe_Product.sync_from_stripe_data(stripe_product)


def update_stripe_subscription(
    subscription: SubscriptionPrepaid, delivery_date: datetime, charge_now: bool = False
):
    """
    Function to update the subscription on Stripe.

    This function will update the subscription on Stripe with the new delivery date. This function is
    already used in conjunction with reschedule_subscription

    :param charge_now:
    :param subscription:
    :param delivery_date:
    :return:
    """

    if charge_now:
        # charge now
        stripe_subscription = stripe.Subscription.modify(
            subscription.djsubscription_id,
            billing_cycle_anchor="now",
            trial_end="now",
        )
    else:
        stripe_subscription = stripe.Subscription.retrieve(
            subscription.djsubscription_id
        )

    dj_subscription = Subscription.sync_from_stripe_data(stripe_subscription)
    plan = Plan.objects.get(id=dj_subscription.plan.id)

    next_billing_date = calculate_next_billing_date(
        delivery_date=delivery_date,
        deliveries_left=subscription.deliveries_left(),
        delivery_frequency=subscription.delivery_frequency,
    )

    next_billing_date = datetime.combine(next_billing_date, time(0, 0))

    dj_subscription.update(
        plan,
        trial_end=int(next_billing_date.timestamp()),
        proration_behavior="none",
    )


def create_or_update_payment_method(
    number: str,
    exp_month: int,
    exp_year: int,
    cvc: str,
    card_holder_name: str,
    customer: Customer,
):
    """
    Update the payment method of the customer

    If customer has payment method, check if the card is the same
    If the card is the same, do nothing
    If the card is different, update the payment method
    If the customer does not have payment method, create a new card

    :param number: full card number
    :param exp_month: expiration month
    :param exp_year: expiration year
    :param cvc: cvs
    :param card_holder_name: name on the card
    :param customer: Customer model object from djstripe
    """

    card_token = Card.create_token(
        number=number,
        exp_month=exp_month,
        exp_year=exp_year,
        cvc=cvc,
        name=card_holder_name,
    )

    if customer.has_valid_source():
        card_info = card_token.get("card")
        card_object = Card.objects.get(id=customer.default_source.id)
        card_info_json = {
            "last4": card_info.get("last4"),
            "exp_month": card_info.get("exp_month"),
            "exp_year": card_info.get("exp_year"),
            "customer": customer,
        }

        card = Card.objects.filter(**card_info_json)

        # if the card is the same, do nothing
        # if the card is different, update the default payment method
        if card.exists():
            if card.first() == card_object:
                pass
            else:
                card = card.first()
                card_id = card.id
                try:
                    payment_method = PaymentMethod.objects.get(id=card_id)
                except PaymentMethod.DoesNotExist:
                    stripe_payment_method_obj = stripe.PaymentMethod.create(
                        type=card_token.type,
                        card={
                            "number": number,
                            "exp_month": exp_month,
                            "exp_year": exp_year,
                            "cvc": cvc,
                        },
                    )
                    payment_method = PaymentMethod.sync_from_stripe_data(
                        stripe_payment_method_obj
                    )

                stripe_customer = customer.api_retrieve()
                stripe_customer.default_source = card_id
                stripe_customer.save()

                customer.add_payment_method(payment_method)

        # if the card does not exist, create a new card
        else:
            new_payment_method = customer.add_card(card_token)
            customer.add_payment_method(new_payment_method)

    # if the customer have no card on file, create a new card
    else:
        new_payment_method = customer.add_card(card_token)
        customer.add_payment_method(new_payment_method)
