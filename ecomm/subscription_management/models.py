from datetime import datetime
from decimal import Decimal

import pytz
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.timezone import localdate, timedelta
from django.utils.translation import gettext_lazy as _
from djstripe.fields import StripeForeignKey
from djstripe.models import Customer, Invoice, Subscription
from oscar.apps.payment.models import Source
from oscar.core.compat import AUTH_USER_MODEL

from ecomm.order.models import Order, ShippingAddress
from ecomm.timestamp_models import TimestampModel
from ecomm.utils.utils import (
    calculate_delivery_date,
    check_product_names,
    int_day_conversion,
)


class DeliveryFrequency(TimestampModel):
    name = models.CharField(max_length=255, db_index=True)
    value = models.IntegerField(
        default=1,
        null=True,
        help_text="this is value of integer, Ex: 1 week is 1, etc.",
    )

    def __str__(self):
        return self.name


class Prepaid(TimestampModel):
    number_of_delivery = models.IntegerField(default=1)
    discount = models.IntegerField(null=True, default=0, help_text="In percent")
    price = models.DecimalField(max_digits=13, decimal_places=2)

    def __str__(self):
        return f"{self.number_of_delivery} Delivery"

    def discount_price(self):
        return round(self.price - (self.price * Decimal(self.discount / 100)))

    def per_bottle(self):
        return self.discount_price() / 6 / self.number_of_delivery


class SubscriptionPrepaid(TimestampModel):
    ACTIVE = "active"
    PAUSED = "paused"
    CANCELLED = "canceled"
    PREPAID = "prepaid"
    PAY_AS_YOU_GO = "pay_as_you_go"

    SUBSCRIPTION_TYPE_CHOICES = (
        (PREPAID, "Prepaid"),
        (PAY_AS_YOU_GO, "Pay As You Go"),
    )
    PACK_CHOICES = (
        (6, 6),
        (12, 12),
        (24, 24),
        (30, 30),
        (36, 36),
        (42, 42),
        (48, 48),
        (54, 54),
    )
    STATUS_CHOICES = (
        (ACTIVE, ACTIVE.capitalize()),
        (PAUSED, PAUSED.capitalize()),
        (CANCELLED, CANCELLED.capitalize()),
    )

    user = models.ForeignKey(
        AUTH_USER_MODEL,
        null=True,
        blank=True,
        verbose_name=_("User"),
        on_delete=models.SET_NULL,
    )
    djsubscription_id = models.CharField(max_length=255, null=True, db_index=True)
    djcustomer_id = models.CharField(max_length=255, null=True, db_index=True)
    pack_size = models.IntegerField(
        null=True, db_index=True, choices=PACK_CHOICES, default=6
    )
    description = models.TextField(null=True)
    delivery_date = models.DateField(null=True, db_index=True)
    delivery_day = models.CharField(max_length=255, null=True, db_index=True)
    delivery_time = models.CharField(max_length=255, null=True, db_index=True)
    delivery_frequency = models.IntegerField(null=True, db_index=True)
    charge_frequency = models.IntegerField(null=True, db_index=True)
    initial_credit = models.IntegerField(null=True, db_index=True)
    credit_per_delivery = models.IntegerField(null=True, db_index=True)
    next_credit_charge = models.IntegerField(null=True, db_index=True)
    next_delivery_date = models.DateField(null=True, db_index=True, blank=True)
    subscription_type = models.CharField(
        max_length=255, null=True, choices=SUBSCRIPTION_TYPE_CHOICES, default="prepaid"
    )
    note = models.TextField(null=True, blank=True)
    shipping_address = models.ForeignKey(
        ShippingAddress, on_delete=models.SET_NULL, null=True, blank=True
    )
    quantity = models.IntegerField(default=1, null=True)
    is_from_shopify = models.BooleanField(
        default=False, null=True, blank=True, db_index=True
    )
    status = models.CharField(max_length=255, default=ACTIVE, choices=STATUS_CHOICES)


    def remaining_credits(self):
        if self.subscription_type == "pay_as_you_go":
            if not self.delivery_date:
                return 0

            today = datetime.today()

            order_exist = Order.objects.filter(
                subscription_id=self.djsubscription_id,
                delivery_date__gte=today,
            ).exists()

            if order_exist:
                return 1
            return 0
        else:
            total_orders = Source.objects.filter(
                reference=self.djsubscription_id, order__delivery_date__lte=localdate()
            ).count()

            return self.initial_credit - (total_orders * self.credit_per_delivery)

    def deliveries_left(self) -> int:
        if self.remaining_credits() == 0:
            return 0
        return int(self.remaining_credits() / self.credit_per_delivery)

    def get_next_delivery_date(self):
        """
        Get the next delivery date.

        This is used on the subscription-detail.html template. It gets the latest delivery date
        from the latest generated order. If the order date has passed, then it will return the
        next delivery date based on the next_delivery_date field.

        If the next_delivery_date field is empty, then it will calculate the next delivery date.

        :return: next delivery date
        """

        subscription = Subscription.objects.get(id=self.djsubscription_id)
        if not self.delivery_date:
            next_delivery_date = calculate_delivery_date(
                self.delivery_day, subscription.current_period_end
            )
            return datetime.strptime(next_delivery_date, "%Y-%m-%d")

        if not self.next_delivery_date:
            self.next_delivery_date = self.delivery_date + timedelta(
                weeks=self.delivery_frequency
            )
            self.save()

        order = Order.objects.filter(
            subscription_id=self.djsubscription_id, delivery_date__gte=localdate()
        ).first()

        if order and order.delivery_date >= self.next_delivery_date:
            return self.next_delivery_date
        elif order and order.delivery_date <= self.next_delivery_date:
            return order.delivery_date
        else:
            return self.delivery_date

    def get_next_current_period_end(self):
        subscription = Subscription.objects.get(id=self.djsubscription_id)
        return subscription.current_period_end + timedelta(
            weeks=int(self.charge_frequency)
        )

    def get_prepaid_credit_left(self):
        if self.subscription_type == "pay_as_you_go":
            return 1
        else:
            tz = pytz.timezone("Asia/Singapore")
            subscription = Subscription.objects.get(id=self.djsubscription_id)
            total_delivery = self.charge_frequency
            current_period_end = localdate(subscription.current_period_end, timezone=tz)
            current_period_start = current_period_end - timedelta(weeks=total_delivery)
            current_period_start_int = int_day_conversion(
                current_period_start.strftime("%A")
            )
            delivery_day = self.delivery_day
            delivery_day_int = int_day_conversion(delivery_day)
            today = localdate(timezone=tz)
            today_day = today.strftime("%A")
            today_day_int = int_day_conversion(today_day)
            delta_delivery_day_int = delivery_day_int - today_day_int
            next_delivery_date = today + timedelta(days=delta_delivery_day_int)
            last_delivery_date = next_delivery_date - timedelta(
                weeks=self.delivery_frequency
            )
            delivery_date_start = current_period_start + timedelta(
                days=(delivery_day_int - current_period_start_int)
            )

            total_sent = (
                ((last_delivery_date - delivery_date_start).days / 7) + 0
                if next_delivery_date > today
                else 1
            )

            if total_sent < 0:
                total_sent = 0

            return total_delivery - int(total_sent)

    def delivery_order_sent(self):
        return self.deliveries_left() < self.initial_credit

    def __str__(self):
        return f"{ self.user.email } - { self.id }"

    def clean(self) -> None:
        # Make sure the note and/or description is valid for the subscription
        if self.note:
            product_valid = check_product_names(self.note)

            if not product_valid:
                product_valid = check_product_names(self.description)
        else:
            product_valid = check_product_names(self.description)

        if not product_valid:
            raise ValidationError(
                "Invalid product names. "
                "Make sure that the product names are in the format of {qty}x {product_name}, {qty}x {product_name}."
            )

    def is_first_time_subscription(self):
        all_subscription_prepaid = SubscriptionPrepaid.objects.filter(
            user=self.user
        ).order_by("id")
        return self.id == all_subscription_prepaid[0].id

    def is_created_in_october(self):
        return localtime(self.created_at).replace(tzinfo=None) >= datetime.strptime(
            "2022-10-01", "%Y-%m-%d"
        )


class SubscriptionCancelled(TimestampModel):
    subscription = StripeForeignKey(Subscription, on_delete=models.CASCADE)
    customer = StripeForeignKey(Customer, on_delete=models.CASCADE)
    reason = models.TextField(null=True, blank=True)


class SubscriptionPaymentFailed(TimestampModel):
    subscription = StripeForeignKey(
        Subscription, on_delete=models.CASCADE, help_text="DjStripe Subscription Object"
    )
    customer = StripeForeignKey(
        Customer,
        null=True,
        on_delete=models.SET_NULL,
        help_text="DjStripe Customer Object",
    )
    date = models.DateField(null=True, db_index=True)
    invoice = StripeForeignKey(
        Invoice,
        null=True,
        on_delete=models.SET_NULL,
        help_text="DjStripe Invoice Object",
    )
    paid = models.BooleanField(null=True, default=False)
    error_message = models.TextField(null=True, blank=True)
