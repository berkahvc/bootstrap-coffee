from django.contrib import admin
from django.shortcuts import render

from .forms import RescheduleAdminForm
from .models import (
    Prepaid,
    SubscriptionCancelled,
    SubscriptionPaymentFailed,
    SubscriptionPrepaid,
)
from ..order.utils import generate_order_manual


def make_reschedule(modeladmin, request, queryset):
    ids = list(set(queryset.values_list("pk", flat=True)))
    ctx = {
        "title": "Reschedule",
        "form": RescheduleAdminForm(
            initial={"subscription_prepaids_ids": ",".join([str(_id) for _id in ids])}
        ),
    }
    return render(request, "admin/reschedule_form.html", ctx)


make_reschedule.short_description = "Reschedule selected subscription"


def create_extra_delivery(modeladmin, request, queryset):
    """Create extra delivery using the generate_order_manual function, from order/utils.py"""
    for subscription_prepaid in queryset:
        generate_order_manual(subscription_prepaid)


create_extra_delivery.short_description = "Create extra delivery"

class SubscriptionPrepaidAdmin(admin.ModelAdmin):
    list_display = (
        "user",
        "djsubscription_id",
        "subscription_type",
        "delivery_date",
        "next_delivery_date",
        "delivery_day",
        "delivery_time",
        "delivery_frequency",
        "charge_frequency",
        "quantity",
        "pack_size",
        "note",
        "shipping_address",
    )
    list_display_links = ("user", "djsubscription_id")
    raw_id_fields = ("user", "shipping_address")
    search_fields = ("user__email", "djsubscription_id")
    actions = [make_reschedule, create_extra_delivery]
    list_filter = ["status", "next_delivery_date", "is_from_shopify"]


@admin.register(SubscriptionCancelled)
class SubscriptionCancelledAdmin(admin.ModelAdmin):
    raw_id_fields = ("subscription", "customer")
    list_display = ("id", "subscription", "customer", "reason")
    list_display_links = ("id",)


@admin.register(SubscriptionPaymentFailed)
class SubscriptionPaymentFailedAdmin(admin.ModelAdmin):
    raw_id_fields = ("subscription", "customer")
    list_display = ("id", "subscription", "customer", "created_at")
    list_display_links = ("id",)


admin.site.register(SubscriptionPrepaid, SubscriptionPrepaidAdmin)
admin.site.register(Prepaid)
