from datetime import datetime, timedelta

import pytest
from freezegun import freeze_time

from ecomm.order.tests.factories import OrderFactory

from ..models import SubscriptionPrepaid

pytestmark = pytest.mark.django_db


class TestSubscriptionPrepaid:
    """
    Test SubscriptionPrepaid model
    """

    def test_str(self, subscription: SubscriptionPrepaid):
        """
        Test string representation
        """
        string = f"{ subscription.user.email } - { subscription.id }"

        assert str(subscription) == string

    def test_get_next_delivery_date_passed(self, subscription: SubscriptionPrepaid):
        """
        Test to get the next delivery date if the delivery date has passed
        """
        next_date = subscription.delivery_date
        today = datetime.strptime(next_date, "%Y-%m-%d") - timedelta(days=1)
        freeze_time(today)

        assert subscription.get_next_delivery_date(), next_date

    def test_get_next_delivery_date_not_passed(self, subscription: SubscriptionPrepaid):
        """
        Test to get the next delivery date if the delivery date has not passed
        """
        next_date = subscription.delivery_date
        today = datetime.strptime(next_date, "%Y-%m-%d") + timedelta(days=1)
        freeze_time(today)

        assert subscription.get_next_delivery_date(), subscription.next_delivery_date

    @freeze_time("2022-09-15")
    def test_remaining_left_pay_as_you_go(self, subscription: SubscriptionPrepaid):
        subscription.delivery_date = datetime.strptime(
            "2022-11-04", "%Y-%m-%d"
        )  # first order
        subscription.subscription_type = "pay_as_you_go"
        subscription.save()

        OrderFactory(
            delivery_date="2022-08-26", subscription_id=subscription.djsubscription_id
        )

        assert subscription.remaining_credits() == 0
