from datetime import datetime, timedelta
from unittest.mock import patch

import pytest
from factory.faker import Faker
from freezegun import freeze_time

from ecomm.catalogue.models import Product
from ecomm.catalogue.tests.factories import CategoryFactory, ProductFactory
from ecomm.order.models import Order

from ..models import SubscriptionPrepaid
from ..utils import create_product_subscription, reschedule_subscription

pytestmark = pytest.mark.django_db


class TestRescheduleSubscription:
    """
    Test reschedule_subscription function with the following scenarios:
    1. Subscription already has an order but not delivered
    2. Subscription already has an order and delivered
    """

    @patch("ecomm.subscription_management.utils.update_stripe_subscription")
    def test_reschedule_subscription_no_undelivered_order(
        self, subscription: SubscriptionPrepaid, order: Order
    ):
        """
        Test reschedule_subscription assuming the following scenario:
        1. Subscription is pay as you go
        2. Subscription does NOT have any undelivered order
        """

        # Set the scene
        freeze_time("2022-04-05")
        current_delivery_date = datetime(2022, 4, 11)
        expected_delivery_date = datetime(2022, 4, 18)

        order.delivery_date = (
            subscription.delivery_date
        ) = current_delivery_date - timedelta(weeks=1)

        subscription.user = order.user
        subscription.djsubscription_id = order.subscription_id = "sub_12345"
        order.save()

        subscription.next_delivery_date = current_delivery_date
        subscription.charge_frequency = 1
        subscription.delivery_frequency = 1
        subscription.save()

        reschedule_subscription(subscription, expected_delivery_date)

        assert subscription.next_delivery_date == expected_delivery_date

    @patch("ecomm.subscription_management.utils.update_stripe_subscription")
    def test_reschedule_subscription_has_undelivered_order(
        self, subscription: SubscriptionPrepaid, order: Order
    ):
        """
        Test reschedule_subscription assuming the following scenario:
        1. Subscription is pay as you go
        2. Subscription have undelivered order
        3. Submitted subscription does not change delivery time
        """

        freeze_time("2022-04-05")
        current_delivery_date = datetime(2022, 4, 7)
        expected_delivery_date = datetime(2022, 4, 14)
        expected_delivery_time = subscription.delivery_time

        order.delivery_date = subscription.delivery_date = current_delivery_date

        subscription.user = order.user
        order.subscription_id = subscription.djsubscription_id = "sub_12345"
        order.save()

        subscription.charge_frequency = 1
        subscription.delivery_frequency = 1
        subscription.next_delivery_date = current_delivery_date + timedelta(
            weeks=subscription.delivery_frequency
        )
        subscription.save()

        reschedule_subscription(subscription, expected_delivery_date, True)

        next_delivery_date = expected_delivery_date + timedelta(
            weeks=subscription.delivery_frequency
        )
        order.refresh_from_db()

        assert (
            subscription.delivery_date == expected_delivery_date
        ), f"Sub id: {subscription.djsubscription_id}"
        assert subscription.next_delivery_date == next_delivery_date
        assert subscription.delivery_time == expected_delivery_time
        assert order.delivery_date == expected_delivery_date.date()


class TestCreateProductSubscription:
    """
    1. Product not exist with title
    2. Product exist with title
    3. Product not exist without title
    4. Product exist without title
    """

    def test_product_not_exist_with_title(self):
        CategoryFactory(name="Subscription Offer", depth=0)
        email = Faker("email")
        create_product_subscription(
            username=email, price=20, title=f"{email}-subscription-1"
        )

        products = Product.objects.filter(title__istartswith=email)
        product = products.first()

        assert products.count() == 1
        assert product.title == f"{email}-subscription-1"

    def test_product_exist_with_title(self):
        CategoryFactory(name="Subscription Offer", depth=0)
        email = Faker("email")
        title = f"{email}-subscription-1"
        ProductFactory(title=title)
        create_product_subscription(username=email, price=20, title=title)

        products = Product.objects.filter(title__istartswith=email).order_by("-id")
        product_first = products.first()
        product_second = products.last()

        assert products.count() == 2
        assert product_first.title == f"{email}-subscription-2"
        assert product_second.title == f"{email}-subscription-1"

    def test_product_not_exist_without_title(self):
        CategoryFactory(name="Subscription Offer", depth=0)
        email = Faker("email")
        create_product_subscription(username=email, price=20)

        products = Product.objects.filter(title__istartswith=email)
        product = products.first()

        assert products.count() == 1
        assert product.title == f"{email}-subscription"

    def test_product_exist_without_title(self):
        CategoryFactory(name="Subscription Offer", depth=0)
        email = Faker("email")
        title = f"{email}-subscription-1"
        ProductFactory(title=title)
        create_product_subscription(username=email, price=20)

        products = Product.objects.filter(title__istartswith=email).order_by("-id")
        product_first = products.first()
        product_second = products.last()

        assert products.count() == 2
        assert product_first.title == f"{email}-subscription-2"
        assert product_second.title == f"{email}-subscription-1"
