import random

from djstripe.models import Customer, Subscription
from factory import SelfAttribute, SubFactory, fuzzy
from factory.django import DjangoModelFactory
from factory.faker import Faker

from ecomm.order.tests.factories import ShippingAddressFactory
from ecomm.subscription_management.models import SubscriptionPrepaid
from ecomm.users.tests.factories import UserFactory


class DJStripeCustomerFactory(DjangoModelFactory):
    class Meta:
        model = Customer

    email = Faker("email")
    id = fuzzy.FuzzyText(length=10)


class DJSubscriptionFactory(DjangoModelFactory):
    class Meta:
        model = Subscription

    plan = None
    quantity = fuzzy.FuzzyInteger(1, 10)
    customer = SubFactory(DJStripeCustomerFactory)
    status = "active"
    cancel_at_period_end = False
    canceled_at = None
    current_period_end = Faker("date_time_between", start_date="-1m", end_date="now")
    current_period_start = Faker("date_time_between", start_date="-2m", end_date="-1m")
    ended_at = None
    trial_end = None
    trial_start = None


class SubscriptionPrepaidFactory(DjangoModelFactory):
    class Params:
        sub = SubFactory(DJSubscriptionFactory)
        customer = SubFactory(DJStripeCustomerFactory)

    user = SubFactory(UserFactory)
    djsubscription_id = SelfAttribute("sub.id")
    djcustomer_id = SelfAttribute("customer.id")
    pack_size = random.randrange(6, 24, 6)
    description = Faker("text")
    delivery_date = Faker("date")
    delivery_time = fuzzy.FuzzyChoice(["morning", "afternoon", "evening"])
    delivery_frequency = random.randrange(1, 4)
    charge_frequency = fuzzy.FuzzyInteger(1, 4)
    initial_credit = int(pack_size / 6 * delivery_frequency)
    credit_per_delivery = pack_size / 6
    next_credit_charge = pack_size / 6
    next_delivery_date = delivery_date
    subscription_type = fuzzy.FuzzyChoice(["prepaid", "pay as you go"])
    note = Faker("text")
    shipping_address = SubFactory(ShippingAddressFactory)
    quantity = fuzzy.FuzzyInteger(1, 3)
    is_from_shopify = False
    status = fuzzy.FuzzyChoice(["active", "paused", "cancelled"])

    class Meta:
        model = SubscriptionPrepaid
