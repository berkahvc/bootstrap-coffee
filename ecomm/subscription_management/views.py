from datetime import datetime, timedelta

import stripe
from braces.views import AjaxResponseMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse, reverse_lazy
from django.utils.timezone import localdate, localtime
from django.views.generic import DetailView, FormView
from djstripe.models import Plan, Subscription
from djstripe.settings import STRIPE_SECRET_KEY
from oscar.core.loading import get_model

from ecomm.catalogue.models import Product
from ecomm.order.models import Order
from ecomm.schedule_management.models import DeliveryTime
from ecomm.schedule_management.utils import get_holidays
from ecomm.utils.utils import calculate_next_billing_date

from .forms import RescheduleAdminForm, SubscriptionForm
from .models import SubscriptionCancelled, SubscriptionPrepaid
from .tasks import (
    send_cancel_subscription_email_confirmation,
    send_paused_indefinitely_email_confirmation,
)
from .utils import (
    create_product_subscription,
    reschedule_subscription,
    update_stripe_product_description,
)

Prepaid = get_model("subscription_management", "Prepaid")


stripe.api_key = STRIPE_SECRET_KEY


class SubscriptionView(FormView):
    template_name = "subscription/subscription.html"
    form_class = SubscriptionForm
    success_url = reverse_lazy("checkout:index")

    def get_context_data(self, **kwargs):
        ctx = super(SubscriptionView, self).get_context_data(**kwargs)
        today = localdate()

        if today.weekday() == 5:
            start_date = today + timedelta(days=3)
        else:
            start_date = today + timedelta(days=2)

        max_date = 6
        index = 0
        dates_available = []
        while index < max_date:
            if start_date.weekday() == 6:
                start_date += timedelta(days=1)
                if start_date.strftime("%m/%d/%Y") in get_holidays():
                    continue
            else:
                if start_date.strftime("%m/%d/%Y") in get_holidays():
                    start_date += timedelta(days=1)
                    continue
                dates_available.append(start_date)
                start_date += timedelta(days=1)
                index += 1

        ctx["products"] = Product.objects.browsable().filter(
            categories__slug="subscription"
        )
        ctx["delivery_times"] = DeliveryTime.objects.filter(is_active=True).order_by(
            "start"
        )
        ctx["start_date"] = start_date
        ctx["dates_available"] = dates_available
        ctx["holidays"] = get_holidays()
        ctx["prepaid_list"] = Prepaid.objects.order_by("number_of_delivery")
        ctx["prepaid_mobile_list"] = Prepaid.objects.order_by("number_of_delivery")[1:]
        return ctx

    def form_valid(self, form):
        pack = form.cleaned_data.get("subsChoosePack")
        description = form.cleaned_data.get("description")
        interval = form.cleaned_data.get("interval")
        choose_frequency = form.cleaned_data.get("subsChooseFrequency")
        delivery_date = form.cleaned_data.get("delivery_date")
        delivery_time = form.cleaned_data.get("subsChooseTime")
        delivery_frequency = form.cleaned_data.get("subsChoosePrepaid")

        # format is {payment interval} - {price} - {subscription type}
        delivery_prepaid_split = delivery_frequency.split("-")
        price = delivery_prepaid_split[1]
        subscription_type = delivery_prepaid_split[2]
        credit_per_delivery = int(pack) / 6

        if subscription_type == "prepaid":
            payment_interval = delivery_prepaid_split[0]
            delivery_frequency = choose_frequency
            delivery_frequency_str = (
                f"<p>Delivery Frequency: {delivery_frequency} week</p>"
            )
        else:
            payment_interval = choose_frequency
            delivery_frequency = choose_frequency
            if int(choose_frequency) == 1:
                delivery_frequency_str = "<p>Delivery Frequency: 1 week</p>"
            else:
                delivery_frequency_str = (
                    f"<p>Delivery Frequency: {choose_frequency} weeks</p>"
                )

        if self.request.basket.is_has_subscription():
            form.add_error(
                "cart_has_subscription", "cannot add more than 1 subscription in a cart"
            )
            return super(SubscriptionView, self).form_invalid(form)

        delivery_date_str = f"<p>Delivery Day: {delivery_date.strftime('%A')}</p>"
        delivery_time_str = f"<p>Delivery Time: {DeliveryTime.objects.get(id=delivery_time).__str__()}</p>"
        combined_description = f"<p>{description}</p>{delivery_frequency_str}{delivery_date_str}{delivery_time_str}"

        product = create_product_subscription(
            self.request.user.username if self.request.user.username else "",
            combined_description,
            float(price) * credit_per_delivery,
        )
        line, _ = self.request.basket.add_product(product, 1)
        line.interval = interval
        line.interval_count = payment_interval
        line.delivery_prepaid = delivery_frequency
        line.subscription_type = subscription_type
        line.credit_per_delivery = credit_per_delivery
        line.delivery_date = delivery_date
        line.delivery_time_id = delivery_time
        line.pack_size = pack
        line.is_subscription = True
        line.save()

        self.request.basket.strategy = self.request.strategy
        self.request.basket.delivery_date = delivery_date
        self.request.basket.delivery_time_id = delivery_time
        self.request.basket.save()

        return super(SubscriptionView, self).form_valid(form)


class CancelSubscriptionView(LoginRequiredMixin, AjaxResponseMixin, DetailView):
    model = Subscription

    def post_ajax(self, request, *args, **kwargs):
        djsub_id = self.kwargs.get("pk")
        reason = self.request.POST.get("reason")

        resp = {"status": "success"}

        try:
            subscription = Subscription.objects.get(id=djsub_id)
            subscription.cancel(at_period_end=False)

            SubscriptionCancelled.objects.create(
                subscription=subscription, customer=subscription.customer, reason=reason
            )
            subscription_prepaid = SubscriptionPrepaid.objects.get(
                djsubscription_id=subscription.id
            )
            subscription_prepaid.status = "canceled"
            subscription_prepaid.save()

            send_cancel_subscription_email_confirmation.delay(
                subscription_prepaid_id=subscription_prepaid.id
            )
        except Subscription.DoesNotExist:
            resp.update({"status": "error", "message": "not found"})
        except Exception as e:
            resp.update({"status": "error", "message": str(e)})

        return JsonResponse(resp)


class SkipSubscriptionView(LoginRequiredMixin, AjaxResponseMixin, DetailView):
    model = Subscription

    def get_ajax(self, request, *args, **kwargs):
        djsub_id = self.kwargs.get("pk")
        delivery_date = self.request.GET.get("delivery_date", None)

        delivery_date_fmt = datetime.strptime(delivery_date, "%d/%m/%Y")
        subscription_prepaid = SubscriptionPrepaid.objects.get(
            djsubscription_id=djsub_id
        )
        new_billing = self.get_calculate_next_billing_date(
            delivery_date_fmt, subscription_prepaid
        )
        resp = {
            "status": "success",
            "new_billing": new_billing.strftime("%d/%m/%Y"),
            "day": delivery_date_fmt.strftime("%A"),
        }
        return JsonResponse(resp)

    def post_ajax(self, request, *args, **kwargs):
        """
        Function to skip/pause a subscription

        The function will update the subscription_prepaid and djstripe subscription model
        with the new billing date.

        These are the steps:
            1. Get the delivery date from the request
            2. Get the subscription object from djsubscription model
            3. Sync the subscription with Stripe to make sure that the subscription is up-to-date
            4. Get the subscription_prepaid object using djsubscription id
            5. Get and update (description string) the delivery interval
            6. Calculate and update (description string) the new billing date
            7. Update the stripe product object with the new description
            8. Sync the djproduct to stripe
            9. Get the djplan and update it with the new "trial date"
            10. Update the subscription_prepaid object

        :return: JsonResponse {"status": "success"}
        """

        djsub_id = self.kwargs.get("pk")
        skip_indefinitely = self.request.POST.get("skip_indefinitely")
        delivery_date = self.request.POST.get("delivery_date", None)
        delivery_time_str = self.request.POST.get("delivery_time_str", None)

        resp = {"status": "success"}

        try:
            stripe_subscription = stripe.Subscription.retrieve(djsub_id)
            subscription = Subscription.sync_from_stripe_data(stripe_subscription)
            subscription_prepaid = SubscriptionPrepaid.objects.get(
                djsubscription_id=subscription.id
            )

            if subscription.status.lower() == "canceled":
                subscription_prepaid.status = "canceled"
                subscription_prepaid.save()
                resp.update(
                    {"status": "error", "message": "Subscription already canceled"}
                )
                return JsonResponse(resp)

            if skip_indefinitely == "true":
                stripe_ref = stripe.Subscription.modify(
                    djsub_id,
                    pause_collection={"behavior": "void"},
                )
                Subscription.sync_from_stripe_data(stripe_ref)
                subscription_prepaid.status = "paused"
                subscription_prepaid.save()

                send_paused_indefinitely_email_confirmation.delay(
                    subscription_prepaid_id=subscription_prepaid.id
                )
            else:
                delivery_date_fmt = datetime.strptime(delivery_date, "%d/%m/%Y")
                interval_count = subscription.plan.interval_count
                new_billing = self.get_calculate_next_billing_date(
                    delivery_date_fmt, subscription_prepaid
                )
                next_billing_cycle_anchor = datetime.combine(
                    new_billing, localtime().now().time()
                )

                update_stripe_product_description(
                    subscription=subscription,
                    subscription_prepaid=subscription_prepaid,
                    delivery_frequency=subscription.plan.interval_count,
                    delivery_day=delivery_date_fmt.strftime("%A"),
                    delivery_time=delivery_time_str,
                )

                plan = Plan.objects.get(id=subscription.plan.id)
                subscription.update(
                    plan,
                    trial_end=int(next_billing_cycle_anchor.timestamp()),
                    proration_behavior="none",
                )

                # update delivery date for subscription prepaid
                subscription_prepaid.delivery_date = delivery_date_fmt
                subscription_prepaid.delivery_time = delivery_time_str
                subscription_prepaid.delivery_day = delivery_date_fmt.strftime("%A")
                subscription_prepaid.next_delivery_date = delivery_date_fmt + timedelta(
                    weeks=interval_count
                )
                subscription_prepaid.status = "paused"
                subscription_prepaid.save()
        except Subscription.DoesNotExist:
            resp.update({"status": "error", "message": "not found"})
        except Exception as e:
            resp.update({"status": "error", "message": str(e)})

        return JsonResponse(resp)

    def get_calculate_next_billing_date(
        self, delivery_date_fmt: datetime, subscription_prepaid: SubscriptionPrepaid
    ):
        new_billing = calculate_next_billing_date(
            delivery_date=delivery_date_fmt,
            deliveries_left=subscription_prepaid.deliveries_left(),
            delivery_frequency=subscription_prepaid.delivery_frequency,
        )

        return new_billing


class ResumeSubscriptionView(LoginRequiredMixin, AjaxResponseMixin, DetailView):
    model = Subscription

    def post_ajax(self, request, *args, **kwargs):
        djsub_id = self.kwargs.get("pk")
        delivery_date = self.request.POST.get("delivery_date", None)
        delivery_time_str = self.request.POST.get("delivery_time_str", None)

        resp = {"status": "success"}

        try:
            stripe_ref = stripe.Subscription.retrieve(djsub_id)
            subscription = Subscription.sync_from_stripe_data(stripe_ref)
            subscription_prepaid = SubscriptionPrepaid.objects.get(
                djsubscription_id=subscription.id
            )

            if stripe_ref.get("pause_collection"):
                stripe_ref = stripe.Subscription.modify(
                    djsub_id,
                    pause_collection="",
                )
                Subscription.sync_from_stripe_data(stripe_ref)
                subscription_prepaid.status = "active"
                subscription_prepaid.save()
                return JsonResponse(resp)

            if delivery_date:
                new_delivery_date = datetime.strptime(delivery_date, "%d/%m/%Y")

                if new_delivery_date.date() != subscription_prepaid.delivery_date:
                    interval_count = subscription.plan.interval_count

                    reschedule_subscription(
                        subscription=subscription_prepaid,
                        delivery_date=new_delivery_date,
                    )

                    update_stripe_product_description(
                        subscription=subscription,
                        subscription_prepaid=subscription_prepaid,
                        delivery_frequency=interval_count,
                        delivery_day=new_delivery_date.strftime("%A"),
                        delivery_time=delivery_time_str,
                    )

            subscription_prepaid.status = "active"
            subscription_prepaid.save()
        except Subscription.DoesNotExist:
            resp.update({"status": "error", "message": "Subscription not found"})
        except Exception as e:
            resp.update({"status": "error", "message": str(e)})

        return JsonResponse(resp)


class RescheduleAdminView(LoginRequiredMixin, FormView):
    form_class = RescheduleAdminForm

    def form_valid(self, form):
        subscription_prepaids_ids = form.cleaned_data.get("subscription_prepaids_ids")
        delivery_date = form.cleaned_data.get("delivery_date")

        for sp_id in subscription_prepaids_ids.split(","):
            subscription_prepaid = SubscriptionPrepaid.objects.get(id=sp_id)
            order_exist = Order.objects.filter(
                subscription_id=subscription_prepaid.djsubscription_id,
                delivery_date__gte=localdate(),
            ).exists()
            reschedule_subscription(
                subscription=subscription_prepaid,
                delivery_date=delivery_date,
                order_exist=order_exist,
            )
        return HttpResponseRedirect(
            reverse("admin:subscription_management_subscriptionprepaid_changelist")
        )


class SubscriptionExtraDeliveryView(LoginRequiredMixin, AjaxResponseMixin, DetailView):
    model = Subscription

    def post_ajax(self, request, *args, **kwargs):
        sub_id = self.kwargs.get("pk")
        subscription = Subscription.objects.get(id=sub_id)
        subscription_prepaid = SubscriptionPrepaid.objects.get(djsubscription_id=sub_id)
        delivery_date = self.request.POST.get("extra_delivery_date", None)
        delivery_time_id = self.request.POST.get("extra_delivery_time_id", None)

        delivery_date_fmt = datetime.strptime(delivery_date, "%d/%m/%Y")

        title = f"Extra delivery for subscription {subscription_prepaid.pack_size} pack"
        description = subscription_prepaid.description
        amount = subscription.plan.amount

        resp = {"status": "success"}
        try:
            product = create_product_subscription(
                self.request.user.username if self.request.user.username else "",
                description,
                float(amount),
                title=title,
            )

            resp = {"status": "success", "next_url": "/checkout/"}

            line, _ = self.request.basket.add_product(product, 1)
            line.delivery_date = delivery_date_fmt
            line.delivery_time_id = delivery_time_id
            line.pack_size = subscription_prepaid.pack_size
            line.is_subscription = False
            line.save()

            self.request.basket.strategy = self.request.strategy
            self.request.basket.delivery_date = delivery_date_fmt
            self.request.basket.delivery_time_id = delivery_time_id
            self.request.basket.save()
        except Exception as err:
            print(err)
            resp.update({"status": "error", "message": str(err)})

        return JsonResponse(resp)
