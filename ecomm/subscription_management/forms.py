from django import forms
from django.contrib.admin import widgets


class SubscriptionForm(forms.Form):
    # username = forms.CharField()
    subsChoosePack = forms.CharField(required=True)
    description = forms.CharField(required=True)
    interval = forms.CharField(required=True)
    total_selected = forms.CharField(required=True)
    subsChooseFrequency = forms.CharField(required=True)
    delivery_date = forms.DateField(required=True, input_formats=["%m/%d/%Y"])
    subsChooseTime = forms.CharField(required=True)
    subsChoosePrepaid = forms.CharField(required=True)
    cart_has_subscription = forms.CharField(required=False)

    def __init__(self, *args, **kwargs):
        super(SubscriptionForm, self).__init__(**kwargs)

        self.fields["subsChooseFrequency"].label = "CHOOSE YOUR FREQUENCY"
        self.fields["description"].label = "CHOOSE YOUR COFFEE"
        self.fields["subsChoosePack"].label = "SELECT YOUR PACK SIZE"
        self.fields["delivery_date"].label = "CHOOSE YOUR DELIVERY DATE"
        self.fields["subsChooseTime"].label = "CHOOSE YOUR DELIVERY TIME"
        self.fields["subsChoosePrepaid"].label = "CHOOSE YOUR DELIVERY TIME"

    def clean_total_selected(self):
        val = self.cleaned_data.get("total_selected", None)
        pack = self.cleaned_data.get("subsChoosePack", None)

        if val is None or pack is None:
            raise forms.ValidationError(
                "Total selected item less than selected pack size"
            )

        if int(val) != int(pack):
            raise forms.ValidationError(
                "Total selected item less than selected pack size"
            )
        return val


class RescheduleAdminForm(forms.Form):
    subscription_prepaids_ids = forms.CharField(
        required=True, widget=forms.HiddenInput()
    )
    delivery_date = forms.DateField(widget=widgets.AdminDateWidget)
