from django.apps import AppConfig


class SubscriptionManagementConfig(AppConfig):
    name = "ecomm.subscription_management"
