# Generated by Django 3.0.11 on 2021-11-24 01:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('subscription_management', '0009_subscriptionprepaid_delivery_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='subscriptionprepaid',
            name='next_delivery_date',
            field=models.DateField(blank=True, db_index=True, null=True),
        ),
    ]
