# Generated by Django 3.0.11 on 2022-04-20 21:50

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import djstripe.fields


class Migration(migrations.Migration):

    dependencies = [
        ('djstripe', '0008_2_5'),
        ('subscription_management', '0016_subscriptionprepaid_status'),
    ]

    operations = [
        migrations.CreateModel(
            name='SubscriptionPaymentFailed',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('customer', djstripe.fields.StripeForeignKey(help_text='DjStripe Customer Object', null=True, on_delete=django.db.models.deletion.SET_NULL, to='djstripe.Customer', to_field=settings.DJSTRIPE_FOREIGN_KEY_TO_FIELD)),
                ('subscription', djstripe.fields.StripeForeignKey(help_text='DjStripe Subscription Object', on_delete=django.db.models.deletion.CASCADE, to='djstripe.Subscription', to_field=settings.DJSTRIPE_FOREIGN_KEY_TO_FIELD)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
