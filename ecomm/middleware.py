from django.core.exceptions import ValidationError
from django.http import JsonResponse


class ExceptionHandlerMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response

    def process_exception(self, _, exception):
        if isinstance(exception, ValidationError):
            return JsonResponse({"error": exception.message}, status=422)
