from oscar.core.loading import get_model

from config import celery_app
from ecomm.common.lodi_client import LodiClient

Product = get_model("catalogue", "Product")


@celery_app.task()
def get_sync_stock_lodi():
    """A pointless Celery task to demonstrate usage."""
    lodi_client = LodiClient()
    products = Product.objects.all()

    for product in products:
        if product.has_stockrecords:
            for stock_record in product.stockrecords.all():
                payload = lodi_client.get_stock_level_by_sku(stock_record.partner_sku)
                # quantity_left = payload[0]["available_qty"] - payload[0]["order_allocation_qty"]
                stock_record.num_in_stock = payload[0]["available_qty"]
                stock_record.save()
