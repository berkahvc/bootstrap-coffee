from factory import SubFactory
from factory.django import DjangoModelFactory

from ecomm.catalogue.models import Category, Product, ProductClass


class CategoryFactory(DjangoModelFactory):
    class Meta:
        model = Category


class ProductClassFactory(DjangoModelFactory):
    class Meta:
        model = ProductClass


class ProductFactory(DjangoModelFactory):
    class Meta:
        model = Product

    product_class = SubFactory(ProductClassFactory)
    structure = Product.STANDALONE
