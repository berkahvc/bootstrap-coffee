from django.templatetags.static import static
from oscar.apps.catalogue.models import Category
from oscar.apps.partner.strategy import Selector

from .constants import BaseCategories
from .models import Product


def initiate_base_categories():
    for category in BaseCategories.values():
        existing_category = Category.objects.filter(name=category).all().count()

        if existing_category:
            print(f"Category {category} is exists")
            continue

        category_count = Category.objects.all().count()

        Category.objects.get_or_create(
            path=f"{category_count + 1}".rjust(4, "0"), name=category, depth=1
        )


def get_product_dto_by_id(product_id):
    strategy = Selector().strategy()

    product = Product.objects.filter(pk=product_id)

    info = strategy.fetch_for_product(product.get())

    product_primary_image = product.first().primary_image()

    try:
        image_url = product_primary_image.original.url
    except Exception:
        image_url = static("images/image_not_found.png")

    product_dto = product.values()[0]
    product_dto["image_url"] = image_url
    product_dto["absolute_url"] = product.get().get_absolute_url()
    product_dto["attributes"] = product.get().get_attributes_map

    # if child
    if product.get().is_child:
        product_dto["parent_title"] = product.get().parent.title

    product_dto["info"] = {}
    product_dto["info"]["price"] = info.price.__dict__
    product_dto["info"]["availability"] = info.availability.__dict__

    return product_dto
