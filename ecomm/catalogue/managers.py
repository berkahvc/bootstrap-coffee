from oscar.apps.catalogue.managers import ProductQuerySet as CoreProductQuerySet


class ProductQuerySet(CoreProductQuerySet):
    SORT_MAP = {
        "title-desc": "-title",
        "title-asc": "title",
        "rating": "-rating",
        "newest": "-date_created",
        "price-desc": "-stockrecords__price_excl_tax",
        "price-asc": "stockrecords__price_excl_tax",
        "relevancy": "stockrecords__num_in_stock",
    }

    def user_search_filter(
        self,
        q: str = None,
        sort=None,
    ):
        """
        :param sort: Sort by this sort parameters:
        :param q: Search key for title
        :return: List of a particular customer's chat
        """

        search_query = self

        if q:
            search_query = search_query.filter(title__icontains=q)

        if sort and sort in self.SORT_MAP:
            search_query = search_query.order_by(self.SORT_MAP[sort])

            if sort.startswith("price"):
                search_query = search_query.filter(stockrecords__num_in_stock__gte=1)

        return search_query
