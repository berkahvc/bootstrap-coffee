from django.db import models
from oscar.apps.catalogue.abstract_models import AbstractProduct

from ecomm.timestamp_models import TimestampModel


class Product(AbstractProduct):
    size_guide = models.FileField(upload_to="size_guides", null=True, blank=True)
    shopify_product_id = models.CharField(
        max_length=255, null=True, blank=True, db_index=True
    )
    shopify_variant_id = models.CharField(
        max_length=255, null=True, blank=True, db_index=True
    )

    class Meta:
        index_together = [
            ["title", "date_created"],
            ["title", "rating"],
        ]
        ordering = ["title"]

    def __str__(self):
        if self.parent:
            return f"{self.parent.title} - {self.title}"
        else:
            return self.title

    @property
    def get_attributes_map(self):
        """
        Return a string of all of a product's attributes
        """
        attributes = self.attribute_values.all()

        attribute_map = {}

        for attribute in attributes:
            attribute_map[attribute.attribute.name] = attribute.value_as_text

        return attribute_map

    @property
    def is_on_wishlist(self):
        return False

    def get_categories_list_name(self):
        categories = list(self.get_categories().all().values_list("name", flat=True))
        return [category.lower() for category in categories]

    def join_categories_list(self):
        return ", ".join([i.capitalize() for i in self.get_categories_list_name()])

    def get_variant_name(self):
        import re

        if self.is_parent:
            title_list = self.children.values_list("title", flat=True)
            result = list(
                set([re.sub(r"\d+ ", "", title).capitalize() for title in title_list])
            )
            return result[0] if len(result) > 0 else "-"

    @property
    def sorted_size_pack(self):
        if self.is_parent:
            return self.children.filter(is_public=True).order_by(
                "stockrecords__price_excl_tax"
            )
        return []

    @property
    def sorted_size_pack_price_from(self):
        if len(self.sorted_size_pack) > 0:
            return self.sorted_size_pack[0].stockrecords.all()[0].price_excl_tax
        return 0

    @property
    def sorted_size_pack_price_retail_from(self):
        if len(self.sorted_size_pack) > 0:
            return self.sorted_size_pack[0].stockrecords.all()[0].price_retail
        return 0


class NutritionalFact(TimestampModel):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=255, null=True, blank=True, db_index=True)
    quantity = models.CharField(max_length=255, null=True, blank=True, db_index=True)
    unit_of_measurement = models.CharField(
        max_length=255, null=True, blank=True, db_index=True
    )


class NutritionalInformation(TimestampModel):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    ingredients = models.CharField(max_length=255, null=True, blank=True, db_index=True)


class Banner(TimestampModel):
    image = models.ImageField(upload_to="banners", db_index=True)
    url = models.URLField(null=True, blank=True, db_index=True)
    open_new_tab = models.BooleanField(default=False)


from oscar.apps.catalogue.models import *  # noqa isort:skip
