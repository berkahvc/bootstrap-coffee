from allauth.account.views import LoginView as CoreLoginView
from allauth.account.views import PasswordResetView
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.gis.db.models.functions import Distance
from django.http import HttpResponseRedirect
from django.middleware.csrf import get_token
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, DetailView, View
from oscar.apps.catalogue.views import CatalogueView as CoreCatalogueView
from oscar.apps.catalogue.views import ProductCategoryView as CoreProductCategoryView
from oscar.apps.catalogue.views import ProductDetailView as CoreProductDetailView
from oscar.core.loading import get_class, get_model
from stores.views import StoreListView as CoreStoreListView

from ecomm.address.forms import ContactUsForm
from ecomm.catalogue.forms import StoreSearchForm as BaseStoreSearchForm

User = get_user_model()
Banner = get_model("catalogue", "Banner")
Product = get_model("catalogue", "Product")
AttributeOption = get_model("catalogue", "AttributeOption")
Category = get_model("catalogue", "Category")
ContactUs = get_model("address", "ContactUs")
ProductAlert = get_model("customer", "ProductAlert")
ProductAlertForm = get_class("customer.forms", "ProductAlertForm")


class CatalogueView(CoreCatalogueView):
    template_name = "pages/collection.html"

    def get_context_data(self, **kwargs):
        ctx = super(CatalogueView, self).get_context_data(**kwargs)
        ctx["products"] = (
            Product.objects.browsable()
            .filter(categories__name__icontains="coffee")
            .exclude(categories__name__icontains="subscription")
        )
        ctx["attribute_options"] = AttributeOption.objects.order_by("option")
        return ctx


class CatalogueFuelUpView(CoreCatalogueView):
    template_name = "pages/collection.html"

    def get_context_data(self, **kwargs):
        ctx = super(CatalogueFuelUpView, self).get_context_data(**kwargs)
        ctx["products"] = (
            Product.objects.browsable()
            .filter(categories__name__icontains="fuel up")
            .exclude(categories__name__icontains="subscription")
        )
        ctx["attribute_options"] = AttributeOption.objects.order_by("option")
        return ctx


class CatalogueTagsView(CoreCatalogueView):
    template_name = "pages/collection.html"

    def get_context_data(self, **kwargs):
        ctx = super(CatalogueTagsView, self).get_context_data(**kwargs)
        tags = self.kwargs.get("tags")
        tags = tags.replace("-", " ")
        ctx["products"] = (
            Product.objects.browsable()
            .filter(categories__name__icontains="coffee")
            .exclude(categories__name__icontains="subscription")
            .filter_by_attributes(tags__icontains=tags)
        )
        ctx["attribute_options"] = AttributeOption.objects.order_by("option")
        ctx["tags_selected"] = tags
        return ctx


class ProductCategoryView(CoreProductCategoryView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["category"] = self.category
        search_context = self.search_handler.get_search_context_data(
            self.context_object_name
        )
        context.update(search_context)

        return context


class ProductDetailView(CoreProductDetailView):
    template_name = "pages/product-detail.html"

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx["alert_form"] = self.get_alert_form()
        ctx["counter"] = 10000
        ctx["has_active_alert"] = self.get_alert_status()
        return ctx


class ProductAdsDetailView(DetailView):
    model = Product
    template_name = "pages/product-detail.html"
    context_object_name = "product"

    def get_object(self, queryset=None):
        slug = self.kwargs.get("slug")
        if slug == "cruffin-and-cold-brew-christmas-bundle":
            slug = "cruffin-and-cold-brew-gift-sets"

        return Product.objects.get(slug=slug)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx["alert_form"] = self.get_alert_form()
        ctx["counter"] = 10000
        ctx["has_active_alert"] = self.get_alert_status()
        return ctx

    def get_alert_form(self):
        return ProductAlertForm(user=self.request.user, product=self.object)

    def get_alert_status(self):
        # Check if this user already have an alert for this product
        has_alert = False
        if self.request.user.is_authenticated:
            alerts = ProductAlert.objects.filter(
                product=self.object, user=self.request.user, status=ProductAlert.ACTIVE
            )
            has_alert = alerts.exists()
        return has_alert

    def dispatch(self, request, *args, **kwargs):
        slug = self.kwargs.get("slug")
        try:
            Product.objects.get(slug=slug)
        except Product.DoesNotExist:
            return HttpResponseRedirect(reverse("catalogue:index"))

        return super().dispatch(request, *args, **kwargs)


class HomeView(CoreCatalogueView):
    context_object_name = "products"
    template_name = "pages/home-page.html"

    def get_context_data(self, **kwargs):
        ctx = super(HomeView, self).get_context_data(**kwargs)
        ctx["products"] = Product.objects.browsable().filter(
            categories__slug="best_seller"
        )
        ctx["product_mix_pack"] = (
            Product.objects.browsable().filter(title__icontains="mix pack").first()
        )
        ctx["product_gift_pack"] = (
            Product.objects.browsable().filter(title__icontains="gift pack").first()
        )
        return ctx


class ContactUsView(CreateView):
    template_name = "pages/contact-us.html"
    model = ContactUs
    form_class = ContactUsForm
    success_url = reverse_lazy("home")

    def form_valid(self, form):
        messages.success(self.request, "Thanks for contact us.")
        return super(ContactUsView, self).form_valid(form)


class LoginView(CoreLoginView):
    def form_invalid(self, form):
        email = form.cleaned_data["login"]
        try:
            user = User.objects.get(username=email)
            if not user.password and user.is_from_shopify:
                self.request.POST = {
                    "email": user.email,
                    "csrfmiddlewaretoken": get_token(self.request),
                }
                PasswordResetView.as_view()(self.request)
                return HttpResponseRedirect(reverse("account_reset_password_done"))
        except User.DoesNotExist:
            pass
        return super(LoginView, self).form_invalid(form)


class StoreListView(CoreStoreListView):
    form_class = BaseStoreSearchForm

    def get_queryset(self):
        queryset = self.model.objects.filter(is_active=True)
        if not self.form.is_valid():
            return queryset

        data = self.form.cleaned_data

        group = data.get("group", None)
        if group:
            queryset = queryset.filter(group=group)

        latlng = self.form.point

        if latlng:
            queryset = queryset.annotate(distance=Distance("location", latlng))

            # Constrain by distance if set up
            max_distance = self.get_max_distance()
            if max_distance:
                queryset = queryset.filter(distance__lte=max_distance)

            # Order by distance
            queryset = queryset.order_by("distance")

        return queryset


class DiscountView(View):
    permanent = False
    query_string = False
    pattern_name = "collections"

    def dispatch(self, request, *args, **kwargs):
        redirect = self.request.GET.get("redirect", None)
        code = self.kwargs.get("code")
        self.request.session["discount_code"] = code
        return HttpResponseRedirect(redirect)
