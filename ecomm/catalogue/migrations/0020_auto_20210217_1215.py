# Generated by Django 3.0.11 on 2021-02-17 05:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0019_auto_20210216_1509'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='colour',
            field=models.CharField(blank=True, db_index=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='size',
            field=models.CharField(blank=True, db_index=True, max_length=255, null=True),
        ),
    ]
