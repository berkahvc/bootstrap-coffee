from django.contrib.gis.geos import GEOSGeometry
from stores.forms import StoreSearchForm as CoreStoreSearchForm
from stores.utils import get_geodetic_srid

from ecomm.catalogue import geocode


class StoreSearchForm(CoreStoreSearchForm):
    def geocoordinates(self, data):
        latitude = data.get("latitude", None)
        longitude = data.get("longitude", None)
        if latitude and longitude:
            return GEOSGeometry(
                "POINT(%s %s)" % (longitude, latitude), srid=get_geodetic_srid()
            )

        query = data.get("query", None)
        if query is not None:
            try:
                return geocode.GeoCodeService().geocode(query)
            except geocode.ServiceError:
                return None
