from django.core.management.base import BaseCommand

from ecomm.catalogue.utils import initiate_base_category

# ./manage.py initiate_base_category


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        initiate_base_category()
