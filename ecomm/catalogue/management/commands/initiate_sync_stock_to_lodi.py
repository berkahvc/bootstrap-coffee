from django.core.management.base import BaseCommand
from django_celery_beat.models import CrontabSchedule, PeriodicTask

# ./manage.py initiate_sync_stock_to_lodi


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        schedule, _ = CrontabSchedule.objects.get_or_create(
            minute=0,
            hour=7,
            day_of_week="*",
            day_of_month="*",
            month_of_year="*",
            timezone="Asia/Jakarta",
        )

        PeriodicTask.objects.get_or_create(
            name="Syncronize stock with lodi",
            task="ecomm.catalogue.tasks.get_sync_stock_lodi",
            crontab=schedule,
            enabled=False,
        )
