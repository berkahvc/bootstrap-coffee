from django.core.management.base import BaseCommand

from ecomm.catalogue.utils import initiate_base_categories

# ./manage.py initiate_base_categories


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        initiate_base_categories()
