from oscar.apps.catalogue.search_handlers import (
    SimpleProductSearchHandler as BaseSimpleProductSearchHandler,
)
from oscar.core.loading import get_model

Product = get_model("catalogue", "Product")


class SimpleProductSearchHandler(BaseSimpleProductSearchHandler):
    NEWEST = "newest"
    PRICE_HIGH_TO_LOW = "price-desc"
    PRICE_LOW_TO_HIGH = "price-asc"

    SORT_BY_MAP = {
        NEWEST: "-date_created",
        PRICE_HIGH_TO_LOW: "-stockrecords__price_excl_tax",
        PRICE_LOW_TO_HIGH: "stockrecords__price_excl_tax",
    }

    def __init__(self, request_data, full_path, categories=None):
        self.categories = categories
        self.kwargs = {
            "page": request_data.get("page", 1),
            "sort_by": request_data.get("sort_by", None),
        }
        self.object_list = self.get_queryset()

    def get_queryset(self):
        qs = Product.objects.browsable().base_queryset()
        if self.categories:
            qs = qs.filter(categories__in=self.categories).distinct()

        if self.kwargs.get("sort_by"):
            sort_field = self.SORT_BY_MAP.get(self.kwargs.get("sort_by"), None)
            qs = qs.order_by(sort_field)
        return qs
