import enum


class BaseCategories(enum.Enum):
    BEST_SELLER = "BEST_SELLER"
    # SALE = "SALE"
    # MEN = "MEN"
    # WOMEN = "WOMEN"
    #
    # UNISEX = "UNISEX"
    # KIDS = "KIDS"
    # CLASSIC = "CLASSIC"
    # LIGHTNESS = "LIGHTNESS"

    @classmethod
    def choices(cls):
        return tuple((i.name, i.value) for i in cls)

    @classmethod
    def values(cls):
        values = []

        for choice in BaseCategories.choices():
            value = choice[1]
            values.append(value)

        return values
