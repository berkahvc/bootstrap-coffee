import re
from collections import namedtuple
from datetime import timedelta

import stripe
from django import template
from django.template.loader import select_template
from django.urls import reverse
from djstripe.settings import STRIPE_SECRET_KEY
from oscar.apps.payment.models import Source
from oscar.core.loading import get_model

from ecomm.catalogue.models import Product
from ecomm.schedule_management.utils import get_holidays
from ecomm.subscription_management.models import SubscriptionPrepaid

# A container for policies
WishlistInfo = namedtuple("PurchaseInfo", ["is_on_wishlist", "remove_url"])
Voucher = get_model("voucher", "Voucher")

register = template.Library()
stripe.api_key = STRIPE_SECRET_KEY


@register.filter(name="addcss")
def addcss(field, css):
    old_css = field.field.widget.attrs.get("class", "")
    return field.as_widget(attrs={"class": ("%s %s" % (old_css, css)).strip()})


@register.filter
def clean(value):
    return value.replace("_", " ")


@register.filter
def join_arr(arr, val):
    return f"{val}".join(arr)


@register.filter
def text_split(text, val):
    return text.split(val)


@register.filter
def is_today_holiday(date):
    holidays = get_holidays()
    return date.strftime("%m/%d/%Y") in holidays


@register.filter
def remove_pack_text(val):
    return val.replace("pack", "").strip()


@register.filter
def get_address(djsub_id):
    source = Source.objects.filter(reference=djsub_id)
    if source.exists():
        order = source.first().order
        return order.shipping_address
    return ""


@register.filter
def is_subscription_prepaid(djsub_id):
    subscription_prepaid = SubscriptionPrepaid.objects.filter(
        djsubscription_id=djsub_id
    )
    if subscription_prepaid.exists():
        if subscription_prepaid.first().subscription_type == "prepaid":
            return "Prepaid"
        else:
            return "Pay as you go"
    return "Pay as you go"


@register.filter
def get_subscription_prepaid(djsub_id):
    sp = SubscriptionPrepaid.objects.filter(djsubscription_id=djsub_id)
    if sp.exists():
        return sp.first()
    return None


@register.filter
def next_delivery_date(current_period_end, delivery_day):
    for i in range(0, 7):
        date = current_period_end + timedelta(days=i)
        if date.strftime("%A") == delivery_day:
            return date.date()


@register.filter
def get_order_lines(djsub_id, is_subscription="0"):
    source = Source.objects.filter(reference=djsub_id)
    if source.exists():
        order = source.first().order
        return order.lines.filter(is_subscription=bool(int(is_subscription)))
    return []


@register.filter
def get_status_paused(djsub_id):
    stripe_ref = stripe.Subscription.retrieve(djsub_id)
    return True if stripe_ref.get("pause_collection") else False


@register.simple_tag
def is_on_wishlist(request, product: Product):
    user_wishlists = (
        request.user.wishlists.all() if request.user.is_authenticated else []
    )

    for wishlist in user_wishlists:
        for line in wishlist.lines.all():
            if product.id == line.product.id:
                return WishlistInfo(
                    is_on_wishlist=True,
                    remove_url=reverse(
                        "customer:wishlists-remove-product",
                        kwargs={"key": wishlist.key, "line_pk": line.pk},
                    ),
                )

    return WishlistInfo(is_on_wishlist=False, remove_url=None)


@register.filter
def get_voucher(discount_name):
    re_match = re.match(r"\'([A-Za-z0-9\s]+)\'", discount_name)
    re_findall = re.findall(r"'(.*?)'", discount_name, re.DOTALL)

    if len(re_findall) > 0:
        try:
            voucher = Voucher.objects.get(code=re_findall[0])
            return voucher
        except Voucher.DoesNotExist:
            return None

    if re_match:
        discount_name = re_match.group(1)
        try:
            voucher = Voucher.objects.get(code=discount_name)
            return voucher
        except Voucher.DoesNotExist:
            return None
    return None


@register.simple_tag(takes_context=True)
def custom_render_product(context, product, category, counter):
    """
    Render a product snippet as you would see in a browsing display.

    This templatetag looks for different templates depending on the UPC and
    product class of the passed product.  This allows alternative templates to
    be used for different product classes.
    """
    if not product:
        # Search index is returning products that don't exist in the
        # database...
        return ""

    names = [
        "oscar/catalogue/partials/product/upc-%s.html" % product.upc,
        "oscar/catalogue/partials/product/class-%s.html"
        % product.get_product_class().slug,
        "oscar/catalogue/partials/product.html",
    ]
    template_ = select_template(names)
    context = context.flatten()

    # Ensure the passed product is in the context as 'product'
    context["product"] = product
    context["category"] = category
    context["counter"] = counter
    return template_.render(context)
