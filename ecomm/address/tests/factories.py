from factory import DjangoModelFactory, SubFactory

from ecomm.address.models import Country, UserAddress
from ecomm.users.tests.factories import UserFactory


class CountryFactory(DjangoModelFactory):
    class Meta:
        model = Country
        django_get_or_create = ("pk",)

    pk = "SG"
    name = "Singapore"


class UserAddressFactory(DjangoModelFactory):
    class Meta:
        model = UserAddress

    user = SubFactory(UserFactory)
    line4 = "51.08_BULELENG"
    district = "51.08.03_Busung biu (Busungbiu)"
    country = SubFactory(CountryFactory)
