import pytest

pytestmark = pytest.mark.django_db


def test_new_format_city_function(user_address):
    assert user_address.city == "BULELENG"


def test_old_format_city_function(user_address):
    user_address.line4 = "51.07"
    user_address.save()

    assert user_address.city == "51.07"


def test_new_format_city_code_function(user_address):
    assert user_address.city_code == "51.08"
