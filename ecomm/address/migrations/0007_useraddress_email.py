# Generated by Django 3.0.11 on 2021-02-19 02:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0006_auto_20181115_1953'),
    ]

    operations = [
        migrations.AddField(
            model_name='useraddress',
            name='email',
            field=models.CharField(blank=True, max_length=255, verbose_name='User email'),
        ),
    ]
