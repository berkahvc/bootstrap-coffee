from django.core import exceptions
from django.db import models
from django.utils.translation import gettext_lazy as _
from oscar.apps.address.abstract_models import (
    AbstractUserAddress as CoreAbstractUserAddress,
)

from ecomm.timestamp_models import TimestampModel


class UserAddress(CoreAbstractUserAddress):
    email = models.CharField(_("User email"), max_length=255, blank=True)
    district = models.CharField(
        _("District"), max_length=255, blank=True, null=True, db_index=True
    )

    def get_field_values(self, fields):
        field_values = []
        for field in fields:
            # Title is special case
            if field == "title":
                value = self.get_title_display()
            elif field == "country":
                try:
                    value = self.country.printable_name
                except exceptions.ObjectDoesNotExist:
                    value = ""
            elif field == "salutation":
                continue
            elif field == "line4":
                value = self.city
            elif field == "district":
                value = self.show_district
            else:
                value = getattr(self, field)
            field_values.append(value)
        return field_values

    @property
    def city(self):
        if self.line4:
            line4_split = self.line4.split("_")
            return line4_split[1] if len(line4_split) > 1 else line4_split[0]
        return "-"

    @property
    def city_code(self):
        if self.line4:
            return self.line4.split("_")[0]
        return "-"

    @property
    def show_district(self):
        if self.district is None:
            return "-"

        district_split = self.district.split("_")
        return district_split[1] if len(district_split) > 1 else district_split[0]


class ContactUs(TimestampModel):
    name = models.CharField(max_length=255, null=True, db_index=True)
    email = models.EmailField(max_length=255, null=True, db_index=True)
    message = models.TextField(null=True)


from oscar.apps.address.models import *  # noqa isort:skip
