from django import forms
from oscar.apps.address.forms import UserAddressForm as CoreUserAddressForm
from oscar.apps.address.models import Country
from oscar.core.loading import get_model

ContactUs = get_model("address", "ContactUs")


class UserAddressForm(CoreUserAddressForm):
    def __init__(self, *args, **kwargs):
        """
        Set fields in OSCAR_REQUIRED_ADDRESS_FIELDS as required.
        """
        super().__init__(*args, **kwargs)

        for field_name in self.fields:
            self.fields[field_name].required = True

        self.fields["country"] = forms.ModelChoiceField(
            queryset=Country.objects.filter(pk="SG").all()
        )
        self.fields["postcode"].label = "Zip Code"
        self.fields["notes"].required = False
        self.fields["country"].initial = "SG"

    class Meta(CoreUserAddressForm.Meta):
        fields = [
            "first_name",
            "last_name",
            "phone_number",
            "email",  # User Information
            "line1",
            "line4",
            "state",
            "country",
            "postcode",
            "notes",
        ]
        widgets = {
            "first_name": forms.TextInput(
                attrs={"placeholder": "Input recipient first name"}
            ),
            "last_name": forms.TextInput(
                attrs={"placeholder": "Input recipient last name"}
            ),
            "email": forms.EmailInput(attrs={"placeholder": "Input email"}),
            "postcode": forms.TextInput(attrs={"placeholder": "Input postal code"}),
            "state": forms.TextInput(attrs={"placeholder": "Input state"}),
            "line4": forms.TextInput(attrs={"placeholder": "Input city"}),
            "line1": forms.TextInput(attrs={"placeholder": "Input address"}),
            "phone_number": forms.TextInput(
                attrs={"placeholder": "Input phone number"}
            ),
            "notes": forms.TextInput(attrs={"placeholder": "Input additional info"}),
        }


class ContactUsForm(forms.ModelForm):
    class Meta:
        model = ContactUs
        fields = "__all__"
        widgets = {
            "name": forms.TextInput(attrs={"placeholder": "Input name"}),
            "email": forms.TextInput(attrs={"placeholder": "Input email address"}),
        }
