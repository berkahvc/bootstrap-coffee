# Generated by Django 3.0.11 on 2022-03-23 10:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('basket', '0018_auto_20220322_1102'),
    ]

    operations = [
        migrations.AlterField(
            model_name='line',
            name='delivery_prepaid',
            field=models.IntegerField(blank=True, db_index=True, help_text='Delivery interval', null=True),
        ),
        migrations.AlterField(
            model_name='line',
            name='interval',
            field=models.CharField(blank=True, help_text='unit of the interval (e.g. month, day, week) we usually use week', max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='line',
            name='interval_count',
            field=models.IntegerField(blank=True, db_index=True, help_text='Billing interval', null=True),
        ),
    ]
