import logging
from datetime import datetime

from braces.views import AjaxResponseMixin
from django.conf import settings
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.http import JsonResponse
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views import generic
from klaviyo_sdk import Client
from oscar.apps.basket import signals
from oscar.apps.basket.views import BasketAddView as CoreBasketAddView
from oscar.apps.basket.views import BasketView as CoreBasketView
from oscar.apps.basket.views import VoucherAddView as CoreVoucherAddView
from oscar.apps.basket.views import VoucherRemoveView as CoreVoucherRemoveView
from oscar.core.loading import get_class, get_model
from oscar.core.utils import redirect_to_referrer

Line = get_model("basket", "Line")
Voucher = get_model("voucher", "Voucher")
DeliveryTime = get_model("schedule_management", "DeliveryTime")
BasketLineForm = get_class("basket.forms", "BasketLineForm")
BasketMessageGenerator = get_class("basket.utils", "BasketMessageGenerator")
Applicator = get_class("offer.applicator", "Applicator")

client = Client(settings.KLAVIYO_API_KEY)


class BasketView(CoreBasketView):
    def get(self, request, *args, **kwargs):
        if (
            self.request.user.is_anonymous
            or not self.request.user.is_superuser
            or not self.request.user.is_staff
        ):
            return redirect("home")

        self.object_list = self.get_queryset()
        return super(BasketView, self).get(request, *args, **kwargs)


class BasketAddView(CoreBasketAddView):
    def post(self, request, *args, **kwargs):
        data = super().post(request, *args, **kwargs)
        ajax_request = self.request.is_ajax()
        if ajax_request:
            if self.request.user.is_authenticated:
                client.ListsSegments.add_members(
                    "Wi76hh", body={"profiles": [{"email": self.request.user.email}]}
                )
            return JsonResponse({"success": True})

        return data

    def form_valid(self, form):
        offers_before = self.request.basket.applied_offers()
        ajax_request = self.request.is_ajax()

        self.request.basket.add_product(
            form.product, form.cleaned_data["quantity"], form.cleaned_options()
        )

        messages.success(
            self.request, self.get_success_message(form), extra_tags="safe noicon"
        )

        # Check for additional offer messages
        BasketMessageGenerator().apply_messages(self.request, offers_before)

        # Send signal for basket addition
        self.add_signal.send(
            sender=self,
            product=form.product,
            user=self.request.user,
            request=self.request,
        )

        # setup CHEERUP voucher
        try:
            discount_code = self.request.session["discount_code"]
            if not self.request.basket.contains_voucher(discount_code):
                try:
                    voucher = Voucher.objects.get(code=discount_code)
                    self.apply_voucher_to_basket(form.product, voucher)
                except Voucher.DoesNotExist:
                    pass
        except Exception as err:
            print(f"===== discount_code error: {err}")

        if ajax_request:
            return JsonResponse({"success": True})

        return super().form_valid(form)

    def apply_voucher_to_basket(self, product, voucher):
        if voucher.is_expired():
            logging.error(
                _("The '%(code)s' voucher has expired") % {"code": voucher.code}
            )
            return

        if not voucher.is_active():
            logging.error(
                _("The '%(code)s' voucher is not active") % {"code": voucher.code}
            )
            return

        is_available, message = voucher.is_available_to_user(self.request.user)
        if not is_available:
            logging.error(message)
            return

        self.request.basket.vouchers.add(voucher)

        # Raise signal
        self.add_signal.send(
            sender=self,
            basket=self.request.basket,
            product=product,
            user=self.request.user,
            voucher=voucher,
        )

        # Recalculate discounts to see if the voucher gives any
        Applicator().apply(self.request.basket, self.request.user, self.request)
        signals.voucher_addition.send(
            sender=None, basket=self.request.basket, voucher=voucher
        )

        try:
            del self.request.session["discount_code"]
        except KeyError:
            pass

        logging.info(_("Voucher '%(code)s' added to basket") % {"code": voucher.code})


class BasketLineUpdateView(AjaxResponseMixin, generic.TemplateView):
    def post_ajax(self, request, *args, **kwargs):
        line_id = self.request.POST.get("lineId")
        quantity = self.request.POST.get("qty")

        line = Line.objects.get(id=line_id)
        availability = self.request.strategy.fetch_for_line(line).availability.__dict__
        num_available = availability.get("num_available")

        total_qty = line.quantity + int(quantity)

        self.request.basket.strategy = self.request.strategy

        resp = {"success": True, "line_deleted": False}

        if num_available:
            if total_qty > num_available:
                resp.update({"success": False, "messages": f"Max is {num_available}"})
                return JsonResponse(resp)
            elif 0 < total_qty <= num_available:
                self.request.basket.add_product(line.product, int(quantity))
            elif total_qty <= 0:
                line.delete()
                resp.update({"line_deleted": True})
        else:
            if total_qty <= 0:
                line.delete()
                resp.update({"line_deleted": True})
            else:
                self.request.basket.add_product(line.product, int(quantity))
        return JsonResponse(resp)


class DeliveryDateAddView(AjaxResponseMixin, generic.TemplateView):
    def post_ajax(self, request, *args, **kwargs):
        delivery_date = self.request.POST.get("date")
        updated = self.request.POST.get("updated", "true")
        selected_time_text = self.request.POST.get("selected_time_text", None)
        delivery_date_format = datetime.strptime(delivery_date, "%m/%d/%Y")
        day = delivery_date_format.strftime("%A")

        delivery_times = DeliveryTime.objects.filter(
            days__icontains=day, is_active=True
        )
        option = '<option value="">--- Select time ---</option>'

        for delivery_time in delivery_times:
            if selected_time_text:
                selected = (
                    " selected" if selected_time_text == delivery_time.__str__() else ""
                )
                option += f'<option value="{delivery_time.id}"{selected}>{delivery_time}</option>'
            else:
                option += f'<option value="{delivery_time.id}">{delivery_time}</option>'

        if updated == "true":
            self.request.basket.strategy = self.request.strategy
            self.request.basket.delivery_date = delivery_date_format
            self.request.basket.delivery_day = day
            self.request.basket.save()
        return JsonResponse({"success": True, "option": option})


class DeliveryTimeAddView(AjaxResponseMixin, generic.TemplateView):
    def post_ajax(self, request, *args, **kwargs):
        delivery_time_id = self.request.POST.get("delivery_time_id")
        self.request.basket.strategy = self.request.strategy
        self.request.basket.delivery_time_id = delivery_time_id
        self.request.basket.save()
        return JsonResponse({"success": True})


class VoucherAddView(CoreVoucherAddView):
    def form_valid(self, form):
        ajax_request = self.request.is_ajax()

        code = form.cleaned_data["code"]
        if not self.request.basket.id:
            if ajax_request:
                message = "No basket created"
                return JsonResponse({"success": False, "message": message})

            return redirect_to_referrer(self.request, "home")
        if self.request.basket.contains_voucher(code):
            discounts_after = self.request.basket.offer_applications
            found_discount = False
            for discount in discounts_after:
                if discount["voucher"] and discount["voucher"].code == code:
                    found_discount = True
                    break

            if not found_discount and ajax_request:
                voucher = self.request.basket.vouchers.get(code=code)
                self.request.basket.vouchers.remove(voucher)
                return JsonResponse({"success": True})

            message = _(
                "You have already added the '%(code)s' voucher to your basket"
            ) % {"code": code}

            if ajax_request:
                return JsonResponse({"success": False, "message": message})

            messages.error(self.request, message)
        else:
            try:
                voucher = self.voucher_model._default_manager.get(code=code)
            except self.voucher_model.DoesNotExist:
                message = _("No voucher found with code '%(code)s'") % {"code": code}

                if ajax_request:
                    return JsonResponse({"success": False, "message": message})

                messages.error(self.request, message)
            else:
                self.apply_voucher_to_basket(voucher)

        if ajax_request:
            return JsonResponse({"success": True})

        return redirect_to_referrer(self.request, "home")

    def apply_voucher_to_basket(self, voucher):
        if voucher.is_expired():
            messages.error(
                self.request,
                _("The '%(code)s' voucher has expired") % {"code": voucher.code},
            )
            return

        if not voucher.is_active():
            messages.error(
                self.request,
                _("The '%(code)s' voucher is not active") % {"code": voucher.code},
            )
            return

        is_available, message = voucher.is_available_to_user(self.request.user)
        if not is_available:
            messages.error(self.request, message)
            return

        self.request.basket.vouchers.add(voucher)

        # Raise signal
        self.add_signal.send(sender=self, basket=self.request.basket, voucher=voucher)

        # Recalculate discounts to see if the voucher gives any
        Applicator().apply(self.request.basket, self.request.user, self.request)

        messages.info(
            self.request,
            _("Voucher '%(code)s' added to basket") % {"code": voucher.code},
        )

    def form_invalid(self, form):
        ajax_request = self.request.is_ajax()

        message = _("Please enter a voucher code")

        if ajax_request:
            raise ValidationError(message)

        messages.error(self.request, message)
        return redirect(reverse("basket:summary") + "#voucher")


class VoucherRemoveView(CoreVoucherRemoveView):
    def post(self, request, *args, **kwargs):
        response = redirect("basket:summary")
        ajax_request = self.request.is_ajax()

        voucher_id = kwargs["pk"]
        if not request.basket.id:
            # Hacking attempt - the basket must be saved for it to have
            # a voucher in it.
            return response
        try:
            voucher = request.basket.vouchers.get(id=voucher_id)
        except ObjectDoesNotExist:
            messages.error(request, _("No voucher found with id '%s'") % voucher_id)
        else:
            request.basket.vouchers.remove(voucher)
            self.remove_signal.send(sender=self, basket=request.basket, voucher=voucher)
            messages.info(request, _("Voucher '%s' removed from basket") % voucher.code)

        if ajax_request:
            return JsonResponse({"success": True})

        return response
