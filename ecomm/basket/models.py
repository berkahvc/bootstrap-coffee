from django.db import models
from oscar.apps.basket.abstract_models import AbstractBasket, AbstractLine

from ecomm.schedule_management.models import DeliveryTime


class Basket(AbstractBasket):
    delivery_date = models.DateField(null=True, blank=True, db_index=True)
    delivery_time = models.ForeignKey(
        DeliveryTime, null=True, blank=True, db_index=True, on_delete=models.PROTECT
    )
    delivery_day = models.CharField(
        max_length=255, null=True, blank=True, db_index=True
    )

    def is_has_subscription(self):
        subscriptions = self.lines.filter(is_subscription=True)
        return subscriptions.exists()


class Line(AbstractLine):
    interval = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        help_text="unit of the interval (e.g. month, day, week) we usually use week",
    )
    interval_count = models.IntegerField(
        null=True, blank=True, db_index=True, help_text="Billing interval"
    )
    delivery_prepaid = models.IntegerField(
        null=True, blank=True, db_index=True, help_text="Delivery interval"
    )
    subscription_type = models.CharField(
        max_length=255, null=True, blank=True, db_index=True
    )
    pack_size = models.IntegerField(null=True, db_index=True)
    credit_per_delivery = models.IntegerField(null=True, db_index=True)
    delivery_date = models.DateField(null=True, blank=True, db_index=True)
    delivery_time = models.ForeignKey(
        DeliveryTime, null=True, blank=True, on_delete=models.SET_NULL
    )
    is_subscription = models.BooleanField(default=False, null=True, db_index=True)


from oscar.apps.basket.models import *  # noqa isort:skip
