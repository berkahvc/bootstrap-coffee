import oscar.apps.basket.apps as apps
from django.conf.urls import url
from oscar.core.loading import get_class


class BasketConfig(apps.BasketConfig):
    name = "ecomm.basket"

    def ready(self):
        super(BasketConfig, self).ready()
        self.ajax_basket_line_update_view = get_class(
            "basket.views", "BasketLineUpdateView"
        )
        self.ajax_delivery_date_add_view = get_class(
            "basket.views", "DeliveryDateAddView"
        )
        self.ajax_delivery_time_add_view = get_class(
            "basket.views", "DeliveryTimeAddView"
        )

    def get_urls(self):
        urls = super(BasketConfig, self).get_urls()

        urls += [
            url(
                r"ajax-basket-line-update-view/$",
                self.ajax_basket_line_update_view.as_view(),
                name="ajax-basket-line-update",
            ),
            url(
                r"ajax-basket-delivery-date-add-view/$",
                self.ajax_delivery_date_add_view.as_view(),
                name="ajax-basket-delivery-date-add",
            ),
            url(
                r"ajax-basket-delivery-time-add-view/$",
                self.ajax_delivery_time_add_view.as_view(),
                name="ajax-basket-delivery-time-add",
            ),
        ]

        return self.post_process_urls(urls)
