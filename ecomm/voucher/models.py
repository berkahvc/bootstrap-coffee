from django.db import models
from oscar.apps.voucher.abstract_models import AbstractVoucher, AbstractVoucherSet
from oscar.apps.voucher.utils import get_unused_code


class Voucher(AbstractVoucher):
    add_free_shipping = models.BooleanField(default=False)
    custom_error_message = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="This voucher is not eligible for your cart",
    )

    @property
    def benefit(self):
        """
        Returns the first offer's benefit instance.

        A voucher is commonly only linked to one offer. In that case,
        this helper can be used for convenience.
        """
        offers = self.offers.all()
        benefit = None
        if offers.count() > 0:
            offer = offers[0]
            benefit = offer.benefit
        return benefit


class VoucherSet(AbstractVoucherSet):
    add_free_shipping = models.BooleanField(default=False)
    custom_error_message = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default="This voucher is not eligible for your cart",
    )
    prefix = models.CharField(
        max_length=128,
        blank=True,
        null=True,
        help_text="Prefix for the voucher code (optional)",
    )
    suffix = models.CharField(
        max_length=128,
        blank=True,
        null=True,
        help_text="Suffix for the voucher code (optional)",
    )

    def add_new(self, **kwargs):
        code = get_unused_code(length=self.code_length)
        if self.prefix:
            code = self.prefix + code
        if self.suffix:
            code = code + self.suffix

        voucher = Voucher.objects.create(
            name=self.name,
            code=code,
            voucher_set=self,
            usage=Voucher.SINGLE_USE,
            start_datetime=self.start_datetime,
            end_datetime=self.end_datetime,
            add_free_shipping=self.add_free_shipping,
        )

        if self.offer:
            voucher.offers.add(self.offer)

        return voucher


from oscar.apps.voucher.models import *  # noqa isort:skip
