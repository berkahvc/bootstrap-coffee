from collections import namedtuple

from oscar.apps.partner.strategy import Structured as CoreStructured

# A container for policies
PurchaseInfo = namedtuple(
    "PurchaseInfo", ["price", "availability", "stockrecord", "is_on_wishlist"]
)


class Structured(CoreStructured):
    def fetch_for_product(self, product, stockrecord=None):
        """
        Return the appropriate ``PurchaseInfo`` instance.

        This method is not intended to be overridden.
        """
        if stockrecord is None:
            stockrecord = self.select_stockrecord(product)
        return PurchaseInfo(
            price=self.pricing_policy(product, stockrecord),
            availability=self.availability_policy(product, stockrecord),
            is_on_wishlist=True,
            stockrecord=stockrecord,
        )

    def fetch_for_parent(self, product):
        # Select children and associated stockrecords
        children_stock = self.select_children_stockrecords(product)

        print("OKEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE")

        return PurchaseInfo(
            price=self.parent_pricing_policy(product, children_stock),
            availability=self.parent_availability_policy(product, children_stock),
            is_on_wishlist=True,
            stockrecord=None,
        )
