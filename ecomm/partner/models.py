from oscar.apps.partner.abstract_models import AbstractStockRecord


class StockRecord(AbstractStockRecord):
    class Meta:
        index_together = [
            ["num_in_stock"],
            ["price_excl_tax"],
        ]


from oscar.apps.partner.models import *  # noqa isort:skip
