from django.contrib.gis.db.models import PointField
from django.db import models

from ecomm.timestamp_models import TimestampModel


class Stockist(TimestampModel):
    ACTIVE = "Active"
    INACTIVE = "Inactive"
    STATUS_CHOICE = ((ACTIVE, ACTIVE), (INACTIVE, INACTIVE))

    name = models.CharField(max_length=255, db_index=True)
    address = models.TextField()
    location = PointField()
    photo = models.ImageField()
    status = models.CharField(
        max_length=255, db_index=True, default=ACTIVE, choices=STATUS_CHOICE
    )
