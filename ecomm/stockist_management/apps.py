from django.apps import AppConfig


class StockistManagementConfig(AppConfig):
    name = "ecomm.stockist_management"
