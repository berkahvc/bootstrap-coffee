import re

from ecomm.common.lodi_client import LodiClient


def cleanup_name(area):
    return re.sub(r"\([^)]*\)", "", area).strip()


class ShippingUtils:
    lodi_client = LodiClient()
    cached_province_data = None
    cached_city_data = {}
    cached_district_data = {}
    cached_sub_district_data = {}
    cached_area_data = None

    def get_provinces(self):
        if self.cached_province_data:
            return self.cached_province_data

        self.cached_province_data = self.lodi_client.get_provinces()

        return self.cached_province_data

    def get_cities_by_provinces(self, province_name):
        if province_name in self.cached_city_data:
            try:
                if (
                    self.cached_city_data[province_name][0]["code"].lower()
                    != "undefined"
                ):
                    return self.cached_city_data[province_name]
            except KeyError:
                pass

        self.cached_city_data[province_name] = self.lodi_client.get_cities_by_provinces(
            province_name=cleanup_name(province_name)
        )

        return self.cached_city_data[province_name]

    def get_districts(self, province_name, city_name):
        key = f"{city_name}"

        if key in self.cached_district_data:
            try:
                if self.cached_district_data[key][0]["code"].lower() != "undefined":
                    return self.cached_district_data[key]
            except KeyError:
                pass

        self.cached_district_data[key] = self.lodi_client.get_districts(
            province_name=cleanup_name(province_name), city_name=cleanup_name(city_name)
        )

        return self.cached_district_data[key]

    def get_sub_districts(self, province_name, city_name, district_name):
        key = f"{district_name}"

        if key in self.cached_sub_district_data:
            return self.cached_sub_district_data[key]

        response = self.lodi_client.get_sub_districts(
            province_name=cleanup_name(province_name),
            city_name=cleanup_name(city_name),
            district_name=cleanup_name(district_name),
        )

        formatted_response = {}

        for data in response:
            split_data = data.split(",")
            name = split_data[0]
            zip_code = split_data[1]

            formatted_response[name] = zip_code

        self.cached_sub_district_data[key] = formatted_response

        return formatted_response

    def get_full_shipping_area(self):
        if self.cached_area_data:
            return self.cached_area_data

        formatted_data = {}

        provinces = self.get_provinces()

        for province in provinces:
            formatted_data[province] = {}

            for city in self.get_cities_by_provinces(province):
                formatted_data[province][city] = {}

                for district in self.get_districts(province, city):
                    sub_districts = self.get_sub_districts(province, city, district)

                    formatted_data[province][city][district] = sub_districts

        self.cached_area_data = formatted_data

        return formatted_data
