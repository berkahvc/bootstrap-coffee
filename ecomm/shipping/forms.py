from django import forms
from django.conf import settings
from oscar.apps.address.models import Country
from oscar.core.loading import get_model
from oscar.forms.mixins import PhoneNumberMixin

ShippingAddress = get_model("order", "ShippingAddress")


class AbstractShippingAddressForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        """
        Set fields in OSCAR_REQUIRED_ADDRESS_FIELDS as required.
        """
        super().__init__(*args, **kwargs)
        field_names = set(self.fields) & set(settings.OSCAR_REQUIRED_ADDRESS_FIELDS)
        for field_name in field_names:
            self.fields[field_name].required = True

        self.fields["country"] = forms.ModelChoiceField(
            queryset=Country.objects.filter(pk="SG").all()
        )
        self.fields["notes"].required = False
        self.fields["country"].initial = "SG"


class ShippingAddressForm(PhoneNumberMixin, AbstractShippingAddressForm):
    class Meta:
        model = ShippingAddress
        exclude = ("district",)

        widgets = {
            "first_name": forms.TextInput(
                attrs={"placeholder": "Input recipient first name"}
            ),
            "last_name": forms.TextInput(
                attrs={"placeholder": "Input recipient last name"}
            ),
            "email": forms.EmailInput(attrs={"placeholder": "Input email"}),
            "postcode": forms.TextInput(attrs={"placeholder": "Input postal code"}),
            "state": forms.TextInput(attrs={"placeholder": "Input state"}),
            "line4": forms.TextInput(attrs={"placeholder": "Input city"}),
            "line1": forms.TextInput(attrs={"placeholder": "Input address"}),
            "phone_number": forms.TextInput(
                attrs={"placeholder": "Input phone number"}
            ),
            "notes": forms.TextInput(attrs={"placeholder": "Input additional info"}),
        }

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.instance.user = user

        self.fields["country"] = forms.ModelChoiceField(
            queryset=Country.objects.filter(pk="SG").all()
        )
        self.fields["notes"].required = False
        self.fields["country"].initial = "SG"
