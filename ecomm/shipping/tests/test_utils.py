import pytest

from ecomm.shipping.utils import ShippingUtils

pytestmark = pytest.mark.django_db


def test_city_undefined():
    province_name = "BALI"
    ShippingUtils.cached_city_data[province_name] = [
        {"code": "undefined", "name": "undefined"},
        {"code": "undefined", "name": "undefined"},
        {"code": "undefined", "name": "undefined"},
        {"code": "undefined", "name": "undefined"},
    ]

    assert ShippingUtils.cached_city_data[province_name][0]["code"] == "undefined"

    ShippingUtils.cached_city_data[
        province_name
    ] = ShippingUtils.lodi_client.get_cities_by_provinces(province_name=province_name)

    assert ShippingUtils.cached_city_data[province_name][0]["code"] == "51.03"


def test_district_undefined():
    province_name = "BALI"
    city_name = "51.03"  # BADUNG's City
    ShippingUtils.cached_district_data[city_name] = [
        {"code": "undefined", "name": "undefined"},
        {"code": "undefined", "name": "undefined"},
        {"code": "undefined", "name": "undefined"},
        {"code": "undefined", "name": "undefined"},
    ]

    assert ShippingUtils.cached_district_data[city_name][0]["code"] == "undefined"

    ShippingUtils.cached_district_data[
        city_name
    ] = ShippingUtils.lodi_client.get_districts(
        province_name=province_name, city_name=city_name
    )

    assert ShippingUtils.cached_district_data[city_name][0]["code"] == "51.03.03"
