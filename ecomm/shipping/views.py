from braces.views import AjaxResponseMixin
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.views.generic import UpdateView
from oscar.core.loading import get_class, get_model

from ecomm.shipping.utils import ShippingUtils

SHIPPING_UTILS = ShippingUtils()
ShippingAddress = get_model("order", "ShippingAddress")

ShippingAddressForm = get_class("shipping.forms", "ShippingAddressForm")


@csrf_exempt
@require_http_methods(["GET"])
def get_provinces_view(_):
    response = SHIPPING_UTILS.get_provinces()

    return JsonResponse({"provinces": response})


@csrf_exempt
@require_http_methods(["GET"])
def get_cities_by_province_view(_, province_name):
    response = SHIPPING_UTILS.get_cities_by_provinces(province_name=province_name)

    return JsonResponse({"cities": response})


@csrf_exempt
@require_http_methods(["GET"])
def get_districts_view(_, province_name, city_name):
    response = SHIPPING_UTILS.get_districts(
        province_name=province_name, city_name=city_name
    )

    return JsonResponse({"districts": response})


@csrf_exempt
@require_http_methods(["GET"])
def get_sub_districts_view(_, province_name, city_name, district_name):
    response = SHIPPING_UTILS.get_sub_districts(
        province_name=province_name, city_name=city_name, district_name=district_name
    )

    return JsonResponse({"sub_districts": response})


@csrf_exempt
@require_http_methods(["GET"])
def get_full_shipping_area_view(_):
    response = SHIPPING_UTILS.get_full_shipping_area()

    return JsonResponse({"provinces": response})


class ShippingAddressDetailView(LoginRequiredMixin, AjaxResponseMixin, UpdateView):
    template_name = "pages/user-page/_edit_shipping_address.html"
    model = ShippingAddress
    form_class = ShippingAddressForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["user"] = self.request.user
        return kwargs

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx["id"] = self.kwargs.get("pk")
        return ctx

    def get_ajax(self, request, *args, **kwargs):
        shipping_address_form = ShippingAddressForm(
            instance=self.get_object(), user=self.request.user
        )
        ctx = {
            "form": shipping_address_form,
            "id": self.kwargs.get("pk"),
            "subscription_id": self.request.GET.get("subscriptionId"),
        }
        return render(self.request, "pages/user-page/_edit_shipping_address.html", ctx)

    def form_valid(self, form):
        form.save()
        messages.success(self.request, "Shipping Address Updated")
        return HttpResponseRedirect(
            reverse(
                "customer:subscription-detail",
                kwargs={"pk": self.request.GET.get("subscriptionId")},
            )
        )

    def form_invalid(self, form):
        subscription_id = self.request.GET.get("subscriptionId", None)
        messages.error(self.request, f"{form.errors}")
        if subscription_id:
            return HttpResponseRedirect(
                reverse("customer:subscription-detail", kwargs={"pk": subscription_id})
            )
        else:
            return super(ShippingAddressDetailView, self).form_invalid(form)
