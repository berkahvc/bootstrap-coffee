from django.urls import path

from .views import (
    ShippingAddressDetailView,
    get_cities_by_province_view,
    get_districts_view,
    get_full_shipping_area_view,
    get_provinces_view,
    get_sub_districts_view,
)

app_name = "custom_shipping"

urlpatterns = [
    path(
        "provinces",
        view=get_provinces_view,
        name="get_provinces_view",
    ),
    path(
        "provinces/<str:province_name>/cities",
        view=get_cities_by_province_view,
        name="get_cities_by_province_view",
    ),
    path(
        "provinces/<str:province_name>/cities/<str:city_name>/districts",
        view=get_districts_view,
        name="get_districts_view",
    ),
    path(
        "provinces/<str:province_name>/cities/<str:city_name>/districts/<str:district_name>/sub-districts",
        view=get_sub_districts_view,
        name="get_sub_districts_view",
    ),
    path(
        "areas",
        view=get_full_shipping_area_view,
        name="get_full_shipping_area_view",
    ),
    path(
        "<int:pk>/edit/",
        view=ShippingAddressDetailView.as_view(),
        name="shipping_address_detail",
    ),
]
