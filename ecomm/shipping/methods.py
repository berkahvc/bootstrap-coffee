from django.utils.translation import gettext_lazy as _
from oscar.apps.shipping.methods import FixedPrice, Free
from oscar.core import prices


class FreeDelivery(Free):
    code = "free-delivery"
    name = _("Free Delivery")

    charge_excl_tax = 0
    charge_incl_tax = 0

    def to_dict(self):
        return {
            "charge_excl_tax": 0,
            "charge_incl_tax": 0,
        }

    def calculate(self, basket):
        return prices.Price(
            currency=basket.currency,
            excl_tax=self.charge_excl_tax,
            incl_tax=self.charge_incl_tax,
        )


class DefaultDelivery(FixedPrice):
    """
    This shipping method indicates that shipping costs a fixed price and
    requires no special calculation.
    """

    code = "default-delivery"
    name = _("Shipping Delivery")

    # Charges can be either declared by subclassing and overriding the
    # class attributes or by passing them to the constructor
    charge_excl_tax = 5
    charge_incl_tax = 5

    def __init__(
        self,
        charge_excl_tax: int = None,
        charge_incl_tax: int = None,
    ):
        if charge_excl_tax is not None:
            self.charge_excl_tax = charge_excl_tax
        if charge_incl_tax is not None:
            self.charge_incl_tax = charge_incl_tax

    def calculate(self, basket):
        return prices.Price(
            currency=basket.currency,
            excl_tax=self.charge_excl_tax,
            incl_tax=self.charge_incl_tax,
        )

    def to_dict(self):
        return {
            "charge_excl_tax": self.charge_excl_tax,
            "charge_incl_tax": self.charge_incl_tax,
        }
