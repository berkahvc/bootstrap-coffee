from oscar.apps.shipping.repository import Repository as CoreRepository
from oscar.core.loading import get_classes

from ecomm.shipping.methods import DefaultDelivery, FreeDelivery

(
    Free,
    NoShippingRequired,
    TaxExclusiveOfferDiscount,
    TaxInclusiveOfferDiscount,
) = get_classes(
    "shipping.methods",
    [
        "Free",
        "NoShippingRequired",
        "TaxExclusiveOfferDiscount",
        "TaxInclusiveOfferDiscount",
    ],
)


class Repository(CoreRepository):
    methods = (DefaultDelivery(),)
    free_methods = (FreeDelivery(),)

    def get_free_shipping_method(self, basket, shipping_addr=None, **kwargs):
        """
        Return a 'default' shipping method to show on the basket page to give
        the customer an indication of what their order will cost.
        """
        return self.free_methods[0]
