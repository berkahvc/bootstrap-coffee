import datetime
import logging

from django.core.mail import EmailMessage
from django.utils.timezone import localtime, timedelta

from config import celery_app
from ecomm.catalogue.models import Product
from ecomm.checkout.tasks import send_email_with_rate_limit
from ecomm.order.enums import OrderStatus
from ecomm.order.utils import generate_customer_list

from .models import Order

logger = logging.getLogger(__name__)


@celery_app.task()
def send_orders_to_optimo(**kwargs):
    tomorrow = localtime().date() + timedelta(days=1)
    orders = Order.objects.filter(delivery_date=tomorrow)

    for order in orders:
        if order.status == OrderStatus.PAID.value:
            order.send_order_to_optimo_route()


@celery_app.task()
def send_email_for_bon_bons():
    """
    Send email to provider of bon bons. The delivery date is calculated to skip Sundays.
    """
    chocolate_product = Product.objects.filter(
        title__contains="Chocolate and Coffee Gift Set"
    ).first()
    try:
        variant_x6 = Product.objects.get(parent=chocolate_product, title="6 Pieces")
        variant_x12 = Product.objects.get(parent=chocolate_product, title="12 Pieces")
    except Product.DoesNotExist:
        logger.error(
            "Could not find chocolate and coffee gift set. Check if product exists."
        )
        return

    chocolate_delivery_day = localtime().date()
    today = localtime().date().weekday()

    # Because Sundays are skipped, we need to add 1 day to the delivery day
    # Skipping Sundah email because it is the same as Saturday's
    if 0 <= today <= 2:
        chocolate_delivery_day += timedelta(days=3)
    elif 3 <= today <= 5:
        chocolate_delivery_day += timedelta(days=4)

    orders = Order.objects.filter(
        lines__product__in=[variant_x6, variant_x12],
        delivery_date=chocolate_delivery_day,
    ).distinct()

    x6 = 0
    x12 = 0
    if orders.count() > 0:
        for order in orders:
            x6_qty = (
                order.lines.filter(product=variant_x6).first().quantity
                if order.lines.filter(product=variant_x6).first()
                else 0
            )
            x12_qty = (
                order.lines.filter(product=variant_x12).first().quantity
                if order.lines.filter(product=variant_x12).first()
                else 0
            )
            x6 += x6_qty
            x12 += x12_qty

        subject = (
            f"[New Order - Delivery Date: {chocolate_delivery_day.strftime('%d/%m/%Y')}]"
            " - Chocolate Bon Bon And Cold Brew Gift Set | Bootstrap Cold Brew Coffee"
        )

        send_email = [
            "hello@mrbucket.com.sg",
            "info@bootstrapbeverages.com",
            "joy.yau@bootstrapbeverages.com",
            "william@bootstrapbeverages.com",
            "shop@bootstrapbeverages.com",
            "william@berkah.vc",
        ]
        from_email = "info@bootstrapbeverages.com"
        body = f"""<p>Hi friends at Mr Bucket,</p>
        <p>We will like to place an order for the following:</p>

        <p>Delivery Date: {chocolate_delivery_day.strftime('%d/%m/%Y')}</p>

        <p>Qty & Product:</p>
        <p>{x6} x Chocolate & Cold Brew Gift Set (6pcs)<br>
        {x12} x Chocolate & Cold Brew Gift Set (12pcs)</p>
        <br>
        <br>

        <p>--<br>
        Best Regards & Thanks,<br>
        <br>
        Team Bootstrap</p>
        <img src="https://storage.googleapis.com/berkah-bootstrap/static/images/boot/bootstrap-beverages-logo.png"
        alt="bootstrap-logo">
        """

        for email in send_email:
            send_email_with_rate_limit.delay(
                subject=subject, to=email, from_email=from_email, body=body
            )


@celery_app.task()
def send_customer_list_email_last_month(**kwargs):
    """Task for sending customer list generated in generate_customer_list"""
    last_month = datetime.datetime.now() - datetime.timedelta(days=1)
    subject = f"Bootstrap Customer List {last_month.month}/{last_month.year}"
    filename = f"{subject}.xlsx"
    email_to = kwargs.get("email_to")

    df = generate_customer_list(month=last_month.month, year=last_month.year)
    df.to_excel(f"{subject}.xlsx")

    email = EmailMessage(
        subject=subject,
        body=f"Attached is the customer list for last month ({last_month.month}/{last_month.year})",
        from_email="no-reply@bootstrapbeverages.com",
        to=email_to,
    )
    email.attach(
        filename=filename,
        content=open(filename, "rb").read(),
        mimetype="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    )
    email.send()
