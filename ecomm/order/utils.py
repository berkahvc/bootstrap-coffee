import logging
import re
from datetime import datetime
from decimal import Decimal as D
from typing import Tuple

import pandas as pd
from django.conf import settings
from django.contrib.sites.models import Site
from django.db import transaction
from django.db.models import F, Sum, Window
from django.template.loader import render_to_string
from django.utils.translation import gettext_lazy as _
from djstripe.models import Customer as DjStripeCustomer
from djstripe.models import Subscription as DjStripeSubscription
from oscar.apps.order.signals import order_placed
from oscar.apps.order.utils import OrderCreator as CoreOrderCreator
from oscar.apps.order.utils import OrderNumberGenerator as CoreOrderNumberGenerator
from oscar.apps.payment.models import Source, SourceType
from oscar.core.loading import get_model

from weasyprint import HTML

from ecomm.catalogue.models import Product
from ecomm.checkout.utils import create_order_number
from ecomm.payment.enums import AvailablePayment
from ecomm.schedule_management.models import DeliveryTime
from ecomm.subscription_management.utils import create_product_subscription
from ecomm.utils.utils import get_delivery_time

logger = logging.getLogger("ecomm.order.utils")

Order = get_model("order", "Order")
OrderNote = get_model("order", "OrderNote")
Line = get_model("order", "Line")
Surcharge = get_model("order", "Surcharge")
SubscriptionPrepaid = get_model("subscription_management", "SubscriptionPrepaid")


class OrderNumberGenerator(CoreOrderNumberGenerator):
    def order_number(self, basket=None, date=None):
        return create_order_number()


class OrderCreator(CoreOrderCreator):
    def place_order(
        self,
        basket,
        total,  # noqa (too complex (12))
        shipping_method,
        shipping_charge,
        user=None,
        shipping_address=None,
        billing_address=None,
        order_number=None,
        status=None,
        request=None,
        surcharges=None,
        **kwargs,
    ):
        """
        Placing an order involves creating all the relevant models based on the
        basket and session data.
        """
        if basket.is_empty:
            raise ValueError(_("Empty baskets cannot be submitted"))
        if not order_number:
            generator = OrderNumberGenerator()
            order_number = generator.order_number(basket)
        if not status and hasattr(settings, "OSCAR_INITIAL_ORDER_STATUS"):
            status = getattr(settings, "OSCAR_INITIAL_ORDER_STATUS")

        if Order._default_manager.filter(number=order_number).exists():
            raise ValueError(
                _("There is already an order with number %s") % order_number
            )

        with transaction.atomic():

            kwargs["surcharges"] = surcharges
            # Ok - everything seems to be in order, let's place the order
            order = self.create_order_model(
                user,
                basket,
                shipping_address,
                shipping_method,
                shipping_charge,
                billing_address,
                total,
                order_number,
                status,
                request,
                **kwargs,
            )
            for line in basket.all_lines():
                extra_line_fields = {
                    "interval": line.interval,
                    "interval_count": line.interval_count,
                    "delivery_prepaid": line.delivery_prepaid,
                    "subscription_type": line.subscription_type,
                    "credit_per_delivery": line.credit_per_delivery,
                    "pack_size": line.pack_size,
                    "is_subscription": line.is_subscription,
                    "partner_line_notes": line.product.description,
                }
                self.create_line_models(
                    order, line, extra_line_fields=extra_line_fields
                )
                self.update_stock_records(line)

            for voucher in basket.vouchers.select_for_update():
                if not voucher.is_active():  # basket ignores inactive vouchers
                    basket.vouchers.remove(voucher)
                else:
                    available_to_user, msg = voucher.is_available_to_user(user=user)
                    if not available_to_user:
                        raise ValueError(msg)

            # Record any discounts associated with this order
            for application in basket.offer_applications:
                # Trigger any deferred benefits from offers and capture the
                # resulting message
                application["message"] = application["offer"].apply_deferred_benefit(
                    basket, order, application
                )
                # Record offer application results
                if application["result"].affects_shipping:
                    # Skip zero shipping discounts
                    shipping_discount = shipping_method.discount(basket)
                    if shipping_discount <= D("0.00"):
                        continue
                    # If a shipping offer, we need to grab the actual discount off
                    # the shipping method instance, which should be wrapped in an
                    # OfferDiscount instance.
                    application["discount"] = shipping_discount
                self.create_discount_model(order, application)
                self.record_discount(application)

            for voucher in basket.vouchers.all():
                self.record_voucher_usage(order, voucher, user)

        # Send signal for analytics to pick up
        order_placed.send(sender=self, order=order, user=user)

        return order

    def create_order_model(
        self,
        user,
        basket,
        shipping_address,
        shipping_method,
        shipping_charge,
        billing_address,
        total,
        order_number,
        status,
        request=None,
        surcharges=None,
        **extra_order_fields,
    ):
        """Create an order model."""

        try:
            shipping_method_details = shipping_method.to_dict()
        except Exception as err:
            print(f"===== {err}")
            shipping_method_details = {
                "charge_excl_tax": f"{float(shipping_charge.excl_tax)}",
                "charge_incl_tax": f"{float(shipping_charge.incl_tax)}",
            }

        order_data = {
            "basket": basket,
            "number": order_number,
            "currency": total.currency,
            "total_incl_tax": total.incl_tax,
            "total_excl_tax": total.excl_tax,
            "shipping_incl_tax": shipping_charge.incl_tax,
            "shipping_excl_tax": shipping_charge.excl_tax,
            "shipping_method": shipping_method.name,
            "shipping_code": shipping_method.code,
            "shipping_method_details": shipping_method_details,
        }

        if shipping_address:
            order_data["shipping_address"] = shipping_address
        if billing_address:
            order_data["billing_address"] = billing_address
        if user and user.is_authenticated:
            order_data["user_id"] = user.id
        if status:
            order_data["status"] = status
        if extra_order_fields:
            order_data.update(extra_order_fields)
        if "site" not in order_data:
            order_data["site"] = Site._default_manager.get_current(request)

        order = Order(**order_data)
        order.save()

        if surcharges is not None:
            for charge in surcharges:
                Surcharge.objects.create(
                    order=order,
                    name=charge.surcharge.name,
                    code=charge.surcharge.code,
                    excl_tax=charge.price.excl_tax,
                    incl_tax=charge.price.incl_tax,
                )
        return order


def generate_order_manual(subscription_prepaid, delivery_date: datetime = None) -> None:
    """
    Function to generate a manual order for a subscription_prepaid

    :param subscription_prepaid: SubscriptionPrepaid object
    :param delivery_date: datetime object
    """
    djstripe_customer = DjStripeCustomer.objects.get(
        id=subscription_prepaid.djcustomer_id
    )
    djstripe_subscription = DjStripeSubscription.objects.get(
        id=subscription_prepaid.djsubscription_id
    )
    amount = djstripe_subscription.plan.amount
    currency = djstripe_subscription.plan.currency
    djstripe_product_name = djstripe_subscription.plan.product.name

    if "subscription" in djstripe_product_name:
        try:
            product = Product.objects.get(title=djstripe_product_name)
        except Product.DoesNotExist:
            product = create_product_subscription(
                title=djstripe_product_name,
            )
    else:
        shopify_product_id = re.search(r"-(\d{10,17})", djstripe_product_name).group(1)
        shopify_variant_id = re.search(r"-(\d{10,17})$", djstripe_product_name).group(1)
        product = Product.objects.get(
            shopify_product_id=shopify_product_id,
            shopify_variant_id=shopify_variant_id,
        )

    user = djstripe_customer.subscriber
    delivery_time = get_delivery_time(subscription_prepaid.delivery_time)

    order_q = {
        "user": user,
        "delivery_time_id": delivery_time.id,
        "delivery_day": subscription_prepaid.delivery_day,
        "subscription_id": subscription_prepaid.djsubscription_id,
    }
    if delivery_date:
        order_q["delivery_date"] = delivery_date
    else:
        order_q["delivery_date"] = subscription_prepaid.next_delivery_date if subscription_prepaid.next_delivery_date \
            else datetime.today()

    order_data = {
        "number": create_order_number(),
        "currency": currency.upper(),
        "shipping_address": subscription_prepaid.shipping_address,
        "total_incl_tax": amount,
        "total_excl_tax": amount,
        "shipping_method": "Shipping Delivery",
        "shipping_code": "default-delivery",
        "status": "PAID",
        "site": Site.objects.first(),
        "shipping_method_details": {"":""},
    }

    order, order_created = Order.objects.get_or_create(**order_q, defaults=order_data)

    if order_created:
        stock_record = product.stockrecords.all().first()

        item_data = {
            "order": order,
            "product": product,
            "title": product.title,
            "partner": stock_record.partner,
            "partner_name": stock_record.partner.name,
            "partner_sku": stock_record.partner_sku,
            "partner_line_notes": subscription_prepaid.note
            if subscription_prepaid.note
            else subscription_prepaid.description,
            "stockrecord": stock_record,
            "quantity": subscription_prepaid.quantity,
            "line_price_incl_tax": stock_record.price_excl_tax
            * int(subscription_prepaid.quantity),
            "line_price_excl_tax": stock_record.price_excl_tax
            * int(subscription_prepaid.quantity),
            "line_price_before_discounts_incl_tax": stock_record.price_excl_tax
            * int(subscription_prepaid.quantity),
            "line_price_before_discounts_excl_tax": stock_record.price_excl_tax
            * int(subscription_prepaid.quantity),
            "credit_per_delivery": int(subscription_prepaid.pack_size) / 6,
            "pack_size": subscription_prepaid.pack_size,
            "is_subscription": True,
        }

        Line.objects.create(**item_data)
        source_type, _ = SourceType.objects.get_or_create(
            name=AvailablePayment.STRIPE.value
        )
        Source.objects.create(
            order=order,
            source_type=source_type,
            amount_allocated=amount,
            amount_debited=amount,
            reference=subscription_prepaid.djsubscription_id,
        )


def fix_order_notes(start_date: str):
    """
    :param start_date: date string with format YYYY-mm-dd
    :return:
    """
    start_date_fmt = datetime.strptime(start_date, "%Y-%m-%d")
    orders = Order.objects.filter(date_placed__gte=start_date_fmt)

    for order in orders:
        sources = Source.objects.filter(order=order)
        for line in order.lines.all():
            if not line.partner_line_notes and line.is_subscription:
                for source in sources:
                    if "sub" in source.reference:
                        try:
                            sp = SubscriptionPrepaid.objects.get(
                                djsubscription_id=source.reference
                            )
                            line.partner_line_notes = (
                                sp.note if sp.note else sp.description
                            )
                            line.save()
                        except SubscriptionPrepaid.DoesNotExist:
                            logger.error(
                                f"SubscriptionPrepaid {source.reference} not found"
                            )
            elif not line.partner_line_notes and not line.is_subscription:
                line.partner_line_notes = line.product.__str__()
                line.save()


def create_order(
    delivery_time: DeliveryTime,
    subscription: DjStripeSubscription,
    subscription_prepaid: SubscriptionPrepaid,
):
    """
    :param subscription: DjStripeSubscription Object
    :param subscription_prepaid: SubscriptionPrepaid Object
    :param delivery_time: DeliveryTime Object
    :return:
    """

    source_type, _ = SourceType.objects.get_or_create(
        name=AvailablePayment.STRIPE.value
    )
    amount = subscription.plan.amount
    order_number = create_order_number()

    djstripe_product_name = subscription.plan.product.name
    if "subscription" in djstripe_product_name or "sub" in djstripe_product_name:
        try:
            product = Product.objects.get(title=subscription.plan.product.name)
        except Product.DoesNotExist:
            product = create_product_subscription(
                title=subscription.plan.product.name,
            )
    else:
        shopify_product_id = re.search(r"-(\d{10,17})", djstripe_product_name).group(1)
        shopify_variant_id = re.search(r"-(\d{10,17})$", djstripe_product_name).group(1)
        product = Product.objects.get(
            shopify_product_id=shopify_product_id,
            shopify_variant_id=shopify_variant_id,
        )

    stock_record = product.stockrecords.all().first()

    order_q = {
        "user": subscription_prepaid.user,
        "delivery_date": subscription_prepaid.delivery_date,
        "delivery_time_id": delivery_time.id,
        "delivery_day": subscription_prepaid.delivery_day,
        "subscription_id": subscription_prepaid.djsubscription_id,
    }

    order_data = {
        "number": order_number,
        "currency": subscription.plan.currency.upper(),
        "shipping_address": subscription_prepaid.shipping_address,
        "total_incl_tax": amount,
        "total_excl_tax": amount,
        "shipping_method": "Shipping Delivery",
        "shipping_code": "default-delivery",
        "status": "PAID",
    }

    order, order_created = Order.objects.get_or_create(**order_q, defaults=order_data)

    if order_created:
        OrderNote.objects.create(
            order=order,
            user=subscription_prepaid.user,
            note_type="Info",
            message=product.description
            if "auto renew" not in product.description.lower()
            else "",
        )

        item_data = {
            "order": order,
            "product": product,
            "title": product.title,
            "partner": stock_record.partner,
            "partner_name": stock_record.partner.name,
            "partner_sku": stock_record.partner_sku,
            "partner_line_notes": subscription_prepaid.note
            if subscription_prepaid.note
            else subscription_prepaid.description,
            "stockrecord": stock_record,
            "quantity": subscription_prepaid.quantity,
            "line_price_incl_tax": stock_record.price_excl_tax
            * int(subscription_prepaid.quantity),
            "line_price_excl_tax": stock_record.price_excl_tax
            * int(subscription_prepaid.quantity),
            "line_price_before_discounts_incl_tax": stock_record.price_excl_tax
            * int(subscription_prepaid.quantity),
            "line_price_before_discounts_excl_tax": stock_record.price_excl_tax
            * int(subscription_prepaid.quantity),
            "credit_per_delivery": int(subscription_prepaid.pack_size) / 6,
            "pack_size": subscription_prepaid.pack_size,
            "is_subscription": True,
        }

        Line.objects.create(**item_data)
        Source.objects.create(
            order=order,
            source_type=source_type,
            amount_allocated=amount,
            amount_debited=amount,
            reference=subscription_prepaid.djsubscription_id,
        )

    return order


def extract_product_name_and_qty(item) -> Tuple[str, int]:
    """
    Get the subscription prepaid model and extract the product and amount

    :param item: string with format "(qty)x product"
    :return: Product name and quantity
    """

    if re.search(r"\d+x", string=item):
        total_amount = item.split("x")[0]
        product_name = extract_product_name(item.split("x")[1].strip())
    else:
        total_amount = re.search(r"\d+", item).group(0)
        product_name = extract_product_name(item.split("-")[0].strip())

    return product_name, int(total_amount)


def extract_product_name(product_name: str) -> str:
    """
    Change the product name to a machine-readable name

    :param product_name:  Product name
    :return: Machine readable product name
    """
    product_name_lower = product_name.lower()

    if "black" in product_name_lower:
        return "Cold Brew Black"
    elif "strong" in product_name_lower:
        return "Cold Brew Strong"
    elif "oat" in product_name_lower:
        return "Cold Brew Oat Milk"
    elif "manuka" in product_name_lower:
        return "Cold Brew Milk & Manuka Honey"
    elif "hojicha" in product_name_lower:
        return "Cold Brew Hojicha Tea"
    elif "mocha" in product_name_lower:
        return "Cold Brew Mocha"
    elif "rooibos" in product_name_lower:
        return "Cold Brew Rooibos Orange"
    else:
        return product_name


def generate_customer_list(month: int, year: int = datetime.now().year) -> pd.DataFrame:
    """
    Generate a list of customers based on the number of customers in the month and year

    :param month: Month to generate the list
    :param year: Year to generate the list
    :return: Pandas DataFrame with the list of customers
    """
    orders = Order.objects.filter(date_placed__month=month, date_placed__year=year)
    annotation = orders.annotate(
        spending=Window(
            expression=Sum("total_excl_tax"),
            partition_by=[
                F("shipping_address__email"),
            ],
        )
    )

    customers = annotation.values_list(
        "shipping_address__first_name",
        "shipping_address__last_name",
        "shipping_address__email",
        "shipping_address__phone_number",
        "total_excl_tax",
    ).distinct()

    df = pd.DataFrame(customers)
    df.columns = ["First Name", "Last Name", "Email", "Phone Number", "Total Spending"]

    return df


def generate_subscriber_list() -> pd.DataFrame:
    """
    Generate a list of subscribers based on the number of customers in the month and year

    :return: Pandas DataFrame with the list of customers
    """
    orders = SubscriptionPrepaid.objects.all()

    customers = orders.values_list(
        "shipping_address__first_name",
        "shipping_address__last_name",
        "shipping_address__email",
        "shipping_address__phone_number",
        "status"
    ).distinct()

    df = pd.DataFrame(customers)
    df.columns = ["First Name", "Last Name", "Email", "Phone Number", "Status"]

    return df


def create_label(delivery_date: str = None, order_list: str = None, driver_name: str = None):
    if delivery_date:
        orders = Order.objects.filter(delivery_date=delivery_date)
    elif order_list:
        order_list = order_list.replace(" ", ",")
        orders = Order.objects.filter(number__in=order_list.strip().split(","))
    else:
        return

    all_orders = []

    for order in orders:
        order_number = order.number
        recipient_name = order.shipping_address.name.upper()
        address = f"{order.shipping_address.active_address_fields()[0]}"
        postcode = f"Singapore {order.shipping_address.active_address_fields()[3]}"
        is_subscription = True if order.subscription_id else False
        line_items = []  # This is for sku. Format {qty}x {sku}
        item_list = []  # This is for the list of item (subscription content if it's a subscription)

        for line in order.lines.all():
            # SKU is for the summary of the order. Subscription will be replaced with "SUBSCRIPTION"
            sku_name = line.get_title()

            item_name = (
                f"{line.partner_line_notes}"
                if line.partner_line_notes
                else line.product.parent.title
            )

            # strip all unused item description
            item_name = re.sub(r"Cold Brew (Coffee|Tea)", "", item_name)
            item_name = item_name.replace("Cold Brew", "")
            item_name = item_name.replace("Subscription", "")
            item_name = item_name.replace("-", "").strip()

            line_items.append(f"{ line.get_line_qty_for_label() }x { sku_name }")
            item_list.append(item_name)

        all_orders.append({
            "order_number": order_number,
            "recipient_name": recipient_name,
            "address":  address,
            "postcode": postcode,
            "is_subscription": is_subscription,
            "line_items": line_items,
            "total_qty": order.total_sixpack(),
            "item_list": item_list,
        })

    context = {"orders": all_orders}

    if driver_name:
        filename = f"boot/Labels for {driver_name}.pdf"
    else:
        filename = f"boot/Labels for {delivery_date}.pdf"

    html = render_to_string("orders/documents/label.html", context)
    HTML(string=html).write_pdf(filename)

    return filename


def convert_bottle_to_qty(line_item: Line, no_of_bottles: int) -> int:
    """
    Calculate the total number of bottles

    :param line_item: The line item object from the Order model
    :param no_of_bottles: The total number of bottles in the order
    :return: Total number of bottles
    """

    if line_item.partner_sku == "MIXPACK":
        return line_item.quantity
    elif line_item.partner_sku == "ZW5Q2":
        return line_item.quantity * 2
    else:
        return int(
            line_item.quantity * int(no_of_bottles) / 6 if int(no_of_bottles) % 6 == 0 else line_item.quantity
        )
