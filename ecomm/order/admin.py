from django.contrib import admin
from django.shortcuts import render
from oscar.apps.order.admin import OrderAdmin
from oscar.core.loading import get_model
from rangefilter.filters import DateRangeFilter

from ecomm.checkout.utils import send_email_for_paid_order

from .forms import RescheduleOrderDateAdminForm

Order = get_model("order", "Order")


def resend_order_email(modeladmin, request, queryset):
    for order in queryset:
        send_email_for_paid_order(order)


resend_order_email.short_description = "Resend order email"


def reschedule_order_date(modeladmin, request, queryset):
    ids = list(set(queryset.values_list("pk", flat=True)))
    ctx = {
        "title": "Reschedule Order Date",
        "form": RescheduleOrderDateAdminForm(
            initial={"order_ids": ",".join([str(_id) for _id in ids])}
        ),
    }
    return render(request, "admin/reschedule_order_date_form.html", ctx)


reschedule_order_date.short_description = "Reschedule order date"


class OrderAdminNew(OrderAdmin):
    model = Order
    list_display = (
        "number",
        "total_incl_tax",
        "user",
        "date_placed",
        "delivery_date",
        "delivery_time",
        "subscription_id",
    )
    list_filter = (
        ("delivery_date", DateRangeFilter),
        "date_placed",
        "delivery_date",
        "delivery_time",
    )
    search_fields = (
        "number",
        "billing_address__first_name",
        "billing_address__last_name",
        "user__email",
    )
    readonly_fields = [
        "currency",
        "number",
        "total_excl_tax",
        "shipping_incl_tax",
        "shipping_excl_tax",
        "total_discount_excl_tax",
    ]

    fieldsets = (
        (
            "Order info",
            {
                "fields": (
                    "user",
                    "number",
                    "date_placed",
                    "delivery_date",
                    "delivery_time",
                    "delivery_day",
                    "subscription_id",
                    "guest_email",
                    "status",
                    "shipping_address",
                )
            },
        ),
        (
            "Order Pricing",
            {
                "fields": (
                    "currency",
                    "total_incl_tax",
                    "total_excl_tax",
                    "shipping_incl_tax",
                    "shipping_excl_tax",
                    "total_discount_excl_tax",
                )
            },
        ),
        (
            "Misc. Info",
            {
                "fields": (
                    "site",
                    "basket",
                    "shipping_method",
                    "shipping_method_details",
                    "is_from_shopify",
                )
            },
        ),
    )

    actions = [resend_order_email, reschedule_order_date]


admin.site.unregister(Order)
admin.site.register(Order, OrderAdminNew)
