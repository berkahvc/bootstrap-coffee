# Generated by Django 3.0.11 on 2021-02-25 08:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0012_auto_20210218_2353'),
    ]

    operations = [
        migrations.AddField(
            model_name='shippingaddress',
            name='district',
            field=models.CharField(blank=True, db_index=True, max_length=255, null=True, verbose_name='District'),
        ),
    ]
