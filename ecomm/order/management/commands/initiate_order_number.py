from constance import config
from django.conf import settings
from django.core.management.base import BaseCommand

# ./manage.py initiate_order_number


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        config.LAST_COUNTER = settings.LAST_COUNTER_DEFAULT
        config.LAST_ORDER_DATE = settings.LAST_ORDER_DATE_DEFAULT
        print("Success reset order number")
        print("LAST_COUNTER is", config.LAST_COUNTER)
        print("LAST_ORDER_DATE is", config.LAST_ORDER_DATE)
