from datetime import datetime

from django.core.management.base import BaseCommand
from django.utils.timezone import localdate, timedelta
from djstripe.models import Customer as DjStripeCustomer
from djstripe.models import Subscription as DjStripeSubscription
from oscar.apps.payment.models import Source, SourceType

from ecomm.catalogue.models import Product
from ecomm.order.models import Line, Order
from ecomm.payment.enums import AvailablePayment
from ecomm.schedule_management.models import DeliveryTime
from ecomm.subscription_management.models import SubscriptionPrepaid

# ./manage.py generate_order_for_subscription


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        today = localdate()
        tomorrow = today + timedelta(days=1)
        # delivery_day = tomorrow.strftime("%A")
        # djstripe_subscriptions = DjStripeSubscription.objects.values_list(
        #     "id", flat=True
        # ).filter(status="active") | DjStripeSubscription.objects.values_list(
        #     "id", flat=True
        # ).filter(
        #     status="trialing"
        # )

        # only for Wednesday of Mar 2nd
        subscription_prepaid_list = (
            SubscriptionPrepaid.objects.filter(delivery_date="2022-03-4")
            | SubscriptionPrepaid.objects.filter(
                delivery_date="2022-02-25", delivery_frequency=1
            )
            | SubscriptionPrepaid.objects.filter(
                delivery_date="2022-02-18", delivery_frequency=2
            )
            | SubscriptionPrepaid.objects.filter(
                delivery_date="2022-02-11", delivery_frequency=3
            )
            | SubscriptionPrepaid.objects.filter(
                delivery_date="2022-02-04", delivery_frequency=4
            )
        )

        print(f"===== total {subscription_prepaid_list.count()}")

        for subscription_prepaid in subscription_prepaid_list:
            print(f"===== subscription_id {subscription_prepaid.djsubscription_id}")
            orders = Order.objects.filter(number__startswith="#4").order_by("-id")
            last_order = None
            if orders.exists():
                last_order = orders.first()

            if not last_order:
                order_number = "#40001"
            else:
                last_order_number = int(last_order.number.replace("#", "")) + 1
                order_number = f"#{last_order_number}"

            djstripe_customer = DjStripeCustomer.objects.get(
                id=subscription_prepaid.djcustomer_id
            )
            djstripe_subcription = DjStripeSubscription.objects.get(
                id=subscription_prepaid.djsubscription_id
            )
            amount = djstripe_subcription.plan.amount
            currency = djstripe_subcription.plan.currency
            description = subscription_prepaid.description.strip()
            description_split = description.split(" - ")

            if len(description_split) > 2:
                parent_title = " - ".join(
                    [description_split[0].strip(), description_split[1].strip()]
                )
                child_title = description_split[2].strip()
            else:
                parent_title = description_split[0].strip()
                child_title = description_split[1].strip()

            user = djstripe_customer.subscriber

            order_data = {
                "currency": currency,
                "user": user,
                "shipping_address": subscription_prepaid.shipping_address,
                "shipping_method": "Free Delivery",
                "shipping_code": "free-delivery",
                "total_incl_tax": amount,
                "total_excl_tax": amount,
                "status": "PAID",
            }

            delivery_time_str = subscription_prepaid.delivery_time
            delivery_time_str_split = delivery_time_str.split(" - ")

            try:
                delivery_time_start = datetime.strptime(
                    delivery_time_str_split[0].strip(), "%I:%M %p"
                ).strftime("%H:%M")
            except ValueError:
                delivery_time_start = datetime.strptime(
                    delivery_time_str_split[0].strip(), "%H:%M %p"
                ).strftime("%H:%M")

            try:
                delivery_time_end = datetime.strptime(
                    delivery_time_str_split[1].strip(), "%I:%M %p"
                ).strftime("%H:%M")
            except ValueError:
                delivery_time_end = datetime.strptime(
                    delivery_time_str_split[1].strip(), "%H:%M %p"
                ).strftime("%H:%M")

            obj_dt = DeliveryTime.objects.filter(
                start=delivery_time_start, end=delivery_time_end
            )
            dt = None
            if obj_dt.exists():
                dt = obj_dt.first()

            order_obj, created_order = Order.objects.get_or_create(
                **{"number": order_number}, defaults=order_data
            )

            order_obj.delivery_date = (
                subscription_prepaid.next_delivery_date
                if subscription_prepaid.next_delivery_date
                else tomorrow
            )

            if dt:
                order_obj.delivery_time_id = dt.id

            order_obj.delivery_day = subscription_prepaid.delivery_day
            order_obj.save()

            product_parent = Product.objects.get(title__iexact=parent_title)
            product_child = Product.objects.get(
                parent=product_parent, title__icontains=child_title
            )
            stock_record = product_child.stockrecords.all().first()

            default_item_data = {
                "order": order_obj,
                "product": product_child,
            }

            item_data = {
                "title": child_title,
                "partner": stock_record.partner,
                "partner_name": stock_record.partner.name,
                "partner_sku": stock_record.partner_sku,
                "partner_line_notes": subscription_prepaid.note if subscription_prepaid.note else subscription_prepaid.description,
                "stockrecord": stock_record,
                "quantity": subscription_prepaid.quantity,
                "line_price_incl_tax": stock_record.price_excl_tax
                * int(subscription_prepaid.quantity),
                "line_price_excl_tax": stock_record.price_excl_tax
                * int(subscription_prepaid.quantity),
                "line_price_before_discounts_incl_tax": stock_record.price_excl_tax
                * int(subscription_prepaid.quantity),
                "line_price_before_discounts_excl_tax": stock_record.price_excl_tax
                * int(subscription_prepaid.quantity),
                "credit_per_delivery": int(subscription_prepaid.pack_size) / 6,
                "pack_size": subscription_prepaid.pack_size,
                "is_subscription": True,
            }

            Line.objects.get_or_create(**default_item_data, defaults=item_data)
            source_type, _ = SourceType.objects.get_or_create(
                name=AvailablePayment.STRIPE.value
            )
            Source.objects.create(
                order=order_obj,
                source_type=source_type,
                amount_allocated=stock_record.price_excl_tax
                * int(subscription_prepaid.quantity),
                amount_debited=stock_record.price_excl_tax
                * int(subscription_prepaid.quantity),
                reference=subscription_prepaid.djsubscription_id,
            )

            subscription_prepaid.next_delivery_date = tomorrow + timedelta(
                weeks=int(subscription_prepaid.delivery_frequency)
            )
            subscription_prepaid.save()
