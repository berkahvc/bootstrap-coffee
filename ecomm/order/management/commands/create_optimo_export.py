import pandas as pd
from django.core.management.base import BaseCommand
from django.http import HttpResponse
from io import BytesIO
from django.utils.timezone import localdate, timedelta

from ecomm.order.models import Order


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        with BytesIO() as b:

            # Use the StringIO object as the filehandle.
            order_number = []
            delivery = []
            shipping1 = []
            shipping2 = []
            name = []
            tw_from = []
            tw_to = []
            handling = []
            email = []
            phone = []
            notes = []

            orders = Order.objects.filter(delivery_date=localdate() + timedelta(days=1))

            for order in orders:
                order_number.append(order.number)
                delivery.append(order.delivery_date)
                shipping1.append(
                    f"Singapore {order.shipping_address.active_address_fields()[3]}"
                )
                shipping2.append(order.shipping_address.line1)
                name.append(order.shipping_address.name)
                tw_from.append(order.delivery_time.start)
                tw_to.append(order.delivery_time.end)
                handling.append("")
                email.append(order.shipping_address.email)
                phone.append(order.shipping_address.phone_number)
                notes.append("")

            df = pd.DataFrame(
                {
                    "Order": order_number,
                    "Attribute 'Delivery-Date'": delivery,
                    "Shipping Address 1": shipping1,
                    "Shipping Address 2": shipping2,
                    "Shipping Name": name,
                    "TW From": tw_from,
                    "TW To": tw_to,
                    "Handling Instructions": handling,
                    "Customer Email": email,
                    "Shipping Phone": phone,
                    "Note": notes,
                }
            )

            writer = pd.ExcelWriter(b, engine='xlsxwriter')
            df.to_excel(writer, sheet_name='Sheet1')
            writer.save()

            # Set up the Http response.
            filename = 'output.xlsx'
            response = HttpResponse(
                b.getvalue(),
                content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            )
            response['Content-Disposition'] = 'attachment; filename=%s' % filename
            return response
