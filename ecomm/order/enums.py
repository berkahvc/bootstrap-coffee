import enum


# Using enum class create enumerations
class OrderStatus(enum.Enum):
    WAIT_FOR_PAYMENT = "WAIT_FOR_PAYMENT"
    PAID = "PAID"
    EXPIRED = "EXPIRED"
    CANCELLED = "CANCELLED"
    DENIED = "DENIED"
    DETECTED_AS_FRAUD = "DETECTED_AS_FRAUD"

    @classmethod
    def choices(cls):
        return tuple((i.name, i.value) for i in cls)

    @classmethod
    def name_choices(cls):
        return tuple(i.name for i in cls)
