from django import forms
from django.contrib.admin import widgets


class RescheduleOrderDateAdminForm(forms.Form):
    order_ids = forms.CharField(required=True, widget=forms.HiddenInput())
    delivery_date = forms.DateField(widget=widgets.AdminDateWidget)
