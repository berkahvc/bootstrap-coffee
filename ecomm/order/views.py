import itertools
import logging
import re
from datetime import datetime

import pandas as pd
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.db.models import Window, Sum, F
from django.http import HttpResponseRedirect
from django.shortcuts import HttpResponse
from django.urls import reverse
from django.utils.timezone import localdate, timedelta
from django.views.generic import FormView, TemplateView
from reportlab.lib.styles import ParagraphStyle, getSampleStyleSheet
from reportlab.lib.units import mm
from reportlab.platypus import PageBreak, Paragraph, SimpleDocTemplate, Table

from ecomm.order.models import Order
from ecomm.order.utils import extract_product_name, extract_product_name_and_qty, generate_customer_list, create_label, \
    generate_subscriber_list
from ecomm.subscription_management.models import SubscriptionPrepaid

from .forms import RescheduleOrderDateAdminForm

logger = logging.getLogger(__name__)


def delivery_labels(request):
    """
    Generate a report of all delivery labels & list for a given date.
    :param request:
    :return:
    """
    story = []
    styles = getSampleStyleSheet()
    styles.add(
        ParagraphStyle(
            name="Number",
            alignment=2,
            parent=styles["Heading1"],
            fontSize=18,
            leading=22,
            spaceAfter=6,
        )
    )
    styles.add(
        ParagraphStyle(
            name="Shipping",
            alignment=0,
            parent=styles["Heading1"],
            fontSize=14,
            leading=12,
            spaceAfter=6,
        )
    )
    styles.add(
        ParagraphStyle(
            name="Packing",
            alignment=1,
            parent=styles["Heading1"],
            fontSize=12,
            spaceAfter=1,
            leading=12,
        )
    )
    styles.add(
        ParagraphStyle(
            name="SubPackingTitleNoSub",
            parent=styles["Packing"],
            leading=12,
            textColor="white",
        )
    )
    styles.add(
        ParagraphStyle(
            name="SubPackingTitle",
            parent=styles["Packing"],
            borderWidth=2,
            borderColor="black",
            borderPadding=1 * mm,
            leading=12,
        )
    )
    style_number = styles["Number"]

    style_shipping = styles["Shipping"]
    style_packing = styles["Packing"]
    style_packing_title_no_sub = styles["SubPackingTitleNoSub"]
    style_packing_title_sub = styles["SubPackingTitle"]

    response = HttpResponse(content_type="application/pdf")
    order_list = request.GET.get("orders").split(",")

    response["Content-Disposition"] = "attachment; filename=delivery_notes.pdf"
    is_x4 = False
    is_x2 = False

    if order_list:
        orders = Order.objects.filter(number__in=order_list)

        for order in orders:
            order_number = order.number

            recipient_name = order.shipping_address.name.upper()

            address1 = f"{order.shipping_address.active_address_fields()[0]} \n"
            address2 = f"Singapore {order.shipping_address.active_address_fields()[3]}"

            item_data = [Paragraph(order_number, style_shipping)]
            if order.subscription_id:
                sub_data = [Paragraph("SUB", style_packing_title_sub)]
            else:
                sub_data = [Paragraph("-", style_packing_title_no_sub)]
            total_qty = 0
            for line in order.lines.all():
                sku_name = f"{line.partner_sku}" if line.product.parent else line.title
                sku_name = re.sub(r"-\d+PACK", "", sku_name)

                item_name = (
                    f"{line.partner_line_notes}"
                    if line.partner_line_notes
                    else line.product.parent.title
                )
                item_name = re.sub(r"Cold Brew (Coffee|Tea)", "", item_name)
                item_name = re.sub(r"Cold Brew", "", item_name)
                item_name = re.sub(r"Subscription", "", item_name)
                item_name = re.sub(r"-", "", item_name).strip()
                bottles = (
                    re.match(r"(\d+)", line.title).group(0)
                    if re.match(r"(\d+)", line.title)
                    else ""
                )
                if line.partner_sku == "MIXPACK":
                    quantity = line.quantity
                else:
                    quantity = (
                        line.quantity * int(bottles) if bottles else line.quantity
                    )
                total_qty += line.quantity * int(bottles) if bottles else line.quantity

                if total_qty == 12:
                    is_x2 = True
                elif total_qty == 24:
                    is_x4 = True

                item_data.append(Paragraph(f"{quantity}x {sku_name}", style_shipping))

                if item_name.split(","):
                    for item in item_name.split(","):
                        sub_data.append(Paragraph(item.upper().strip(), style_packing))
                else:
                    sub_data.append(Paragraph(f"{item_name.upper()}", style_packing))

            tbl = Table(
                list(map(list, itertools.zip_longest(*[item_data, sub_data]))),
                colWidths=[45 * mm],
            )

            # Write everything to the page
            story.append(
                Paragraph(
                    f"{order_number} X 4"
                    if is_x4
                    else f"{order_number} x 2"
                    if is_x2
                    else f"{order_number}",
                    style_number,
                )
            )

            # reset the flag
            is_x2 = False
            is_x4 = False

            story.append(Paragraph(recipient_name, style_shipping))
            story.append(Paragraph(address1, style_shipping))
            story.append(Paragraph(address2, style_shipping))
            story.append(Paragraph("", style_shipping))
            story.append(tbl)
            story.append(PageBreak())

        # f.addFromList(story, page)

    doc = SimpleDocTemplate(
        response,
        pagesize=(100 * mm, 62 * mm),
        rightMargin=10,
        leftMargin=10,
        topMargin=0,
        bottomMargin=0,
    )
    doc.build(story)

    return response


def optimo_orders(request):
    response = HttpResponse(content_type="application/xlsx")

    order_number = []
    delivery = []
    shipping1 = []
    shipping2 = []
    name = []
    tw_from = []
    tw_to = []
    handling = []
    email = []
    phone = []
    notes = []
    if request.GET.get("date"):
        delivery_date = datetime.strptime(request.GET.get("date"), "%Y-%m-%d")
    else:
        delivery_date = localdate() + timedelta(days=1)

    orders = Order.objects.filter(delivery_date=delivery_date)

    for order in orders:
        try:
            order_number.append(order.number)
            delivery.append(order.delivery_date)
            shipping1.append(
                f"Singapore {order.shipping_address.active_address_fields()[3]}"
            )
            shipping2.append(order.shipping_address.active_address_fields()[0])
            name.append(order.shipping_address.name)
            tw_from.append(order.delivery_time.start if order.delivery_time else "")
            tw_to.append(order.delivery_time.end if order.delivery_time else "")
            handling.append(
                order.shipping_address.notes if order.shipping_address.notes else ""
            )
            email.append(order.shipping_address.email)
            phone.append(order.shipping_address.phone_number)
            notes.append(
                f"{order.lines.all()[0].partner_line_notes}"
                if order.lines.all()[0].partner_line_notes
                else f"{order.lines.all()[0].product.parent.title} - {order.lines.all()[0].title}"
                if order.lines.all()[0].product.parent
                else order.lines.all()[0].title
            )

        except AttributeError:
            print("==========")
            print(order.number)
            continue

        except ValueError:
            print("==========")
            print(order.number)
            print(
                f"{order.lines.all()[0].parent.title} - {order.lines.all()[0].title}"
                if order.lines.all()[0].parent
                else order.lines.all()[0].title
            )

    df = pd.DataFrame(
        {
            "Order": order_number,
            "Attribute 'Delivery-Date'": delivery,
            "Shipping Address 1": shipping1,
            "Shipping Address 2": shipping2,
            "Shipping Name": name,
            "TW From": tw_from,
            "TW To": tw_to,
            "Handling Instructions": handling,
            "Customer Email": email,
            "Shipping Phone": phone,
            "Note": notes,
        }
    )

    response = HttpResponse(
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    )
    response["Content-Disposition"] = 'attachment; filename="filename.xlsx"'
    df.to_excel(response)

    return response


def print_production_list(request):
    delivery = []
    order_number = []
    order_name = []
    order_phone = []
    product_name = []
    variant_name = []
    variant_sku = []
    order_notes = []
    shipping_notes = []
    quantity = []

    if request.GET.get("date"):
        delivery_date = datetime.strptime(request.GET.get("date"), "%Y-%m-%d")
    else:
        delivery_date = localdate() + timedelta(days=1)

    if delivery_date.weekday() == 0:
        # if the delivery date is a monday, we need to get an estimate for the production
        orders = SubscriptionPrepaid.objects.filter(
            next_delivery_date=delivery_date
        ) | SubscriptionPrepaid.objects.filter(delivery_date=delivery_date)
        for order in orders:
            delivery.append(order.delivery_date)
            order_number.append("N/A")
            order_name.append(order.shipping_address.name)
            order_phone.append(order.shipping_address.phone_number)
            shipping_notes.append(
                order.shipping_address.notes if order.shipping_address.notes else ""
            )
            product_name.append(order.note if order.note else order.description)
            variant_name.append("N/A")
            variant_sku.append("N/A")
            order_notes.append("N/A")
            quantity.append("N/A")

    orders = Order.objects.filter(delivery_date=delivery_date)
    for order in orders:
        try:
            for line in order.lines.all():
                delivery.append(order.delivery_date)
                order_number.append(order.number)
                order_name.append(order.shipping_address.name)
                order_phone.append(order.shipping_address.phone_number)
                shipping_notes.append(
                    order.shipping_address.notes if order.shipping_address.notes else ""
                )

                product_name.append(
                    line.product.parent.title if line.product.parent else line.title
                )
                variant_name.append(line.product.title if line.product.parent else "")
                variant_sku.append(line.partner_sku if line.partner_sku else "")
                order_notes.append(
                    line.partner_line_notes if line.partner_line_notes else ""
                )
                quantity.append(line.quantity)
        except AttributeError as e:
            logger.error(f"This order has an error [{order.number}] - Error: {e}")
            continue

        except ValueError as e:
            logger.error(f"This order has an error [{order.number}] - Error: {e}")
            continue

    df = pd.DataFrame(
        {
            "Delivery Date": delivery,
            "Order Number": order_number,
            "Order Name": order_name,
            "Order Phone": order_phone,
            "Product Name": product_name,
            "Variant Name": variant_name,
            "Variant SKU": variant_sku,
            "Order Notes": order_notes,
            "Other Notes": shipping_notes,
            "Quantity": quantity,
        }
    )

    response = HttpResponse(
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    )

    df.to_excel(response)
    response["Content-Disposition"] = 'attachment; filename="production list.xlsx"'
    return response


def print_production_list_by_date(request):
    start_date = request.GET.get("start_date")
    end_date = request.GET.get("end_date")

    orders = Order.objects.filter(delivery_date__range=[start_date, end_date])
    # iterate through line items and make sure not "subscription" > get product name and variant
    # > add variant and insert into product name

    delivery = {}

    def insert_to_delivery_dict(product: str, amount: int) -> None:
        """
        Inserts a product to the delivery dictionary

        :param product: Product name
        :param amount: amount of product
        """
        try:
            delivery[product] += int(amount)
        except KeyError:
            delivery[product] = int(amount)

    def handle_subscription_order(order_item: Order) -> None:
        subscription_id = order_item.subscription_id
        sub = SubscriptionPrepaid.objects.get(djsubscription_id=subscription_id)
        description_in_note = re.search(
            r"black|mocha|strong|manuka|oat|hojicha|rooibos|mix|coffee",
            sub.note.lower()
        ) if sub.note else None
        items = sub.note.split(",") if description_in_note else sub.description.split(",")

        for item in items:
            try:
                product_name, total_amount = extract_product_name_and_qty(item)
            except (IndexError, AttributeError) as e:
                logger.info(
                    f"Error with item: {item} for order: {order_item.number}. Error: {e}"
                )
            except KeyError:
                try:
                    product_name, total_amount = extract_product_name_and_qty(
                        sub.description.split(",")[0]
                    )
                except (IndexError, AttributeError) as e:
                    logger.info(
                        f"Error with description of: {item} for order: {order_item.number}. Error: {e}"
                    )
            insert_to_delivery_dict(product_name, total_amount)

    def handle_non_subscription_order(order_item: Order) -> None:
        for line in order_item.lines.all():
            variant_name = line.product.title if line.product.parent else ""
            variant_name = extract_product_name(variant_name)
            try:
                amount_in_variant = re.search(r"\d+", variant_name).group(0)
                total_amount = int(amount_in_variant) * int(line.quantity)
            except (IndexError, AttributeError):
                logger.info(
                    f"No amount found in variant {variant_name} [{order_item.number}] inserted 1 instead"
                )
                total_amount = line.quantity

            product_name = line.product.parent.title if line.product.parent else line.title
            product_name = extract_product_name(product_name)

            contains_all_variant = ["mix pack", "chocolate"]
            if any(x in product_name.lower() for x in contains_all_variant):
                for variant in ["black", "mocha", "manuka", "oat", "hojicha", "rooibos"]:
                    insert_to_delivery_dict(variant, total_amount)
            else:
                insert_to_delivery_dict(product_name, total_amount)

    for order in orders:
        try:
            if order.subscription_id:
                handle_subscription_order(order)
            else:
                handle_non_subscription_order(order)

        except Exception as e:
            logger.info(f"This order has an error [{order.number}]: {e}")

    df = pd.DataFrame(delivery, index=["No of bottles"]).T
    logger.info(f"DataFrame { df }")

    response = HttpResponse(
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    )

    df.to_excel(response)
    response["Content-Disposition"] = 'attachment; filename="production list.xlsx"'
    return response


def print_packing_list_by_date(request):
    start_date = request.GET.get("start_date")
    end_date = request.GET.get("end_date")

    orders = Order.objects.filter(delivery_date__range=[start_date, end_date])

    delivery = {}

    for order in orders:
        logger.info(f"{order.number} is processing")
        try:
            if order.subscription_id:
                logger.info("Subscription is triggered")
                subscription_id = order.subscription_id
                sub = SubscriptionPrepaid.objects.get(djsubscription_id=subscription_id)
                description_in_note = re.search(r"black|mocha|strong|manuka|oat|hojicha|rooibos|mix|coffee", sub.note)
                product_name = sub.note if description_in_note else sub.description
                total_amount = 1

            else:
                logger.info("Normal sub is triggered")
                for line in order.lines.all():
                    logger.info(line)
                    variant_name = line.product.title if line.product.parent else ""
                    variant_name = extract_product_name(variant_name)
                    try:
                        amount_in_variant = re.search(r"\d+", variant_name).group(0)
                        total_amount = int(line.quantity)
                        pack = amount_in_variant
                    except (IndexError, AttributeError):
                        logger.info(f"No amount found in variant {variant_name} [{order.number}] inserted 1 instead")
                        total_amount = line.quantity
                        pack = total_amount

                    product_name = line.product.parent.title if line.product.parent else line.title
                    product_name = f"{product_name} | {pack} pack"

        except Exception as e:
            logger.info(f"This order has an error [{order.number}]: {e}")
            pass

        if "mix pack" in variant_name.lower():
            for variant in ["black", "mocha", "manuka", "oat", "hojicha", "rooibos"]:
                insert_to_delivery_dict(variant, total_amount)
        else:
            insert_to_delivery_dict(product_name, total_amount)

    df = pd.DataFrame(delivery, index=[0]).T
    logger.info(f"DataFrame {df}")

    response = HttpResponse(
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    )

    df.to_excel(response)
    response["Content-Disposition"] = 'attachment; filename="production list.xlsx"'
    return response


def print_cruffin_orders(request):
    orders = (
        Order.objects.filter(lines__product_id=93, date_placed__gte="2022-02-28")
        .order_by("number")
        .distinct("number")
    )
    if request.GET.get("date_from"):
        orders = orders.filter(delivery_date__gte=request.GET.get("date_from"))
        print(request.GET.get("date_from"))
    else:
        orders = orders.filter(delivery_date__gte=localdate())

    if request.GET.get("date_to"):
        orders = orders.filter(delivery_date__lte="2022-03-18")
        print(request.GET.get("date_to"))

    delivery_date = list()
    order_number = list()
    order_billing_name = list()
    product_name = list()
    quantity = list()
    order_note = list()

    for order in orders:
        product_quantity = 0
        for line in order.lines.all():
            if line.product_id == 93:
                product_quantity += line.quantity

        delivery_date.append(order.delivery_date)
        order_number.append(order.number)
        print(order.number)
        order_billing_name.append(order.shipping_address.name)
        product_name.append(order.lines.filter(product_id=93)[0].title)
        quantity.append(product_quantity)
        order_note.append(
            order.lines.filter(product_id=93)[0].partner_line_notes
            if order.lines.filter(product_id=93)[0].partner_line_notes
            else order.shipping_address.notes
            if order.shipping_address.notes
            else ""
        )

    df = pd.DataFrame(
        {
            "Delivery Date": delivery_date,
            "Order": order_number,
            "Billing Name": order_billing_name,
            "Product Name": product_name,
            "Quantity": quantity,
            "Order Note": order_note,
        }
    )

    response = HttpResponse(
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    )
    response["Content-Disposition"] = 'attachment; filename="cruffin.xlsx"'
    df.to_excel(response)

    return response


class PrintOperationStuff(UserPassesTestMixin, TemplateView):
    template_name = "orders/print_production.html"

    def test_func(self):
        return self.request.user.is_staff


print_operation_stuff = PrintOperationStuff.as_view()


class RescheduleOrderDateAdminView(LoginRequiredMixin, FormView):
    form_class = RescheduleOrderDateAdminForm

    def form_valid(self, form):
        order_ids = form.cleaned_data.get("order_ids")
        delivery_date = form.cleaned_data.get("delivery_date")

        for order_id in order_ids.split(","):
            order = Order.objects.get(id=order_id)
            order.delivery_date = delivery_date
            order.save()

        return HttpResponseRedirect(reverse("admin:order_order_changelist"))


reschedule_order_date_admin_view = RescheduleOrderDateAdminView.as_view()


class PrintMonthlyReport(UserPassesTestMixin, TemplateView):
    template_name = "orders/print_monthly_report.html"

    def test_func(self):
        return self.request.user.is_staff


print_monthly_report = PrintMonthlyReport.as_view()


@user_passes_test(lambda u: u.is_staff)
def print_customer_list(request):
    """
    Creates an Excel file for printing all customers for a particular month
    """
    month = int(request.GET.get("month"))
    year = int(request.GET.get("year"))

    df = generate_customer_list(month, year)

    response = HttpResponse(
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    )

    df.to_excel(response)
    response["Content-Disposition"] = f'attachment; filename="customer list {month}-{year}.xlsx"'
    return response


@user_passes_test(lambda u: u.is_staff)
def print_subscriber_list(request):
    """
    Creates an Excel file for printing all customers for a particular month
    """

    df = generate_subscriber_list()

    response = HttpResponse(
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    )

    df.to_excel(response)
    response["Content-Disposition"] = f'attachment; filename="Subscriber list.xlsx"'
    return response


class LabelView(TemplateView):
    template_name = "orders/documents/label.html"

    def get_context_data(self, **kwargs):
        context = create_label("2022-09-22")
        return context


def download_label(request):
    date = request.GET.get("date") if request.GET.get("date") else None
    order_list = request.GET.get("orders") if request.GET.get("orders") else None
    driver_name = request.GET.get("driver") if request.GET.get("driver") else None
    file = create_label(date, order_list, driver_name)

    response = HttpResponse(
        content_type="application/pdf",
        content=open(file, "rb").read()
    )
    response["Content-Disposition"] = f'attachment; filename=Shipping Labels for {date}.pdf'
    return response
