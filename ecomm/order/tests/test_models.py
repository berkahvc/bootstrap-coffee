import pytest

pytestmark = pytest.mark.django_db


def test_new_format_city_function(order, shipping_address):
    assert order.shipping_address.city == "BULELENG"


def test_old_format_city_function(order, shipping_address):
    shipping_address.line4 = "51.07"
    shipping_address.save()

    order.shipping_address = shipping_address
    order.save()

    assert order.shipping_address.city == "51.07"


def test_new_format_city_code_function(order, shipping_address):
    assert order.shipping_address.city_code == "51.08"
