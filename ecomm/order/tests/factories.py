from datetime import date, timedelta

from factory import DjangoModelFactory, Faker, Sequence, SubFactory, fuzzy

from ecomm.address.models import Country
from ecomm.order.models import Order, ShippingAddress


class CountryFactory(DjangoModelFactory):
    class Meta:
        model = Country
        django_get_or_create = ("pk",)

    pk = "ID"
    name = "Indonesia"


class ShippingAddressFactory(DjangoModelFactory):
    class Meta:
        model = ShippingAddress

    line4 = "51.08_BULELENG"
    district = "51.08.03_Busung biu (Busungbiu)"
    country = SubFactory(CountryFactory)


class OrderFactory(DjangoModelFactory):
    class Meta:
        model = Order

    number = Sequence(lambda n: u"#{}".format(n))
    currency = "SGD"
    total_excl_tax = fuzzy.FuzzyDecimal(0, 100)
    total_incl_tax = total_excl_tax
    delivery_date = fuzzy.FuzzyDate(
        start_date=date.today() - timedelta(days=30),
        end_date=date.today() + timedelta(days=30),
    )
    delivery_day = Faker("day_of_week")
    is_from_shopify = False
    subscription_id = None
    shipping_address = SubFactory(ShippingAddressFactory)
