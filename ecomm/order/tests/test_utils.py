import pytest
from constance import config
from django.utils.timezone import localdate

from ..utils import OrderNumberGenerator, extract_product_name, extract_product_name_and_qty

pytestmark = pytest.mark.django_db


@pytest.mark.skip(reason="Not implemented")
def test_order_number_set_the_date(settings):
    today = "240221"
    result = OrderNumberGenerator().order_number(date=today)
    order_number = "%05d" % (int("00000") + 1)
    assert result == f"AL{today}{order_number}"

    today = "240221"
    result = OrderNumberGenerator().order_number(date=today)
    order_number = "%05d" % (int("00000") + 2)
    assert result == f"AL{today}{order_number}"

    today = "240221"
    result = OrderNumberGenerator().order_number(date=today)
    order_number = "%05d" % (int("00000") + 3)
    assert result == f"AL{today}{order_number}"

    today = "250221"
    result = OrderNumberGenerator().order_number(date=today)
    order_number = "%05d" % (int("00000") + 1)
    assert result == f"AL{today}{order_number}"

    config.LAST_COUNTER = settings.LAST_COUNTER_DEFAULT
    config.LAST_ORDER_DATE = settings.LAST_ORDER_DATE_DEFAULT


@pytest.mark.skip("Not implemented")
def test_order_number_not_set_the_date(settings):
    today = localdate().today().strftime("%d%m%y")
    result = OrderNumberGenerator().order_number()
    order_number = "%05d" % (int("00000") + 1)
    assert result == f"AL{today}{order_number}"

    config.LAST_COUNTER = settings.LAST_COUNTER_DEFAULT
    config.LAST_ORDER_DATE = settings.LAST_ORDER_DATE_DEFAULT


def test_extract_product_name():
    product_name = extract_product_name("Cold Brew Milk & Manuka Honey")
    assert "Cold Brew Milk & Manuka Honey" == product_name
    product_name = extract_product_name("black")
    assert "Cold Brew Black" == product_name
    product_name = extract_product_name("Manukah")
    assert "Cold Brew Milk & Manuka Honey" == product_name
    product_name = extract_product_name("strong")
    assert "Cold Brew Strong" == product_name
    product_name = extract_product_name("Hojicha Cold Brew")
    assert "Cold Brew Hojicha Tea" == product_name
    product_name = extract_product_name("oat")
    assert "Cold Brew Oat Milk" == product_name
    product_name = extract_product_name("rooibos")
    assert "Cold Brew Rooibos Orange" == product_name
    product_name = extract_product_name("Mix Pack")
    assert "Mix Pack" == product_name


def test_extract_product_name_and_qty():
    item = "6x Cold Brew Black"

    product_name, qty = extract_product_name_and_qty(item)
    assert "Cold Brew Black" == product_name
    assert 6 == qty

    item = "4x Strong"
    product_name, qty = extract_product_name_and_qty(item)
    assert "Cold Brew Strong" == product_name
    assert 4 == qty

    item = "3x Cold Brew Oat Milk"
    product_name, qty = extract_product_name_and_qty(item)
    assert "Cold Brew Oat Milk" == product_name
    assert 3 == qty

    items = "Cold Brew Milk & Manuka Honey - Subscription - 6 pack".split(",")
    product_name, qty = extract_product_name_and_qty(items[0])
    assert "Cold Brew Milk & Manuka Honey" == product_name
    assert 6 == qty
