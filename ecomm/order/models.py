import logging
import re

from django.contrib.postgres.fields import JSONField
from django.core import exceptions
from django.db import models
from django.utils.translation import gettext_lazy as _
from oscar.apps.address.abstract_models import AbstractShippingAddress
from oscar.apps.order.abstract_models import AbstractLine, AbstractOrder

from ecomm.schedule_management.models import DeliveryTime
from ecomm.utils.optimo_route_client import OptimoRouteClient

logger = logging.getLogger(__name__)


class Order(AbstractOrder):
    shipping_method_details = JSONField(default={})
    delivery_date = models.DateField(null=True, blank=True, db_index=True)
    delivery_time = models.ForeignKey(
        DeliveryTime, null=True, blank=True, db_index=True, on_delete=models.SET_NULL
    )
    delivery_day = models.CharField(
        max_length=255, null=True, blank=True, db_index=True
    )
    is_from_shopify = models.BooleanField(
        default=False, null=True, blank=True, db_index=True
    )
    subscription_id = models.CharField(
        max_length=255, null=True, blank=True, db_index=True
    )

    def __str__(self):
        return "%s" % (self.number,)

    def items(self):
        items = []
        for i, line in enumerate(self.lines.all(), start=1):
            data = {
                "line_id": i,
                "qty": line.quantity,
                "uom": "PC",
                "product_id": line.partner_sku,
            }

            items.append(data)
        return items

    def optimo_route_payload(self):
        return {
            "operation": "CREATE",
            "orderNo": f"{self.number}",
            "type": "D",
            "date": f"{self.delivery_date}",
            "location": {
                "address": f"{self.shipping_address.summary}",
                "acceptPartialMatch": True,
                "acceptMultipleResults": True,
            },
            "duration": 20,
            "twFrom": f"{self.delivery_time.start_time()}",
            "twTo": f"{self.delivery_time.end_time()}",
        }

    def send_order_to_optimo_route(self):
        logger.info(
            f"send order to OptimoRoute {self.number}",
            extra={
                "delivery_date": self.delivery_date,
                "delivery_time": self.delivery_time,
                "delivery_day": self.delivery_day,
            },
        )
        if self.delivery_time and self.delivery_date:
            optimo_route = OptimoRouteClient()
            optimo_route.create_order(self)

    def total_sixpack(self) -> int:
        """
        Function to get the total number of six packs in an order

        Used in label printing
        :return: Total number of packs
        """
        total_sixpack = 0

        for line in self.lines.all():
            total_sixpack += line.get_bottle_qty()
        return total_sixpack


class Line(AbstractLine):
    interval = models.CharField(max_length=255, null=True, blank=True, db_index=True)
    interval_count = models.IntegerField(null=True, blank=True, db_index=True)
    delivery_prepaid = models.IntegerField(null=True, blank=True, db_index=True)
    subscription_type = models.CharField(
        max_length=255, null=True, blank=True, db_index=True
    )
    credit_per_delivery = models.IntegerField(null=True, db_index=True)
    pack_size = models.IntegerField(null=True, db_index=True)
    is_subscription = models.BooleanField(default=False, null=True, db_index=True)

    def get_title(self) -> str:
        """Get the title name depending on the category of the product (sub, with/without parent)"""
        if self.is_subscription:
            return f"Subscription - {self.credit_per_delivery * 6} pack"
        elif self.product.parent:
            return self.product.parent.title
        else:
            return self.product.title

    def get_description(self) -> str:
        """Get the description based on the item's category"""
        if self.is_subscription:
            return self.partner_line_notes
        else:
            return ""

    def get_bottle_qty(self) -> int:
        """
        Get the total bottle quantity for the line item
        """
        if self.is_subscription or "subscription" in self.product.title:
            total_bottle = 0
            try:
                for item in self.partner_line_notes.split(","):
                    bottle = self.extract_product_qty(item)
                    total_bottle += int(bottle)
            except AttributeError:
                from ecomm.subscription_management.models import SubscriptionPrepaid

                subscription = SubscriptionPrepaid.objects.get(djsubscription_id=self.order.subscription_id)
                for item in subscription.description.split(","):
                    bottle = self.extract_product_qty(item)
                    total_bottle += int(bottle)

            return int(total_bottle / 6)
        elif self.partner_sku in ["MIXPACK", "BSC2"]:
            return self.quantity
        elif self.partner_sku == "ZW5Q2":
            return self.quantity * 2
        else:
            bottle = self.extract_product_qty(self.product.title)
            return int(bottle / 6) if bottle % 6 == 0 else int(bottle)

    def get_line_qty_for_label(self) -> int:
        """
        Get the total line item for label purposes

        if there is a "subscription" in the name, return 1. Else return the number of bottles
        :return:
        """
        if self.is_subscription or "subscription" in self.product.title:
            return 1
        else:
            try:
                return self.quantity * self.extract_product_qty(self.product.title)
            except AttributeError:
                return self.quantity

    @staticmethod
    def extract_product_qty(item) -> int:
        """
        Get the subscription prepaid model and extract the product and amount

        :param item: string with format "(qty)x product"
        :return: Product quantity
        """

        if re.search(r"\d+x", string=item):
            total_amount = item.split("x")[0]
        else:
            total_amount = re.search(r"\d+", item).group(0)

        return int(total_amount)


class ShippingAddress(AbstractShippingAddress):
    line4 = models.CharField(
        _("City"), max_length=255, blank=True, null=True, db_index=True
    )
    district = models.CharField(
        _("District"), max_length=255, blank=True, null=True, db_index=True
    )
    email = models.EmailField(
        _("User email"), max_length=255, blank=True, db_index=True
    )

    @property
    def city(self):
        if self.line4:
            line4_split = self.line4.split("_")
            return line4_split[1] if len(line4_split) > 1 else line4_split[0]
        return "-"

    @property
    def city_code(self):
        if self.line4:
            return self.line4.split("_")[0]
        return "-"

    def get_field_values(self, fields):
        field_values = []
        for field in fields:
            # Title is special case
            if field == "title":
                value = self.get_title_display()
            elif field == "country":
                try:
                    value = self.country.printable_name
                except exceptions.ObjectDoesNotExist:
                    value = ""
            elif field == "salutation":
                continue
            elif field == "line4":
                value = self.city
            elif field == "district":
                value = self.show_district
            else:
                value = getattr(self, field)
            field_values.append(value)
        return field_values

    @property
    def show_district(self):
        if self.district is None:
            return "-"

        district_split = self.district.split("_")
        return district_split[1] if len(district_split) > 1 else district_split[0]


from oscar.apps.order.models import *  # noqa isort:skip
