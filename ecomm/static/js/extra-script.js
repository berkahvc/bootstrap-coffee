const csrfmiddlewaretoken = $("input[name='csrfmiddlewaretoken']").val();
let product_subscription_selected = [];
let product_counter_arr = [];
let total_product_selected = 0;
let pack_chosen = 0;
let delivery_time_id = null;

syncCart();

function openCart() {
  syncCart();
}

function syncCart () {
  $.ajax({
    type: "GET",
    url: "/checkout/ajax-order-information",
    data: {
      csrfmiddlewaretoken
    },
    success: function (data) {
      $("#subTotal").html(`${data.price_currency}${data.total_incl_tax_excl_discounts}`);
      const lines = data.lines;
      const holidays = data.holidays;

      if ($('#datetimepicker5').data("DateTimePicker")) {
        $('#datetimepicker5').data("DateTimePicker").destroy();
      }

      let dateOptions = {
        format: 'DD/MM/YYYY',
        useCurrent: false,
        disabledDates: holidays,
        icons: {
          previous: "fa fa-chevron-circle-left",
          next: "fa fa-chevron-circle-right",
        }
      }

      if (data.has_cruffin) {
        dateOptions['daysOfWeekDisabled'] = [0,6];
      } else {
        dateOptions['daysOfWeekDisabled'] = [0];
      }

      if (data.start_date !== undefined) {
        dateOptions['minDate'] = moment(`${data.start_date}`, 'YYYY-MM-DD').millisecond(0).second(0).minute(0).hour(0);

        if (data.selectedDate !== undefined) {
          dateOptions['date'] = data.selectedDate;
        }

        console.log(data.delivery_time_id);

        if (data.delivery_time_id !== null) {
          delivery_time_id = data.delivery_time_id;
          $('#selectDeliveryTime option:selected').val(data.delivery_time_id);
        } else {
          $('#selectDeliveryTime').empty();
        }
      }

      $('#datetimepicker5').datetimepicker(dateOptions);

      let appendHtml = "";
      let totalItem = 0;
      for (let idx in lines) {
        const line = lines[idx];
        totalItem += line.quantity;
        let description = '';
        if (line.is_parent) {
          description = `${line.description}`;
        } else {
          description = `Size: ${line.product.title}`;
        }

        if (line.is_subscription) {
          appendHtml += `
          <div class="d-grid justify-content-between mb-4" style="grid-template-columns: 35% 62%">
            <input type="hidden" name="lineId" id="lineId_${idx}" value="${line.id}">
            <input type="hidden" name="basketId" id="basketId_${idx}" value="${line.basketId}">
            <img src="https://storage.googleapis.com/berkah-bootstrap/static/images/boot/prepaid.jpg" alt="${line.title}" class="w-100"/>
            <div class="d-grid">
            <div class="d-grid justify-content-between" style="grid-template-columns: 1fr max-content; line-break: anywhere;">
              <p class="text-bold">Subscription</p>
              <img src="${buttonRemoveProduct}" alt="button-close" class="cursor-pointer" style="width: 20px" onclick="removeCartSubscription(${idx})"/>
            </div>
              <p class="label-default">${description}</p>
              <p class="text-end text-bold">${line.price_currency}${line.line_price_excl_tax}</p>
            </div>
          </div>`;
        } else if (line.title === "Coffee Concentrate"){
          appendHtml += `
          <div class="d-grid justify-content-between mb-4" style="grid-template-columns: 35% 62%">
            <input type="hidden" name="lineId" id="lineId_${idx}" value="${line.id}">
            <input type="hidden" name="basketId" id="basketId_${idx}" value="${line.basketId}">
            <img src="${line.image_url}" alt="${line.title}" class="w-100"/>
            <div class="d-grid">
              <div class="d-grid justify-content-between" style="grid-template-columns: 1fr max-content;">
                <p class="text-bold">${line.title}</p>
                <img src="${buttonRemoveProduct}" alt="button-close" class="cursor-pointer" style="width: 20px" onclick="cartProductDelete(${idx}, ${line.quantity})"/>
              </div>
              <p class="label-default">${description}</p>
              <div class="d-grid justify-content-between align-items-center" style="grid-template-columns: 1fr 1fr; line-break: anywhere;">
                <div class="button-cart__container font-primary">
                  <button class="btn-counter btn-counter--decrement" onclick="cartSubst(${idx}, 2)">-</button>
                  <div id="cartQty_${idx}" class="text-center">${line.quantity}</div>
                  <button class="btn-counter btn-counter--increment" onclick="cartAdd(${idx})">+</button>
                </div>
                <p class="text-end text-bold">${line.price_currency}${line.line_price_excl_tax}</p>
              </div>
            </div>
          </div>`;
        } else {
          appendHtml += `
          <div class="d-grid justify-content-between mb-4" style="grid-template-columns: 35% 62%">
            <input type="hidden" name="lineId" id="lineId_${idx}" value="${line.id}">
            <input type="hidden" name="basketId" id="basketId_${idx}" value="${line.basketId}">
            <img src="${line.image_url}" alt="${line.title}" class="w-100"/>
            <div class="d-grid">
              <div class="d-grid justify-content-between" style="grid-template-columns: 1fr max-content;">
                <p class="text-bold">${line.title}</p>
                <img src="${buttonRemoveProduct}" alt="button-close" class="cursor-pointer" style="width: 20px" onclick="cartProductDelete(${idx}, ${line.quantity})"/>
              </div>
              <p class="label-default">${description}</p>
              <div class="d-grid justify-content-between align-items-center" style="grid-template-columns: 1fr 1fr; line-break: anywhere;">
                <div class="button-cart__container font-primary">
                  <button class="btn-counter btn-counter--decrement" onclick="cartSubst(${idx})">-</button>
                  <div id="cartQty_${idx}" class="text-center">${line.quantity}</div>
                  <button class="btn-counter btn-counter--increment" onclick="cartAdd(${idx})">+</button>
                </div>
                <p class="text-end text-bold">${line.price_currency}${line.line_price_excl_tax}</p>
              </div>
            </div>
          </div>`;
        }
      }

      if (totalItem === 0) {
        $(".cart-empty__wrapper").removeClass('none');
        $(".cart-filled__wrapper").addClass('none');
        $("#totalCart").text(0);
        $("#mtotalCart").text(0);
      } else {
        $(".cart-empty__wrapper").addClass('none');
        $(".cart-filled__wrapper").removeClass('none');
        $("#totalCart").text(lines.length);
        $("#mtotalCart").text(lines.length);

        $(".cart-count-d").attr("data-count", totalItem);
        $(".cart-count").attr("data-count", totalItem);
        $("#bodyCart").html(appendHtml);
      }

      // $("#cart").removeClass("none");
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function useThisAddress(addressId) {
  $("#select_shipping_address_" + addressId).submit();
}

$("#datetimepicker5").on("dp.change", function (e) {
  let dateSelected = e.date.format('L');
  let data = {
    "date": dateSelected

  };
  $.ajax({
    headers: {'X-CSRFToken': csrfmiddlewaretoken},
    url : '/basket/ajax-basket-delivery-date-add-view/',
    data : data,
    type : 'POST',
    success: function (resp) {
      $("#selectDeliveryTime").html(resp.option);
      if (delivery_time_id !== null) {
        $("#selectDeliveryTime").val(delivery_time_id).change();
      }
    },
    error: function (data) {
      console.log(data);
    }
  });

});

$("#selectDeliveryTime").change(function() {
  let selectedId = $(this).val();
  if (selectedId !== "") {
    let data = {
      "delivery_time_id": selectedId
    };
    $.ajax({
      headers: {'X-CSRFToken': csrfmiddlewaretoken},
      url : '/basket/ajax-basket-delivery-time-add-view/',
      data : data,
      type : 'POST',
      success: function (resp) {
        console.log(resp)
      },
      error: function (data) {
        console.log(data);
      }
    });
  }
});

// syncCart();

function showCart(str) {
  if (str === 'open') {
    $("#cart").removeClass("none");
  } else {
    $("#cart").addClass("none");
  }
}


// login scripts
function getLoginFormData () {
  const formData = {};

  $("#form-signin input").each(function(){
    const name = $(this).attr('name');
    const val = $(this).val();
    formData[name] = val;
  });

  return formData;
}

function submitLoginForm() {
  const data = getLoginFormData();

  $.ajax({
    type: "POST",
    url: "/accounts/login/",
    data,
    success: function (data) {
      displayLoading();
      location.reload();
    },
    error: function (data) {
      alertErrorResponse(data);
    }
  });
}

// signup scripts
function getSignupFormData () {
  const formData = {};

  $("#form-signup input").each(function(){
    const name = $(this).attr('name');
    const val = $(this).val();
    formData[name] = val;
  });

  return formData;
}

function submitSignupForm() {
  let data = $("#form-signup").serialize();

  $.ajax({
    type: "POST",
    url: "/accounts/signup/",
    data: data,
    success: function (data) {
      location.reload();
    },
    error: function (data) {
      const errors = findAllByKey(data, 'errors');

      if (errors.length > 0) {
        alert(errors[0]);
      }
    }
  });
}

function showPasswordResetForm() {
  $("#loginForm").addClass("hidden");
  $("#registerForm").addClass("hidden");
  $("#resetForm").removeClass("hidden");
}

function showLoginForm() {
  $("#loginForm").removeClass("hidden");
  $("#registerForm").addClass("hidden");
  $("#resetForm").addClass("hidden");
}

function showRegisterFrom() {
  $("#loginForm").addClass("hidden");
  $("#registerForm").removeClass("hidden");
  $("#resetForm").addClass("hidden");
}

function submitForgotPassword() {
  let data = $("#form-password-reset").serialize();

  $.ajax({
    type: "POST",
    url: "/accounts/password/reset/",
    data: data,
    success: function (data) {
      displayLoading();
      alert("We have sent you an e-mail. Please contact us if you do not receive it within a few minutes.");
      location.reload();
    },
    error: function (data) {
      const errors = findAllByKey(data, 'errors');

      if (errors.length > 0) {
        alert(errors[0]);
      }
    }
  });
}

// General Scripts
function changeIcon(x) {
  x.classList.toggle("fa-times");
}


function addToCart(counter, productId) {
  let url = "/basket/add/" + productId + "/";
  let qty = $("#quantity_" + counter).val();
  let qty_int = parseInt(qty, 10);

  if (qty_int === 0) {
    qty_int = 1;
  }

  let form = {
    "quantity": qty_int,
    "csrfmiddlewaretoken": csrfmiddlewaretoken
  }
  $.post(url, form, function (resp) {
    if (resp.success) {
      syncCart();
      $("#successText").text("ITEM ADDED!");
      $("#successAddToCart").removeClass("none");
      setTimeout(function () {
        $("#successAddToCart").addClass("none");
      }, 2000);
    }
  })
}

function changeUrl(counter, newProductId,  price) {
  $("[id^=add_to_basket_form_" + counter + "]").each(function() {
      let new_id = "add_to_basket_form_" + counter + "_" + newProductId;
      // let newOnclick = '$(\'#' + new_id + '\').submit();';
      let newOnclick = "addToCart(" + counter + ", " + newProductId + ");";
      let newAction = "/basket/add/" + newProductId + "/"
      let submitId = $("#submit_" + counter);
      let submitDisabledId = $("#submitDisabled_" + counter);
      $("form#" + this.id).prop('id', new_id);
      $("#"+ new_id).attr('action', newAction);
      submitId.attr('onclick', newOnclick);
      $("#price-detail").text(price);
      $("#price-detail-" + counter).text(price);

      let $rooms = $("#noOfRoom_" + counter);
      if (parseInt($rooms.text(), 10) > 0) {
        submitId.removeClass('button--disabled');
        submitId.removeClass('none');

        submitDisabledId.addClass('none');
      } else {
        submitDisabledId.removeClass('none');
        submitId.addClass('none');
      }
  })
}

function add(number) {
  let $rooms = $("#noOfRoom_" + number);
  let a = parseInt($rooms.text(), 10);
  a++;
  $("#subs").prop("disabled", !a);
  $rooms.text(a);
  $("#quantity_" + number).val(a);

  if (a > 0) {
    let btnElement = $("#submit_" + number);
    let submitDisabledId = $("#submitDisabled_" + number);
    let isParentEl = $("#productIsParent_" + number).text();

    if (isParentEl === 'True') {
      if ($('input[name=pack_' + number + ']').is(':checked')) {
        btnElement.removeClass('button--disabled');
        btnElement.removeClass('none');
        submitDisabledId.addClass('none');
      } else {
        btnElement.addClass('button--disabled');
        btnElement.addClass('none');
        submitDisabledId.removeClass('none');
      }
    } else {
      btnElement.removeClass('button--disabled');
      btnElement.removeClass('none');
      submitDisabledId.addClass('none');
    }
  }
}

function subst(number, min=1) {
  let $rooms = $("#noOfRoom_" + number);
  let b = parseInt($rooms.text(), 10);
  if (b > min) {
      b--;
      if (b >= 0) {
        $rooms.text(b);
        $("#quantity_" + number).val(b);
      }

      if (b === min - 1) {
        let btnElement = $("#submit_" + number);
        btnElement.addClass('button--disabled');
        btnElement.off('click');
      }
  }
  else {
      $("#subs").prop("disabled", true);
  }
}

function addCounter(number) {
  let $rooms = $("#noOfRoom_" + number);
  let a = parseInt($rooms.text(), 10);
  let $pack_chosen = $("#textPackChosen");
  let progress_bar = $(".progress-bar");
  let pack_chosen_split = $pack_chosen.text().split('/');
  pack_chosen = parseInt(pack_chosen_split[1], 10);
  let total_counter = parseInt(pack_chosen_split[0], 10);
  a++;

  if (total_counter < parseInt(pack_chosen_split[1], 10)) {
    $rooms.text(a);
    total_counter++;
    $pack_chosen.text(total_counter + "/" + pack_chosen_split[1]);
    if (!_isContains(product_subscription_selected, number, 'add')) {
      product_subscription_selected.push({
        'product_counter': number,
        'title': $.trim($("#titleProduct_" + number).text()),
        'price': parseInt($("#priceProduct_"+ number).text().replace('SGD', ''), 10),
        'qty': a
      });
      product_counter_arr.push(number);
    }
  }
  $("#id_total_selected").val(total_counter);

  generateOrderSummary();
  let progress = (total_counter/pack_chosen_split[1] * 100).toString();
  progress_bar.css("width", progress.concat("%"));

  console.log(product_subscription_selected);
}

function substCounter(number) {
  let $rooms = $("#noOfRoom_" + number);
  let b = parseInt($rooms.text(), 10);
  let $pack_chosen = $("#textPackChosen");
  let progress_bar = $(".progress-bar");
  let pack_chosen_split = $pack_chosen.text().split('/');
  pack_chosen = parseInt(pack_chosen_split[1], 10);
  let total_counter = parseInt(pack_chosen_split[0], 10);
  if (b >= 1) {
    total_counter--;
    b--;
    if (b >= 0) {
      $rooms.text(b);
      $pack_chosen.text(total_counter + "/" + pack_chosen_split[1]);
      if (!_isContains(product_subscription_selected, number, 'subs')) {
        product_subscription_selected.push({
          'product_counter': number,
          'title': $.trim($("#titleProduct_" + number).text()),
          'price': parseInt($("#priceProduct_"+ number).text().replace('USD', ''), 10),
          'qty': b
        });
      } else {}
    }
  }
  $("#id_total_selected").val(total_counter);
  generateOrderSummary();

  let progress = (total_counter/pack_chosen_split[1]*100).toString();
  progress_bar.css("width", progress.concat("%"));

  console.log(product_subscription_selected);
}

function generateOrderSummary() {
  let $orderSummary = $("#bodyOrderSummary");
  let $goToPhaseThreeBtn = $("#goToPhaseThreeBtn");
  let $updateProductButton = $("#updateProductButton");
  let $clearAllProductSelected = $("#clearAllProductSelected");
  $orderSummary.empty();
  let appendHtml = '';
  let total_price = 0;
  let description = [];
  for (let index = 0; index < product_subscription_selected.length; index++) {
    let data = product_subscription_selected[index];
    if (data.qty > 0) {
      total_price += data.price * data.qty;
      description.push(`${data.qty}x ${data.title}`);
      appendHtml += `
        <div class="subscription-order-quantity d-flex">
          <div class="choose-coffee__summary-dot">&#9679;</div>
          <p> ${data.qty}x ${data.title}</p>
        </div>`;
    }
  }

  if (appendHtml !== '') {
    $orderSummary.html(appendHtml);
    $goToPhaseThreeBtn.removeClass('none');
    $clearAllProductSelected.removeClass('none');

    if (parseInt($("#id_total_selected").val(), 10) === pack_chosen) {
      $updateProductButton.removeClass('none');
    } else {
      $updateProductButton.addClass('none');
    }
    $("#id_price").val(total_price);
    $("#id_description").val(description.join());
  } else {
    $goToPhaseThreeBtn.addClass('none');
    $clearAllProductSelected.addClass('none');
    $updateProductButton.addClass('none');
    $("#id_price").val("");
    $("#id_description").val("")
  }
}

function _isContains(data_arr, value, func) {
  let hasMatch = false;
  for (let index = 0; index < data_arr.length; ++index) {
    let data = data_arr[index];
    if (data['product_counter'] === value) {
      if (func === 'add') {
        data['qty']++;
      } else {
        data['qty']--;
      }
      return true;
    }
  }
  return hasMatch
}

function clearAllProductSelected() {
  product_subscription_selected = [];
  for (let index = 0; index < product_counter_arr.length; index++) {
    let data = product_counter_arr[index];
    $("#noOfRoom_" + data).text(0);
  }
  let $pack_chosen = $("#textPackChosen");
  let pack_chosen_split = $pack_chosen.text().split('/');

  $pack_chosen.text("0/" + pack_chosen_split[1]);

  let progress_bar = $(".progress-bar");
  progress_bar.css("width", 0)
  generateOrderSummary();
}

function cartAdd(number) {
  let data = {
    "lineId": $('#lineId_' + number).val(),
    "qty": 1
  };
  $.ajax({
    headers: {'X-CSRFToken': csrfmiddlewaretoken},
    url : '/basket/ajax-basket-line-update-view/',
    data : data,
    type : 'POST',
    success: function (resp) {
      console.log(resp)
      syncCart();
      if (resp.line_deleted) {
        $("#successText").text("ITEM DELETED!");
      } else {
        $("#successText").text("QUANTITY UPDATED!");
      }

      $("#successAddToCart").removeClass("none");
      setTimeout(function () {
        $("#successAddToCart").addClass("none");
      }, 2000);
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function cartSubst(number, minimum=0) {
  let current_qty = $("#cartQty_" + number).text();
  let data = {
    "lineId": $('#lineId_' + number).val(),
  };

  if (minimum === 0) {
    data["qty"] = -1
  } else {
    if ((parseInt(current_qty, 10) - minimum) === 0) {
      data["qty"] = -1 - minimum
    } else {
      data["qty"] = -1
    }
  }

  $.ajax({
    headers: {'X-CSRFToken': csrfmiddlewaretoken},
    url : '/basket/ajax-basket-line-update-view/',
    data : data,
    type : 'POST',
    success: function (resp) {
      syncCart();
      if (resp.line_deleted) {
        $("#successText").text("ITEM DELETED!");

      } else {
        $("#successText").text("QUANTITY UPDATED!");
      }

      $("#successAddToCart").removeClass("none");
      setTimeout(function () {
        $("#successAddToCart").addClass("none");
      }, 2000);
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function cartProductDelete(number, quantity) {
  let data = {
    "lineId": $('#lineId_' + number).val(),
    "qty": -quantity
  };

  $.ajax({
    headers: {'X-CSRFToken': csrfmiddlewaretoken},
    url : '/basket/ajax-basket-line-update-view/',
    data : data,
    type : 'POST',
    success: function (resp) {
      console.log(resp)
      syncCart();
      if (resp.line_deleted) {
        $("#successText").text("ITEM DELETED!");

      } else {
        $("#successText").text("QUANTITY UPDATED!");
      }

      $("#successAddToCart").removeClass("none");
      setTimeout(function () {
        $("#successAddToCart").addClass("none");
      }, 2000);
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function removeCartSubscription(number) {
  let data = {
    "lineId": $('#lineId_' + number).val(),
    "qty": -1
  };
  $.ajax({
    headers: {'X-CSRFToken': csrfmiddlewaretoken},
    url : '/basket/ajax-basket-line-update-view/',
    data : data,
    type : 'POST',
    success: function (resp) {
      console.log(resp)
      syncCart();
      if (resp.line_deleted) {
        $("#successText").text("ITEM DELETED!");

      } else {
        $("#successText").text("QUANTITY UPDATED!");
      }

      $("#successAddToCart").removeClass("none");
      setTimeout(function () {
        $("#successAddToCart").addClass("none");
      }, 2000);
    },
    error: function (data) {
      console.log(data);
    }
  });
}

(function ($) {
  'use strict';
  //search pop
  $('a[href="#search"]').on('click', function(event) {
    $('#search').addClass('open');
    $('#search > form > input[type="search"]').focus();
  });

  $('#search, #search button.close').on('click keyup', function(event) {
    if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
      $(this).removeClass('open');
    }
  });

  //Wishlist
  function showToast(messageToast){
    hideLoading();

    $.toast({
      text: messageToast,
      showHideTransition: 'fade',
      allowToastClose: false,
      hideAfter: 2000,
      stack: 1,
      position: 'bottom-center',
      bgColor: '#000',
      textColor: '#fff',
      textAlign: 'center',
      loader: true,
      loaderBg: '#9EC600',
    });
  }

  $('.heart').on('click', function(event) {
    const csrfmiddlewaretoken = $("input[name='csrfmiddlewaretoken']").val();
    const removeAction = $(this).hasClass('red');
    const element = $(this);
    const target = $(event.target);

    displayLoading();

    if (removeAction){
      const url = element.data("remove-url");

      $.ajax({
        type: "POST",
        url,
        data: {
          csrfmiddlewaretoken
        },
        success: function (data) {
          $(element).removeClass('red');
          showToast('Removed from wishlist');
        },
        error: function (data) {
          showToast("Failed");
        }
      });
    } else{
      const url = element.data("add-url");

      $.ajax({
        type: "POST",
        url,
        data: {
          csrfmiddlewaretoken
        },
        success: function (data) {
          $(element).addClass('red');
          showToast('Added from wishlist');
          $(target).data("remove-url", data.remove_url);
        },
        error: function (data) {
          showToast("Failed");
        }
      });
    }
  });

  $('.quick-add').on('click', function(event) {
    const csrfmiddlewaretoken = $("input[name='csrfmiddlewaretoken']").val();
    const url = $(this).data("url");

    displayLoading();

    $.ajax({
      type: "POST",
      url,
      data: {
        csrfmiddlewaretoken,
        quantity: 1
      },
      success: function (data) {
        displayAddedItemToast();
        displayLoading();
        syncCart();
      },
      error: function (data) {
        showToast("Failed");
      }
    });
  });

  //wishlist on detail page
  $('.btn-heart').on('click', function(event) {
    if ($('.heart').hasClass('red')){
      //action remove wishlist here
      $('.text-heart').text('Add to Wishlist');
      $('.heart').removeClass('red');
      showToast('Remove from wishlist');
    } else {
      $('.text-heart').text('Removed from wishlist');
      //action add wishlist here
      $('.heart').toggleClass('red');
      showToast('Added to wishlist');
    }
  });

  function displayAddedItemToast () {
    hideLoading();

    $.toast({
      text: "<a href='/basket/summary'>View shopping bag?</a>",
      heading: 'Added to shopping bag',
      showHideTransition: 'fade',
      allowToastClose: true,
      hideAfter: 15000,
      stack: 1,
      position: 'bottom-center',
      bgColor: '#000000',
      textColor: '#ffffff',
      textAlign: 'center',
      loader: true,
      loaderBg: '#9EC600',
    });
  }

  //add to cart
  $('.btn-checkout').on('click', function(event) {
    displayAddedItemToast();
    //action here
  });

  //
  $('.btn-quickmodal-add').on('click', function(event) {
    //action here
    displayAddedItemToast();

    //close modal
    $('#modalQuick').modal('hide');
  });

  //mobile pick color
  $('.pick-color input:radio').change(function() {
    var nameColor = $(this).attr("val")
    $('.warna').text(nameColor);
  });

  $('.pick-color-big input:radio').change(function() {
    //action here and close modal
    var pickColor = $('input[name="colorGroup"]:checked').val();
    var nameColor = $(this).attr("val")

    $('.warna-big').text(nameColor);
    $('.bg-color').css('background',pickColor);
    $('.bg-color').html(nameColor);

    setTimeout(function(){
      $('#fullscreen_modal').modal('hide');
    },1000);
  });

  //count button
  $('.minus').click(function () {
    var $input = $(this).parent().find('input');
    var count = parseInt($input.val()) - 1;

    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();

    return false;
  });

  $('.plus').click(function () {
    var $input = $(this).parent().find('input');
    $input.val(parseInt($input.val()) + 1);
    $input.change();

    return false;
  });

  $(".input-number").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .

    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
    // Allow: Ctrl+A
      (e.keyCode == 65 && e.ctrlKey === true) ||
       // Allow: home, end, left, right
      (e.keyCode >= 35 && e.keyCode <= 39)) {

      // let it happen, don't do anything
      return;
    }

     // Ensure that it is a number and stop the keypress
     if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
     }
  });

  $(".input-number").change(function() {
    var max = parseInt($(this).attr('max'));
    var min = parseInt($(this).attr('min'));

    if ($(this).val() > max) {
      $(this).val(max);
    } else if ($(this).val() < min) {
      $(this).val(min);
    }
  });

})(jQuery);

function showModalEditProfile(str) {
    if (str === 'open') {
        $('.edit-profile').removeClass('none')
    } else {
        $('.edit-profile').addClass('none')
    }
}
