function displayLoading () {
  $body = $("body");
  $body.addClass("on-loading");
}

function hideLoading () {
  $body = $("body");
  $body.removeClass("on-loading");
}

$("form").submit(function (e) {
    displayLoading();
});

/* Project specific Javascript goes here. */
function docReady(fn) {
  // see if DOM is already available
  if (document.readyState === "complete" || document.readyState === "interactive") {
    // call on next available tick
    setTimeout(fn, 1);
  } else {
    document.addEventListener("DOMContentLoaded", fn);
  }
}

docReady(function() {
  $(document).on({
    ajaxStart: function() { displayLoading(); },
    ajaxStop: function() { hideLoading (); }
  });
});

function alertErrorResponse (data) {
  if (data.responseJSON.form.errors) {
    alert (data.responseJSON.form.errors[0])
  }
}

function findAllByKey(obj, keyToFind) {
  return Object.entries(obj)
    .reduce((acc, [key, value]) => (key === keyToFind)
      ? acc.concat(value)
      : (typeof value === 'object')
      ? acc.concat(findAllByKey(value, keyToFind))
      : acc
    , [])
}
