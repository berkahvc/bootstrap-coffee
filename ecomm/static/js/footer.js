
(function ($) {
  'use strict';
  //loader image
  $('.lazy').lazy();

  //slider
  $('.slides').slick({
    accessibility: false,
    arrows: false,
    autoplay: true,
    dots: true,
  });

  $('.slide-thumb').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
    dots: false,
  });

  const magnify = 2;

  $('#bla').zoom({
    magnify
  });

  //main image + thumb
  $('.thumbs').delegate('img','click', function(){
    $('.mainImage').attr('src',$(this).attr('src').replace('thumb','big'));
    $('#bla').zoom({
      magnify
    });
  });
})(jQuery);
