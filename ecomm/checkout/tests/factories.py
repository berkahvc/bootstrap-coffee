from django.contrib.auth.models import User
from factory.django import DjangoModelFactory

from ecomm.order.models import Order


class OrderFactory(DjangoModelFactory):
    class Meta:
        model = Order


class AuthUserFactory(DjangoModelFactory):
    class Meta:
        model = User
