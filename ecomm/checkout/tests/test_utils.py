import json
import logging
import os
from unittest.mock import patch

import pytest
from django.core.exceptions import ValidationError
from django.test import TestCase

from ecomm.order.enums import OrderStatus

from ...shipping.methods import DefaultDelivery
from ..enums import MidtransFraudStatus, MidtransTransactionStatus
from ..utils import (
    decide_order_status,
    handle_midtrans_payment,
)
from .factories import OrderFactory

LOGGER = logging.getLogger(__name__)

pytestmark = pytest.mark.django_db

path_to_current_file = os.path.realpath(__file__)
current_directory = os.path.split(path_to_current_file)[0]


def test_decide_order_status_should_work_fine():
    result = decide_order_status(
        transaction_status=MidtransTransactionStatus.SETTLEMENT.value,
        fraud_status=MidtransFraudStatus.ACCEPT.value,
    )

    assert result, OrderStatus.DETECTED_AS_FRAUD.value


def test_decide_order_status_should_work_fine_on_pending():
    result = decide_order_status(
        transaction_status=MidtransTransactionStatus.PENDING.value,
        fraud_status=MidtransFraudStatus.ACCEPT.value,
    )

    assert result, OrderStatus.WAIT_FOR_PAYMENT.value


def test_decide_order_status_should_handle_fraud_properly():
    result = decide_order_status(
        transaction_status=MidtransTransactionStatus.CAPTURE.value,
        fraud_status=MidtransFraudStatus.CHALLENGE.value,
    )

    assert result == OrderStatus.DETECTED_AS_FRAUD.value


def test_decide_order_status_should_handle_non_fraud_properly():
    result = decide_order_status(
        transaction_status=MidtransTransactionStatus.CAPTURE.value,
        fraud_status=MidtransFraudStatus.ACCEPT.value,
    )

    assert result == OrderStatus.PAID.value


def test_decide_order_status_should_work_fine_on_denied_transaction():
    result = decide_order_status(
        transaction_status=MidtransTransactionStatus.DENY.value,
        fraud_status=MidtransFraudStatus.ACCEPT.value,
    )

    assert result == OrderStatus.DENIED.value


def test_decide_order_status_should_work_fine_on_cancelled_transaction():
    result = decide_order_status(
        transaction_status=MidtransTransactionStatus.CANCEL.value,
        fraud_status=MidtransFraudStatus.ACCEPT.value,
    )

    assert result == OrderStatus.CANCELLED.value


def test_decide_order_status_should_work_fine_on_expired_transaction():
    result = decide_order_status(
        transaction_status=MidtransTransactionStatus.EXPIRE.value,
        fraud_status=MidtransFraudStatus.ACCEPT.value,
    )

    assert result == OrderStatus.EXPIRED.value


@pytest.mark.django_db
def test_handle_midtrans_payment(settings):
    path_to_file = os.path.join(current_directory, "data/midtrans_payment_webhook.json")
    with open(path_to_file) as json_file:
        midtrans_payment_webhook = json.load(json_file)

        order_id = midtrans_payment_webhook["order_id"]

        OrderFactory(
            id=order_id,
            number=order_id,
            total_incl_tax=5000,
            total_excl_tax=5000,
            status=OrderStatus.WAIT_FOR_PAYMENT.value,
        )

        result = handle_midtrans_payment(
            midtrans_webhook_request=midtrans_payment_webhook
        )

        assert result.status == OrderStatus.EXPIRED.value


@pytest.mark.django_db
def test_handle_midtrans_payment_with_invalid_status(caplog):
    path_to_file = os.path.join(current_directory, "data/midtrans_payment_webhook.json")
    with open(path_to_file) as json_file:
        midtrans_payment_webhook = json.load(json_file)

        order_id = midtrans_payment_webhook["order_id"]

        OrderFactory(
            id=order_id,
            number=order_id,
            total_incl_tax=5000,
            total_excl_tax=5000,
            status=OrderStatus.PAID.value,
        )
        result = handle_midtrans_payment(
            midtrans_webhook_request=midtrans_payment_webhook
        )

        assert result.status == OrderStatus.PAID.value
        assert (
            "'EXPIRED' is not a valid status for order 100009 (current status: 'PAID')"
            in caplog.text
        )


@pytest.mark.django_db
def test_handle_midtrans_payment_with_invalid_signature_key():
    path_to_file = os.path.join(current_directory, "data/midtrans_payment_webhook.json")
    with open(path_to_file) as json_file:
        with pytest.raises(ValidationError):
            midtrans_payment_webhook = json.load(json_file)

            midtrans_payment_webhook["signature_key"] = "invalid"

            result = handle_midtrans_payment(
                midtrans_webhook_request=midtrans_payment_webhook
            )

            assert result.status == OrderStatus.EXPIRED.value


@pytest.mark.django_db
def test_handle_midtrans_payment_with_accept_status(caplog, user):
    path_to_file = os.path.join(
        current_directory, "data/midtrans_payment_webhook_paid.json"
    )
    with open(path_to_file) as json_file:
        midtrans_payment_webhook = json.load(json_file)

        order_id = midtrans_payment_webhook["order_id"]

        OrderFactory(
            id=order_id,
            user=user,
            number=order_id,
            total_incl_tax=5000,
            total_excl_tax=5000,
            status=OrderStatus.WAIT_FOR_PAYMENT.value,
        )
        result = handle_midtrans_payment(
            midtrans_webhook_request=midtrans_payment_webhook
        )

        assert result.status == OrderStatus.PAID.value
