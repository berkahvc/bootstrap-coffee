import enum


# Using enum class create enumerations
class MidtransTransactionStatus(enum.Enum):
    PENDING = "PENDING"
    SETTLEMENT = "SETTLEMENT"
    CAPTURE = "CAPTURE"
    CANCEL = "CANCEL"
    DENY = "DENY"
    EXPIRE = "EXPIRE"

    @classmethod
    def choices(cls):
        return tuple((i.name, i.value) for i in cls)

    @classmethod
    def name_choices(cls):
        return tuple(i.name for i in cls)


class MidtransFraudStatus(enum.Enum):
    CHALLENGE = "CHALLENGE"
    ACCEPT = "ACCEPT"

    @classmethod
    def choices(cls):
        return tuple((i.name, i.value) for i in cls)

    @classmethod
    def name_choices(cls):
        return tuple(i.name for i in cls)
