from django.http import HttpResponseRedirect
from oscar.apps.checkout.mixins import OrderPlacementMixin as CoreOrderPlacementMixin

from ecomm.subscription_management.models import SubscriptionPrepaid


class OrderPlacementMixin(CoreOrderPlacementMixin):
    def handle_successful_order(self, order):
        """
        Handle the various steps required after an order has been successfully
        placed.

        Override this view if you want to perform custom actions when an
        order is submitted.
        """
        # Send confirmation message (normally an email)
        # self.send_order_placed_email(order)
        subscription_id = self.checkout_session.subscription_id()

        # Flush all session data
        self.checkout_session.flush()

        # Save order id in session so thank-you page can load it
        self.request.session["checkout_order_id"] = order.id

        if subscription_id:
            subscription_prepaid = SubscriptionPrepaid.objects.get(
                djsubscription_id=subscription_id
            )
            subscription_prepaid.shipping_address = order.shipping_address
            subscription_prepaid.save()

            order.subscription_id = subscription_id
            order.save()

        response = HttpResponseRedirect(self.get_success_url())
        self.send_signal(self.request, response, order)
        return response
