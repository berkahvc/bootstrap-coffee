from datetime import timedelta

import pytz
from django.core.management.base import BaseCommand
from django.utils.timezone import localtime
from djstripe.models import Customer, Subscription

from ecomm.subscription_management.models import SubscriptionPrepaid
from ecomm.utils.utils import int_day_conversion

# ./manage.py fix_delivery_date


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        _ = [
            "tanya.ragu28@gmail.com",
            "natong1411@gmail.com",
            "luv1362@gmail.com",
            "winniefreddd@gmail.com",
            "sionghoe@hotmail.com",
            "umav2807@gmail.com",
            "xingyu74@gmail.com",
            "yeocharmian@gmail.com",
            "kadanjoe@hotmail",
            "seeyun@gmail.com",
            "alegnaxu@gmail.com",
        ]
        subscription_prepaids = SubscriptionPrepaid.objects.filter(
            updated_at__lt="2022-03-07"
        ).exclude(charge_frequency__in=[26, 52])

        for sp in subscription_prepaids:
            subscription = Subscription.objects.get(id=sp.djsubscription_id)

            if subscription.livemode:
                customer = Customer.objects.get(id=sp.djcustomer_id)
                tz = pytz.timezone("Asia/Singapore")
                next_delivery_date = localtime(
                    subscription.current_period_end, timezone=tz
                ) + timedelta(days=(int_day_conversion(sp.delivery_day) + 1))
                print("================== ///// ==================")
                print(
                    f"===== current_period_end {localtime(subscription.current_period_end, timezone=tz)}"
                )
                print(f"===== email {customer.subscriber.email}")
                print(f"===== sub_id {sp.djsubscription_id}")
                print(f"===== delivery day {sp.delivery_day}")
                print(f"===== next delivery date {next_delivery_date}")
                print("================== ///// ==================")
                sp.next_delivery_date = next_delivery_date
                sp.save()
