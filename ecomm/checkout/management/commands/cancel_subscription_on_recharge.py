from django.core.management.base import BaseCommand

from ecomm.common.recharge_client import RechargeClient

# ./manage.py cancel_subscription_on_recharge


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("--customer_email", action="append", type=str)

    def handle(self, *args, **kwargs):
        recharge_client = RechargeClient()
        total_subscriptions = recharge_client.get_count_subscriptions(
            params={"status": "ACTIVE"}
        )["count"]
        per_page = 50

        if kwargs["customer_email"]:
            customer = None
            for email in kwargs["customer_email"]:
                params = {"email": email}
                customer_result = recharge_client.get_customers(params)
                customers = customer_result["customers"]

                if len(customers) > 0:
                    customer = customers[0]
                break

            if customer:
                result = recharge_client.get_user_subscription_by_customer_id(
                    customer["id"], "ACTIVE"
                )
                process_cancel_subscription(result["subscriptions"], recharge_client)
        else:
            total_page = int(total_subscriptions / per_page)
            total_all_page = total_page * per_page

            if total_all_page < total_subscriptions:
                total_page += 1

            for i in range(0, total_page):
                page = i + 1
                print(f"Page {page}")
                result = recharge_client.get_subscriptions(
                    params={"status": "ACTIVE", "page": page}
                )

                process_cancel_subscription(result["subscriptions"], recharge_client)


def process_cancel_subscription(subscriptions, recharge_client):

    for subscription in subscriptions:
        subscription_id = subscription["id"]
        print(f"===== start cancel subscription id {subscription_id}")
        recharge_client.cancel_subscription(
            subscription_id, "Cancelled for migration. Subscription still continues"
        )
        print(f"===== done cancel subscription id {subscription_id}")
