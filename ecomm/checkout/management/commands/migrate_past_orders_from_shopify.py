from django.core.management.base import BaseCommand

from ecomm.common.shopify_client import ShopifyPrivateAppsClient

# ./manage.py migrate_past_orders_from_shopify


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        shopify_private_apps = ShopifyPrivateAppsClient()
        shopify_private_apps.read_orders()
