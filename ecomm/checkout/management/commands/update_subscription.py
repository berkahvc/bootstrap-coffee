from django.core.management.base import BaseCommand
from djstripe.models import SubscriptionItem as DjSubscriptionItem

from ecomm.address.models import Country
from ecomm.common.recharge_client import RechargeClient
from ecomm.order.models import ShippingAddress
from ecomm.subscription_management.models import SubscriptionPrepaid

# ./manage.py update_subscription


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        recharge_client = RechargeClient()

        subscription_prepaids = (
            SubscriptionPrepaid.objects.values_list("user__email", flat=True)
            .filter(shipping_address__isnull=True)
            .distinct()
        )

        for subscription_prepaid in subscription_prepaids:
            print(f"===== starting email {subscription_prepaid}")
            customers = recharge_client.get_customers({"email": subscription_prepaid})

            if len(customers["customers"]) > 0:
                customer_id = customers["customers"][0]["id"]
                subscription_params = {
                    "status": "CANCELLED",
                    "updated_at_min": "2022-02-25",
                    "customer_id": customer_id,
                }
                subscriptions = recharge_client.get_subscriptions(subscription_params)

                for subscription in subscriptions["subscriptions"]:
                    address_id = subscription["address_id"]
                    quantity = subscription["quantity"]
                    subscription_id = subscription["id"]
                    shopify_product_id = subscription["shopify_product_id"]
                    shopify_variant_id = subscription["shopify_variant_id"]
                    product_title = subscription["product_title"]  # NOTE!

                    djstripe_product_title = f"{subscription_prepaid}-{shopify_product_id}-{shopify_variant_id}"

                    order_params = {"subscription_id": subscription_id}
                    addresses = recharge_client.get_address_by_id(address_id)
                    orders = recharge_client.get_orders(params=order_params)
                    latest_order = orders["orders"][0]
                    order_note = latest_order.get("note", None)
                    address = addresses["address"]
                    cart_note = address.get("cart_note", None)

                    note = None
                    if cart_note:
                        note = cart_note
                    elif cart_note:
                        note = order_note

                    """
                    "shipping_address": {
                        "address1": "51 Jurong East Ave 1",
                        "address2": "#15-04",
                        "city": "Singapore",
                        "company": null,
                        "country": "Singapore",
                        "first_name": "Zhi Jing",
                        "last_name": "Lui",
                        "phone": "84486898",
                        "province": "",
                        "zip": "609782"
                    },
                    """

                    shipping_address_order = latest_order["shipping_address"]

                    country = Country.objects.get(
                        printable_name=shipping_address_order["country"]
                    )

                    phone = None
                    shipping_address = None
                    try:
                        phone = shipping_address_order["phone"]
                        if phone.startswith("+65", 0, 3):
                            pass
                        elif phone.startswith("65", 0, 2):
                            phone = f"+{phone}"
                        else:
                            phone = f"+65{phone}"
                    except Exception as err:
                        print(f"==== {err}")

                    default_shipping_address = {
                        "first_name": shipping_address_order["first_name"],
                        "last_name": shipping_address_order["last_name"],
                        "line4": shipping_address_order["city"],
                        "state": shipping_address_order["province"] or "Singapore",
                        "phone_number": phone,
                        "country": country,
                    }

                    full_address = shipping_address_order["address1"]

                    if shipping_address_order["address2"]:
                        full_address += f', {shipping_address_order["address2"]}'

                    try:
                        shipping_address, _ = ShippingAddress.objects.get_or_create(
                            **{
                                "email": subscription_prepaid,
                                "line1": full_address,
                                "postcode": shipping_address_order["zip"],
                            },
                            defaults=default_shipping_address,
                        )
                    except ShippingAddress.MultipleObjectsReturned:
                        shipping_address_list = ShippingAddress.objects.filter(
                            **{
                                "email": subscription_prepaid,
                                "line1": full_address,
                                "postcode": shipping_address_order["zip"],
                            }
                        ).order_by("-id")

                        shipping_address = shipping_address_list.first()
                        shipping_address.first_name = shipping_address_order[
                            "first_name"
                        ]
                        shipping_address.last_name = shipping_address_order["last_name"]
                        shipping_address.line1 = full_address
                        shipping_address.line4 = shipping_address_order["city"]
                        shipping_address.state = (
                            shipping_address_order["province"] or "Singapore"
                        )
                        shipping_address.phone_number = phone
                        shipping_address.save()
                    except Exception as e:
                        print(f"===== {e}")

                    try:
                        djsubscription_item = DjSubscriptionItem.objects.get(
                            plan__product__name=djstripe_product_title
                        )
                        djstripe_subscription_id = djsubscription_item.subscription.id
                        sp = SubscriptionPrepaid.objects.get(
                            djsubscription_id=djstripe_subscription_id
                        )
                        sp.note = note if note else product_title
                        sp.quantity = quantity
                        sp.shipping_address = shipping_address
                        sp.save()
                    except DjSubscriptionItem.MultipleObjectsReturned:
                        print(f"===== {djstripe_product_title} found multiple")
                        djsubscription_item = DjSubscriptionItem.objects.filter(
                            plan__product__name=djstripe_product_title
                        )
                        for subs in djsubscription_item:
                            djstripe_subscription_id = subs.subscription.id
                            try:
                                sp = SubscriptionPrepaid.objects.get(
                                    djsubscription_id=djstripe_subscription_id
                                )
                                sp.note = product_title
                                sp.quantity = quantity
                                sp.shipping_address = shipping_address
                                sp.save()
                            except SubscriptionPrepaid.DoesNotExist:
                                continue
                    except DjSubscriptionItem.DoesNotExist:
                        print(f"===== {djstripe_product_title} not found")
                        continue
                    except Exception as err:
                        print(f"===== {djstripe_product_title} not found: {err}")
                        continue
