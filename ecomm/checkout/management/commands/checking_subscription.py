from collections import defaultdict

from django.core.management.base import BaseCommand

from ecomm.common.recharge_client import RechargeClient
from ecomm.order.models import Order
from ecomm.subscription_management.models import SubscriptionPrepaid

# ./manage.py checking_subscription


class Command(BaseCommand):
    def add_arguments(self, parser) -> None:
        parser.add_argument("--date", type=str, help="date", nargs="?")

    def handle(self, *args, **kwargs):
        recharge_client = RechargeClient()

        subscription_prepaids = SubscriptionPrepaid.objects.all()
        max_number = subscription_prepaids.count()
        current_number = 0
        user_list = list()
        list_of_missed_subscriptions = defaultdict(list)

        for subscription_prepaid in subscription_prepaids:
            current_number += 1
            user_email = subscription_prepaid.user.email
            customers = recharge_client.get_customers({"email": user_email})
            is_sent = Order.objects.filter(
                user__email=user_email, number__startswith="#4"
            ).exists()

            if len(customers["customers"]) > 0:
                customer_id = customers["customers"][0]["id"]
                subscription_params = {
                    "status": "CANCELLED",
                    "updated_at_min": "2022-02-25",
                    "customer_id": customer_id,
                }
                subscriptions = recharge_client.get_subscriptions(subscription_params)

                for subscription in subscriptions["subscriptions"]:
                    address_id = subscription["address_id"]
                    _ = subscription["id"]
                    # order_interval_frequency = subscription["order_interval_frequency"]
                    # product_title = subscription["product_title"]

                    addresses = recharge_client.get_address_by_id(address_id)
                    address = addresses["address"]
                    note_attributes = address.get("note_attributes", [])
                    delivery_date = None
                    # delivery_day = None
                    # delivery_time = None

                    for attribute in note_attributes:
                        if attribute["name"] == "Delivery-Date":
                            delivery_date = attribute["value"].replace("/", "-")
                        # elif attribute["name"] == "Delivery-Time":
                        #     delivery_time = attribute["value"]
                        # elif attribute["name"] == "Delivery-Day":
                        #     delivery_day = attribute["value"]

                    if kwargs["date"]:
                        date = [kwargs["date"]]
                    else:
                        date = [
                            "2022-03-05",
                            "2022-03-04",
                            "2022-03-03",
                            "2022-03-02",
                            "2022-03-01",
                            "2022-02-28",
                        ]

                    print(
                        f"{float(current_number/max_number)*100}% ({current_number}/{max_number})"
                    )
                    if delivery_date in date and not is_sent:
                        list_of_missed_subscriptions[delivery_date].append(user_email)
                        user_list.append(user_email)

                    # elif delivery_date == "2022-03-05" and not is_sent:
                    #     print(f"========== {user_email} is delivering Sat")
                    #     print("\n")
        print(user_list)
        print(list_of_missed_subscriptions)
