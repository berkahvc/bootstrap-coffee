import calendar
import json
import logging
import os
from datetime import datetime

import dateutil.relativedelta as relativedelta
import dateutil.rrule as rrule
import shopify
import stripe
from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils.crypto import get_random_string
from djstripe.models import Customer as DjStripeCustomer
from djstripe.models import PaymentMethod as DjStripePaymentMethod
from djstripe.models import Price as DjStripePrice
from djstripe.models import Product as DjStripeProduct
from djstripe.models import Subscription as DjStripeSubscription
from djstripe.models import SubscriptionItem as DjSubscriptionItem
from djstripe.settings import STRIPE_SECRET_KEY

from ecomm.address.models import Country, UserAddress
from ecomm.catalogue.models import Product, ProductClass
from ecomm.common.recharge_client import RechargeClient
from ecomm.order.models import ShippingAddress
from ecomm.partner.models import Partner, StockRecord
from ecomm.subscription_management.models import SubscriptionPrepaid
from ecomm.users.models import User

# ./manage.py migrate_subscription_from_recharge

stripe.api_key = STRIPE_SECRET_KEY


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            "--test",
            action="store_true",
            help="Run use test data",
        )
        parser.add_argument(
            "--test_file",
            action="store_true",
            help="Run use test data",
        )

        parser.add_argument(
            "customer_email", nargs="?", type=str, help="Customer email"
        )

    def handle(self, *args, **kwargs):
        logging.info(f"STRIPE_LIVE_MODE: {settings.STRIPE_LIVE_MODE}")
        print(f"STRIPE_LIVE_MODE: {settings.STRIPE_LIVE_MODE}")

        recharge_client = RechargeClient()

        is_test = False
        is_data_live = True
        use_fake_credit_card = False
        total_subscriptions = recharge_client.get_count_subscriptions(
            params={"status": "ACTIVE"}
        )["count"]
        per_page = 50

        if kwargs["test"]:
            is_test = True
            use_fake_credit_card = True

            total_page = int(total_subscriptions / per_page)
            total_all_page = total_page * per_page

            if total_all_page < total_subscriptions:
                total_page += 1

            error_next_charge = []
            error_invalid_timestamp = []

            for i in range(0, total_page):
                page = i + 1
                print(f"Page {page}")
                result = recharge_client.get_subscriptions(
                    params={"status": "ACTIVE", "page": page}
                )

                err_next_charge, err_invalid_timestamp = process_subscription_migration(
                    result["subscriptions"],
                    is_test=is_test,
                    is_data_live=is_data_live,
                    use_fake_credit_card=use_fake_credit_card,
                )

                error_next_charge += err_next_charge
                error_invalid_timestamp += err_invalid_timestamp

            print(f"=============== **** {error_next_charge}")
            print(
                f"=============== **** total error next_charge is null {len(error_next_charge)}"
            )

            print(f"=============== **** {error_invalid_timestamp}")
            print(
                f"=============== **** total error invalid_timestamp {len(error_invalid_timestamp)}"
            )
        elif kwargs["test_file"]:
            is_test = True
            is_data_live = False
            use_fake_credit_card = True
            path_to_current_file = os.path.realpath(__file__)
            current_directory = os.path.split(path_to_current_file)[0]
            path_to_file = os.path.join(
                current_directory, "data/recharge_subscription.json"
            )
            result = json.load(open(path_to_file))
            result.update(
                {
                    "is_test": is_test,
                    "is_data_live": is_data_live,
                    "use_fake_credit_card": use_fake_credit_card,
                }
            )
        elif kwargs["customer_email"]:
            customer = None
            result = {"subscriptions": []}
            for email in kwargs["customer_email"]:
                params = {"email": email}
                customer_result = recharge_client.get_customers(params)
                customers = customer_result["customers"]

                if len(customers) > 0:
                    customer = customers[0]
                break

            if customer:
                result = recharge_client.get_user_subscription_by_customer_id(
                    customer["id"], "ACTIVE"
                )
        else:
            total_page = int(total_subscriptions / per_page)
            total_all_page = total_page * per_page

            if total_all_page < total_subscriptions:
                total_page += 1

            error_next_charge = []
            error_invalid_timestamp = []

            for i in range(0, total_page):
                page = i + 1
                print(f"Page {page}")
                result = recharge_client.get_subscriptions(
                    params={"status": "ACTIVE", "page": page}
                )
                err_next_charge, err_invalid_timestamp = process_subscription_migration(
                    result["subscriptions"],
                    is_test=is_test,
                    is_data_live=is_data_live,
                    use_fake_credit_card=use_fake_credit_card,
                )

                error_next_charge += err_next_charge
                error_invalid_timestamp += err_invalid_timestamp

            print(f"=============== **** {error_next_charge}")
            print(
                f"=============== **** total error next_charge is null {len(error_next_charge)}"
            )

            print(f"=============== **** {error_invalid_timestamp}")
            print(
                f"=============== **** total error invalid_timestamp {len(error_invalid_timestamp)}"
            )


def process_subscription_migration(
    subscriptions, is_test, is_data_live, use_fake_credit_card
):
    print(f"===== total subscriptions {len(subscriptions)}")

    shopify_password = settings.SHOPIFY_PASSWORD
    shop_url = settings.SHOP_URL
    api_version = "2021-10"
    today = datetime.now()
    month = today.month
    year = today.year
    _calendar = calendar.monthrange(year, month)

    first_day = _calendar[0]
    last_day = _calendar[1]

    before = datetime(year, month, first_day)
    after = datetime(year, month, last_day)
    rr = rrule.rrule(rrule.WEEKLY, byweekday=relativedelta.SU, dtstart=before)

    logging.info(f"STRIPE_LIVE_MODE: {settings.STRIPE_LIVE_MODE}")
    print(f"STRIPE_LIVE_MODE: {settings.STRIPE_LIVE_MODE}")

    recharge_client = RechargeClient()

    session = shopify.Session(shop_url, api_version, shopify_password)
    shopify.ShopifyResource.activate_session(session)

    next_charge_dt_test = None
    if not is_data_live:
        for i in rr.between(before, after, inc=True):
            if i > today:
                next_charge_dt_test = i
                break

    error_next_charge = []
    error_invalid_timestamp = []

    for subscription in subscriptions:
        address_id = subscription["address_id"]
        customer_id = subscription["customer_id"]
        email = subscription["email"]
        shopify_variant_id = subscription["shopify_variant_id"]
        shopify_product_id = subscription["shopify_product_id"]
        charge_interval_frequency = subscription["charge_interval_frequency"]
        order_interval_frequency = subscription["order_interval_frequency"]
        order_interval_unit = subscription["order_interval_unit"]
        _ = subscription["order_day_of_week"]  # delivery date
        next_charge_scheduled_at = subscription["next_charge_scheduled_at"]
        subscription_price = subscription["price"]
        quantity = subscription["quantity"]
        variant_title = subscription["variant_title"]

        if not next_charge_scheduled_at:
            error_next_charge.append(email)
            continue

        addresses = recharge_client.get_address_by_id(address_id)
        address = addresses["address"]
        note_attributes = address["note_attributes"]
        cart_note = address.get("cart_note", None)
        delivery_date = None
        delivery_day = None
        delivery_time = None

        for attribute in note_attributes:
            if attribute["name"] == "Delivery-Date":
                delivery_date = attribute["value"].replace("/", "-")
            elif attribute["name"] == "Delivery-Time":
                delivery_time = attribute["value"]
            elif attribute["name"] == "Delivery-Day":
                delivery_day = attribute["value"]

        if delivery_day is None:
            delivery_day = (
                datetime.strptime(delivery_date, "%Y-%m-%d").strftime("%A")
                if delivery_date
                else None
            )

        # find customer
        user = None
        try:
            user = User.objects.get(email=email)
            print("User found", email)
        except User.DoesNotExist:
            print("User not found", email)
            try:
                recharge_customer = recharge_client.get_customers_by_customer_id(
                    customer_id
                )
                customer = recharge_customer["customer"]
                first_name = customer["first_name"]
                last_name = customer["last_name"] or ""
                email = customer["email"]

                data = {
                    "first_name": first_name,
                    "last_name": last_name,
                    "email": email,
                }

                user, created = User.objects.get_or_create(
                    **{"username": email}, defaults=data
                )
                if created:
                    user.is_from_shopify = True
                    user.shopify_customer_id = customer["shopify_customer_id"]
                    user.set_unusable_password()
                    user.save()

                    country = Country.objects.get(
                        printable_name=customer["billing_country"]
                    )
                    phone = customer["billing_phone"].replace(" ", "")
                    if phone.startswith("+65", 0, 3):
                        pass
                    else:
                        phone = f"+65{phone}"
                    user_address = UserAddress()
                    user_address.user = user
                    user_address.email = email
                    user_address.first_name = first_name
                    user_address.last_name = last_name
                    user_address.line1 = address["address1"]
                    user_address.line4 = customer["billing_city"]
                    user_address.state = customer["billing_province"] or "Singapore"
                    user_address.postcode = customer["billing_zip"]
                    user_address.phone_number = phone
                    user_address.country = country
                    user_address.save()
                else:
                    user.is_from_shopify = True
                    user.shopify_customer_id = customer["shopify_customer_id"]
                    user.save()

            except Exception as err:
                logging.info(f"{err}")

        """
        shipping address
        """
        phone = None
        country = Country.objects.get(printable_name=address["country"])
        shipping_address = None
        try:
            phone = address["phone"]
            if phone.startswith("+65", 0, 3):
                pass
            elif phone.startswith("65", 0, 2):
                phone = f"+{phone}"
            else:
                phone = f"+65{phone}"
        except Exception as err:
            print(f"==== {err}")

        default_shipping_address = {
            "first_name": address["first_name"],
            "last_name": address["last_name"],
            "line4": address["city"],
            "state": address["province"] or "Singapore",
            "phone_number": phone,
            "country": country,
        }

        full_address = address["address1"]

        if address["address2"]:
            full_address += f', {address["address2"]}'

        try:
            shipping_address, _ = ShippingAddress.objects.get_or_create(
                **{
                    "email": email,
                    "line1": full_address,
                    "postcode": address["zip"],
                },
                defaults=default_shipping_address,
            )
        except ShippingAddress.MultipleObjectsReturned:
            shipping_address_list = ShippingAddress.objects.filter(
                **{
                    "email": email,
                    "line1": full_address,
                    "postcode": address["zip"],
                }
            ).order_by("-id")

            shipping_address = shipping_address_list.first()
            shipping_address.first_name = address["first_name"]
            shipping_address.last_name = address["last_name"]
            shipping_address.line1 = full_address
            shipping_address.line4 = address["city"]
            shipping_address.state = address["province"] or "Singapore"
            shipping_address.phone_number = phone
            shipping_address.save()
        except Exception as e:
            print(f"===== {e}")

        # find product
        try:
            product_child = Product.objects.get(shopify_variant_id=shopify_variant_id)
            product_child.shopify_product_id = shopify_product_id
            product_child.save()
        except Product.DoesNotExist:
            product_class, _ = ProductClass.objects.get_or_create(
                name="ShopifyProduct", track_stock=False
            )

            shopify_product = shopify.Product.find(shopify_product_id)
            variants = shopify_product.variants

            product_data = {
                "is_public": False,
                "shopify_product_id": shopify_product_id,
            }

            random_sku = get_random_string(5).upper()
            partner, _ = Partner.objects.get_or_create(name="Singapore")
            (product_parent, product_parent_created,) = Product.objects.get_or_create(
                **{
                    "title": shopify_product.title,
                    "product_class": product_class,
                },
                defaults=product_data,
            )

            if len(variants) > 0:
                product_parent.structure = "parent"
                product_parent.save()

                for variant in variants:
                    price = variant.price
                    sku = variant.sku
                    child_data = {
                        "title": variant.title,
                        "parent": product_parent,
                        "shopify_product_id": shopify_product_id,
                        "shopify_variant_id": variant.id,
                    }
                    child_data_defaults = {
                        "is_public": False,
                        "structure": "child",
                    }
                    child, child_created = Product.objects.get_or_create(
                        **child_data, defaults=child_data_defaults
                    )
                    if child_created:
                        StockRecord.objects.create(
                            product=child,
                            partner=partner,
                            partner_sku=sku or random_sku,
                            price_excl_tax=price,
                        )
            else:
                if product_parent_created:
                    StockRecord.objects.create(
                        product=product_parent,
                        partner=partner,
                        partner_sku=random_sku,
                        price_excl_tax=shopify_product.price,
                    )

        print("===== start creating subscription")
        if user:
            stripe_customers = stripe.Customer.list(email=user.email)
            if len(stripe_customers["data"]) > 0:
                stripe_customer_obj = stripe_customers["data"][0]
                djstripe_customer = DjStripeCustomer.sync_from_stripe_data(
                    stripe_customer_obj
                )
                djstripe_customer.subscriber = user
                djstripe_customer.save()
            else:
                djstripe_customer, _ = DjStripeCustomer.get_or_create(user)

            if djstripe_customer:
                djstripe_payment_method = None

                if is_test and use_fake_credit_card:
                    stripe_payment_method_obj = stripe.PaymentMethod.create(
                        type="card",
                        card={
                            "number": "4242424242424242",
                            "exp_month": 5,
                            "exp_year": 2030,
                            "cvc": "314",
                        },
                    )

                    djstripe_customer.add_payment_method(stripe_payment_method_obj)

                    djstripe_payment_method = (
                        DjStripePaymentMethod.sync_from_stripe_data(
                            stripe_payment_method_obj
                        )
                    )

                else:
                    stripe_payment_methods = stripe.Customer.list_payment_methods(
                        djstripe_customer.id, type="card"
                    )

                    if len(stripe_payment_methods["data"]) > 0:
                        stripe_payment_method_obj = stripe_payment_methods["data"][0]
                        djstripe_payment_method = (
                            DjStripePaymentMethod.sync_from_stripe_data(
                                stripe_payment_method_obj
                            )
                        )

                        djstripe_customer.add_payment_method(stripe_payment_method_obj)

                if djstripe_payment_method:
                    if (
                        djstripe_customer.default_source
                        or djstripe_customer.default_payment_method
                    ):
                        try:
                            pack = variant_title.lower().replace("pack", "").strip()
                            credit_per_delivery = int(pack) / 6
                        except ValueError:
                            pack = 6
                            credit_per_delivery = 1

                        product = Product.objects.get(
                            shopify_product_id=shopify_product_id,
                            shopify_variant_id=shopify_variant_id,
                        )
                        print(f"===== product parent {product.parent.title}")
                        print(f"===== product child {product.title}")
                        delivery_date_str = f"Delivery Day: {delivery_day}"
                        delivery_time_str = f"Delivery Time: {delivery_time}"
                        product_title = f"{product.parent.title} - {product.title}"
                        combined_description = (
                            f"{product_title}, {delivery_date_str}, {delivery_time_str}"
                        )
                        title = (
                            f"{user.username}-{shopify_product_id}-{shopify_variant_id}"
                        )
                        initial_credit = credit_per_delivery * int(
                            order_interval_frequency
                        )

                        if is_test and not is_data_live:
                            next_charge_dt = next_charge_dt_test
                        else:
                            next_charge_dt = datetime.strptime(
                                next_charge_scheduled_at, "%Y-%m-%dT%H:%M:%S"
                            )

                        try:
                            djstripe_product = DjStripeProduct.objects.get(name=title)
                            print("========================== djstripe_product found")
                            print(
                                f"========================== djstripe_product {title}"
                            )
                        except DjStripeProduct.MultipleObjectsReturned:
                            print(
                                f"============== DjStripeProduct.MultipleObjectsReturned {title}"
                            )
                            continue
                        except DjStripeProduct.DoesNotExist:
                            stripe_product = stripe.Product.create(
                                name=title,
                                description=combined_description,
                                metadata={"djstripe_subcription": title},
                            )
                            djstripe_product = DjStripeProduct.sync_from_stripe_data(
                                stripe_product
                            )

                        try:
                            djstripe_price = DjStripePrice.objects.get(
                                product=djstripe_product
                            )
                        except DjStripePrice.DoesNotExist:
                            stripe_price = stripe.Price.create(
                                currency="sgd",
                                unit_amount=f"{int(float(subscription_price) * 100)}",
                                recurring={
                                    "interval": order_interval_unit,
                                    "interval_count": charge_interval_frequency,
                                },
                                product=djstripe_product.id,
                                metadata={"djstripe_subcription": product.title},
                            )
                            djstripe_price = DjStripePrice.sync_from_stripe_data(
                                stripe_price
                            )
                        except DjStripePrice.MultipleObjectsReturned:
                            djstripe_price = (
                                DjStripePrice.objects.filter(product=djstripe_product)
                                .order_by("-created")
                                .first()
                            )

                        try:
                            djsubscription_item = DjSubscriptionItem.objects.get(
                                plan__product=djstripe_product, price=djstripe_price
                            )
                            print("========================== DjSubscriptionItem found")
                            djstripe_subscription_id = (
                                djsubscription_item.subscription.id
                            )
                            sp = SubscriptionPrepaid.objects.get(
                                djsubscription_id=djstripe_subscription_id
                            )
                            sp.note = cart_note
                            sp.shipping_address = shipping_address
                            sp.next_delivery_date = sp.delivery_date
                            sp.save()
                        except DjSubscriptionItem.DoesNotExist:
                            try:
                                stripe_subscription = stripe.Subscription.create(
                                    customer=f"{djstripe_customer.id}",
                                    items=[
                                        {
                                            "price": f"{djstripe_price.id}",
                                            "quantity": quantity,
                                        }
                                    ],
                                    billing_cycle_anchor=int(
                                        next_charge_dt.timestamp()
                                    ),
                                    trial_end=int(next_charge_dt.timestamp()),
                                )
                            except Exception as err:
                                logging.error(str(err))
                                error_invalid_timestamp.append(email)
                                continue

                            djstripe_subscription = (
                                DjStripeSubscription.sync_from_stripe_data(
                                    stripe_subscription
                                )
                            )

                            SubscriptionPrepaid.objects.create(
                                user=user,
                                djsubscription_id=djstripe_subscription.id,
                                djcustomer_id=djstripe_customer.id,
                                pack_size=pack,
                                description=product_title,
                                delivery_date=delivery_date,
                                delivery_day=delivery_day,
                                delivery_time=delivery_time,
                                delivery_frequency=int(order_interval_frequency),
                                charge_frequency=int(charge_interval_frequency),
                                initial_credit=initial_credit,
                                credit_per_delivery=int(credit_per_delivery),
                                next_credit_charge=int(credit_per_delivery),
                                next_delivery_date=delivery_date,
                                quantity=int(quantity),
                                note=cart_note,
                                shipping_address=shipping_address,
                            )
                else:
                    print(
                        "====== Not create subscription, because doesn't have payment method"
                    )
        print("======================")
    shopify.ShopifyResource.clear_session()

    return error_next_charge, error_invalid_timestamp
