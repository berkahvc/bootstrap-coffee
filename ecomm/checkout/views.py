import logging
from copy import copy
from datetime import datetime, timedelta
from decimal import Decimal

import stripe
import stripe.error
from django import http
from django.conf import settings
from django.http import JsonResponse
from django.templatetags.static import static
from django.urls import reverse
from django.utils.translation import gettext as _
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.views.generic import TemplateView
from djstripe import webhooks
from djstripe.models import Card, Customer, Price, Product
from djstripe.settings import STRIPE_SECRET_KEY
from facebook_business.adobjects.serverside.content import Content
from facebook_business.adobjects.serverside.custom_data import CustomData
from facebook_business.adobjects.serverside.event import Event
from facebook_business.adobjects.serverside.event_request import EventRequest
from facebook_business.adobjects.serverside.user_data import UserData
from facebook_business.api import FacebookAdsApi
from klaviyo_sdk import Client
from oscar.apps.checkout import signals
from oscar.apps.checkout.views import IndexView as CoreIndexView
from oscar.apps.checkout.views import PaymentDetailsView as CorePaymentDetailsView
from oscar.apps.checkout.views import ShippingAddressView as CoreShippingAddressView
from oscar.apps.checkout.views import ShippingMethodView as CoreShippingMethodView
from oscar.apps.checkout.views import ThankYouView as CoreThankYouView
from oscar.apps.payment.models import Source, SourceType
from oscar.core.loading import get_class, get_classes, get_model
from oscar.templatetags.purchase_info_tags import purchase_info_for_line

from ecomm.checkout.utils import (
    get_is_free_shipping,
    handle_stripe_payment,
    send_email_for_paid_order,
)
from ecomm.order.enums import OrderStatus
from ecomm.schedule_management.utils import (
    check_delivery_date_validation,
    get_holidays,
    get_start_available_day,
)
from ecomm.subscription_management.models import SubscriptionPrepaid
from ecomm.subscription_management.utils import create_or_update_payment_method
from ecomm.users.models import User
from ecomm.utils.utils import get_body_json

from ..catalogue.utils import get_product_dto_by_id
from ..payment.enums import AvailablePayment
from .tasks import (
    renew_transaction_history,
    send_subscription_more_than_52_weeks_to_william,
)

# Standard logger for checkout events
logger = logging.getLogger("ecomm.checkout.views")

client = Client(settings.KLAVIYO_API_KEY)

RedirectRequired, UnableToTakePayment, PaymentError = get_classes(
    "payment.exceptions", ["RedirectRequired", "UnableToTakePayment", "PaymentError"]
)
Repository = get_class("shipping.repository", "Repository")
OrderTotalCalculator = get_class("checkout.calculators", "OrderTotalCalculator")
SurchargeApplicator = get_class("checkout.applicator", "SurchargeApplicator")
NoShippingRequired = get_class("shipping.methods", "NoShippingRequired")

Order = get_model("order", "Order")
UnableToPlaceOrder = get_class("order.exceptions", "UnableToPlaceOrder")
UserAddress = get_model("address", "UserAddress")

stripe.api_key = STRIPE_SECRET_KEY
FacebookAdsApi.init(access_token=settings.PIXEL_ID)


@csrf_exempt
@require_http_methods(["POST"])
@webhooks.handler("invoice.payment_succeeded", "charge.succeeded")
def payment_webhook(request):
    body = get_body_json(request)
    handle_stripe_payment(body)
    return JsonResponse({"success": True})


def create_card_token(card):
    number = card.get("number")
    exp_month = card.get("exp_month")
    exp_year = card.get("exp_year")
    cvc = card.get("cvc")
    name = card.get("cardName")

    card_token = Card.create_token(number, exp_month, exp_year, cvc, name=name)
    return card_token


class AjaxOrderInformationView(CoreShippingMethodView, generic.TemplateView):
    def get(self, request, *args, **kwargs):
        basket = self.request.basket
        shipping_address = self.get_shipping_address(basket)
        shipping_method = self.get_shipping_method(
            basket=basket, shipping_address=shipping_address
        )

        payment_method = self.checkout_session.payment_method()

        shipping_address_dict = None
        if shipping_address:
            shipping_address_dict = copy(shipping_address).__dict__
            del shipping_address_dict["_state"]
            del shipping_address_dict["phone_number"]
            shipping_address_dict[
                "phone_number"
            ] = shipping_address.phone_number.__dict__

        shipping_method_dict = {}
        total = 0

        if shipping_method:
            method = self.get_default_shipping_method(self.request.basket)
            shipping_method_dict["code"] = shipping_method.code
            shipping_charge = method.calculate(self.request.basket)

            total = self.get_order_totals(
                basket,
                shipping_charge=shipping_charge,
                surcharges=None,
                **kwargs,
            ).__dict__

        lines = []
        price_currency = None
        selected_date = None
        delivery_time_id = None
        categories = []
        included_cruffin = []

        for line in basket.all_lines():
            line_info = purchase_info_for_line(request, line)
            product_primary_image = line.product.primary_image()

            categories += line.product.get_categories().all()

            if line.product.parent:
                if line.product.parent.title == "Cruffin And Cold Brew Gift Sets":
                    included_cruffin.append(True)
                else:
                    included_cruffin.append(False)
            else:
                if line.product.title == "Cruffin And Cold Brew Gift Sets":
                    included_cruffin.append(True)
                else:
                    included_cruffin.append(False)

            try:
                image_url = product_primary_image.original.url
            except Exception as err:
                logger.info(f"{err}")
                image_url = static("images/image_not_found.jpg")

            if line.delivery_date:
                selected_date = line.delivery_date

            if line.delivery_time:
                delivery_time_id = line.delivery_time_id

            line_dto = {
                "lineUrl": request.build_absolute_uri(
                    reverse(
                        "basket-line-detail",
                        kwargs={"basket_pk": basket.id, "pk": line.id},
                    )
                ),
                "basketId": basket.id,
                "id": line.id,
                "product": get_product_dto_by_id(line.product.id),
                "is_parent": False if line.product.parent else True,
                "description": ""
                if line.product.parent
                else "<br>".join(
                    [f"- {i}" for i in line.product.description.split(",")]
                ),
                "title": line.product.parent.title
                if line.product.parent
                else line.product.title,
                "quantity": line.quantity,
                "image_url": image_url,
                "line_price_excl_tax": line.line_price_excl_tax,
                "price_currency": line.price_currency,
                "price": line_info.price.__dict__,
                "availability": line_info.availability.__dict__,
                "is_subscription": line.is_subscription,
            }

            price_currency = line.price_currency

            lines.append(line_dto)

        offer_discounts = basket.offer_discounts
        grouped_voucher_discounts = basket.grouped_voucher_discounts

        offer_discounts_dict = []

        for offer_discount in offer_discounts:
            offer_discount_dict = copy(offer_discount)

            del offer_discount_dict["offer"]
            del offer_discount_dict["result"]

            offer_discounts_dict.append(offer_discount_dict)

        grouped_voucher_discounts_dict = []

        for grouped_voucher_discount in grouped_voucher_discounts:
            grouped_voucher_discount_dict = copy(grouped_voucher_discount)
            voucher_dict = grouped_voucher_discount_dict["voucher"].__dict__

            del voucher_dict["_state"]
            grouped_voucher_discount_dict["voucher"] = voucher_dict

            grouped_voucher_discount_dict["name"] = voucher_dict["name"]

            grouped_voucher_discounts_dict.append(grouped_voucher_discount_dict)

        start_date = get_start_available_day(categories)
        holidays = get_holidays()

        if basket.delivery_date and selected_date is None:
            if basket.delivery_date >= datetime.strptime(start_date, "%Y-%m-%d").date():
                selected_date = basket.delivery_date

        if basket.delivery_time_id and delivery_time_id is None:
            delivery_time_id = basket.delivery_time_id

        if True in included_cruffin:
            selected_date = None
            delivery_time_id = None

        response = {
            "total_incl_tax_excl_discounts": basket.total_incl_tax_excl_discounts,
            "total_incl_tax": basket.total_incl_tax,
            "offer_discounts": offer_discounts_dict,
            "grouped_voucher_discounts": grouped_voucher_discounts_dict,
            "lines": lines,
            "payment_method": payment_method,
            "shipping_address": shipping_address_dict,
            "shipping_method": shipping_method_dict,
            "total": total,
            "price_currency": price_currency,
            "start_date": start_date,
            "selectedDate": selected_date,
            "delivery_time_id": delivery_time_id,
            "holidays": holidays,
            "has_cruffin": True in included_cruffin,
        }

        return JsonResponse(response)

    def get_shipping_methods(self, basket):
        return Repository().get_shipping_methods(
            basket=self.request.basket, user=self.request.user, request=self.request
        )

    def get_default_shipping_address(self):
        if self.request.user.is_authenticated:
            return self.request.user.addresses.filter(
                is_default_for_shipping=True
            ).first()

    def get_default_shipping_method(self, basket):
        return Repository().get_default_shipping_method(
            basket=self.request.basket,
            user=self.request.user,
            request=self.request,
            shipping_addr=self.get_default_shipping_address(),
        )

    def get_free_shipping_method(self, basket):
        return Repository().get_free_shipping_method(
            basket=self.request.basket,
            user=self.request.user,
            request=self.request,
            shipping_addr=self.get_default_shipping_address(),
        )


class PaidThankYouView(generic.DetailView):
    """
    DEPRECATED FUNCTION. Use ThankYouView instead
    """

    template_name = "oscar/checkout/paid_thank_you.html"
    context_object_name = "order"

    def get_object(self, queryset=None):
        # We allow superusers to force an order thank-you page for testing
        order = None
        if self.request.user.is_superuser:
            if "order_number" in self.request.GET:
                order = Order._default_manager.get(
                    number=self.request.GET["order_number"]
                )
            elif "order_id" in self.request.GET:
                order = Order._default_manager.get(id=self.request.GET["order_id"])

        if not order:
            if "checkout_order_id" in self.request.session:
                order = Order._default_manager.get(
                    pk=self.request.session["checkout_order_id"]
                )
            else:
                raise http.Http404(_("No order found"))

        return order

    def get_context_data(self, *args, **kwargs):
        ctx = super().get_context_data(**kwargs)
        client.ListsSegments.unsubscribe(
            "Wi76hh", body={{"emails": [self.request.user.email]}}
        )
        client.ListsSegments.get_list_members(
            "Wi76hh", body={{"emails": [self.request.user.email]}}
        )
        # Remember whether this view has been loaded.
        # Only send tracking information on the first load.
        key = "order_{}_paid_thankyou_viewed".format(ctx["order"].pk)
        if not self.request.session.get(key, False):
            self.request.session[key] = True
            ctx["send_analytics_event"] = True

        else:
            ctx["send_analytics_event"] = False

        return ctx


class IndexView(CoreIndexView):
    template_name = "pages/checkout/personal_info.html"
    pre_conditions = [
        "check_basket_is_not_empty",
        "check_basket_is_valid",
        "check_basket_is_delivery_date_empty",
        "check_basket_is_delivery_time_empty",
        "check_a_valid_subscriptions_total",
        "check_a_valid_basket_delivery_date",
    ]

    def get_context_data(self, *args, **kwargs):
        ctx = super().get_context_data(**kwargs)

        # hardcode for SPH vouchers
        is_free_shipping = get_is_free_shipping(self.request.basket)

        method = self.get_default_shipping_method(self.request.basket)
        shipping_methods = self.get_shipping_methods(self.request.basket)
        if is_free_shipping:
            method = self.get_free_shipping_method(self.request.basket)
            shipping_methods = Repository().free_methods
        shipping_charge = method.calculate(self.request.basket)
        surcharges = SurchargeApplicator(self.request, ctx).get_applicable_surcharges(
            self.request.basket
        )

        ctx["shipping_methods"] = shipping_methods
        ctx["shipping_method"] = method
        ctx["shipping_charge"] = shipping_charge
        ctx["order_total"] = OrderTotalCalculator().calculate(
            self.request.basket, shipping_charge, surcharges=surcharges
        )
        ctx["basket"] = self.request.basket
        ctx["is_has_subscription"] = self.request.basket.is_has_subscription()
        return ctx

    def get_shipping_methods(self, basket):
        return Repository().get_shipping_methods(
            basket=self.request.basket, user=self.request.user, request=self.request
        )

    def get_default_shipping_address(self):
        if self.request.user.is_authenticated:
            return self.request.user.addresses.filter(
                is_default_for_shipping=True
            ).first()

    def get_default_shipping_method(self, basket):
        return Repository().get_default_shipping_method(
            basket=self.request.basket,
            user=self.request.user,
            request=self.request,
            shipping_addr=self.get_default_shipping_address(),
        )

    def get_free_shipping_method(self, basket):
        return Repository().get_free_shipping_method(
            basket=self.request.basket,
            user=self.request.user,
            request=self.request,
            shipping_addr=self.get_default_shipping_address(),
        )


class ShippingAddressView(CoreShippingAddressView):
    pre_conditions = [
        "check_basket_is_not_empty",
        "check_basket_is_valid",
        "check_user_email_is_captured",
        "check_basket_is_delivery_date_empty",
        "check_basket_is_delivery_time_empty",
        "check_a_valid_subscriptions_total",
        "check_a_valid_basket_delivery_date",
    ]

    def get_context_data(self, *args, **kwargs):
        ctx = super().get_context_data(**kwargs)

        # hardcode for SPH vouchers
        is_free_shipping = get_is_free_shipping(self.request.basket)

        method = self.get_default_shipping_method(self.request.basket)
        shipping_methods = self.get_shipping_methods(self.request.basket)
        if is_free_shipping:
            method = self.get_free_shipping_method(self.request.basket)
            shipping_methods = Repository().free_methods
        shipping_charge = method.calculate(self.request.basket)
        surcharges = SurchargeApplicator(self.request, ctx).get_applicable_surcharges(
            self.request.basket
        )

        ctx["shipping_methods"] = shipping_methods
        ctx["shipping_method"] = method
        ctx["shipping_charge"] = shipping_charge
        ctx["order_total"] = OrderTotalCalculator().calculate(
            self.request.basket, shipping_charge, surcharges=surcharges
        )
        ctx["basket"] = self.request.basket
        return ctx

    def get_shipping_methods(self, basket):
        return Repository().get_shipping_methods(
            basket=self.request.basket, user=self.request.user, request=self.request
        )

    def get_default_shipping_address(self):
        if self.request.user.is_authenticated:
            return self.request.user.addresses.filter(
                is_default_for_shipping=True
            ).first()

    def get_default_shipping_method(self, basket):
        return Repository().get_default_shipping_method(
            basket=self.request.basket,
            user=self.request.user,
            request=self.request,
            shipping_addr=self.get_default_shipping_address(),
        )

    def get_free_shipping_method(self, basket):
        return Repository().get_free_shipping_method(
            basket=self.request.basket,
            user=self.request.user,
            request=self.request,
            shipping_addr=self.get_default_shipping_address(),
        )


class PaymentDetailsView(CorePaymentDetailsView):
    pre_conditions = [
        "check_basket_is_not_empty",
        "check_basket_is_valid",
        "check_user_email_is_captured",
        "check_shipping_data_is_captured",
        "check_basket_is_delivery_date_empty",
        "check_basket_is_delivery_time_empty",
        "check_a_valid_subscriptions_total",
        "check_a_valid_basket_delivery_date",
    ]
    preview = False

    def get_context_data(self, *args, **kwargs):
        ctx = super().get_context_data(**kwargs)

        # hardcode for SPH vouchers
        is_free_shipping = get_is_free_shipping(self.request.basket)

        method = self.get_default_shipping_method(self.request.basket)
        shipping_methods = self.get_shipping_methods(self.request.basket)
        if is_free_shipping:
            method = self.get_free_shipping_method(self.request.basket)
            shipping_methods = Repository().free_methods
        shipping_charge = method.calculate(self.request.basket)
        surcharges = SurchargeApplicator(self.request, ctx).get_applicable_surcharges(
            self.request.basket
        )

        ctx["shipping_methods"] = shipping_methods
        ctx["shipping_method"] = method
        ctx["shipping_charge"] = shipping_charge
        ctx["order_total"] = OrderTotalCalculator().calculate(
            self.request.basket, shipping_charge, surcharges=surcharges
        )
        ctx["available_payment_methods"] = AvailablePayment.name_choices()
        only_one_payment_available = len(AvailablePayment.name_choices()) == 1
        if only_one_payment_available:
            ctx["payment"] = AvailablePayment.name_choices()[0]

        return ctx

    def get_shipping_methods(self, basket):
        return Repository().get_shipping_methods(
            basket=self.request.basket, user=self.request.user, request=self.request
        )

    def get_default_shipping_address(self):
        if self.request.user.is_authenticated:
            return self.request.user.addresses.filter(
                is_default_for_shipping=True
            ).first()

    def get_default_shipping_method(self, basket):
        return Repository().get_default_shipping_method(
            basket=self.request.basket,
            user=self.request.user,
            request=self.request,
            shipping_addr=self.get_default_shipping_address(),
        )

    def get_free_shipping_method(self, basket):
        return Repository().get_free_shipping_method(
            basket=self.request.basket,
            user=self.request.user,
            request=self.request,
            shipping_addr=self.get_default_shipping_address(),
        )

    def post(self, request, *args, **kwargs):
        # We use a custom parameter to indicate if this is an attempt to place
        # an order (normally from the preview page).  Without this, we assume a
        # payment form is being submitted from the payment details view. In
        # this case, the form needs validating and the order preview shown.
        if request.POST.get("action", "") == "place_order":
            # check delivery_date
            start_date = check_delivery_date_validation(request.basket._lines)
            if request.basket.delivery_date < start_date:
                error_msg = _(
                    "Sorry your delivery schedule has already passed from today's cut off time"
                )
                payment_kwargs = {}
                return self.render_payment_details(
                    request, error=error_msg, **payment_kwargs
                )

            return self.handle_place_order_submission(request)
        return self.handle_payment_details_submission(request)

    def submit(
        self,
        user,
        basket,
        shipping_address,
        shipping_method,  # noqa (too complex (10))
        shipping_charge,
        billing_address,
        order_total,
        payment_kwargs=None,
        order_kwargs=None,
        surcharges=None,
    ):
        """
        Submit a basket for order placement.

        The process runs as follows:

         * Generate an order number
         * Freeze the basket so it cannot be modified any more (important when
           redirecting the user to another site for payment as it prevents the
           basket being manipulated during the payment process).
         * Attempt to take payment for the order
           - If payment is successful, place the order
           - If a redirect is required (e.g. PayPal, 3D Secure), redirect
           - If payment is unsuccessful, show an appropriate error message

        :basket: The basket to submit.
        :payment_kwargs: Additional kwargs to pass to the handle_payment
                         method. It normally makes sense to pass form
                         instances (rather than model instances) so that the
                         forms can be re-rendered correctly if payment fails.
        :order_kwargs: Additional kwargs to pass to the place_order method
        """
        if payment_kwargs is None:
            payment_kwargs = {}
        if order_kwargs is None:
            order_kwargs = {}

        # Taxes must be known at this point
        assert (
            basket.is_tax_known
        ), "Basket tax must be set before a user can place an order"
        assert (
            shipping_charge.is_tax_known
        ), "Shipping charge tax must be set before a user can place an order"

        # We generate the order number first as this will be used
        # in payment requests (ie before the order model has been
        # created).  We also save it in the session for multi-stage
        # checkouts (e.g. where we redirect to a 3rd party site and place
        # the order on a different request).
        order_number = self.generate_order_number(basket)
        self.checkout_session.set_order_number(order_number)
        logger.info(
            "Order #%s: beginning submission process for basket #%d",
            order_number,
            basket.id,
        )

        # Freeze the basket so it cannot be manipulated while the customer is
        # completing payment on a 3rd party site.  Also, store a reference to
        # the basket in the session so that we know which basket to thaw if we
        # get an unsuccessful payment response when redirecting to a 3rd party
        # site.
        self.freeze_basket(basket)
        self.checkout_session.set_submitted_basket(basket)

        # We define a general error message for when an unanticipated payment
        # error occurs.
        error_msg = _(
            "A problem occurred while processing payment for this "
            "order - no payment has been taken.  Please "
            "contact customer services if this problem persists"
        )

        signals.pre_payment.send_robust(sender=self, view=self)

        try:
            self.handle_payment(basket, order_number, order_total, **payment_kwargs)
        except RedirectRequired as e:
            # Redirect required (e.g. PayPal, 3DS)
            logger.info("Order #%s: redirecting to %s", order_number, e.url)
            return http.HttpResponseRedirect(e.url)
        except UnableToTakePayment as e:
            # Something went wrong with payment but in an anticipated way.  Eg
            # their bankcard has expired, wrong card number - that kind of
            # thing. This type of exception is supposed to set a friendly error
            # message that makes sense to the customer.
            msg = str(e)
            logger.warning(
                "Order #%s: unable to take payment (%s) - restoring basket",
                order_number,
                msg,
            )
            self.restore_frozen_basket()

            # We assume that the details submitted on the payment details view
            # were invalid (e.g. expired bankcard).
            return self.render_payment_details(
                self.request, error=msg, **payment_kwargs
            )
        except PaymentError as e:
            # A general payment error - Something went wrong which wasn't
            # anticipated.  Eg, the payment gateway is down (it happens), your
            # credentials are wrong - that king of thing.
            # It makes sense to configure the checkout logger to
            # mail admins on an error as this issue warrants some further
            # investigation.
            msg = str(e)
            logger.error(
                "Order #%s: payment error (%s)", order_number, msg, exc_info=True
            )
            self.restore_frozen_basket()
            # return self.render_preview(self.request, error=error_msg, **payment_kwargs)
            return self.render_payment_details(
                self.request, error=msg, **payment_kwargs
            )
        except stripe.error.CardError as e:
            logger.exception(
                "Order #%s: card error (%s)",
                order_number,
                e,
            )
            self.restore_frozen_basket()
            # return self.render_preview(self.request, error=str(e).split(':')[1].strip(), **payment_kwargs)
            return self.render_payment_details(
                self.request, error=str(e).split(":")[1].strip(), **payment_kwargs
            )
        except Exception as e:
            # Unhandled exception - hopefully, you will only ever see this in
            # development...
            logger.exception(
                "Order #%s: unhandled exception while taking payment (%s)",
                order_number,
                e,
            )
            self.restore_frozen_basket()
            # return self.render_preview(self.request, error=str(e), **payment_kwargs)
            return self.render_payment_details(
                self.request, error=error_msg, **payment_kwargs
            )

        signals.post_payment.send_robust(sender=self, view=self)

        # If all is ok with payment, try and place order
        logger.info("Order #%s: payment successful, placing order", order_number)

        order_kwargs["status"] = OrderStatus.PAID.value
        order_kwargs["delivery_date"] = basket.delivery_date
        order_kwargs["delivery_day"] = basket.delivery_day
        order_kwargs["delivery_time_id"] = basket.delivery_time_id

        try:
            return self.handle_order_placement(
                order_number,
                user,
                basket,
                shipping_address,
                shipping_method,
                shipping_charge,
                billing_address,
                order_total,
                surcharges=surcharges,
                **order_kwargs,
            )
        except UnableToPlaceOrder as e:
            # It's possible that something will go wrong while trying to
            # actually place an order.  Not a good situation to be in as a
            # payment transaction may already have taken place, but needs
            # to be handled gracefully.
            msg = str(e)
            logger.error(
                "Order #%s: unable to place order - %s",
                order_number,
                msg,
                exc_info=True,
            )
            self.restore_frozen_basket()
            return self.render_payment_details(
                self.request, error=msg, **payment_kwargs
            )
        except Exception as e:
            # Hopefully you only ever reach this in development
            logger.exception(
                "Order #%s: unhandled exception while placing order (%s)",
                order_number,
                e,
            )
            error_msg = _(
                "A problem occurred while placing this order. Please contact customer services."
            )
            self.restore_frozen_basket()
            return self.render_payment_details(
                self.request, error=error_msg, **payment_kwargs
            )

    def handle_payment(self, basket, order_number, total, **kwargs):
        # Talk to payment gateway.  If unsuccessful/error, raise a
        # PaymentError exception which we allow to percolate up to be caught
        # and handled by the core PaymentDetailsView.

        if total.incl_tax > 0:

            total_charge_buy_now = 0
            total_subscription_charge_now = 0
            product_subscriptions = []

            for line in basket.all_lines():
                if line.is_subscription:
                    total_subscription_charge_now += line.price_incl_tax * line.quantity
                    product_subscriptions.append(
                        {
                            "title": line.product.title,
                            "description": line.product.description.strip(),
                            "credit_per_delivery": line.credit_per_delivery,
                            "pack_size": line.pack_size,
                            "price": line.price_incl_tax,
                            "qty": line.quantity,
                            "interval": line.interval,
                            "interval_count": line.interval_count,
                            "delivery_prepaid": line.delivery_prepaid,
                            "subscription_type": line.subscription_type,
                        }
                    )
                else:
                    total_charge_buy_now += line.price_incl_tax * line.quantity

            total_charge = total_charge_buy_now + total_subscription_charge_now
            shipping = total.incl_tax - total_charge

            if not self.request.user.is_authenticated:
                email = self.checkout_session.get_guest_email()
                try:
                    user = User.objects.get(username=email)
                    user.email = email
                    user.save()
                except User.DoesNotExist:
                    user = User.objects.create(username=email, email=email)

                subscriber = user
            else:
                subscriber = self.request.user

            customer, created = Customer.get_or_create(subscriber=subscriber)

            card = {
                "number": self.request.POST.get("number"),
                "exp_month": self.request.POST.get("exp_month"),
                "exp_year": self.request.POST.get("exp_year"),
                "cvc": self.request.POST.get("cvc"),
                "card_holder_name": self.request.POST.get("cardName"),
            }

            create_or_update_payment_method(**card, customer=customer)

            if total_charge_buy_now > 0:
                if shipping == 0:
                    stripe_ref = customer.charge(
                        Decimal(total_charge_buy_now), currency="sgd"
                    )
                else:
                    stripe_ref = customer.charge(total.incl_tax, currency="sgd")

                # Payment successful! Record payment source
                source_type, _ = SourceType.objects.get_or_create(
                    name=AvailablePayment.STRIPE.value
                )

                source = Source(
                    source_type=source_type,
                    amount_allocated=stripe_ref.amount_captured,
                    amount_debited=stripe_ref.amount_captured,
                    reference=stripe_ref.id,
                )

                self.add_payment_source(source)

            if len(product_subscriptions) > 0:
                for product in product_subscriptions:
                    stripe_product = stripe.Product.create(
                        name=product["title"],
                        description=product["description"],
                        metadata={"djstripe_subcription": product["title"]},
                    )
                    djstripe_product = Product.sync_from_stripe_data(stripe_product)
                    interval_count_left = 0

                    if product["subscription_type"] == "prepaid":
                        initial_credit = int(product["credit_per_delivery"]) * int(
                            product["interval_count"]
                        )
                        delivery_frequency = int(product["delivery_prepaid"])
                        interval_count = product["interval_count"] * delivery_frequency
                        next_delivery_date = basket.delivery_date + timedelta(
                            weeks=delivery_frequency
                        )

                        if interval_count > 52:
                            # todo: send email to admin
                            interval_count_left = interval_count - 52
                            interval_count = 52
                    else:
                        initial_credit = int(product["pack_size"]) / 6
                        delivery_frequency = int(product["delivery_prepaid"])
                        interval_count = delivery_frequency
                        next_delivery_date = basket.delivery_date + timedelta(
                            weeks=delivery_frequency
                        )

                    stripe_price = stripe.Price.create(
                        currency="sgd",
                        unit_amount=f"{int(float(product['price']) * 100)}",
                        recurring={
                            "interval": product["interval"],
                            "interval_count": interval_count,
                        },
                        product=djstripe_product.id,
                        metadata={"djstripe_subcription": product["title"]},
                    )
                    djstripe_price = Price.sync_from_stripe_data(stripe_price)
                    stripe_ref = customer.subscribe(price=djstripe_price)

                    # to charge every sunday if prepaid
                    # todo: move to a separate function
                    current_period_end = stripe_ref.current_period_end
                    i = 0
                    while current_period_end.weekday() != 6:
                        i += 1
                        current_period_end += timedelta(days=1)

                    if i > 0:
                        stripe_ref.extend(timedelta(days=i))

                    if interval_count_left > 0:
                        # todo: uncomment this if need it
                        # resumes_at = new_billing_date + timedelta(weeks=interval_count_left)
                        # new_resumes_at = int(datetime(resumes_at.year, resumes_at.month, resumes_at.day).timestamp())
                        # stripe_subs = stripe.Subscription.modify(
                        #     stripe_ref.id,
                        #     pause_collection={
                        #         "behavior": "void",
                        #         "resumes_at": new_resumes_at
                        #     },
                        # )
                        logger.error(
                            f"subscription id {stripe_ref.id} more than 52 weeks, total weeks left "
                            f"{interval_count_left}"
                        )
                        send_subscription_more_than_52_weeks_to_william.delay(
                            subscription_id=stripe_ref.id
                        )

                    try:
                        SubscriptionPrepaid.objects.create(
                            user=subscriber,
                            djsubscription_id=stripe_ref.id,
                            djcustomer_id=customer.id,
                            pack_size=int(product["pack_size"]),
                            description=product["description"]
                            .split("</p><p>")[0]
                            .replace("<p>", "")
                            .replace("</p>", ""),
                            delivery_date=basket.delivery_date,
                            delivery_day=basket.delivery_date.strftime("%A"),
                            delivery_time=basket.delivery_time.__str__(),
                            delivery_frequency=delivery_frequency,
                            charge_frequency=int(interval_count),
                            initial_credit=initial_credit,
                            credit_per_delivery=int(product["credit_per_delivery"]),
                            next_credit_charge=int(product["credit_per_delivery"]),
                            subscription_type=product["subscription_type"],
                            next_delivery_date=next_delivery_date,
                            quantity=1,
                        )
                    except Exception as e:
                        logger.error(f"{e}")

                    self.checkout_session.use_subscription_id(stripe_ref.id)

                    source_type, _ = SourceType.objects.get_or_create(
                        name=AvailablePayment.STRIPE.value
                    )

                    source = Source(
                        source_type=source_type,
                        currency="sgd",
                        amount_allocated=product["price"],
                        amount_debited=product["price"],
                        reference=stripe_ref.id,
                    )

                    self.add_payment_source(source)

        self.add_payment_event("Purchase", total.incl_tax)

        renew_transaction_history.delay()


class ThankYouView(CoreThankYouView):
    """
    Class used for the thank-you page
    """

    @staticmethod
    def send_facebook_conversion_api(order: Order) -> None:
        """
        Function to send the conversion event to Facebook

        :param order: The Order object
        """
        user_data = UserData(
            email=order.guest_email if order.guest_email else order.user.email,
        )
        content = []
        for line in order.lines.all():
            product_name = (
                line.product.parent.title if line.product.parent else line.title
            )

            content.append(
                Content(
                    product_id=product_name,
                    quantity=line.quantity,
                )
            )
        data = CustomData(
            contents=content,
            value=float(order.total_excl_tax),
            currency="sgd",
        )
        event = Event(
            event_name="Purchase",
            user_data=user_data,
            custom_data=data,
            event_time=int(order.date_placed.timestamp()),
        )

        event_request = EventRequest(
            events=[event],
            pixel_id="783187055379725",
        )
        response = event_request.execute()
        logger.info(f"Conversion API response: {response}")

    def get_context_data(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            client.ListsSegments.unsubscribe(
                "Wi76hh", body={"emails": [self.request.user.email]}
            )

        ctx = super().get_context_data(*args, **kwargs)
        order = Order.objects.get(pk=ctx["order"].pk)

        self.send_facebook_conversion_api(order)

        # Remember whether this view has been loaded.
        # Only send tracking information on the first load.
        key = "order_{}_thankyou_viewed".format(ctx["order"].pk)
        if not self.request.session.get(key, False):
            self.request.session[key] = True
            ctx["send_analytics_event"] = True
        else:
            ctx["send_analytics_event"] = False

        ctx["back_to_home"] = reverse("home")

        # Send email after all context are ready
        send_email_for_paid_order(ctx["order"])

        if order.lines.filter(product__parent__title__icontains="Concentrate").exists():
            try:
                client.ListsSegments.add_members(
                    "S6JFzp", body={"profiles": [{"email": self.request.user.email}]}
                )
            except AttributeError:
                client.ListsSegments.add_members(
                    "S6JFzp", body={"profiles": [{"email": order.guest_email}]}
                )
            except Exception as e:
                logger.error(f"{e}", extra={"order": order.number})

        return ctx


class ReceiptView(TemplateView):
    template_name = "orders/receipt.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["order"] = Order.objects.first()
        return context
