from django.urls import path

from ecomm.schedule_management.views import DeliveryAvailabilityView, HolidaysView

from .views import payment_webhook

app_name = "custom_checkout"

urlpatterns = [
    path(
        "payment-webhook",
        view=payment_webhook,
        name="payment_webhook",
    ),
    path(
        "delivery-availability",
        view=DeliveryAvailabilityView.as_view(),
        name="delivery_availability",
    ),
    path("holidays", view=HolidaysView.as_view(), name="holidays"),
]
