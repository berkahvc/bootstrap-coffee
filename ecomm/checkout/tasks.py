import logging
import re

import stripe
from allauth.utils import build_absolute_uri
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.mail import EmailMessage
from django.core.management import call_command
from django.template.loader import render_to_string
from django.utils.timezone import localdate, timedelta
from djstripe.models import Customer as DjStripeCustomer
from djstripe.models import Subscription as DjStripeSubscription
from djstripe.settings import STRIPE_SECRET_KEY
from oscar.apps.payment.models import Source, SourceType

from config import celery_app
from ecomm.catalogue.models import Product
from ecomm.order.models import Line, Order, OrderNote
from ecomm.payment.enums import AvailablePayment
from ecomm.subscription_management.models import SubscriptionPrepaid
from ecomm.subscription_management.utils import create_product_subscription
from ecomm.utils.utils import get_delivery_time, is_delivery_date_in_this_week

stripe.api_key = STRIPE_SECRET_KEY
logger = logging.getLogger("ecomm.checkout.tasks")


def attachment_absolute_uri(request, email_blast):
    attachment_url = None
    if email_blast.attachment:
        attachment_url = build_absolute_uri(request, email_blast.attachment.url)

    return attachment_url


def create_order_number():
    orders = Order.objects.filter(number__startswith="#4").order_by("-id")
    last_order = None
    if orders.exists():
        last_order = orders.first()

    if not last_order:
        order_number = "#40001"
    else:
        last_order_number = int(last_order.number.replace("#", "")) + 1
        order_number = f"#{last_order_number}"
    return order_number


@celery_app.task()
def renew_transaction_history():
    call_command("djstripe_sync_models", "Charge")


@celery_app.task()
def renew_subscription():
    call_command("djstripe_sync_models", "Subscription")


@celery_app.task()
def send_email_error_to_william(*args, **kwargs):
    subscription_id = kwargs.get("subscription_id")
    description = kwargs.get("description")
    mail = EmailMessage(
        subject=f"Data subscription prepaid {subscription_id} tidak ada",
        body=f"<p>Subscription ID: {subscription_id}<br>Description: {description}<br></p>",
        from_email=settings.DEFAULT_FROM_EMAIL,
        to=["william@berkah.vc"],
    )
    mail.content_subtype = "html"
    mail.send()


@celery_app.task()
def send_subscription_more_than_52_weeks_to_william(*args, **kwargs):
    subscription_id = kwargs.get("subscription_id")
    mail = EmailMessage(
        subject=f"Subscription ID {subscription_id} lebih dari 52 minggu",
        body=f"<p>Tolong dicek Subscription ID: {subscription_id} karena lebih dari 52 minggu</p>",
        from_email=settings.DEFAULT_FROM_EMAIL,
        to=["william@berkah.vc"],
    )
    mail.content_subtype = "html"
    mail.send()


@celery_app.task()
def send_email_subscription_renewed(*args, **kwargs):
    """
    This email will be sent to the user when the subscription is renewed successfully

    :param args:
    :param kwargs:
    :return:
    """
    subscription_id = kwargs.get("subscription_id")
    sp = SubscriptionPrepaid.objects.get(djsubscription_id=subscription_id)
    email = kwargs.get("email")
    customer_name = kwargs.get("full_name")
    kwargs.update(
        {
            "description": sp.description,
            "qty": sp.quantity,
            "domain": Site.objects.all().first().domain,
            "shipping_address": sp.shipping_address,
            "customer_name": customer_name,
        }
    )
    template_prefix = "oscar/communication/emails/renewed_customer_subscription"
    subject = render_to_string(
        "{0}_subject.html".format(template_prefix), kwargs
    ).strip()

    template_name = "{0}_message.html".format(template_prefix)
    body = render_to_string(template_name, kwargs).strip()

    send_email_with_rate_limit.delay(subject=subject, body=body, to=email)


@celery_app.task()
def email_subscription_reminder():
    """
    This email will be sent to the subscriber 2 days before their billing happens

    :return:
    """

    subscriptions = SubscriptionPrepaid.objects.all()
    customers_sent = list()
    print(subscriptions.count())

    for subscription in subscriptions:
        try:
            if not DjStripeSubscription.objects.get(
                id=subscription.djsubscription_id
            ).livemode:
                continue

            stripe_subscription = stripe.Subscription.retrieve(
                subscription.djsubscription_id
            )
            if stripe_subscription.get("pause_collection"):
                continue

            sub_detail = DjStripeSubscription.sync_from_stripe_data(stripe_subscription)
            billing_date = localdate(sub_detail.current_period_end)
            saturday = localdate() + timedelta(days=1)
            if billing_date == (localdate() + timedelta(days=2)) and (
                sub_detail.status == "active" or sub_detail.status == "trialing"
            ) and (subscription.user.email not in customers_sent):
                kwargs = {
                    "customer_name": subscription.user.name,
                    "saturday": saturday.strftime("%B %d, %Y"),
                }
                template_prefix = (
                    "oscar/communication/emails/subscription_delivery_reminder"
                )
                subject = render_to_string(
                    "{0}_subject.html".format(template_prefix), kwargs
                ).strip()

                template_name = "{0}_message.html".format(template_prefix)
                body = render_to_string(template_name, kwargs).strip()
                send_email_with_rate_limit.delay(
                    subject=subject, body=body, to=subscription.user.email
                )
                customers_sent.append(subscription.user.email)

        except Exception as e:
            logging.error(e, extra={"subscription_id": subscription.djsubscription_id})
            continue


@celery_app.task()
def send_email_with_rate_limit(**kwargs):
    """
    This function will send the email with rate limit imposed in
    config.celery_app.

    :param kwargs: subject, body, to, from
    """
    from_email = (
        settings.DEFAULT_FROM_EMAIL
        if not kwargs.get("from_email")
        else kwargs.get("from_email")
    )
    msg = EmailMessage(
        kwargs.get("subject"),
        kwargs.get("body"),
        from_email=from_email,
        to=[kwargs.get("to")],
    )
    msg.content_subtype = "html"
    msg.send()


@celery_app.task()
def generate_prepaid_order():
    """
    this function run on every Sunday
    :return:
    """
    subscription_prepaid_list = SubscriptionPrepaid.objects.filter(
        subscription_type=SubscriptionPrepaid.PREPAID, status="active"
    ).exclude(user__username="yosefin@berkah.vc", status=SubscriptionPrepaid.CANCELLED)

    source_type, _ = SourceType.objects.get_or_create(
        name=AvailablePayment.STRIPE.value
    )

    for subscription_prepaid in subscription_prepaid_list:
        try:
            stripe_subscription = stripe.Subscription.retrieve(
                subscription_prepaid.djsubscription_id
            )

            if stripe_subscription.get("pause_collection"):
                logger.error(
                    "Subscription paused",
                    extra={"djsubscription_id": subscription_prepaid.djsubscription_id},
                )
                continue

            # sync with local db #
            sub_detail = DjStripeSubscription.sync_from_stripe_data(stripe_subscription)
            # ===== ////// ===== #

            if sub_detail.livemode == settings.STRIPE_LIVE_MODE and (
                sub_detail.status == "active" or sub_detail.status == "trialing"
            ):
                print(f"===== {subscription_prepaid.djsubscription_id}")
                customer = DjStripeCustomer.objects.get(
                    id=subscription_prepaid.djcustomer_id
                )
                delivery_time = get_delivery_time(subscription_prepaid.delivery_time)
                if subscription_prepaid.next_delivery_date:
                    delivery_date = subscription_prepaid.next_delivery_date.strftime(
                        "%Y-%m-%d"
                    )
                else:
                    delivery_date = subscription_prepaid.delivery_date

                djstripe_product_name = sub_detail.plan.product.name
                amount_paid = 0
                currency = sub_detail.plan.currency

                if is_delivery_date_in_this_week(delivery_date):
                    print("===== Creating Order")
                    if "subscription" in djstripe_product_name:
                        try:
                            product = Product.objects.get(
                                title=sub_detail.plan.product.name
                            )
                        except Product.DoesNotExist:
                            product = create_product_subscription(
                                title=sub_detail.plan.product.name,
                            )
                    else:
                        shopify_product_id = re.search(
                            r"-(\d{10,17})", djstripe_product_name
                        ).group(1)
                        shopify_variant_id = re.search(
                            r"-(\d{10,17})$", djstripe_product_name
                        ).group(1)
                        product = Product.objects.get(
                            shopify_product_id=shopify_product_id,
                            shopify_variant_id=shopify_variant_id,
                        )

                    stock_record = product.stockrecords.all().first()
                    order_q = {
                        "user": customer.subscriber,
                        "delivery_date": delivery_date,
                        "delivery_time_id": delivery_time.id,
                        "delivery_day": subscription_prepaid.delivery_day,
                        "subscription_id": subscription_prepaid.djsubscription_id,
                    }

                    order_data = {
                        "number": create_order_number(),
                        "currency": currency.upper(),
                        "shipping_address": subscription_prepaid.shipping_address,
                        "total_incl_tax": amount_paid,
                        "total_excl_tax": amount_paid,
                        "shipping_method": "Shipping Delivery",
                        "shipping_code": "default-delivery",
                        "status": "PAID",
                    }

                    order, order_created = Order.objects.get_or_create(
                        **order_q, defaults=order_data
                    )

                    if order_created:
                        print(
                            f"===== order created for subs id {subscription_prepaid.djsubscription_id}"
                        )
                        OrderNote.objects.create(
                            order=order,
                            user=customer.subscriber,
                            note_type="Info",
                            message=product.description
                            if "auto renew" not in product.description.lower()
                            else "",
                        )

                        item_data = {
                            "order": order,
                            "product": product,
                            "title": product.title,
                            "partner": stock_record.partner,
                            "partner_name": stock_record.partner.name,
                            "partner_sku": stock_record.partner_sku,
                            "partner_line_notes": subscription_prepaid.note
                            if subscription_prepaid.note
                            else subscription_prepaid.description,
                            "stockrecord": stock_record,
                            "quantity": subscription_prepaid.quantity,
                            "line_price_incl_tax": stock_record.price_excl_tax * int(subscription_prepaid.quantity),
                            "line_price_excl_tax": stock_record.price_excl_tax * int(subscription_prepaid.quantity),
                            "line_price_before_discounts_incl_tax": stock_record.price_excl_tax * int(subscription_prepaid.quantity),
                            "line_price_before_discounts_excl_tax": stock_record.price_excl_tax
                            * int(subscription_prepaid.quantity),
                            "credit_per_delivery": int(subscription_prepaid.pack_size)
                            / 6,
                            "pack_size": subscription_prepaid.pack_size,
                            "is_subscription": True,
                        }

                        Line.objects.create(**item_data)
                        Source.objects.create(
                            order=order,
                            source_type=source_type,
                            amount_allocated=amount_paid,
                            amount_debited=amount_paid,
                            reference=subscription_prepaid.djsubscription_id,
                        )

                        subscription_prepaid.delivery_date = delivery_date
                        try:
                            subscription_prepaid.next_delivery_date += timedelta(
                                weeks=subscription_prepaid.delivery_frequency
                            )
                        except TypeError:
                            subscription_prepaid.next_delivery_date = (
                                subscription_prepaid.delivery_date
                                + timedelta(
                                    weeks=subscription_prepaid.delivery_frequency
                                )
                            )

                        subscription_prepaid.save()
                    else:
                        print("===== Order Existing")
        except Exception as err:
            logger.error(
                str(err),
                extra={"djsubscription_id": subscription_prepaid.djsubscription_id},
            )
            print(f"===== error {err}")
            print(
                f"===== subscription_prepaid.djsubscription_id {subscription_prepaid.djsubscription_id}"
            )
