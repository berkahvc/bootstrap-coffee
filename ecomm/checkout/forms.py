from django import forms
from oscar.apps.address.models import Country
from oscar.apps.checkout.forms import GatewayForm as CoreGatewayForm
from oscar.apps.checkout.forms import ShippingAddressForm as CoreShippingAddressForm
from oscar.core.compat import get_user_model

from ecomm.shipping.utils import ShippingUtils

User = get_user_model()
SHIPPING_UTILS = ShippingUtils()


class GatewayForm(CoreGatewayForm):
    def clean(self):
        if self.is_guest_checkout() or self.is_new_account_checkout():
            if "password" in self.errors:
                del self.errors["password"]
            return self.cleaned_data
        return super().clean()


class ShippingAddressForm(CoreShippingAddressForm):
    def __init__(self, *args, **kwargs):
        """
        Set fields in OSCAR_REQUIRED_ADDRESS_FIELDS as required.
        """
        super().__init__(*args, **kwargs)

        for field_name in self.fields:
            self.fields[field_name].required = True

        self.fields["country"] = forms.ModelChoiceField(
            queryset=Country.objects.filter(pk="SG").all()
        )
        self.fields["postcode"].label = "Zip Code"
        self.fields["notes"].required = False
        self.fields["country"].initial = "SG"

    class Meta(CoreShippingAddressForm.Meta):
        fields = [
            "first_name",
            "last_name",
            "phone_number",
            "email",  # User Information
            "state",
            "line1",
            "line4",
            "postcode",
            "country",
            "notes",
        ]
        widgets = {
            "first_name": forms.TextInput(
                attrs={"placeholder": "Input recipient first name"}
            ),
            "last_name": forms.TextInput(
                attrs={"placeholder": "Input recipient last name"}
            ),
            "email": forms.EmailInput(attrs={"placeholder": "Input email"}),
            "postcode": forms.TextInput(attrs={"placeholder": "Input postal code"}),
            "state": forms.TextInput(attrs={"placeholder": "Input state"}),
            "line4": forms.TextInput(attrs={"placeholder": "Input city"}),
            "line1": forms.TextInput(attrs={"placeholder": "Input address"}),
            "phone_number": forms.TextInput(
                attrs={"placeholder": "Input phone number"}
            ),
            "notes": forms.TextInput(attrs={"placeholder": "Input additional info"}),
        }
