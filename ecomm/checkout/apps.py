import oscar.apps.checkout.apps as apps
from django.conf.urls import url
from oscar.core.loading import get_class


class CheckoutConfig(apps.CheckoutConfig):
    name = "ecomm.checkout"

    def ready(self):
        super(CheckoutConfig, self).ready()
        self.ajax_order_information_view = get_class(
            "checkout.views", "AjaxOrderInformationView"
        )
        self.paid_thank_you_view = get_class("checkout.views", "PaidThankYouView")

    def get_urls(self):
        urls = super(CheckoutConfig, self).get_urls()

        urls += [
            url(
                r"ajax-order-information/$",
                self.ajax_order_information_view.as_view(),
                name="ajax-order-information",
            ),
            url(
                r"paid/thank-you/$",
                self.paid_thank_you_view.as_view(),
                name="paid-thank-you",
            ),
        ]

        return self.post_process_urls(urls)
