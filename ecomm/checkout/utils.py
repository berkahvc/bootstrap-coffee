import decimal
import hashlib
import logging
import re
from copy import copy
from datetime import date, datetime
from typing import Tuple

import pytz
import stripe
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.exceptions import ValidationError
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.urls import NoReverseMatch, reverse
from django.utils.timezone import localdate, localtime, timedelta
from djstripe.models import Customer, Invoice, Subscription
from djstripe.settings import STRIPE_SECRET_KEY
from oscar.apps.checkout.utils import CheckoutSessionData as CoreCheckoutSessionData
from oscar.apps.order.utils import OrderDispatcher as CoreOrderDispatcher
from oscar.apps.payment.models import Source, SourceType
from weasyprint import CSS, HTML

from ecomm.catalogue.models import Product
from ecomm.checkout.tasks import (  # send_email_subscription_renewed,
    send_email_error_to_william,
    send_email_subscription_renewed,
)
from ecomm.order.enums import OrderStatus
from ecomm.order.models import Line, Order, OrderNote
from ecomm.payment.enums import AvailablePayment
from ecomm.shipping.methods import DefaultDelivery
from ecomm.subscription_management.models import (
    SubscriptionPaymentFailed,
    SubscriptionPrepaid,
)
from ecomm.subscription_management.tasks import (
    send_subscription_payment_failed_to_customer,
)
from ecomm.subscription_management.utils import create_product_subscription
from ecomm.utils.utils import calculate_delivery_date, get_delivery_time

from .enums import MidtransFraudStatus, MidtransTransactionStatus

logger = logging.getLogger("ecomm.checkout.utils")

midtrans_signature_key_hash = hashlib.sha512()
stripe.api_key = STRIPE_SECRET_KEY


def decide_order_status(transaction_status, fraud_status):
    target_status = OrderStatus.WAIT_FOR_PAYMENT.value

    if transaction_status == MidtransTransactionStatus.CAPTURE.value:
        if fraud_status == MidtransFraudStatus.CHALLENGE.value:
            target_status = OrderStatus.DETECTED_AS_FRAUD.value
        elif fraud_status == MidtransFraudStatus.ACCEPT.value:
            target_status = OrderStatus.PAID.value
    elif transaction_status == MidtransTransactionStatus.EXPIRE.value:
        target_status = OrderStatus.EXPIRED.value
    elif transaction_status == MidtransTransactionStatus.DENY.value:
        target_status = OrderStatus.DENIED.value
    elif transaction_status == MidtransTransactionStatus.CANCEL.value:
        target_status = OrderStatus.CANCELLED.value
    elif transaction_status == MidtransTransactionStatus.PENDING.value:
        target_status = OrderStatus.WAIT_FOR_PAYMENT.value
    elif transaction_status == MidtransTransactionStatus.SETTLEMENT.value:
        target_status = OrderStatus.PAID.value

    return target_status


def get_invoice_object(invoice_id: str) -> Invoice:
    """
    Function to get the invoice object from djstripe used for subscription webhook

    If the invoice does not exist, then sync to stripe first to make sure that the most recent
    data is updated.

    :param invoice_id: invoice_id from stripe
    :return: Invoice object from djstripe
    """
    try:
        invoice = Invoice.objects.get(id=invoice_id)
    except Invoice.DoesNotExist:
        stripe_invoice = stripe.Invoice.retrieve(invoice_id)
        invoice = Invoice.sync_from_stripe_data(stripe_invoice)

    return invoice


def get_or_create_subscription_product(
    stripe_subscription: Subscription, invoice: Invoice = None
) -> Product:
    """
    Function to get or create a product based on the subscription object

    :param stripe_subscription: subscription object from djstripe
    :param invoice: invoice object from djstripe
    :return: Product object
    """
    djstripe_product_name = stripe_subscription.plan.product.name

    if "subscription" in djstripe_product_name:
        try:
            product = Product.objects.get(title=stripe_subscription.plan.product.name)
        except Product.DoesNotExist:
            product = create_product_subscription(
                price=invoice.amount_paid,
                title=stripe_subscription.plan.product.name,
            )
    else:
        shopify_product_id = re.search(r"-(\d{10,17})", djstripe_product_name).group(1)
        shopify_variant_id = re.search(r"-(\d{10,17})$", djstripe_product_name).group(1)
        product = Product.objects.get(
            shopify_product_id=shopify_product_id,
            shopify_variant_id=shopify_variant_id,
        )

    return product


def update_subscription_delivery_date(
    subscription_id: str, stripe_subscription: Subscription
) -> Tuple[SubscriptionPrepaid, date]:
    """
    Function to update the delivery date of the subscription
    """
    delivery_date = None
    subscription_prepaid = None
    date_format = "%Y-%m-%d"

    try:
        subscription_prepaid = SubscriptionPrepaid.objects.get(
            djsubscription_id=subscription_id
        )
        delivery_date = calculate_delivery_date(
            subscription_prepaid.delivery_day,
            localtime(timezone=pytz.timezone("Asia/Singapore")),
        )

        subscription_prepaid.delivery_date = delivery_date

        try:
            subscription_prepaid.next_delivery_date = (
                datetime.strptime(delivery_date, date_format)
                + timedelta(weeks=subscription_prepaid.delivery_frequency)
            ).strftime(date_format)
        except TypeError:
            subscription_prepaid.next_delivery_date = (
                datetime.strptime(subscription_prepaid.delivery_date, date_format)
                + timedelta(weeks=subscription_prepaid.delivery_frequency)
            ).strftime(date_format)

        subscription_prepaid.save()

    except SubscriptionPrepaid.DoesNotExist:
        description = (
            stripe_subscription.plan.nickname
            if stripe_subscription.plan.nickname
            else stripe_subscription.plan.product.description
        )
        send_email_error_to_william.delay(
            subscription_id=subscription_id, description=description
        )

    return subscription_prepaid, delivery_date


def handle_stripe_payment(event, **kwargs):
    """
    Handle Stripe webhooks. Currently handled:
    - invoice.paid

    :param event:
    :param kwargs:
    :return:
    """
    print("Triggered webhook", event)
    print("Type", event["type"])
    event_type = event["type"]
    data = event["data"]

    """
    this is for subscription [invoice.payment_succeeded]
    billing_reason list:
    1. subscription_create => charge when create subscription first time
    2. subscription_update => charge trial to active
    3. subscription_cycle => charge next payment subscription
    """

    if event_type == "invoice.paid":
        obj = data["object"]
        invoice_id = obj["id"]
        billing_reason = obj["billing_reason"]
        currency = obj["currency"]
        customer_id = obj["customer"]
        paid = obj["paid"]
        subscription_id = obj["subscription"]

        source_type, _ = SourceType.objects.get_or_create(
            name=AvailablePayment.STRIPE.value
        )

        if paid and billing_reason == "subscription_cycle":
            djstripe_subscription = Subscription.objects.get(id=subscription_id)
            customer = Customer.objects.get(id=customer_id)
            invoice = get_invoice_object(invoice_id)
            product = get_or_create_subscription_product(
                stripe_subscription=djstripe_subscription, invoice=invoice
            )
            subscription_prepaid, delivery_date = update_subscription_delivery_date(
                subscription_id, djstripe_subscription
            )
            order_number = create_order_number()

            if subscription_prepaid:
                delivery_time = get_delivery_time(subscription_prepaid.delivery_time)

                order_q = {
                    "user": customer.subscriber,
                    "delivery_date": delivery_date,
                    "delivery_time_id": delivery_time.id,
                    "delivery_day": subscription_prepaid.delivery_day,
                    "subscription_id": subscription_id,
                }

                order_data = {
                    "number": order_number,
                    "currency": currency.upper(),
                    "shipping_address": subscription_prepaid.shipping_address,
                    "total_incl_tax": invoice.amount_paid,
                    "total_excl_tax": invoice.amount_paid,
                    "shipping_method": "Shipping Delivery",
                    "shipping_code": "default-delivery",
                    "status": "PAID",
                }

                order, order_created = Order.objects.get_or_create(
                    **order_q, defaults=order_data
                )

                if order_created:
                    OrderNote.objects.create(
                        order=order,
                        user=customer.subscriber,
                        note_type="Info",
                        message=product.description
                        if "auto renew" not in product.description.lower()
                        else "",
                    )

                    if subscription_prepaid.note:
                        partner_line_notes = subscription_prepaid.note
                    else:
                        partner_line_notes = subscription_prepaid.description

                    stock_record = product.stockrecords.all().first()

                    item_data = {
                        "order": order,
                        "product": product,
                        "title": product.title,
                        "partner": stock_record.partner,
                        "partner_name": stock_record.partner.name,
                        "partner_sku": stock_record.partner_sku,
                        "partner_line_notes": partner_line_notes,
                        "stockrecord": stock_record,
                        "quantity": subscription_prepaid.quantity,
                        "line_price_incl_tax": stock_record.price_excl_tax
                        * int(subscription_prepaid.quantity),
                        "line_price_excl_tax": stock_record.price_excl_tax
                        * int(subscription_prepaid.quantity),
                        "line_price_before_discounts_incl_tax": stock_record.price_excl_tax
                        * int(subscription_prepaid.quantity),
                        "line_price_before_discounts_excl_tax": stock_record.price_excl_tax
                        * int(subscription_prepaid.quantity),
                        "credit_per_delivery": int(subscription_prepaid.pack_size) / 6,
                        "pack_size": subscription_prepaid.pack_size,
                        "is_subscription": True,
                    }

                    Line.objects.create(**item_data)
                    Source.objects.create(
                        order=order,
                        source_type=source_type,
                        amount_allocated=invoice.amount_paid,
                        amount_debited=invoice.amount_paid,
                        reference=subscription_id,
                    )

                    renewed_kwargs = {
                        "subscription_id": subscription_id,
                        "email": customer.subscriber.email,
                        "full_name": customer.subscriber.get_full_name(),
                    }
                    send_email_subscription_renewed.delay(**renewed_kwargs)

                if not delivery_date:
                    logger.error(
                        f"Order {order_number} has no delivery date! Check immediately!",
                        extra={
                            "order_number": order_number,
                            "subscriber": customer.subscriber.email,
                        },
                    )
        elif paid and billing_reason == "subscription_create":
            try:
                Invoice.objects.get(id=invoice_id)
            except Invoice.DoesNotExist:
                stripe_invoice = stripe.Invoice.retrieve(invoice_id)
                Invoice.sync_from_stripe_data(stripe_invoice)

            source = (
                Source.objects.filter(reference=subscription_id).order_by("id").first()
            )
            if source:
                send_email_for_paid_order(source.order)
    elif event_type == "invoice.payment_failed":
        obj = data["object"]
        customer_id = obj["customer"]
        invoice_id = obj["id"]
        subscription_id = obj["subscription"]
        last_finalization_error = obj.get("last_finalization_error", None)

        djstripe_customer = Customer.objects.get(id=customer_id)
        djstripe_subscription = Subscription.objects.get(id=subscription_id)

        try:
            invoice = Invoice.objects.get(id=invoice_id)
        except Invoice.DoesNotExist:
            stripe_invoice = stripe.Invoice.retrieve(invoice_id)
            invoice = Invoice.sync_from_stripe_data(stripe_invoice)

        data = {
            "subscription": djstripe_subscription,
            "customer": djstripe_customer,
            "date": localdate(timezone=pytz.timezone("Asia/Singapore")),
            "invoice": invoice,
            "error_message": last_finalization_error["message"]
            if last_finalization_error
            else "No message error",
        }

        obj, created = SubscriptionPaymentFailed.objects.get_or_create(**data)

        if created:
            email = djstripe_customer.subscriber.email
            full_name = djstripe_customer.subscriber.get_full_name()
            send_subscription_payment_failed_to_customer.delay(
                email=email, full_name=full_name, subscription_id=obj.subscription.id
            )


def handle_midtrans_payment(midtrans_webhook_request):
    """
    :param midtrans_webhook_request: Request from midtrans webhook
    :return:
    """
    signature_key = midtrans_webhook_request["signature_key"]
    order_id = midtrans_webhook_request["order_id"]
    status_code = midtrans_webhook_request["status_code"]
    gross_amount = midtrans_webhook_request["gross_amount"]
    server_key = settings.MIDTRANS_SERVER_KEY
    transaction_status = midtrans_webhook_request["transaction_status"].upper()
    fraud_status = midtrans_webhook_request["fraud_status"].upper()

    signature_item = f"{order_id}{status_code}{gross_amount}{server_key}"
    built_signature_key = hashlib.sha512(
        str(signature_item).encode("utf-8")
    ).hexdigest()

    if signature_key != built_signature_key:
        raise ValidationError("Mismatch signature key")

    logger.info(
        "Transaction notification received. Order ID: {0}. Transaction status: {1}. Fraud status: {2}".format(
            order_id, transaction_status, fraud_status
        )
    )

    target_status = decide_order_status(
        transaction_status=transaction_status, fraud_status=fraud_status
    )

    order = get_object_or_404(Order, number=order_id)

    try:
        order.set_status(target_status)

        logger.info(
            "Successfully update Order #%s status to %s", order_id, target_status
        )

        if target_status == "PAID":

            # ========= send email =========
            send_email_for_paid_order(order)
            # ==============================
    except Exception as e:
        logger.exception(
            "Order #%s: unhandled exception while receiving webhook (%s)",
            order_id,
            e,
        )

    return order


class OrderDispatcher(CoreOrderDispatcher):
    # Event codes
    ORDER_PAID_EVENT_CODE = "ORDER_PAID"

    def send_order_paid_email_for_user(self, order, extra_context, attachments=None):
        event_code = self.ORDER_PAID_EVENT_CODE
        messages = self.dispatcher.get_messages(event_code, extra_context)
        self.dispatch_order_messages(
            order, messages, event_code, attachments=attachments
        )


def send_email_for_paid_order(order):
    ctx = {
        "user": order.user if order.user else None,
        "email_address": order.guest_email if order.is_anonymous else None,
        "order": order,
        "lines": order.lines.all(),
    }
    filename = f"Receipt for { order.number }.pdf"

    create_receipt(order)
    attachment = open(filename, "rb").read()

    dispatcher = OrderDispatcher(logger=logger)
    dispatcher.send_order_paid_email_for_user(
        order=order,
        extra_context=ctx,
        attachments=[[filename, attachment, "application/pdf"]]
    )


def send_order_placed_email(order):
    extra_context = get_message_context(order)
    dispatcher = OrderDispatcher(logger=logger)
    dispatcher.send_order_placed_email_for_user(order, extra_context)


def get_message_context(order):
    user = order.user
    ctx = {"user": user, "order": order, "lines": order.lines.all()}

    # Attempt to add the order status URL to the email template ctx.
    try:
        if user:
            path = reverse("customer:order", kwargs={"order_number": order.number})
        else:
            path = reverse(
                "customer:anon-order",
                kwargs={
                    "order_number": order.number,
                    "hash": order.verification_hash(),
                },
            )
    except NoReverseMatch:
        # We don't care that much if we can't resolve the URL
        pass
    else:
        site = Site.objects.get_current()
        ctx["status_url"] = "http://%s%s" % (site.domain, path)
    return ctx


class CheckoutSessionData(CoreCheckoutSessionData):
    def set_delivery_shipping_information(
        self, shipping_delivery_information: DefaultDelivery
    ):
        """
        Set shipping method code to session
        """
        self._set(
            "shipping",
            "shipping_delivery_information",
            shipping_delivery_information.to_dict(),
        )

    def use_subscription_id(self, code):
        """
        Set subscription id to session
        """
        self._set("subscription", "subscription_id", code)

    def subscription_id(self):
        """
        Return the subscription id
        """
        return self._get("subscription", "subscription_id")


def create_order_number():
    orders = Order.objects.filter(number__startswith="#4").order_by("-id")
    last_order = None
    if orders.exists():
        last_order = orders.first()

    if not last_order:
        order_number = "#40001"
    else:
        last_order_number = int(last_order.number.replace("#", "")) + 1
        order_number = f"#{last_order_number}"
    return order_number


def get_is_free_shipping(basket):
    grouped_voucher_discounts = basket.grouped_voucher_discounts
    is_free_shipping = False
    for grouped_voucher_discount in grouped_voucher_discounts:
        grouped_voucher_discount_dict = copy(grouped_voucher_discount)
        voucher_dict = grouped_voucher_discount_dict["voucher"].__dict__
        first_three_digits_code = voucher_dict["code"][:3]
        if first_three_digits_code == "SPH" or voucher_dict["add_free_shipping"]:
            is_free_shipping = True
            break

    return is_free_shipping


def create_receipt(order: Order = None):
    """
    Generate a receipt for a particular order

    :param order: Order object to generate the receipt from
    :return:
    """
    if not order:
        order = Order.objects.last()

    shipping_total = order.shipping_incl_tax
    invoice_no = order.number
    date_placed = order.date_placed.date()
    payment_method = "Credit Card"
    shipping_text = "Charges Shipping" if shipping_total > 0 else "Free Shipping"
    notes = order.notes if order.notes.first() else "-"
    delivery_date = order.delivery_date
    delivery_time = order.delivery_time
    total_discount_excl_tax = f"-${ order.total_discount_incl_tax }" if order.total_discount_incl_tax > 0 else "-"

    total = order.total_incl_tax
    tax = total * decimal.Decimal(0.07)
    subtotal = total - tax - shipping_total

    address = f"{ order.shipping_address.line1 } { order.shipping_address.line2 }"
    postcode = f"Singapore { order.shipping_address.postcode }"
    recipient_name = f"{ order.shipping_address.first_name } { order.shipping_address.last_name }"

    context = {
        "invoice_no": invoice_no,
        "date_placed": date_placed,
        "payment_method": payment_method,
        "shipping": shipping_text,
        "notes": notes,
        "delivery_date": delivery_date,
        "delivery_time": delivery_time,
        "address": address,
        "postcode": postcode,
        "recipient_name": recipient_name,
        "total": round(total, 2),
        "tax": round(tax, 2),
        "subtotal": round(subtotal, 2),
        "order": order,
        "shipping_total": "Free" if shipping_total == 0 else shipping_total,
        "total_discount_excl_tax": total_discount_excl_tax
    }
    html = render_to_string(
        "orders/receipt.html", context=context
    )

    css = CSS(string=render_to_string(
        "orders/receipt_style.css"
    ))

    file = HTML(string=html).write_pdf(f"Receipt for {order.number}.pdf", stylesheets=[css])
    return file
