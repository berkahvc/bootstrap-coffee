from oscar.apps.checkout.calculators import (
    OrderTotalCalculator as CoreOrderTotalCalculator,
)
from oscar.core import prices


class OrderTotalCalculator(CoreOrderTotalCalculator):
    def calculate(self, basket, shipping_charge, surcharges=None, **kwargs):
        excl_tax = basket.total_excl_tax

        if shipping_charge and shipping_charge.excl_tax:
            excl_tax += shipping_charge.excl_tax

        if (
            basket
            and basket.is_tax_known
            and shipping_charge
            and shipping_charge.is_tax_known
        ):
            incl_tax = basket.total_incl_tax + shipping_charge.incl_tax
        else:
            incl_tax = None

        if surcharges is not None:
            excl_tax += surcharges.total.excl_tax
            if incl_tax is not None:
                incl_tax += surcharges.total.incl_tax

        return prices.Price(
            currency=basket.currency, excl_tax=excl_tax, incl_tax=incl_tax
        )
