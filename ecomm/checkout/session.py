from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from oscar.apps.checkout import exceptions
from oscar.apps.checkout.session import CheckoutSessionMixin as CoreCheckoutSessionMixin
from oscar.core.loading import get_class, get_model

from ecomm.schedule_management.utils import check_delivery_date_validation

SurchargeApplicator = get_class("checkout.applicator", "SurchargeApplicator")
ShippingAddress = get_model("order", "ShippingAddress")
Category = get_model("catalogue", "Category")
Repository = get_class("shipping.repository", "Repository")


class CheckoutSessionMixin(CoreCheckoutSessionMixin):
    def check_basket_is_delivery_date_empty(self, request):
        delivery_date = request.basket.delivery_date or None
        for line in request.basket._lines:
            if line.is_subscription:
                delivery_date = line.delivery_date
                break

        if not delivery_date:
            raise exceptions.FailedPreCondition(
                url=reverse("home"), message=_("Please select a delivery date")
            )

    def check_basket_is_delivery_time_empty(self, request):
        delivery_time = request.basket.delivery_time or None
        for line in request.basket._lines:
            if line.is_subscription:
                delivery_time = line.delivery_time
                break

        if not delivery_time:
            raise exceptions.FailedPreCondition(
                url=reverse("home"), message=_("Please select a delivery time")
            )

    def check_basket_is_not_empty(self, request):
        if request.basket.is_empty:
            raise exceptions.FailedPreCondition(
                url=reverse("basket:summary"),
                message=_("You need to add some items to your cart before checkout"),
            )

    def check_a_valid_basket_delivery_date(self, request):
        delivery_date = request.basket.delivery_date or None

        if not delivery_date:
            raise exceptions.FailedPreCondition(
                url=reverse("home"), message=_("Please select a delivery date")
            )

        start_date = check_delivery_date_validation(request.basket._lines)
        if delivery_date < start_date:
            raise exceptions.FailedPreCondition(
                url=reverse("home"),
                message=_(
                    "Sorry your delivery schedule has already passed from today's cut off time"
                ),
            )

    def check_a_valid_subscriptions_total(self, request):
        total = 0
        for line in request.basket._lines:
            if line.is_subscription:
                total += 1

        if total > 1:
            raise exceptions.FailedPreCondition(
                url=reverse("home"),
                message=_("Multiple subscriptions doesn't allowed"),
            )

    def build_submission(self, **kwargs):
        from ecomm.checkout.utils import get_is_free_shipping

        submission = super(CheckoutSessionMixin, self).build_submission()

        basket = self.request.basket
        shipping_address = self.get_shipping_address(basket)
        is_free_shipping = get_is_free_shipping(basket)

        if is_free_shipping:
            shipping_method = Repository().free_methods[0]
        else:
            shipping_method = self.get_shipping_method(basket)

        order_args = submission["order_kwargs"]

        if "guest_email" in order_args:
            no_guest_email = submission["order_kwargs"]["guest_email"] is None
        else:
            no_guest_email = True

        surcharges = SurchargeApplicator(
            self.request, submission
        ).get_applicable_surcharges(self.request.basket)
        if not shipping_method:
            total = shipping_charge = None
        else:
            shipping_charge = shipping_method.calculate(basket)
            total = self.get_order_totals(
                basket, shipping_charge=shipping_charge, surcharges=surcharges, **kwargs
            )

        submission["shipping_charge"] = shipping_charge
        submission["order_total"] = total
        submission["surcharges"] = surcharges

        if no_guest_email:
            email = self.checkout_session.get_guest_email()

            # If user didn't signup and we couldn't get from checkout session,
            # we will use the shipping address information instead
            if email is None and shipping_address:
                email = shipping_address.email
                self.checkout_session.set_guest_email(email)

            submission["order_kwargs"]["guest_email"] = email

        return submission

    def check_user_email_is_captured(self, request):
        # Will get the email from shipping_address information
        return
