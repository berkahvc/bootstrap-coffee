import pytest
from djstripe.models import Customer, Subscription

from ecomm.address.models import UserAddress
from ecomm.address.tests.factories import UserAddressFactory
from ecomm.order.models import Order, ShippingAddress
from ecomm.order.tests.factories import OrderFactory, ShippingAddressFactory
from ecomm.subscription_management.models import SubscriptionPrepaid
from ecomm.subscription_management.tests.factories import (
    DJStripeCustomerFactory,
    DJSubscriptionFactory,
    SubscriptionPrepaidFactory,
)
from ecomm.users.models import User
from ecomm.users.tests.factories import UserFactory


@pytest.fixture(autouse=True)
def media_storage(settings, tmpdir):
    settings.MEDIA_ROOT = tmpdir.strpath


@pytest.fixture
def user() -> User:
    return UserFactory()


@pytest.fixture
def user_address() -> UserAddress:
    return UserAddressFactory()


@pytest.fixture
def order() -> Order:
    return OrderFactory()


@pytest.fixture
def shipping_address() -> ShippingAddress:
    return ShippingAddressFactory()


@pytest.fixture
def subscription() -> SubscriptionPrepaid:
    return SubscriptionPrepaidFactory()


@pytest.fixture
def djstripe_customer() -> Customer:
    return DJStripeCustomerFactory()


@pytest.fixture
def djstripe_subscription() -> Subscription:
    return DJSubscriptionFactory()
