from oscar.core.loading import get_model

from ecomm.catalogue.constants import BaseCategories
from ecomm.schedule_management.models import DeliveryTime

Product = get_model("catalogue", "Product")
Category = get_model("catalogue", "Category")


def base_categories(_):
    category_map = {}
    for category in BaseCategories.values():
        try:
            cat = Category.objects.get(name=category)
            category_map[f"{category.replace(' ', '_')}_CATEGORY"] = cat
        except Category.DoesNotExist:
            pass

    return category_map


def get_delivery_time(request):
    delivery_times = DeliveryTime.objects.filter(is_active=True)
    return {"delivery_times": delivery_times}


def get_product_navbar(request):
    product_navbar = (
        Product.objects.browsable()
        .filter(categories__name__icontains="coffee")
        .exclude(categories__name__icontains="subscription")
    )
    return {"product_navbar": product_navbar}
