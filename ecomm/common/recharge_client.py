import json

import requests
from django.conf import settings
from django.core.exceptions import ValidationError

from ecomm.utils.utils import merge_two_dicts

RECHARGE_BASE_URL = "https://api.rechargeapps.com"


class RechargeClient:
    @staticmethod
    def check_and_return_response(response):
        response_body = str(response.text)

        if response_body is None or response_body == "":
            return json.loads("{}")

        response_json = json.loads(response_body)

        if "error" in response_json:
            raise ValidationError(response_json["error"])

        if "errors" in response_json:
            raise ValidationError(response_json["errors"])

        return response_json

    @staticmethod
    def get(path, params=None):
        """
        Takes the "url" of the request

        :param params: query string of the request
        :param path: target path of the request
        :return: the result of the request
        """

        headers = settings.RECHARGE_HEADER
        headers["Accept"] = "application/json"
        headers["Content-Type"] = "application/json"

        result = requests.get(
            url=RECHARGE_BASE_URL + path, headers=headers, params=params
        )

        return RechargeClient.check_and_return_response(response=result)

    @staticmethod
    def put(path, params=None, data=None):
        """
        Takes the "url" of the request

        :param params: query string of the request
        :param path: target path of the request
        :param data: data of the request in dictionary format
        :return: the result of the request
        """

        headers = settings.RECHARGE_HEADER
        headers["Accept"] = "application/json"
        headers["Content-Type"] = "application/json"

        result = requests.put(
            url=RECHARGE_BASE_URL + path,
            headers=headers,
            params=params,
            data=json.dumps(data),
        )

        return RechargeClient.check_and_return_response(response=result)

    @staticmethod
    def post(path, params=None, data=None):
        """
        Takes the "url" of the request

        :param params: query string of the request
        :param path: target path of the request
        :param data: data of the request in dictionary format
        :return: the result of the request
        """

        if data is None:
            data = {}
        headers = settings.RECHARGE_HEADER
        headers["Accept"] = "application/json"
        headers["Content-Type"] = "application/json"

        result = requests.post(
            url=RECHARGE_BASE_URL + path,
            headers=headers,
            params=params,
            data=json.dumps(data),
        )

        return RechargeClient.check_and_return_response(response=result)

    @staticmethod
    def delete(path):
        headers = settings.RECHARGE_HEADER
        headers["Accept"] = "application/json"
        headers["Content-Type"] = "application/json"

        result = requests.delete(
            url=RECHARGE_BASE_URL + path,
            headers=headers,
        )

        return RechargeClient.check_and_return_response(response=result)

    @staticmethod
    def get_customer_subscriptions(params=None):
        return RechargeClient.get("/subscriptions", params)

    @staticmethod
    def get_customers(params):
        return RechargeClient.get("/customers", params)

    @staticmethod
    def get_customers_by_customer_id(recharge_customer_id):
        return RechargeClient.get("/customers/" + str(recharge_customer_id))

    @staticmethod
    def get_customer_orders(params=None):
        return RechargeClient.get("/orders", params)

    @staticmethod
    def get_user_subscription_by_customer_id(customer_id, status=None):
        params = {"customer_id": customer_id, "status": status}

        return RechargeClient.get_customer_subscriptions(params)

    @staticmethod
    def get_customer_orders_by_customer_id(customer_id, params):
        customer_id_params = {"customer_id": customer_id}
        combined_params = merge_two_dicts(customer_id_params, params)

        return RechargeClient.get_customer_orders(combined_params)

    @staticmethod
    def get_product_by_recharge_product_id(recharge_product_id):
        return RechargeClient.get("/products/" + str(recharge_product_id))

    @staticmethod
    def create_customer_address(customer_id, address):
        return RechargeClient.post(
            path=f"/customers/{customer_id}/addresses", data=address
        )

    @staticmethod
    def update_address(address_id, params):
        return RechargeClient.put(path=f"/addresses/{address_id}", data=params)

    @staticmethod
    def get_customer_addresses(customer_id, params):
        return RechargeClient.get(
            path=f"/customers/{customer_id}/addresses", params=params
        )

    @staticmethod
    def get_orders(params=None):
        return RechargeClient.get(path="/orders", params=params)

    @staticmethod
    def get_subscriptions(params=None):
        return RechargeClient.get(path="/subscriptions", params=params)

    @staticmethod
    def get_count_subscriptions(params=None):
        return RechargeClient.get(path="/subscriptions/count", params=params)

    @staticmethod
    def create_subscription(subscription):
        return RechargeClient.post(path="/subscriptions", data=subscription)

    @staticmethod
    def create_onetime_product(address_id, onetime):
        return RechargeClient.post(path=f"/onetimes/address/{address_id}", data=onetime)

    @staticmethod
    def get_address_by_id(address_id):
        return RechargeClient.get("/addresses/" + str(address_id))

    @staticmethod
    def get_subscription_by_id(subscription_id):
        return RechargeClient.get("/subscriptions/" + str(subscription_id))

    @staticmethod
    def get_onetime_by_id(onetime_id):
        return RechargeClient.get(f"/onetimes/{onetime_id}")

    @staticmethod
    def delete_onetime_by_id(onetime_id):
        return RechargeClient.delete(f"/onetimes/{onetime_id}")

    @staticmethod
    def get_onetimes(params):
        return RechargeClient.get("/onetimes", params=params)

    @staticmethod
    def update_onetime_by_id(onetime_id, data):
        return RechargeClient.put(f"/onetimes/{onetime_id}", data=data)

    @staticmethod
    def update_subscription(subscription_id, data):
        return RechargeClient.put("/subscriptions/" + str(subscription_id), data=data)

    @staticmethod
    def update_subscription_next_charged_date(subscription_id, next_charge_date):
        """
        :param subscription_id: subscription id
        :param next_charge_date: in format of "%Y-%m-%d"
        :return: the result of the request
        """

        return RechargeClient.post(
            f"/subscriptions/{subscription_id}/set_next_charge_date",
            data={"date": next_charge_date},
        )

    @staticmethod
    def cancel_subscription(subscription_id, cancellation_reason, send_email=False):
        data = {"cancellation_reason": cancellation_reason, "send_email": send_email}

        return RechargeClient.post(
            f"/subscriptions/{subscription_id}/cancel", data=data
        )

    @staticmethod
    def activate_subscription(subscription_id):
        return RechargeClient.post(f"/subscriptions/{subscription_id}/activate")

    @staticmethod
    def get_payment_methods_by_customer_id(customer_id):
        params = {"customer_id": customer_id}
        return RechargeClient.get("/payment_methods/", params=params)
