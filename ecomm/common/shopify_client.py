import logging
import time
from datetime import datetime
from decimal import Decimal

import shopify
from django.conf import settings
from django.utils.crypto import get_random_string

from ecomm.address.models import Country, UserAddress
from ecomm.catalogue.models import Product, ProductClass
from ecomm.order.models import Line, Order, ShippingAddress
from ecomm.partner.models import Partner, StockRecord
from ecomm.users.models import User

logger = logging.getLogger(__name__)

shopify_api_key = settings.SHOPIFY_API_KEY
shopify_password = settings.SHOPIFY_PASSWORD
shopify_api_secret = settings.SHOPIFY_API_SECRET
shop_url = settings.SHOP_URL
redirect_uri = settings.SHOPIFY_REDIRECT_URL
api_version = "2021-10"


class ShopifyPublicAppsClient:
    @staticmethod
    def session_setup():
        return shopify.Session.setup(api_key=shopify_api_key, secret=shopify_api_secret)

    def read_customers(self):
        self.session_setup()
        scope = ["read_customers"]
        new_session = shopify.Session(shop_url, api_version)
        _ = new_session.create_permission_url(scope, redirect_uri)


class ShopifyPrivateAppsClient:
    def read_customers(self):
        logger.info("create shopify session")
        session = shopify.Session(shop_url, api_version, shopify_password)

        logger.info("activate session")
        shopify.ShopifyResource.activate_session(session)

        has_next_page = True
        next_page_url = None
        while has_next_page:
            logger.info("starting find customers")
            if next_page_url:
                customers = shopify.Customer.find(from_=next_page_url)
            else:
                customers = shopify.Customer.find()

            logger.info(f"found customers with total: {len(customers)}")
            logger.info("processing create user")
            for customer in customers:
                first_name = customer.first_name
                last_name = customer.last_name or ""
                email = customer.email
                addresses = customer.addresses
                address = None

                if len(addresses) > 0:
                    address = addresses[0]

                data = {
                    "first_name": first_name,
                    "last_name": last_name,
                    "email": email,
                }

                user, created = User.objects.get_or_create(
                    **{"username": email}, defaults=data
                )
                if created:
                    logger.info(f"user: {user}")

                    user.is_from_shopify = True
                    user.shopify_customer_id = customer.id
                    user.set_unusable_password()
                    user.save()

                    logger.info("created user")

                    if address:
                        logger.info("creating user address")
                        country = Country.objects.get(
                            printable_name=address.country_name
                        )
                        phone = None
                        try:
                            phone = address.phone.replace(" ", "")
                            if phone.startswith("+65", 0, 3):
                                pass
                            else:
                                phone = f"+65{phone}"
                        except Exception as err:
                            logger.info(str(err))

                        default_address = {
                            "first_name": address.first_name,
                            "last_name": address.last_name,
                            "line4": address.city,
                            "state": address.province or "Singapore",
                            "phone_number": phone,
                            "country": country,
                        }

                        try:
                            user_address, _ = UserAddress.objects.get_or_create(
                                **{
                                    "user": user,
                                    "email": email,
                                    "line1": address.address1,
                                    "postcode": address.zip,
                                },
                                defaults=default_address,
                            )
                            logger.info(f"user address: {user_address}")
                            logger.info("created user address")
                        except Exception as e:
                            logger.error(f"{e}")

            has_next_page = customers.has_next_page()
            next_page_url = customers.next_page_url
            time.sleep(5)

        logger.info("clear session")
        shopify.ShopifyResource.clear_session()

        return "OK"

    def read_orders(self):
        logger.info("create shopify session")
        session = shopify.Session(shop_url, api_version, shopify_password)

        logger.info("activate session")
        shopify.ShopifyResource.activate_session(session)

        product_class, _ = ProductClass.objects.get_or_create(
            name="ShopifyProduct", track_stock=False
        )

        has_next_page = True
        next_page_url = None
        while has_next_page:
            logger.info("starting find orders")
            if next_page_url:
                orders = shopify.Order.find(from_=next_page_url)
            else:
                orders = shopify.Order.find(status="any")
            logger.info(f"found orders with total: {len(orders)}")
            logger.info("processing create orders and products")

            for order in orders:
                logger.info(f"shopifyOrder {order.id}")
                logger.info(f"shopifyOrderId {order.id}")
                customer = order.customer
                customer_default_address = customer.default_address
                total_discount = Decimal(order.total_discounts)
                total_shipping_price_set = order.total_shipping_price_set
                total_shipping_price_set_amount = Decimal(
                    total_shipping_price_set.shop_money.amount
                )
                line_items = order.line_items  # order line
                financial_status = order.financial_status  # order line
                cancelled_at = order.cancelled_at
                note_attributes = order.note_attributes

                delivery_date = None
                delivery_day = None
                delivery_time = None

                for attribute in note_attributes:
                    if attribute["name"] == "Delivery-Date":
                        delivery_date = attribute["value"].replace("/", "-")
                    elif attribute["name"] == "Delivery-Time":
                        delivery_time = attribute["value"]
                    elif attribute["name"] == "Delivery-Day":
                        delivery_day = attribute["value"]

                if delivery_day is None:
                    delivery_day = (
                        datetime.strptime(delivery_date, "%Y-%m-%d").strftime("%A")
                        if delivery_date
                        else None
                    )

                order_status = "WAIT_FOR_PAYMENT"

                if financial_status.lower() == "paid":
                    order_status = "PAID"

                if cancelled_at:
                    order_status = "CANCELLED"

                logger.info(f"customer email {customer.email}")

                discount = total_discount - total_shipping_price_set_amount
                discount_per_item = discount / len(line_items)

                try:
                    _ = order.payment_details
                except Exception as err:
                    logger.error(
                        f"payment_details not found: {str(err)}",
                        extra={"shopifyOrderId": order.id},
                    )
                    pass

                shipping_address = None

                email_split = customer.email.split("@")

                first_name = customer.first_name or email_split[0]
                last_name = customer.last_name or email_split[0]
                email = customer.email

                customer_data = {
                    "first_name": first_name,
                    "last_name": last_name,
                    "email": email,
                }

                user, created_user = User.objects.get_or_create(
                    **{"username": email}, defaults=customer_data
                )

                if customer_default_address:
                    country = Country.objects.get(
                        printable_name=customer_default_address.country_name
                    )
                    phone = None
                    try:
                        phone = customer_default_address.phone.replace(" ", "")
                        if phone.startswith("+65", 0, 3):
                            pass
                        else:
                            phone = f"+65{phone}"
                    except Exception as err:
                        logger.info(str(err))

                    default_address = {
                        "first_name": customer_default_address.first_name,
                        "last_name": customer_default_address.last_name,
                        "line4": customer_default_address.city,
                        "state": customer_default_address.province or "Singapore",
                        "phone_number": phone,
                        "country": country,
                    }

                    try:
                        default_address, _ = UserAddress.objects.get_or_create(
                            **{
                                "user": user,
                                "email": email,
                                "line1": customer_default_address.address1,
                                "postcode": customer_default_address.zip,
                            },
                            defaults=default_address,
                        )
                    except Exception as e:
                        logger.error(f"{e}")

                try:
                    if order.shipping_address:
                        country = Country.objects.get(
                            printable_name=order.shipping_address.country
                        )
                        phone = None
                        try:
                            phone = order.shipping_address.phone.replace(" ", "")
                            if phone.startswith("+65", 0, 3):
                                pass
                            else:
                                phone = f"+65{phone}"
                        except Exception as err:
                            logger.info(str(err))

                        default_shipping_address = {
                            "first_name": customer_default_address.first_name
                            or email_split[0],
                            "last_name": customer_default_address.last_name
                            or email_split[0],
                            "line4": customer_default_address.city,
                            "state": customer_default_address.province or "Singapore",
                            "phone_number": phone,
                            "country": country,
                        }

                        try:
                            shipping_address, _ = ShippingAddress.objects.get_or_create(
                                **{
                                    "email": email,
                                    "line1": customer_default_address.address1,
                                    "postcode": customer_default_address.zip,
                                },
                                defaults=default_shipping_address,
                            )
                        except Exception as e:
                            logger.error(f"{e}")
                except Exception as err:
                    logger.error(f"{err}")

                order_data = {
                    "currency": order.currency,
                    "user": user,
                    "shipping_address": shipping_address,
                    "shipping_method": "Free Delivery",
                    "shipping_code": "free-delivery",
                    "total_incl_tax": order.total_price,
                    "total_excl_tax": order.total_price,
                    "shipping_incl_tax": order.total_shipping_price_set.shop_money.amount,
                    "shipping_excl_tax": order.total_shipping_price_set.shop_money.amount,
                    "status": order_status,
                    "is_from_shopify": True,
                }

                order_obj, created_order = Order.objects.get_or_create(
                    **{"number": order.order_number}, defaults=order_data
                )

                order_obj.delivery_date = delivery_date
                order_obj.delivery_time = delivery_time
                order_obj.delivery_day = delivery_day
                order_obj.save()

                if created_user:
                    logger.info(f"user: {user}")

                    user.is_from_shopify = True
                    user.set_unusable_password()
                    user.save()

                    logger.info("created user")

                for item in line_items:
                    logger.info(f"item shopify product_id: {item.product_id}")
                    logger.info(f"item shopify variant_id: {item.variant_id}")

                    shopify_product = shopify.Product.find(item.product_id)
                    variants = shopify_product.variants
                    quantity = item.quantity
                    product_data = {
                        "is_public": False,
                        "shopify_product_id": item.product_id,
                    }

                    random_sku = get_random_string(5).upper()
                    partner, _ = Partner.objects.get_or_create(name="Singapore")
                    (
                        product_parent,
                        product_parent_created,
                    ) = Product.objects.get_or_create(
                        **{
                            "title": shopify_product.title,
                            "product_class": product_class,
                        },
                        defaults=product_data,
                    )

                    if len(variants) > 0:
                        logger.info("product has variants")
                        product_parent.structure = "parent"
                        product_parent.save()

                        for variant in variants:
                            price = variant.price
                            sku = variant.sku
                            child_data = {
                                "title": variant.title,
                                "parent": product_parent,
                                "shopify_product_id": item.product_id,
                                "shopify_variant_id": variant.id,
                            }
                            child_data_defaults = {
                                "is_public": False,
                                "structure": "child",
                            }
                            logger.info(f"product parent: {product_parent.title}")
                            logger.info(f"product child_data: {child_data}")
                            logger.info(
                                f"product child_data_defaults: {child_data_defaults}"
                            )
                            child, child_created = Product.objects.get_or_create(
                                **child_data, defaults=child_data_defaults
                            )
                            if child_created:
                                StockRecord.objects.create(
                                    product=child,
                                    partner=partner,
                                    partner_sku=sku or random_sku,
                                    price_excl_tax=price,
                                )
                    else:
                        if product_parent_created:
                            StockRecord.objects.create(
                                product=product_parent,
                                partner=partner,
                                partner_sku=random_sku,
                                price_excl_tax=shopify_product.price,
                            )

                    line_product = Product.objects.get(
                        shopify_product_id=item.product_id,
                        shopify_variant_id=item.variant_id,
                    )

                    default_item_data = {
                        "order": order_obj,
                        "product": line_product,
                    }

                    price = Decimal(item.price) * int(quantity)

                    item_data = {
                        "title": item.variant_title,
                        "partner_sku": item.sku,
                        "quantity": quantity,
                        "line_price_incl_tax": price - discount_per_item,
                        "line_price_excl_tax": price - discount_per_item,
                        "line_price_before_discounts_incl_tax": price,
                        "line_price_before_discounts_excl_tax": price,
                    }
                    Line.objects.get_or_create(**default_item_data, defaults=item_data)

            has_next_page = orders.has_next_page()
            next_page_url = orders.next_page_url
            time.sleep(5)

        logger.info("clear session")
        shopify.ShopifyResource.clear_session()

        return "OK"
