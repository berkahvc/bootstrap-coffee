import json
import logging

import requests
from django.conf import settings

from ecomm.utils.utils import merge_two_dicts

LODI_USER_KEY = settings.LODI_USER_KEY
LODI_BASE_URL = settings.LODI_BASE_URL
logger = logging.getLogger(__name__)


class LodiClient:
    token = None

    @staticmethod
    def check_and_return_response(response):
        response_body = str(response.text)

        if response_body is None or response_body == "":
            return json.loads("{}")

        response_json = json.loads(response_body)

        return response_json

    def get(self, path, params=None, url=LODI_BASE_URL, additional_headers={}):
        """
        Takes the "url" of the request

        :param params: query string of the request
        :param path: target path of the request
        :param url: target url of the request
        :param additional_headers: additional headers of the request
        :return: the result of the request
        """

        headers = merge_two_dicts(
            {"Accept": "application/json", "Content-Type": "application/json"},
            additional_headers,
        )
        result = requests.get(url=url + path, headers=headers, data=params)

        return self.check_and_return_response(response=result)

    def post(self, path, params=None, url=LODI_BASE_URL, additional_headers={}):
        """
        Takes the "url" of the request

        :param params: query string of the request
        :param path: target path of the request
        :param url: target url of the request
        :param additional_headers: additional headers of the request
        :return: the result of the request
        """

        headers = merge_two_dicts(
            {"Accept": "application/json", "Content-Type": "application/json"},
            additional_headers,
        )
        result = requests.post(url=url + path, headers=headers, json=params)

        return self.check_and_return_response(response=result)

    def get_request_token(self):
        response = self.get(
            path="v2/users/authorize",
            params={
                "userkey": LODI_USER_KEY,
            },
        )

        self.token = response["id_token"]

        return response["id_token"]

    def get_provinces(self):
        return self.get(
            path="v2/provinces",
            additional_headers={"Authorization": self.get_request_token()},
        )

    def get_cities_by_provinces(self, province_name):
        return self.get(
            path=f"v2/provinces/{province_name.lower()}/cities",
            additional_headers={"Authorization": self.get_request_token()},
        )

    def get_districts(self, province_name, city_name):
        return self.get(
            path=f"v2/cities/{city_name}/districts",
            additional_headers={"Authorization": self.get_request_token()},
        )

    def get_sub_districts(self, province_name, city_name, district_name):
        return self.get(
            path=(
                f"v2/provinces/{province_name.lower()}/cities/{city_name.lower()}/"
                f"districts/{district_name.lower()}//sub-district-poscodes"
            ),
            additional_headers={"Authorization": self.get_request_token()},
        )

    def get_order_rates(self, origin, destination):
        # data = {
        #     "origin": origin,
        #     "destination": destination,
        #     "client_id": self.client_id,
        # }

        return [
            {
                "courier": "SAP",
                "service_type": "REGULER",
                "rate": 73400,
                "estimated_lead_time": "3",
                "minimal_weight": 1,
            },
            {
                "courier": "JNE",
                "service_type": "REGULER",
                "rate": 62000,
                "estimated_lead_time": "7",
                "minimal_weight": 1,
            },
            {
                "courier": "SICEPAT",
                "service_type": "REGULER",
                "rate": 55800,
                "estimated_lead_time": "7",
                "minimal_weight": 1,
            },
        ]

    def post_create_order(self, payload):
        resp = self.post(
            path="/v2/orders/create",
            additional_headers={"Authorization": self.get_request_token()},
            params=payload,
        )

        logger.info(f"CREATE ORDER LODI RESPONSE: {resp}")

    def get_stock_level_by_sku(self, sku):
        resp = self.get(
            path=f"/v2/inventories/{sku}/stocks",
            additional_headers={"Authorization": self.get_request_token()},
        )

        logger.info(f"GET STOCK LEVEL BY SKU '{sku}' RESPONSE: {resp}")

        return resp.get("payload")
